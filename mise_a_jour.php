<?php
/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 05/01/2016
 * Time: 15:17
 */
    include_once 'class/class_fli.php';
    include_once 'class/class_helper.php';
    include_once 'modeles/mod_form_list.php';


    //Création base de données
    // $objPdo->tl_exec('CREATE DATABASE IF NOT EXISTS '.$_POST['database']) or die('Erreur lors de la création de la base de données');
    //Ajout des routes par défaut des modules par défaut
    $guid_administration = class_helper::guid();
    $guid_modules = class_helper::guid();
    $guid_menu = class_helper::guid();
    $guid_utilisateurs = class_helper::guid();
    $guid_groupes = class_helper::guid();
    $guid_routes_groupes = class_helper::guid();
    $guid_routes_utilisateurs = class_helper::guid();
    $guid_phpinfo = class_helper::guid();
    $guid_password = class_helper::guid();
    $guid_utilisateurs_groupes = class_helper::guid();
    $sInsertRoutes = "INSERT INTO ".$_POST['prefixe']."routes (id_route, route_route, intitule_menu_route, pere_menu_route, afficher_menu_route, ordre_menu_route, target_menu_route, lien_menu_route, supplogique_route, guid_route) VALUES
    (1,'fli_admin','Administration','',TRUE,'','',FALSE,'N','" . $guid_administration . "'),
    (2,'fli_admin-modules','Gestion des modules','fli_admin',TRUE,'','',TRUE,'N','" . $guid_modules . "'),
    (3,'fli_admin-menu','Gestion du menu','fli_admin',TRUE,'','',TRUE,'N','" . $guid_menu . "'),
    (4,'fli_admin-utilisateurs','Gestion des utilisateurs','fli_admin',TRUE,'','',TRUE,'N','" . $guid_utilisateurs . "'),
    (5,'fli_admin-groupes','Gestion des groupes','fli_admin',TRUE,'','',TRUE,'N','" . $guid_groupes . "'),
    (6,'fli_admin-routes_groupes','Gestion des droits (Groupes)','fli_admin',TRUE,'','',TRUE,'N','" . $guid_routes_groupes . "'),
    (7,'fli_admin-routes_utilisateurs','Gestion des droits (Utilisateurs)','fli_admin',TRUE,'','',TRUE,'N','" . $guid_routes_utilisateurs . "'),
    (8,'fli_admin-phpinfo','PhpInfo','fli_admin',TRUE,'','',TRUE,'N','" . $guid_phpinfo . "'),
    (9,'fli_admin-password','','fli_admin',FALSE,'','',FALSE,'N','" . $guid_password . "'),
    (10,'fli_admin-utilisateurs_groupes','','fli_admin',FALSE,'','',FALSE,'N','" . $guid_utilisateurs_groupes . "')";
    $objPdo->tl_exec($sInsertRoutes);


    $sInsertGroupesRoutes = "INSERT INTO ".$_POST['prefixe']."groupes_routes (id_groupe_route, guid_groupe, guid_route, ajout_groupe_route, modif_groupe_route, suppr_groupe_route, visu_groupe_route, supplogique_groupe_route, guid_groupe_route) VALUES
    (1,'" . $guid_groupe . "','" . $guid_administration . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (2,'" . $guid_groupe . "','" . $guid_modules . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (3,'" . $guid_groupe . "','" . $guid_menu . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (4,'" . $guid_groupe . "','" . $guid_utilisateurs . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (5,'" . $guid_groupe . "','" . $guid_groupes . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (6,'" . $guid_groupe . "','" . $guid_routes_groupes . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (7,'" . $guid_groupe . "','" . $guid_routes_utilisateurs . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (8,'" . $guid_groupe . "','" . $guid_phpinfo . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (9,'" . $guid_groupe . "','" . $guid_password . "',true,true,true,true,'N','" . class_helper::guid() . "'),
    (10,'" . $guid_groupe . "','" . $guid_utilisateurs_groupes . "',true,true,true,true,'N','" . class_helper::guid() . "') ON DUPLICATE KEY UPDATE supplogique_groupe_route='N'";
    $objPdo->tl_exec($sInsertGroupesRoutes);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mise à jour de Fli Framework</title>
    <link type="text/css" href="public/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="public/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="public/css/simple-sidebar.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name="google-site-verification" content="sOuXqP6oDwIfUf6biVsfI_wNMug2oNR1I6J8G8pdgBg"/>
    <!--[if IE]>
    <link rel="stylesheet" media="all" type="text/css" href="public/css/pro_dropline_ie.css"/>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) 1.11.2 -->
    <script src="public/js/jquery-1.11.2.min.js"></script>
    <style>
        body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #eee;
        }

        .form-signin {
            max-width: 500px;
            padding: 15px;
            margin: 0 auto;
        }

        .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .form-signin input[type="text"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .minLabel{
            width: 180px;
            display: inline-block;
        }

    </style>
</head>
<body>

<div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="row">

            <div class="container">



                <div style="text-align: center;"><img src="public/images/flilogo.png" alt="logo"></div>
                <h3>Mise à jour de FLi framework</h3>
            </div>

        </div>
    </div>
</div>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="public/js/bootstrap.min.js"></script>
</body>
</html>
