<?php

/**
 * Nom: ctrl_fli_authentification
 * Description: Permet de déterminer l'authentification et les droits de l'utilisateur
 * Date: 10/12/2015
 */
class ctrl_fli_authentification
{

    public function verifier_authentification()
    {
        $sPrefixeDb = class_fli::get_prefixe();

        $aTabDroits = array('a'=>0,'m'=>0,'s'=>0,'v'=>0,'w'=>0);
        //echo "toto";

        $aSession = class_fli::get_session();
        class_fli::set_aData('guid_user',NULL);
        class_fli::set_aData('guid_groupes',NULL);
        if( !empty($aSession) ) {
            $objImport = new class_import();
            $objImport->import_modele('fli_authentification', 'mod_fli_authentification');
            $objModAuth = new mod_fli_authentification();

            //Vérification si l'utilisateur existe grâce son guid ($aSession qui provient du module fli_session)
            $aTabUser = $objModAuth->check_user($aSession);
            if( !empty($aTabUser) ) {
                //Mise à jour des variables static (connexion, guid_user, guid_groupes)
                class_fli::set_fli_est_connecte(true);
                class_fli::set_aData('guid_user',$aSession);
                class_fli::set_aData('langue_user',$aTabUser[0]['langue_user']);
                class_fli::set_aData('autorisation_traduction',$aTabUser[0]['activtraductionmodif_user']);
                $aGroupes = $objModAuth->get_groupes_user(class_fli::get_aData('guid_user'));
                $sGroupes = '';
                if(count($aGroupes)>1) {
                    foreach( $aGroupes as $sGroupe ) {
                        $sGroupes .= $sGroupe.';';
                    }
                }elseif(count($aGroupes)==1){
                    if($aGroupes[0]['guid_groupe']!=''){
                        $sGroupes = $aGroupes[0]['guid_groupe'];
                    }
                }
                class_fli::set_aData('guid_groupes',$sGroupes);
                if(!empty($aGroupes)) {
                    $sRequeteGroupes = 'AND (';
                    foreach( $aGroupes as $key => $value ) {
                        if($value['nom_groupe'] == 'SuperAdmin'){
                            class_fli::set_super_admin(true);
                        }
                        if($key>0){
                            $sRequeteGroupes .= ' OR ';
                        }
                        $sRequeteGroupes .= $sPrefixeDb."groupes_routes.guid_groupe='".$value['guid_groupe']."'";
                    }
                    $sRequeteGroupes .= ')';
                }else{
                    $sRequeteGroupes = '';
                }
                $sRoute = class_fli::get_fli_module().'-'.class_fli::get_fli_controleur().'-'.class_fli::get_fli_fonction();
                $aDroits = $objModAuth->get_droits($sRoute,$aSession,$sRequeteGroupes);

                if(!empty($aDroits)){

                    if(isset($aDroits[0]['ajout_user_route']) && !empty($aDroits[0]['ajout_user_route']) ){
                        $aTabDroits['a'] = $aDroits[0]['ajout_user_route'];
                    }elseif( (!isset($aDroits[0]['ajout_user_route']) || empty($aDroits[0]['ajout_user_route'])) && isset($aDroits[0]['ajout_groupe_route']) && !empty($aDroits[0]['ajout_groupe_route'])){
                        $aTabDroits['a'] = $aDroits[0]['ajout_groupe_route'];
                    }else{
                        $aTabDroits['a'] = 0;
                    }

                    if(isset($aDroits[0]['modif_user_route']) && !empty($aDroits[0]['modif_user_route']) ){
                        $aTabDroits['m'] = $aDroits[0]['modif_user_route'];
                    }elseif( (!isset($aDroits[0]['modif_user_route']) || empty($aDroits[0]['modif_user_route'])) && isset($aDroits[0]['modif_groupe_route']) && !empty($aDroits[0]['modif_groupe_route'])){
                        $aTabDroits['m'] = $aDroits[0]['modif_groupe_route'];
                    }else{
                        $aTabDroits['m'] = 0;
                    }

                    if(isset($aDroits[0]['suppr_user_route']) && !empty($aDroits[0]['suppr_user_route']) ){
                        $aTabDroits['s'] = $aDroits[0]['suppr_user_route'];
                    }elseif( (!isset($aDroits[0]['suppr_user_route']) || empty($aDroits[0]['suppr_user_route'])) && isset($aDroits[0]['suppr_groupe_route']) && !empty($aDroits[0]['suppr_groupe_route'])){
                        $aTabDroits['s'] = $aDroits[0]['suppr_groupe_route'];
                    }else{
                        $aTabDroits['s'] = 0;
                    }

                    if(isset($aDroits[0]['visu_user_route']) && !empty($aDroits[0]['visu_user_route']) ){
                        $aTabDroits['v'] = $aDroits[0]['visu_user_route'];
                    }elseif( (!isset($aDroits[0]['visu_user_route']) || empty($aDroits[0]['visu_user_route'])) && isset($aDroits[0]['visu_groupe_route']) && !empty($aDroits[0]['visu_groupe_route'])){
                        $aTabDroits['v'] = $aDroits[0]['visu_groupe_route'];
                    }else{
                        $aTabDroits['v'] = 0;
                    }

                    if(isset($aDroits[0]['switch_wizard_user_routes']) && !empty($aDroits[0]['switch_wizard_user_routes']) ){
                        $aTabDroits['w'] = $aDroits[0]['switch_wizard_user_routes'];
                    }elseif( (!isset($aDroits[0]['switch_wizard_user_routes']) || empty($aDroits[0]['switch_wizard_user_routes'])) && isset($aDroits[0]['switch_wizard_groupes_routes']) && !empty($aDroits[0]['switch_wizard_groupes_routes'])){
                        $aTabDroits['w'] = $aDroits[0]['switch_wizard_groupes_routes'];
                    }else{
                        $aTabDroits['w'] = 0;
                    }

                }else{
                    //echo"<pre>";print_r($aDroits);echo"</pre>";
                    //Si l'utilisateur n'existe pas
                    $aTabDroits['v'] = 0;
                    $aTabDroits['s'] = 0;
                    $aTabDroits['m'] = 0;
                    $aTabDroits['a'] = 0;
                    $aTabDroits['w'] = 0;

                }
            }else{

                //Si l'utilisateur n'existe pas
                class_fli::set_aData('guid_user',NULL);
                class_fli::set_aData('guid_groupes',NULL);
            }
        }else{
            //Si il n'y a pas de tentative de connexion
            class_fli::set_aData('guid_user',NULL);
            class_fli::set_aData('guid_groupes',NULL);
            //echo "oo";
        }
        //echo"sssss<pre>";print_r(class_fli::get_aData('guid_groupes'));echo"</pre>";
        class_fli::set_droits($aTabDroits);

        return $aTabDroits;
    }

}