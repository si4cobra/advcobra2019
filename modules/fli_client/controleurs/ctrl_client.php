<?php

/**
 * Created by PhpStorm.
 * User: Arslan
 * Date: 09/10/2019
 * Time: 17:27
 */
class ctrl_client  extends class_form_list
{





    public function fli_client()
    {
        $this->sNomTable = 'client';                            //string Nom de la table liée
        $this->sChampId = 'id_client';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = 'guid_client';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_client';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = true;                         //bool
        $this->bAffMod = true;                           //bool Permet la modification
        $this->bAffSupp = true;                          //bool Permet la suppression
        $this->bPagination = true;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = '';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = 'Formulaire client';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = 'Enregistrement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = 'Modification du test';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = 'Modification Réussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = '';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = '';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = 'Ajouter un client';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = '';                      //string Titre de la section recherche
        $this->sTitreListe = 'Formulaire client';                          //string Titre de la liste
        $this->sLabelNbrLigne = ' Ligne(s)';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = 'Modification';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = true;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'nom_client', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_client', // la valeur de l attribut "name" dans le formulaire
            'alias_champ' => 'nom_client', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'nom client', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => 'like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'index_champ_sql' => 'KEY' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));





        $aTabTpl[] = $this->run();
        //Le retour de run() contient le template déjà rempli
        return $aTabTpl;
    }







}