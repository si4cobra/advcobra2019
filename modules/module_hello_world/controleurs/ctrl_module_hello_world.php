<?php
/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 16/12/2015
 * Time: 17:27
 */

class ctrl_module_hello_world extends class_form_list{

    /**
     * @return mixed
     * Fonction qui renvoie le menu en fonction des différents modules présents dans le framework et selon le fichier install.php
     */
    public function hello_world(){

    	$this->sNomTable=$this->sPrefixeDb.'users';
        $this->sChampId='id_user';
        $this->sChampSupplogique='supplogique_user';
        $this->bTraiteConnexion=false;

        $this->chargement();        

        $aTabTpl[] = class_fli::get_html_home();

        return $aTabTpl;
    }

}