<?php
/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 16/12/2015
 * Time: 17:27
 */

class ctrl_fli_connexion extends class_form_list{

    /**
     * @return mixed
     * Fonction qui permet la connexion d'un utilisateur / mot de passe en SHA-256
     */
    public function connexion(){

        $aVar = array( 'login', 'password','guid','bdebug' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $erreur = "";
        $aTabUser=array();

        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $objImport = new class_import();
            $objImport->import_modele('fli_connexion','mod_fli_connexion');

            $objModCon = new mod_fli_connexion();

            $aSel = $objModCon->get_sel_user($login);
            if(isset($aSel[0]['sel_user']) && !empty($aSel[0]['sel_user'])){
                $sSel = $aSel[0]['sel_user'];
            }else{
                $sSel = '';
            }

            $password = hash( 'sha512', $password.$sSel );

            $aTabUser = $objModCon->get_user($login,$password);

            if(!empty($aTabUser)){
                //Création du cookie
                class_fli::set_guid_user($aTabUser[0]['guid_user']);

                setcookie(class_fli::get_nom_cookie(), $aTabUser[0]['guid_user'], (time() + 604800));
                header('Location: '.class_fli::get_chemin_acces_absolu().'/'.class_fli::get_module_defaut().'-'.class_fli::get_controleur_defaut().'-'.class_fli::get_fonction_defaut());
                exit;
            }else{
                $erreur = "Le login et/ou le mot de passe ne correspond(ent) pas !";
            }
        }

        $this->sNomTable=$this->sPrefixeDb.'users';
        $this->sChampId='id_user';
        $this->sChampSupplogique='supplogique_user';
        $this->sDebugRequete=true;
        $this->sLabelCreationElem=true;
        $this->sAffFiche=false;
        $this->sAffMod=true;
        $this->sAffSupp=true;
        $this->sPagination=true;
        $this->sNombrePage=50;
        $this->sFiltreliste='';
        $this->sTitreForm = 'Gestion de vos taches et suivi';
        $this->FiltrelisteOrderBy='';
        $this->sTitreCreationForm='Création d\'une nouvelle ';
        $this->sTitreModificationForm='Modification d\'une ';
        $this->sMessageCreationSuccesForm='La ligne  a été créee';
        $this->sMessageModificationSuccesForm='La  a bien été modifiée';
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';
        $this->sMessageSupprElem = 'La ligne  a été supprimée';
        $this->sLabelCreationElem = 'Créer une tache ou un suvi ';
        $this->sTitreListe = 'Gestion de vos taches et suivi ';
        $this->sLabelNbrLigne = 'Tâche(s) correspond(ent) à votre recherche';
        $this->sLabelRecherche = 'Recherche d\'une ';
        $this->sLabelRetourListe = '';
        $this->sRetourValidationForm = 'liste';
        $this->sCategorieListe = '';
        $this->sListeTpl = 'login.tpl';

        $this->chargement('','');

        $this->objSmarty->assign('erreur',$erreur);

        //Ajout des css du module


        $aTabTpl[] = $this->run();

        return $aTabTpl;
    }

}