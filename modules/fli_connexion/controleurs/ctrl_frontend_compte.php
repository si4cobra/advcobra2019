<?php

/**
 * Created by PhpStorm.
 * User: Yannick
 * Date: 15/12/2016
 * Time: 10:33
 */
class ctrl_frontend_compte extends class_form_list
{
    protected $id_ville=null;
    protected $sCheminAbsoluPassionnement=null;
    protected $url_central=null;
    protected $objBdd;


    public function __construct($objSmartyImport,$sModule){

        parent::class_list($objSmartyImport,$sModule);
        $objImport = new class_import();
        $objImport->import_modele('fli_connexion', 'mod_frontend_compte');
        $objMod = new mod_frontend_compte();
        $this->objBdd = $objMod;
    }



    public function controle_form_connexion()
    {

        $aVar = array( "mail","choix","nom","prenom","mail","tel","validation" );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $aTabVerif = $this->objBdd->verifUserByMail($mail);
        //echo"<pre>";print_r($aTabVerif);echo"</pre>";

        if(empty($choix) && $validation == 'inscription')
        {
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $aTabUser = $this->objBdd->get_contact($nom, $prenom,$mail);
                //echo $login;
                //echo"<pre>";print_r($aTabUser);echo"</pre>";
                if(empty($aTabUser)) {
                    $aTabContact['nom'] = $nom;
                    $aTabContact['prenom'] = $prenom;
                    $aTabContact['mail'] = $mail;
                    $aTabContact['tel'] = $tel;
                    $this->objBdd->insert_contact($aTabContact);
                }
            }


        }
        if(empty($aTabVerif))
        {
            $sTableauRetourControl['result'] = true;

        }
        else
        {
            $sTableauRetourControl['result'] = false;
            $sTableauRetourControl['message'][0] = "Vous êtes déja membre de Passionnément Catalan";
        }

        //echo"<pre>";print_r($sTableauRetourControl);echo"</pre>";
        return $sTableauRetourControl;
        

    }

    public function post_form_insert_connexion()
    {
        $aVar = array( "mail",'motdepasse','nom','prenom' );
        $aVarVal = class_params::nettoie_get_post($aVar);
        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }




        $aTabVerif = $this->objBdd->verifUserByMail($mail);

        
        
        $aTabUserInsert['login_user'] = $mail;
        $aTabUserInsert['mdp'] = $motdepasse;
        $aTabUserInsert['url'] = "<a href='www.passionnement-catalan.fr/frontend-ctrl_frontend_compte-fli_validation?guid=".$aTabVerif[0]['guid_user']."'> Valider </a>";
        
        //echo"<pre>";print_r($aTabUserInsert['url']);echo"</pre>";
        
        $aTabUserInsert['guid'] = $aTabVerif[0]['guid_user'];
        $aTabUserInsert['nom'] = $nom;
        $aTabUserInsert['prenom'] = $prenom;

        $aTabParamMail =array();
        $aTabParamMail['idalerte'] = "";
        $aTabParamMail['sujet'] = "Validation de votre compte";
        $aTabParamMail['message'] = "Cliquez ici pour valider votre compte <a href='www.passionnement-catalan.fr/frontend-ctrl_frontend_compte-fli_validation?guid='".$aTabVerif[0]['guid_user']."'> ICI </a>";
        $aTabParamMail['expediteur']="info@passionnement-catalan.fr";
        $aTabParamMail['mailenvoyeur']="info@passionnement-catalan.fr";
        $aTabParamMail['titre']="Passionnement Catalan";
        $aTabParamMail['monmail']="Creation de votre compte ";
        $aTabParamMail['guid'] = $aTabVerif[0]['guid_user'];
        $aTabParamMail['lemail'] = $mail;

        $this->objBdd->programmation_envoi_mail($aTabUserInsert);

        
         $this->objBdd->maj_mot_de_passe($aTabUserInsert);

    }

    public function fli_validation(){
        


        $aVar = array( "guid" );
        $aVarVal = class_params::nettoie_get_post($aVar);
        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $this->objBdd->updateCompte($guid);




        //BDD
        $this->sNomTable='pa_users';                            //string Nom de la table liée
        $this->sChampId='id_user';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='guid_user';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_user';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche=false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod=false;                           //bool Permet la modification
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination=false;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm='Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm='Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessageSupprElem='Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe='';                          //string Titre de la liste
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->sLabelNbrLigne='nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion='';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour='';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination='';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl='validation.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie='principal_frontend.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        //Array
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl=array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule='Accueil';
        $this->sMenuVisibile=true;
        $this->sMenuTarge=false;
        $this->chargement('frontend','version1');                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */



        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;
    }


    public function motpasseoublier()
    {

        $aVar = array( 'login','validation' );

        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }



        //BDD
        $this->sNomTable=$this->sPrefixeDb.'users';                            //string Nom de la table liée
        $this->sChampId='id_user';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='guid_user';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_user';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche=false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod=false;                           //bool Permet la modification
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination=false;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm='Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm='Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessageSupprElem='Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe='';                          //string Titre de la liste
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->sLabelNbrLigne='nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion='';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour='';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination='';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl='motdepasse.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        //Array
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl=array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule='Accueil';
        $this->sMenuVisibile=true;
        $this->sMenuTarge=false;
        $this->chargement('fli_connexion','version1');                              //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module
        $aTaleauRetour['bresult']=false;
        /* --------- $this->Ajout_champ() ici --------- */
        $sLink =  $_SERVER['HTTP_HOST'];

        if($validation=="ok") {

            $aTaleauRetour = $this->objBdd->renvoi_link_init_mdp($login, $sLink,$this->sPrefixeDb);
            //echo"<pre>";print_r($aTaleauRetour);echo"</pre>";
            if( $aTaleauRetour['bresult'] ) {
                //$objMailjet = new class_mail_jet_v3();
               // //echo"<pre>";print_r($aTaleauRetour);echo"</pre>";
                //$result = $objMailjet->send_mail('passionnementcatalan@cobranaja.com', $login, $aTaleauRetour['sujet'], $aTaleauRetour['message'], "");
                //echo"<pre>";print_r($result);echo"</pre>";
                $this->objSmarty->assign('message_info', "Email d'initialisation bien envoyé");
                $this->objSmarty->assign('bMessageErreurForm', true);
            } else{
                //echo "toto";
                $this->objSmarty->assign('sMessageErreurForm_push', "Aucun compte ne corrrespond à cette  Email " . $login);
                $this->objSmarty->assign('bMessageErreurForm_push', true);
            }
        }



        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;
    }

    /*
       * Fonction envoi de mail pour reinitialisation du mot de passe
       */
    public function fli_initmdphome(){


        /* $objImport = new class_import();
         $objImport->import_modele('frontend', 'mod_frontend');
         $objModfrontend = new mod_frontend();
         $_GET['guid_type_annonce'] = "Particuliers";
         $aTableauCategorie = $objModfrontend->renvoi_categorie();
         //echo"<pre>";print_r($aTableauCategorie);echo"</pre>";



         $this->affection_info($aTableauCategorie, "frontend-ctrl_frontend-listparticulier");
         class_fli::set_aData("choix", $choix);
        */

        //récupération des variable passé en parametre
        $aVar = array( 'choix', 'typecate', 'fli_fonction',"login" );
        $aVarVal = class_params::nettoie_get_post($aVar);
        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $this->sNomTable='pa_users';                            //string Nom de la table liée
        $this->sChampId='id_user';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='guid_user';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_user';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche=true;                         //bool
        $this->bAffMod=false;                           //bool Permet la modification
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bPagination=false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm='';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm='';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem='';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='';                      //string Titre de la section recherche
        $this->sTitreListe='Initialisation de votre mot de passe';                          //string Titre de la liste
        $this->sLabelNbrLigne='';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl='login_frontend.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour='';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination='';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie='principal_frontend.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement('frontend', 'version1');                        //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        $aTaleauRetour['bresult']=false;
        /* --------- $this->Ajout_champ() ici --------- */
        $aTaleauRetour = $this->objBdd->renvoi_link_init_mdp($login,$this->url_central);

        if($aTaleauRetour['bresult']) {
            $objMailjet = new class_mail_jet_v3();
            //echo"<pre>";print_r($aTaleauRetour);echo"</pre>";
            $result = $objMailjet->send_mail('passionnementcatalan@cobranaja.com', $login, $aTaleauRetour['sujet'], $aTaleauRetour['message'], "");
            //echo"<pre>";print_r($result);echo"</pre>";
            $this->objSmarty->assign('message_info', "Email d'initialisation bien envoyé");
        }else{
            $this->objSmarty->assign('message_erreur', "Aucun compte ne corrrespond à cette  Email ".$login);
        }
        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;

    }
    
    public function erreur_connexion(){
        //BDD
        $this->objSmarty->assign('sTitre', "Problème création Mot de passe");
        $this->objSmarty->assign('sMessage', "Certains utilisateurs ayant des difficultés à activer leur nouveau mot de passe, merci d'utiliser le nouveau lien fourni par email. Si des difficultés persistent, merci de contacter le SISTPM");
        class_fli::set_tpl_sortie('message_erreur_generique.tpl');

    }
    
    /*
     * rmodification mot de passe
     */
    public function fli_modimdp(){


        //récupération des variable passé en parametre
        $aVar = array( 'password', 'password2','validation','guid' );
        $aVarVal = class_params::nettoie_get_post($aVar);
        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        if($guid=="[[GUID_USER]]"){
            header('Location:fli_connexion-ctrl_frontend_compte-erreur_connexion');
        }


        $this->sNomTable=$this->sPrefixeDb.'users';                            //string Nom de la table liée
        $this->sChampId='id_user';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='guid_user';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_user';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche=true;                         //bool
        $this->bAffMod=false;                           //bool Permet la modification
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bPagination=false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm='';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm='';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem='';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='';                      //string Titre de la section recherche
        $this->sTitreListe='Modification de votre mot de passe';                          //string Titre de la liste
        $this->sLabelNbrLigne='';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl='init_motpass.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sMenuIntitule='Modifier votre mot de passe';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sMenuVisibile=false;           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sMenuTarget='';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour='';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination='';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement('fli_connexion', 'version1');                        //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        $bsuccess=false;
        //Modification du mot de pass
        if($validation=="ok") {
           /* echo "<pre>";
            print_r($_POST);
            echo "</pre>";

            echo "guid " . $guid;
            */
            if($password2!=$password){
                $erreur  =" Les deux mots de passes saisis ne sont pas identiques";
                $this->objSmarty->assign('erreur', $erreur);
                $this->objSmarty->assign('sMessageErreurForm_push', "Les deux mots de passes saisis ne sont pas identiques");
                $this->objSmarty->assign('bMessageErreurForm_push', true);
            }elseif(trim($password2=="") or trim($password)==""){
                $erreur  =" Les mots de passe saisis ne doivent pas être vide";
                $this->objSmarty->assign('sMessageErreurForm_push', "Les mots de passe saisis ne doivent pas être vide");
                $this->objSmarty->assign('bMessageErreurForm_push', true);
            }elseif(strlen($password2)<4){
                $this->objSmarty->assign('sMessageErreurForm_push', "Les mots de passe saisis doivent comporter minimun 4 caractères");
                $this->objSmarty->assign('bMessageErreurForm_push', true);
            }
            else{
                $bsuccess=true;
                $aTabInfo = array();
                $aTabInfo['guid_user']=$guid;
                $aTabInfo['mdp']=$password;
                $bresult= $this->objBdd->maj_mot_de_passe($aTabInfo,$this->sPrefixeDb);
                if(!$bresult){
                    $erreur  ="Un problème technique est survenu pendant la modification de votre mot de passe<br> recommencer ou contacter le service technique";
                    $bsuccess=false;
                    $this->objSmarty->assign('sMessageErreurForm_push', "Un problème technique est survenu pendant la modification de votre mot de passe<br> recommencer ou contacter le service technique");
                    $this->objSmarty->assign('bMessageErreurForm_push', true);
                }else{
                    $bsuccess=true;
                    $this->objSmarty->assign('message_info', "Mot de passe modifié cliquez sur login pour vous connecter");
                    $this->objSmarty->assign('bMessageErreurForm', true);
                }
            }
        }
        $this->objSmarty->assign('bsuccess', $bsuccess);
        $this->objSmarty->assign('url_central', $this->url_central);
        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;

    }



    public function post_form_insert_fli_bug(){

        $aVar = array( 'sujet_pcat_bug','email_bug','description_pcat_bug' );
        $aVarVal = class_params::nettoie_get_post($aVar);
        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $aParam=array();
        $sSujet = "Alerte Signalisation Bug passionnement ";
        $sMessage=" Message reçu<br>";
        $sMessage.=" Sujet : ".$sujet_pcat_bug."<br>";
        $sMessage.=" Email : ".$email_bug."<br>";
        $sMessage.=" Message <br>".$description_pcat_bug;


        $aParam['idalerte']="alaertebug";
        $aParam['message']=$sMessage;
        $aParam['sujet']=$sSujet;
        $aParam['monmail']="Bug Passionnement catalan";


        $this->objBdd->envoi_mail_liste($aParam);
        //return $aTableauPreAcion;
    }
    /*
  * rmodification mot de passe
  */
    public function fli_bug()
    {
        //récupération des variable passé en parametre
        $aVar = array( 'id_client','passform');
        $aVarVal = class_params::nettoie_get_post($aVar);
        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $bformaff=true;

        if($passform=="ok"){
            $bformaff=false;
        }

        $this->sNomTable = 'pcat_bug';                            //string Nom de la table liée
        $this->sChampId = 'id_pcat_bug';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = 'guid_pcat_bug';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_pcat_bug';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = true;                         //bool
        $this->bAffMod = false;                           //bool Permet la modification
        $this->bAffSupp = false;                          //bool Permet la suppression
        $this->bPagination = false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = '';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = '';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = 'Merci, nous allons traiter l\'anomalie dans les plus bref délais.';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = '';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = '';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = '';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = 'Une erreur est survenu lors de l\'enregistrement de l\'anomalie.';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = '';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = '';                      //string Titre de la section recherche
        $this->sTitreListe = '';                          //string Titre de la liste
        $this->sLabelNbrLigne = '';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'form';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'form';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sMenuIntitule = '';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sMenuVisibile = false;           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sMenuTarget = '';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'bug.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = '';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal_frontend.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement('frontend', 'version1');                        //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module


        $this->objSmarty->assign('urlretour', '');
        $this->objSmarty->assign('aTabInfoALaUne', '');
        $this->objSmarty->assign('url_inforalaune', '');
        $this->objSmarty->assign('bformaff', $bformaff);

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'sujet_pcat_bug', // le nom du champ dans la table selectionnee
            'nom_variable' => 'sujet_pcat_bug', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Sujet', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'email_bug', // le nom du champ dans la table selectionnee
            'nom_variable' => 'email_bug', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Votre eMail', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
            'type_champ' => 'textarea',
            'mapping_champ' => 'description_pcat_bug', // le nom du champ dans la table selectionnee
            'nom_variable' => 'description_pcat_bug', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Description', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
            'type_champ' => 'hidden',
            'mapping_champ' => 'id_client', // le nom du champ dans la table selectionnee
            'nom_variable' => 'id_client', // la valeur de l attribut "name" dans le formulaire
            'text_label' => '', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_form' => 'ok',
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|'
            'transfert_inter_module'=>'ok',
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;

    }

}