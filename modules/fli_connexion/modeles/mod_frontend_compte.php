<?php

/**
 * Created by PhpStorm.
 * User: Yannick
 * Date: 15/12/2016
 * Time: 10:33
 */

class mod_frontend_compte extends mod_form_list
{

    public function get_sel_user($login){
        $aParam[] = $login;

        $aResultRequete = $this->renvoi_info_requete("SELECT sel_user 
                                                      FROM pa_users 
                                                      WHERE supplogique_user='N' 
                                                      AND login_user=?",$aParam);

        return $aResultRequete;
    }

    public function get_user($login, $password){
        $aParam[] = $login;
        //$aParam[] = $password;

        $aResultRequete = $this->renvoi_info_requete("SELECT guid_user,Concat (prenom_user,' ',nom_user) as user
                                                      FROM pa_users 
                                                      WHERE supplogique_user='N' 
                                                      AND active_compte_user = 'Y'
                                                      AND login_user=? 
                                                      ",$aParam);

        return $aResultRequete;
    }

    public function get_user_by_login_and_pwd($login,$password)
    {
        $aParam[] = $login;
        $aParam[] = $password;

        $aResultRequete = $this->renvoi_info_requete("SELECT guid_user,Concat (prenom_user,' ',nom_user) as user
                                                      FROM pa_users 
                                                      WHERE supplogique_user='N'
                                                      AND active_compte_user = 'Y'
                                                      AND login_user=? 
                                                      AND password_user=?",$aParam);

        return $aResultRequete;
    }

    public function get_contact($nom,$prenom,$mail)
    {
        $sResultRequete = "Select id_contact from pc_contact where nom_contact = '".$nom."' AND prenom_contact = '".$prenom."' or mail_contact = '".$mail."' ";

        

        $aResultRequete = $this->renvoi_info_requete($sResultRequete);

        return $aResultRequete;

    }

    public function update_user($aTabInfo){

        $sRequete_update = "UPDATE pa_users SET nom_user='".$aTabInfo['nom']."',
            prenom_user='".$aTabInfo['prenom']."',
            pseudo_user='".$aTabInfo['pseudo']."',
            tel_user='".$aTabInfo['tel']."'
            WHERE guid_user='".$aTabInfo['guid']."'";
        $id = $this->executionRequeteId($sRequete_update);

        return $id;
    }

    public function lst_petite_annonce($guid_user){

        $tabRetour = array();

        $sRequete_update = "SELECT statut_annonce,guid_annonce, nom_annonce, image1_annonce, 
                              DATE_FORMAT(date_enreg_annonce,'%d/%m/%Y') as date_enreg_annonce,
                              DATE_FORMAT(date_mise_ligne_annonce,'%d/%m/%Y') as date_mise_ligne_annonce,
                              prix_annonce 
                              FROM pa_annonce 
                              WHERE guid_user='".$guid_user."'
                              AND supplogique_annonce='N'";

        $tabRetour = $this->renvoi_info_requete($sRequete_update);

        return $tabRetour;
    }

    public function get_messageEtatAnnonce()
    {
        $sRequeteMessageEtatAnnonce ="Select valeur_variables,longcontenu_variable from variables where libelle_variables = 'message_bloque' or libelle_variables = 'message_attente'";
        $aTabMessageEtatAnnonce = $this->renvoi_info_requete($sRequeteMessageEtatAnnonce);
        return $aTabMessageEtatAnnonce;

    }

    public function lst_vente_privee($guid_user){

        $tabRetour = array();

        $sRequete_update = "SELECT guid_vente_privee, date_user_vente_privee 
                              FROM pa_user_vente_privee 
                              WHERE guid_user='".$guid_user."'
                              AND supplogique_user_vp='N'";

        $aResultRequete1 = $this->renvoi_info_requete($sRequete_update);

        if(!empty($aResultRequete1)){
            foreach($aResultRequete1 as $tabLien){
                $sRequete_update = "SELECT guid_vente_privee, DATE_FORMAT(datedebut_vente_privee,'%d/%m/%Y') as datedebut_vente_privee, 
                              DATE_FORMAT(datefin_vente_privee,'%d/%m/%Y') as datefin_vente_privee, contenu_vente_privee, 
                              desc_vente_privee, image1_vente_privee, infoalaune_vente_privee.id_categorie, enseigne_contenu
                              FROM infoalaune_vente_privee 
                              INNER JOIN contenu_portail on (contenu_portail.id_portail = infoalaune_vente_privee.id_portail)
                              WHERE guid_vente_privee='".$tabLien['guid_vente_privee']."'
                              AND supplogique_vente_privee='N'
                              AND datedebut_vente_privee <= NOW() + INTERVAL 10 DAY
                              AND datefin_vente_privee >= NOW()";

                $aResultRequete = $this->renvoi_info_requete($sRequete_update);
                if(!empty($aResultRequete)) {
                    $tabRetour[] = array( 'guid_vente_privee' => $aResultRequete[0]['guid_vente_privee'],
                        'datedebut_vente_privee' => $aResultRequete[0]['datedebut_vente_privee'],
                        'datefin_vente_privee' => $aResultRequete[0]['datefin_vente_privee'],
                        'contenu_vente_privee' => $aResultRequete[0]['contenu_vente_privee'],
                        'desc_vente_privee' => $aResultRequete[0]['desc_vente_privee'],
                        'image1_vente_privee' => $aResultRequete[0]['image1_vente_privee'],
                        'id_categorie' => $aResultRequete[0]['id_categorie'],
                        'date_user_vente_privee' => $tabLien['date_user_vente_privee'],
                        'enseigne_contenu' => $aResultRequete[0]['enseigne_contenu']);
                }
            }

        }

        return $tabRetour;
    }

    /*
    *Enooi le link
    */
    public function renvoi_link_init_mdp($login,$sLink,$prefixe){

        $aTableauRetour=array();
        $aTableauRetour['message']="";
        $aTableauRetour['sujet']="";
        $aTableauRetour['bresult']=false;

        $sMessageRetour="";

        $sRequete_info_user ="SELECT guid_user,
        login_user
        FROM ".$prefixe."users
        WHERE login_user='".$login."'";
        $aTableauInfoUser = $this->renvoi_info_requete($sRequete_info_user);

        $SRequete_info_message="SELECT objet_tm,
        corps_tm
        FROM   f_template_message
        WHERE code_tm='initmdppa'";
        $aTableauInfoMessage = $this->renvoi_info_requete($SRequete_info_message);

        if(!empty($aTableauInfoUser)) {

            $sLink = "http://".$sLink."/fli_connexion-ctrl_frontend_compte-fli_modimdp?guid=" . $aTableauInfoUser[0]['guid_user'] . "";
            //$sMessageRetour =str_replace("[annonce]",$aTableauInfoAnnonce[0]['nom_annonce'],$aTableauInfoMessage[0]['text_config_message']);
            $sMessageRetour = str_replace("[mail]", $aTableauInfoUser[0]['login_user'], $aTableauInfoMessage[0]['corps_tm']);
            $sMessageRetour = str_replace("[lien]", $sLink, $sMessageRetour);
            $aTableauRetour['message'] = $sMessageRetour;
            $sSujet =  $aTableauInfoMessage[0]['objet_tm'];
            $aTableauRetour['sujet'] = $sSujet;
            $aTableauRetour['email'] = $aTableauInfoUser[0]['login_user'];
            $aTableauRetour['bresult']=true;

            $sRequete_enreg_envoi_mail="INSERT f_mail_envoi set giud_mail_envoi='".class_helper::guid()."',
                message_mail_envoi='".addslashes($sMessageRetour)."',
                sujet_mail_envoi='".$sSujet."',
                nom_mail_envoi='Mail reinitialisation login ".$aTableauInfoUser[0]['login_user']."',
                envoye_mail_envoi='".$aTableauInfoUser[0]['login_user'].";',
                type_mail='reinit'";

            //echo $sRequete_enreg_envoi_mail."<br>";

            $this->execute_requete($sRequete_enreg_envoi_mail);
        }
        return $aTableauRetour;


    }
    /*
     * Mise à jour mot de passe
     */
    public function maj_mot_de_passe($tabInfo,$prefixe){

        $sSupl="";
        //récupération des informations sur l'utilisateur
        if(!empty($tabInfo['login_user']))
        {
            $sRequete_info_user ="SELECT  sel_user,id_user FROM ".$prefixe."users 
        WHERE login_user='".$tabInfo['login_user']."'";
        }
        else
        {
            $sRequete_info_user ="SELECT  sel_user,id_user FROM ".$prefixe."users 
        WHERE guid_user='".$tabInfo['guid_user']."'";
        }
        

        
        $aTableauInfoUser = $this->renvoi_info_requete($sRequete_info_user);

        //echo"<pre>";print_r($aTableauInfoUser);echo"</pre>";
        if(!empty($aTableauInfoUser)) {
            if( $aTableauInfoUser[0]['sel_user'] != "" ) {
                $password = hash('sha512', $tabInfo['mdp'] . $aTableauInfoUser[0]['sel_user']);
            } else {
                $sSel = sha1(md5(uniqid(rand(), true)));
                $password = hash('sha512', $tabInfo['mdp'] . $sSel);
                $sSupl = ", sel_user='" . $sSel . "' ";
            }
            //MAj le mot de passa
            if( !empty($tabInfo['login_user']) ) {
                $sRequete_update = "UPDATE " . $prefixe . "users set 
        password_user='" . $password . "',supplogique_user='N' " . $sSupl . "
         WHERE login_user='" . $tabInfo['login_user'] . "'";
            } else {
                $sRequete_update = "UPDATE " . $prefixe . "users set 
        password_user='" . $password . "',supplogique_user='N' " . $sSupl . "
         WHERE guid_user='" . $tabInfo['guid_user'] . "'";
            }
            return $this->executionRequeteId($sRequete_update);
        }else{
            return false;
        }

        //echo$sRequete_update."<br>";
        //exit();


    }

    public function envoi_mail_liste($aTabParam){

        ///recuperation de la liste des personne dand l'alerte
        $sRequeteliste="SELECT lesmail_listediffussion FROM utilisateur_listediffussion
        WHERE filtre_listediffussion ='".$aTabParam['idalerte']."'";

        $aTableauListe = $this->renvoi_info_requete($sRequeteliste);

        if(empty($aTableauListe)){
            ///recuperation de la liste des personne dand l'alerte
            $sRequeteliste="SELECT lesmail_listediffussion FROM utilisateur_listediffussion
            WHERE filtre_listediffussion ='allpasscat'";

            $aTableauListe = $this->renvoi_info_requete($sRequeteliste);
        }

        if(isset($aTabParam['mailenvoyeur'])){
            if(trim($aTabParam['mailenvoyeur'])=="")
                $aTabParam['mailenvoyeur']="info@passionnement-catalan.fr";
        }else{
            $aTabParam['mailenvoyeur']="info@passionnement-catalan.fr";
        }

        if(isset($aTabParam['monmail'])){
            if(trim($aTabParam['monmail'])=="")
                $aTabParam['monmail']="info@passionnement-catalan.fr";
        }else{
            $aTabParam['monmail']="Passionnement Catalan";
        }

        if(!empty($aTableauListe)) {

            $sRequete_insert_mail ="Insert pa_mail_envoi set date_mail_envoi=now()
            ,statut_mail_envoi='attente',
            type_mail='".$aTabParam['idalerte']."',
            message_mail_envoi='".addslashes($aTabParam['message'])."',
            sujet_mail_envoi='".addslashes($aTabParam['sujet'])."',
            giud_mail_envoi='".class_helper::guid()."',
            mailquienvoi_mail_envoi='".$aTabParam['mailenvoyeur']."',
            nom_mail_envoi='".$aTabParam['monmail']."',
            envoye_mail_envoi='".addslashes($aTableauListe[0]['lesmail_listediffussion'])."'";
            //echo $sRequete_insert_mail."<br>";
            $this->execute_requete($sRequete_insert_mail);


        }

    }

    public function insert_contact($aTabContact)
    {
        $sRequeteInsertContact = "Insert into pc_contact (nom_contact,prenom_contact,mail_contact,tel_contact,datetime_contact) VALUES ('".$aTabContact['nom']."','".$aTabContact['prenom']."','".$aTabContact['mail']."','".$aTabContact['tel']."','NOW()')";

        //echo $sRequeteInsertContact;

        $id = $this->executionRequeteId($sRequeteInsertContact);

        return $id;
    }

    public function renvoi_info_image($idville){

        $aTableauRetour=array();


        $sRequete_liste_image="Select chemin_image,filtre_image from  ville_image
        where id_ville='".$idville."'";

        $aTableauInfo = $this->renvoi_info_requete($sRequete_liste_image);

        if(!empty($aTableauInfo)){
            foreach($aTableauInfo as $valeur){

                $aTableauRetour[$valeur['filtre_image']]=$valeur['chemin_image'];
            }
        }

        return $aTableauRetour;

    }

    public function verifUserByMail($mail)
    {
        $sRequeteVerifUserByMail = "Select id_user,guid_user from pa_users where login_user = '" . $mail . "'";

        $aTabRetoutVerifUserByMail = $this->renvoi_info_requete($sRequeteVerifUserByMail);

        //echo"<pre>";print_r($aTabRetoutVerifUserByMail);echo"</pre>";
        
        return $aTabRetoutVerifUserByMail;

    }

    public function updateCompte($guid)
    {
        $sRequeteUpdateCompte = "Update pa_users Set active_compte_user = 'Y' WHERE guid_user='".$guid."' ";

        $id = $this->executionRequeteId($sRequeteUpdateCompte);

        return $id;


    }

    public function programmation_envoi_mail($aTabParam)
    {



        $sRequete_info_message_validation_compte = "SELECT sujet_config_message,
        text_config_message
        FROM  pa_config_message
        WHERE identifiant_config_message='confirmationcreationcompte'";

        //text_config_message = message



        $aTableau_info_message_validation_compte = $this->renvoi_info_requete($sRequete_info_message_validation_compte);

        $sMessageRetour = str_replace("[login]",$aTabParam['login_user'],$aTableau_info_message_validation_compte[0]['text_config_message']);
        $sMessageRetour = str_replace("[lien]",$aTabParam['url'],$sMessageRetour);
        $sMessageRetour = str_replace("[nom]",$aTabParam['nom'],$sMessageRetour);
        $sMessageRetour = str_replace("[prenom]",$aTabParam['prenom'],$sMessageRetour);

        $sRequete_insert_mail = "Insert pa_mail_envoi set date_mail_envoi=now()
            ,statut_mail_envoi='attente',
            type_mail='',
            message_mail_envoi='" . addslashes($sMessageRetour) . "',
            sujet_mail_envoi='" . addslashes($aTableau_info_message_validation_compte[0]['sujet_config_message']) . "',
            giud_mail_envoi='" . class_helper::guid() . "',
            mailquienvoi_mail_envoi='info@passionnement-catalan.fr',
            nom_mail_envoi='Creation de votre compte',
            guid_pa='" . $aTabParam['guid'] . "',
            guid_vp='',
            envoye_mail_envoi='" . $aTabParam['login_user'] . "'";
        //echo $sRequete_insert_mail."<br>";
        $this->execute_requete($sRequete_insert_mail);
    }

}