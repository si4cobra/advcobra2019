<?php

/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 22/12/2015
 * Time: 09:35
 */
class mod_fli_connexion extends mod_form_list
{
    public function get_user($login,$password){
        $aParam[] = $login;
        $aParam[] = $password;

        $aResultRequete = $this->renvoi_info_requete("SELECT id_user,guid_user,langue_user,activtraductionmodif_user FROM ".$this->sPrefixeDb."users WHERE supplogique_user='N' AND login_user=? AND password_user=?",$aParam);

        return $aResultRequete;
    }

    public function get_sel_user($login){
        $aParam[] = $login;

        $aResultRequete = $this->renvoi_info_requete("SELECT sel_user FROM ".$this->sPrefixeDb."users WHERE supplogique_user='N' AND login_user=?",$aParam);

        return $aResultRequete;
    }

    /*
    *Envoi le link
    */
    public function renvoi_link_init_mdp($login,$sLink,$prefixe){

        $aTableauRetour=array();
        $aTableauRetour['message']="";
        $aTableauRetour['sujet']="";
        $aTableauRetour['bresult']=false;

        $sMessageRetour="";

        $sRequete_info_user ="SELECT guid_user,
        login_user
        FROM ".$prefixe."users
        WHERE login_user='".$login."'";
        $aTableauInfoUser = $this->renvoi_info_requete($sRequete_info_user);

        $SRequete_info_message="SELECT objet_tm,
        corps_tm
        FROM   f_template_message
        WHERE code_tm='initmdppa'";
        $aTableauInfoMessage = $this->renvoi_info_requete($SRequete_info_message);

        if(!empty($aTableauInfoUser)) {

            $sLink = "<a href='".$sLink."frontend-ctrl_frontend_compte-fli_modimdp?guid=" . $aTableauInfoUser[0]['guid_user'] . "'>Reinitialiser votre mot de passe</a>";
            //$sMessageRetour =str_replace("[annonce]",$aTableauInfoAnnonce[0]['nom_annonce'],$aTableauInfoMessage[0]['text_config_message']);
            $sMessageRetour = str_replace("[mail]", $aTableauInfoUser[0]['login_user'], $aTableauInfoMessage[0]['corps_tm']);
            $sMessageRetour = str_replace("[lien]", $sLink, $sMessageRetour);
            $aTableauRetour['message'] = $sMessageRetour;
            $sSujet =  $aTableauInfoMessage[0]['objet_tm'];
            $aTableauRetour['sujet'] = $sSujet;
            $aTableauRetour['email'] = $aTableauInfoUser[0]['login_user'];
            $aTableauRetour['bresult']=true;
        }
        return $aTableauRetour;


    }
}