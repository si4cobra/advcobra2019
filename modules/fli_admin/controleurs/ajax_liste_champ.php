<?php
/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 07/09/2017
 * Time: 17:39
 */

ini_set("display_errors","ON");
//echo $_SERVER["DOCUMENT_ROOT"];

require_once $_SERVER["DOCUMENT_ROOT"].'/libs/initialise_framework.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/config/config.inc.php';


$aVariable = array("table","filtre","bdebug","typerepas");

for ($i=0;$i<count($aVariable);$i++) {
    if (isset($_POST[$aVariable[$i]])) {
        $$aVariable[$i] = $_POST[$aVariable[$i]];
    } else {
        $$aVariable[$i] = "";
    }
}

if(!empty($_GET)){
    for ($i=0;$i<count($aVariable);$i++) {
        if (isset($_GET[$aVariable[$i]])) {
            $$aVariable[$i] = $_GET[$aVariable[$i]];
        } else {
            $$aVariable[$i] = "";
        }
    }
}

$objImport = new class_import();
$objImport->import_modele('fli_admin', 'mod_fli_statistique');
$objMod = new mod_fli_statistique();

$aTableauDetailRepas = $objMod->renvoi_champ_table($table,$filtre,"ajax",$bdebug);


echo json_encode($aTableauDetailRepas);