<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 06/12/2017
 * Time: 15:41
 */
class ctrl_traduction  extends class_form_list
{
    public function fli_traduction(){
        
        
        //BDD
        $this->sNomTable='f_langage';                            //string Nom de la table liée
        $this->sChampId='id_langage';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_langage';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion=true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bKeyMultiple=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffDup=false;                            //bool bAffDup Permet de dupliquer une ligne
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche=false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod=true;                           //bool Permet la modification
        $this->bAffRecapLigne=false;                     //bool  bAffRecapLigne Permet d'affiche dans unenouvelle fenetre le recap de la ligne
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination=true;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bUseDelete=false;                       //bool Permet l'affichage de la pagination
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm='Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sMessageDuplicationSucces='Duplication de la ligne reussie';                           // sMessageDuplicationSucces Message affiché lorsquela duplication de la ligne s'est déroulé avec succès
        $this->sMessageDuplicationError='Erreur pendant la duplication de la ligne';                          //string sMessageDuplicationError Message affiché lorsque la duplication de la ligne s'est déroulé avec uen erreur
        $this->sTitreModificationForm='Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessageSupprElem='Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe='Gestion contenu';                          //string Titre de la liste
        $this->sTitreForm='Gestion contenu';                           //string Titre du formulaire
        $this->sLabelNbrLigne='nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion='';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour='';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination='';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl='liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->sListeFitreNopDuplicate='';              //string sListeFitreNopDuplicate renvoi la liste des champs qu'on veut exclure de la duplication séparé par un point virgule
        //Array
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl=array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule='Gestion contenu';
        $this->sMenuVisibile=true;
        $this->sMenuTarge=false;
        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module
        
        /* --------- $this->Ajout_champ() ici --------- */
        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'identifiant_langage', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'identifiant_langage', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Identification', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'link_langage', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'link_langage', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Lien page', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'base_langage', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'base_langage', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Base de donnée', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'champbase_langage', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'champbase_langage', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Champ base', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'text',
        	'mapping_champ' => 'fr_langage', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'fr_langage', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'Fr', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'html_editable_td'=>'ok',
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'eng_langage', // le nom du champ dans la table selectionnee
            'nom_variable' => 'eng_langage', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Eng', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'html_editable_td'=>'ok',
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));








        /* --------- fin $this->Ajout_champ() --------- */
        
        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli
        
        return $aTabTpl;
        
    }
}