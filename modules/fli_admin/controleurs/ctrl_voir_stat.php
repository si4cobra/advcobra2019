<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 22/09/2017
 * Time: 17:21
 */
class ctrl_voir_stat extends class_form_list
{


    public function __construct($objSmartyImport,$sModule)
    {


        parent::class_list($objSmartyImport, $sModule);


        $sMethode = class_fli::get_fli_fonction();
        $objImport = new class_import();
        $objImport->import_modele('fli_admin', 'mod_fli_statistique');
        $objMod = new mod_fli_statistique();
        $this->objBdd = $objMod;
    }


    public function fli_voir_stat(){



       $aVar = array( 'id_statistique');
           $aVarVal = class_params::nettoie_get_post($aVar);

           foreach( $aVar as $key => $value ) {
               $$value = $aVarVal[$key];
           }

        $sNomStat="";
        $aTableauStat=array();

           $aTableauInfostat = $this->objBdd->renvoi_info_stat($id_statistique,false);
           if(!empty($aTableauInfostat)){
               $sNomStat=$aTableauInfostat['nom_statistique'];
               $sNomFunction = "calcul_stat_".$aTableauInfostat['type_statisque'];
               $aTableauStat=$this->objBdd->$sNomFunction($aTableauInfostat,"2017-01-01","2017-12-01");
           }




        //BDD
        $this->sNomTable='generation_statistique';                            //string Nom de la table liée
        $this->sChampId='id_statistique';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_statistique';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion=true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bKeyMultiple=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffDup=false;                            //bool bAffDup Permet de dupliquer une ligne
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche=false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod=false;                           //bool Permet la modification
        $this->bAffRecapLigne=false;                     //bool  bAffRecapLigne Permet d'affiche dans unenouvelle fenetre le recap de la ligne
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bBtnRetour=true;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination=true;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bUseDelete=false;                       //bool Permet l'affichage de la pagination
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm='Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sMessageDuplicationSucces='Duplication de la ligne reussie';                           // sMessageDuplicationSucces Message affiché lorsquela duplication de la ligne s'est déroulé avec succès
        $this->sMessageDuplicationError='Erreur pendant la duplication de la ligne';                          //string sMessageDuplicationError Message affiché lorsque la duplication de la ligne s'est déroulé avec uen erreur
        $this->sTitreModificationForm='Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessageSupprElem='Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe='Stat calculée pour '.$sNomStat;                          //string Titre de la liste
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->sLabelNbrLigne='nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion='';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour='fli_admin-ctrl_crea_statistique-fli_crea_statistique?';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination='';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl='stat_unique.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->sListeFitreNopDuplicate='';              //string sListeFitreNopDuplicate renvoi la liste des champs qu'on veut exclure de la duplication séparé par un point virgule
        //Array
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl=array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule='Voir stat unique';
        $this->sMenuVisibile=false;
        $this->sMenuTarge=false;
        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module
        
        /* --------- $this->Ajout_champ() ici --------- */


        $this->objSmarty->assign('aTabMenuStat', $aTableauStat);
        /* --------- fin $this->Ajout_champ() --------- */
        
        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli
        
        return $aTabTpl;
    }

}