<?php

/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 08/01/2016
 * Time: 15:02
 */
class ctrl_fli_admin extends class_form_list
{

    /*public function entete_text($objElement){
        $aTabRetour = array();

        if($objElement['mapping_champ']=="nom_module")
            $aTabRetour = array( 'sUrl' => 'toto', 'sLabel' =>'too', 'objElement' => $objElement, 'sAction' => 'href' );
        else
            $aTabRetour= parent::entete_principal($objElement);

        return $aTabRetour;
    }*/

    public function modules()
    {
        $this->sNomTable = $this->sPrefixeDb . 'modules';
        $this->sChampId = 'id_module';
        $this->sChampGuid = 'guid_module';
        $this->sChampSupplogique = 'supplogique_module';
        $this->bDebugRequete = false;
        $this->bLabelCreationElem = true;
        $this->bAffFiche = false;
        $this->bAffMod = false;
        $this->bAffSupp = false;
        $this->bPagination = true;
        $this->iNombrePage = 50;
        $this->sFiltreliste = '';
        $this->sTitreForm = 'Gestion des modules';
        $this->FiltrelisteOrderBy = '';
        $this->sTitreCreationForm = 'Installation d\'un nouveau module';
        $this->sTitreModificationForm = 'Modification d\'une ';
        $this->sMessageCreationSuccesForm = 'Le module a été installé';
        $this->sMessageModificationSuccesForm = 'La  a bien été modifiée';
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';
        $this->sMessageSupprElem = 'La ligne  a été supprimée';
        $this->sLabelCreationElem = 'Installer un nouveau module ';
        $this->sTitreListe = 'Gestion des modules';
        $this->sLabelNbrLigne = 'Module(s) correspond(ent) à votre recherche';
        $this->sLabelRecherche = 'Recherche d\'un module';
        $this->sLabelRetourListe = '';
        $this->sRetourValidationForm = 'liste';
        $this->sCategorieListe = '';
        $this->bTraiteConnexion = true;
        $this->sFormulaireTpl = 'install_module.tpl';

        $this->chargement('fli_admin', 'version1');

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'nom_module', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_module', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Module', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'version_module', // le nom du champ dans la table selectionnee
            'nom_variable' => 'version_module', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Version', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $aTabTpl[] = $this->run();

        return $aTabTpl;
    }

    public function menu()
    {
        $this->sNomTable = $this->sPrefixeDb . 'routes';
        $this->sChampId = 'id_route';
        $this->sChampGuid = 'guid_route';
        $this->sChampSupplogique = 'supplogique_route';
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = false;                         //bool
        $this->bAffMod = true;                           //bool Permet la modification
        $this->bAffSupp = true;                          //bool Permet la suppression
        $this->bPagination = false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = '';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = 'Création d\'un nouvel élément de menu';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = 'L\'élément de menu a été créé';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = 'Modification d\'un élément de menu';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = 'L\'élément de menu a bien été modifié';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = 'L\'élément de menu a été supprimé';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';                  //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = 'Créer un nouvel élément de menu';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = 'Recherche d\'un élément de menu';                     //string Titre de la section recherche
        $this->sTitreListe = 'Gestion du menu';                          //string Titre de la liste
        $this->sLabelNbrLigne = 'Element(s) de menu correspond(ent) à votre recherche';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = 'Gestion du menu';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();
        $aTableauMenuDeroulant=Array();
        $aTableauMenuDeroulant['href']="fli_admin-ctrl_gestion_contenu-fli_contenu?link_langage=[route_route]";
        $aTableauMenuDeroulant['javascript']="";
        $aTableauMenuDeroulant['key_replace']="route_route";
        $aTableauMenuDeroulant['target']="target=''";
        $aTableauMenuDeroulant['mapping_champ']="route_route";
        $aTableauMenuDeroulant['attribut']="class='glyphicon glyphicon-align-justify'";
        $aTableauMenuDeroulant['intitule']="Gestion Contenu page";
        $this->set_menu_deroulant($aTableauMenuDeroulant);

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'intitule_menu_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'intitule_menu_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Intitulé', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'route_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'route_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Route', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'pere_menu_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'pere_menu_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Elément père', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'afficher_menu_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'afficher_menu_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Afficher dans le menu', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => '', // la table liee pour ce champ
            'id_table_item' => '', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'concat(<bdd>.,\' \',<bdd>.)', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => '',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'lesitem'=>array('1'=>'OUI','0'=>'NON'),
            'bdebug' => ""

        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'ordre_menu_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'ordre_menu_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Ordre', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'target_menu_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'target_menu_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Target', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'lien_menu_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'lien_menu_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => '',
            'text_label' => 'Lien', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => '', // la table liee pour ce champ
            'id_table_item' => '', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'concat(<bdd>.,\' \',<bdd>.)', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => '',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'lesitem'=>array('1'=>'OUI','0'=>'NON'),
            'bdebug' => ""
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'icone_routes', // le nom du champ dans la table selectionnee
            'nom_variable' => 'icone_routes', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'info supp menu', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));




        $aTabTpl[] = $this->run();

        return $aTabTpl;
    }

    public function utilisateurs()
    {


        $aVar = array( 'bdebug' );

        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $this->sNomTable = $this->sPrefixeDb . 'users';
        $this->sChampId = 'id_user';
        $this->sChampGuid = 'guid_user';
        $this->sChampSupplogique = 'supplogique_user';
        $this->bDebugRequete = $bdebug;
        $this->bLabelCreationElem = true;
        $this->bAffFiche = false;
        $this->bAffMod = true;
        $this->bAffSupp = true;
        $this->bPagination = true;
        $this->iNombrePage = 50;
        $this->sFiltreliste = '';
        $this->sTitreForm = 'Gestion des utilisateurs';
        $this->FiltrelisteOrderBy = '';
        $this->sTitreCreationForm = 'Création d\'un nouvel utilisateur';
        $this->sTitreModificationForm = 'Modification d\'un utilisateur';
        $this->sMessageCreationSuccesForm = 'L\'utilisateur a été créé';
        $this->sMessageModificationSuccesForm = 'L\'utilisateur a bien été modifié';
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';
        $this->sMessageSupprElem = 'L\'utilisateur a été supprimé';
        $this->sLabelCreationElem = 'Créer un nouvel utilisateur';
        $this->sTitreListe = 'Gestion des utilisateurs';
        $this->sLabelNbrLigne = 'Utilisateur(s) correspond(ent) à votre recherche';
        $this->sLabelRecherche = 'Recherche d\'un utilisateur';
        $this->sLabelRetourListe = '';
        $this->sRetourValidationForm = 'liste';
        $this->sCategorieListe = '';



        $this->chargement();


        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'nom_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_user', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Nom', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'prenom_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'prenom_user', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Prénom', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'login_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'login_user', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Login', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $aTmpList = array();
        $aTmpList["all"] ="Allemend";
        $aTmpList["eng"] ="Anglais";
        $aTmpList["esp"] ="Espagnol";
        $aTmpList["fr"] = "Fançais";

        $this->ajout_champ(array(
        	'type_champ' => 'select',
        	'mapping_champ' => 'langue_user', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'langue_user', // la valeur de l attribut "name" dans le formulaire
        	'text_label_filtre' => 'Saisir un nom',
        	'text_label' => 'Langue de l\'utilisateur', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        	'lesitem'=>$aTmpList,
        	'bdebug' => ""

        ));


        $aTmpList = array();
        $aTmpList["Y"] = "OUI";
        $aTmpList["N"] ="NON";

        $this->ajout_champ(array(
        	'type_champ' => 'select',
        	'mapping_champ' => 'activtraductionmodif_user', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'activtraductionmodif_user', // la valeur de l attribut "name" dans le formulaire
        	'text_label_filtre' => 'Saisir un nom',
        	'text_label' => 'Activation traduction', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        	'lesitem'=>$aTmpList,
        	'bdebug' => ""

        ));


        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'activredacdoc_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'activredacdoc_user', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Activation redac documentation', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem'=>$aTmpList,
            'bdebug' => ""

        ));

        $aTmpList = array();
        $aTmpList["0"] ="Utilisateur";
        $aTmpList["5"] ="Admin";
        $aTmpList["10"] ="Super admin";

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'is_admin', // le nom du champ dans la table selectionnee
            'nom_variable' => 'is_admin', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Type super', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem'=>$aTmpList,
            'bdebug' => ""
        ));

        $aTmpList = array();
        $aTmpList["crm"] = "Groupe commerciaux";
        $aTmpList["technicien"] ="Groupe technicien";

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'groupeadmin_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'groupeadmin_user', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Fait partie', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem'=>$aTmpList,
            'bdebug' => ""
        ));




        $this->ajout_champ(array(
            'type_champ' => 'injection_code_liste',
            'nom_variable' => 'actioninjection',
            'text_label' => 'Mot de passe',
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'tab_replace_injection_code' => array( '[champ]' => 'guid_user' ),
            'injection_code' => '<a href=\'fli_admin-ctrl_fli_admin-fli_password?&action=form&guid_user=[champ]\'>Modifier le mot de passe</a><br/>
                                <a href=\'fli_admin-ctrl_fli_admin-utilisateurs_groupes?&guid_user=[champ]\'>Groupe(s) de l\'utilisateur</a>'
        ));

        $aTabTpl[] = $this->run();

        return $aTabTpl;
    }

    public function groupes()
    {
        $this->sNomTable = $this->sPrefixeDb . 'groupes';
        $this->sChampId = 'id_groupe';
        $this->sChampGuid = 'guid_groupe';
        $this->sChampSupplogique = 'supplogique_groupe';
        $this->bDebugRequete = false;
        $this->bLabelCreationElem = true;
        $this->bAffFiche = false;
        $this->bAffMod = true;
        $this->bAffSupp = true;
        $this->sPagination = true;
        $this->iNombrePage = 50;
        $this->sFiltreliste = '';
        $this->sTitreForm = 'Gestion des groupes';
        $this->FiltrelisteOrderBy = '';
        $this->sTitreCreationForm = 'Création d\'un nouveau groupe';
        $this->sTitreModificationForm = 'Modification d\'un groupe';
        $this->sMessageCreationSuccesForm = 'Le groupe a été créé';
        $this->sMessageModificationSuccesForm = 'Le groupe a bien été modifié';
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';
        $this->sMessageSupprElem = 'Le groupe a été supprimé';
        $this->sLabelCreationElem = 'Créer un nouveau groupe';
        $this->sTitreListe = 'Gestion des groupes';
        $this->sLabelNbrLigne = 'Groupe(s) correspond(ent) à votre recherche';
        $this->sLabelRecherche = 'Recherche d\'un groupe';
        $this->sLabelRetourListe = '';
        $this->sRetourValidationForm = 'liste';
        $this->sCategorieListe = '';
        $this->bAffDebug = false;
        $this->bTraiteConnexion = true;

        $this->chargement();

        $aTableauMenuDeroulant=Array();
        $aTableauMenuDeroulant['href']="fli_admin-ctrl_menu_groupe-fli_menu_groupe?guid_groupe=[guid_groupe]";
        $aTableauMenuDeroulant['javascript']="";
        $aTableauMenuDeroulant['key_replace']="guid_groupe";
        $aTableauMenuDeroulant['mapping_champ']="guid_groupe";
        $aTableauMenuDeroulant['attribut']="class='glyphicon glyphicon-align-justify'";
        $aTableauMenuDeroulant['intitule']="Gestion menu du groupe";
        $this->set_menu_deroulant($aTableauMenuDeroulant);



        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'nom_groupe', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_groupe', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Nom du groupe', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));




        $aTabTpl[] = $this->run();

        return $aTabTpl;
    }


    public function utilisateurs_groupes()
    {
        $aVar = array( 'guid_user','sLogin' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        if(!empty($guid_user)){
            $objImport = new class_import();
            $objImport->import_modele('fli_admin','mod_fli_admin');

            $objModCon = new mod_fli_admin();

            $aUser = $objModCon->get_user($guid_user);

            if(!empty($aUser)){
                $sLogin = $aUser[0]['login_user'];
            }
        }

        $this->sNomTable = $this->sPrefixeDb . 'users_groupes';
        $this->sChampId = 'id_user_groupe';
        $this->sChampGuid = 'guid_user_groupe';
        $this->sChampSupplogique = 'supplogique_user_groupe';
        $this->bDebugRequete = false;
        $this->bLabelCreationElem = true;
        $this->bAffFiche = false;
        $this->bAffMod = true;
        $this->bAffSupp = true;
        $this->bBtnRetour = true;
        $this->sDirRetour = 'fli_admin-ctrl_fli_admin-utilisateurs?';
        $this->sPagination = true;
        $this->iNombrePage = 50;
        $this->sFiltreliste = '';
        $this->sTitreForm = 'Gestion des groupes de l\'utilisateur : '.$sLogin;
        $this->FiltrelisteOrderBy = '';
        $this->sTitreCreationForm = 'Ajout d\'un nouveau groupe à l\'utilisateur';
        $this->sTitreModificationForm = 'Modification d\'un groupe de l\'utilisateur';
        $this->sMessageCreationSuccesForm = 'L\'utilisateur a été correctement lié au groupe';
        $this->sMessageModificationSuccesForm = 'L\'utilisateur a correctement changé de groupe';
        $this->sMessageErreurForm = 'Veuillez compléter tous les champs obligatoires';
        $this->sMessageSupprElem = 'L\'utilisateur a été retiré du groupe';
        $this->sLabelCreationElem = 'Ajouter un nouveau groupe à cet utilisateur';
        $this->sTitreListe = 'Gestion des groupes de l\'utilisateur : '.$sLogin;
        $this->sLabelNbrLigne = 'Groupe(s) correspond(ent) à votre recherche';
        $this->sLabelRecherche = 'Recherche d\'un groupe';
        $this->sLabelRetourListe = '';
        $this->sCategorieListe = '';
        $this->bAffDebug = false;
        $this->bTraiteConnexion = true;
        $this->sRetourValidationForm = 'externe';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = 'fli_admin-ctrl_fli_admin-utilisateurs_groupes?&guid_user='.$guid_user;         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'externe';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne = 'fli_admin-ctrl_fli_admin-utilisateurs_groupes?&guid_user='.$guid_user;         //string Permet de spécifier l'url de l'annulation

        $this->chargement();

        $this->ajout_champ(array(
            'type_champ' => 'hidden',
            'mapping_champ' => 'guid_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_user', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'guid_user', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' =>'', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'transfert_inter_module' => 'ok',
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
        'type_champ' => 'select',
        'mapping_champ' => 'guid_groupe', // le nom du champ dans la table selectionnee
        'nom_variable' => 'guid_groupe2', // la valeur de l attribut "name" dans le formulaire
        'text_label_filtre' => 'Groupe',
        'text_label' => 'Groupe', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
        'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
        'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
        'size_champ' => '', // taille max du champ
        'style' => '', // ajout du style sur le champ
        'tableau_attribut' => '', // ajout d attributs sur le champ
        'fonction_javascript' => '', // ajout du javascript sur le champ
        'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        'table_item' => $this->sPrefixeDb . 'groupes', // la table liee pour ce champ
        'id_table_item' => 'guid_groupe', // le champ de la table liee qui sert de cle primaire
        'affichage_table_item' => 'nom_groupe', // le champ de la table liee qui sert de label d affichage
        'supplogique_table_item' => 'supplogique_groupe',
        'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
        'select_autocomplete' => '', //si on fait de l'autocompletion
        'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
        'bdebug' => ""

    ));




        //class_fli::set_bDebug(true);
        //echo"<pre>";print_r($this->renvoi_formulaire());echo"</pre>";
        $aTabTpl[] = $this->run();

        return $aTabTpl;
    }

    public function routes_groupes()
    {

        $aVar = array( 'guid_groupe' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        if( isset($guid_groupe) && !empty($guid_groupe) ) {
            $sFiltreListe = ' AND principal.guid_groupe=\'' . $guid_groupe . '\'';
        } else {
            $sFiltreListe = '';
        }

        //echo 'Filtre : '.$sFiltreListe;

        $this->sNomTable = $this->sPrefixeDb . 'groupes_routes';                            //string Nom de la table liée
        $this->sChampId = 'id_groupe_route';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = 'guid_groupe_route';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_groupe_route';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = false;                         //bool
        $this->bAffMod = true;                           //bool Permet la modification
        $this->bAffSupp = true;                          //bool Permet la suppression
        $this->bPagination = false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = $sFiltreListe;                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = 'Ajout de droits par groupe';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = 'Les droits ont été accordés correctement au groupe sur la route choisie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = 'Modification des droits par groupe';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = 'Les droits ont été modifiés correctement pour le groupe sur la route choisie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = 'Les droits n\'ont pas été retirés';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = '';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = 'Ajouter des droits à un groupe';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = '';                      //string Titre de la section recherche
        $this->sTitreListe = 'Gestion des droits par groupe';                          //string Titre de la liste
        $this->sLabelNbrLigne = ' résultat(s)';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = 'Gestion des droits par groupe';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'guid_groupe', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_groupe', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Groupe',
            'text_label' => 'Groupe', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => $this->sPrefixeDb . 'groupes', // la table liee pour ce champ
            'id_table_item' => 'guid_groupe', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'nom_groupe', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => 'supplogique_groupe',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""

        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'guid_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Route',
            'text_label' => 'Route', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => $this->sPrefixeDb . 'routes', // la table liee pour ce champ
            'id_table_item' => 'guid_route', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'route_route', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => 'supplogique_route',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""

        ));


        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'guid_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_route2', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Route',
            'text_label' => 'Intitulé', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => '', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => $this->sPrefixeDb . 'routes', // la table liee pour ce champ
            'id_table_item' => 'guid_route', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'intitule_menu_route', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => 'supplogique_route',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""

        ));


        $aTmpList = array();
        $aTmpList["1"] = "OUI";
        $aTmpList["0"] ="NON";

        $this->ajout_champ(array(
            'type_champ' => 'radio',
            'mapping_champ' => 'ajout_groupe_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'ajout_groupe_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Droit d\'ajout', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'html_editable_radio' => 'ok', //rajoute dans la liste un button on|off <td> du liste.tpl ok|'
            'html_value_bouton_on_radio' => '1', // valeur a enregistrer quand on clique sur le bouton on
            'html_value_bouton_off_radio' => '0', // valeur a enregistrer quand on clique sur le bouton OFF
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem'=>$aTmpList,
            'bdebug' => ""

        ));

        $aTmpList = array();
        $aTmpList["1"] = "OUI";
        $aTmpList["0"] ="NON";

        $this->ajout_champ(array(
            'type_champ' => 'radio',
            'mapping_champ' => 'modif_groupe_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'modif_groupe_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Droit de modification', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'html_editable_radio' => 'ok', //rajoute dans la liste un button on|off <td> du liste.tpl ok|'
            'html_value_bouton_on_radio' => '1', // valeur a enregistrer quand on clique sur le bouton on
            'html_value_bouton_off_radio' => '0', // valeur a enregistrer quand on clique sur le bouton OFF
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem'=>$aTmpList,
            'bdebug' => ""

        ));


        $aTmpList = array();
        $aTmpList["1"] = "OUI";
        $aTmpList["0"] ="NON";

        $this->ajout_champ(array(
            'type_champ' => 'radio',
            'mapping_champ' => 'suppr_groupe_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'suppr_groupe_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Droit de suppression', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'html_editable_radio' => 'ok', //rajoute dans la liste un button on|off <td> du liste.tpl ok|'
            'html_value_bouton_on_radio' => '1', // valeur a enregistrer quand on clique sur le bouton on
            'html_value_bouton_off_radio' => '0', // valeur a enregistrer quand on clique sur le bouton OFF
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem'=>$aTmpList,
            'bdebug' => ""

        ));


        $aTmpList = array();
        $aTmpList["1"] = "OUI";
        $aTmpList["0"] ="NON";

        $this->ajout_champ(array(
            'type_champ' => 'radio',
            'mapping_champ' => 'visu_groupe_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'visu_groupe_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Droit de visualisation', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'html_editable_radio' => 'ok', //rajoute dans la liste un button on|off <td> du liste.tpl ok|'
            'html_value_bouton_on_radio' => '1', // valeur a enregistrer quand on clique sur le bouton on
            'html_value_bouton_off_radio' => '0', // valeur a enregistrer quand on clique sur le bouton OFF
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'lesitem'=>$aTmpList,
            'bdebug' => ""

        ));


        $aTmpList = array();
        $aTmpList["1"] = "OUI";
        $aTmpList["0"] ="NON";

        $this->ajout_champ(array(
        	'type_champ' => 'radio',
        	'mapping_champ' => 'switch_wizard_groupes_routes', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'switch_wizard_groupes_routes', // la valeur de l attribut "name" dans le formulaire
        	'text_label_filtre' => 'Saisir un nom',
        	'text_label' => 'Switch le wizard', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'html_editable_radio' => 'ok', //rajoute dans la liste un button on|off <td> du liste.tpl ok|'
        	'html_value_bouton_on_radio' => '1', // valeur a enregistrer quand on clique sur le bouton on
        	'html_value_bouton_off_radio' => '0', // valeur a enregistrer quand on clique sur le bouton OFF
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        	'lesitem'=>$aTmpList,
        	'bdebug' => ""

        ));



        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        //echo"mod<pre>";print_r($aTabTpl);echo"</pre>";

        return $aTabTpl;
    }

    public function routes_utilisateurs()
    {

        $aVar = array( 'guid_route' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        if( isset($guid_route) && !empty($guid_route) ) {
            $sFiltreListe = ' AND principal.guid_route=\'' . $guid_route . '\'';
        } else {
            $sFiltreListe = '';
        }

        //echo $sFiltreListe;

        $this->sNomTable = $this->sPrefixeDb . 'users_routes';                            //string Nom de la table liée
        $this->sChampId = 'id_user_route';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = 'guid_user_route';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_user_route';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = false;                         //bool
        $this->bAffMod = true;                           //bool Permet la modification
        $this->bAffSupp = true;                          //bool Permet la suppression
        $this->bPagination = false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = $sFiltreListe;                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = 'Ajout de droits par utilisateur';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = 'Les droits ont été accordés correctement à l\'utilisateur sur la route choisie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = 'Modification des droits par groupe';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = 'Les droits ont été modifiés correctement pour l\'utilisateur sur la route choisie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = 'Les droits n\'ont pas été retirés';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = '';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = 'Ajouter des droits à un utilisateur';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = '';                      //string Titre de la section recherche
        $this->sTitreListe = 'Gestion des droits par utilisateur';                          //string Titre de la liste
        $this->sLabelNbrLigne = ' résultat(s)';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = 'Gestion des droits par utilisateur';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'guid_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_user', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Utilisateur',
            'text_label' => 'Utilisateur', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => $this->sPrefixeDb . 'users', // la table liee pour ce champ
            'id_table_item' => 'guid_user', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'nom_user', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => 'supplogique_user',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""

        ));

        $this->ajout_champ(array(
            'type_champ' => 'select',
            'mapping_champ' => 'guid_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_route', // la valeur de l attribut "name" dans le formulaire
            'text_label_filtre' => 'Route',
            'text_label' => 'Route', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => $this->sPrefixeDb . 'routes', // la table liee pour ce champ
            'id_table_item' => 'guid_route', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'route_route', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => 'supplogique_route',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'bdebug' => ""

        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'ajout_user_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'ajout_user_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Droit d\'ajout', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'modif_user_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'modif_user_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Droit de modification', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'suppr_user_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'suppr_user_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Droit de suppression', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'visu_user_route', // le nom du champ dans la table selectionnee
            'nom_variable' => 'visu_user_route', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Droit de visualisation', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $aTmpList = array();
        $aTmpList["1"] = "OUI";
        $aTmpList["0"] ="NON";

        $this->ajout_champ(array(
        	'type_champ' => 'select',
        	'mapping_champ' => 'switch_wizard_user_routes', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'switch_wizard_user_routes', // la valeur de l attribut "name" dans le formulaire
        	'text_label_filtre' => 'Saisir un nom',
        	'text_label' => 'Switch le wizard', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        	'lesitem'=>$aTmpList,
        	'bdebug' => ""

        ));


        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        //echo"<pre>";print_r(class_fli::get_droits());echo"</pre>";

        return $aTabTpl;
    }

    public function phpinfo()
    {
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->chargement();
        ob_start();
        phpinfo();
        $strPhpInfo = ob_get_contents();
        ob_clean();
        $strPhpInfo = preg_replace('%^.*<body>(.*)</body>.*$%ms', '$1', $strPhpInfo);
        $aTabTpl[] =  '<div style="text-align: center;"><div style="text-align: initial;display: inline-block;width: 60%;">' . $strPhpInfo . '</div></div>';
        return $aTabTpl;
    }

    public function fli_password()
    {
        $this->sNomTable = $this->sPrefixeDb . 'users';                            //string Nom de la table liée
        $this->sChampId = 'id_user';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = 'guid_user';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_user';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = false;                         //bool
        $this->bAffMod = true;                           //bool Permet la modification
        $this->bAffSupp = false;                          //bool Permet la suppression
        $this->bPagination = false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = '';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = '';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = '';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = 'Modification du mot de passe';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = '';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = '';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = '';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = '';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = '';                      //string Titre de la section recherche
        $this->sTitreListe = '';                          //string Titre de la liste
        $this->sLabelNbrLigne = '';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'externe';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = 'fli_admin-ctrl_fli_admin-utilisateurs';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'externe';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = 'fli_admin-ctrl_fli_admin-utilisateurs';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = 'Gestion du mot de passe';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = false;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */

        $this->ajout_champ(array(
            'type_champ' => 'password',
            'mapping_champ' => 'password_user', // le nom du champ dans la table selectionnee
            'nom_variable' => 'password_user', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Mot de passe', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => '', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => '', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => '', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => array( 'size' => 50 ), // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'double_password' => 'ok',
            'length_password' => 5
        ));

        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;
    }

    public function install_module()
    {
        if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
            if( !empty($_FILES) ) {
                $zip = new ZipArchive;
                if( $zip->open($_FILES['module']['tmp_name']) === TRUE ) {

                    try {
                        //$zip->extractTo('modules/');
                    } catch( Exception $e ) {
                        echo '<br/><div style="text-align: center;"><h1 style="font-size: 150px;color: rgb(162, 39, 38);">&#10008;</h1>
                                <h3>Vous ne disposez pas des droits nécessaires pour extraire l\'archive dans le dossier modules</h3>
                                </div>';
                    }
                    $zip->close();
                } else {
                    echo '<br/><div style="text-align: center;"><h1 style="font-size: 150px;color: rgb(162, 39, 38);">&#10008;</h1>
                            <h3>Echec de l\'ouverture de l\'archive</h3>
                            </div>';
                }
            }
        }
        //header('Location: fli_admin-modules');
    }

    public function fli_documentation(){

        $aVar = array( 'login', 'route', 'email_annonce', 'typecate','pseudo' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $objImport = new class_import();
        $objImport->import_modele('fli_admin','mod_fli_admin');
        $objModCon = new mod_fli_admin();

        $aTableauInfoRoute =$objModCon->get_info_menu($route);
        
        //echo"<pre>";print_r($aTableauInfoRoute);echo"</pre>";
        
        
        //BDD
        $this->sNomTable='pc_users';                            //string Nom de la table liée
        $this->sChampId='id_user';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='guid_user';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_user';                    //string Nom du champ supplogique dans la table liée
        //Booléen
        $this->bTraiteConnexion=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bKeyMultiple=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffDup=false;                            //bool bAffDup Permet de dupliquer une ligne
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->bAffFiche=false;                         //bool Permet d'afficher la liste (si aucun des autres droits vaut true)
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->bAffMod=true;                           //bool Permet la modification
        $this->bAffRecapLigne=false;                     //bool  bAffRecapLigne Permet d'affiche dans unenouvelle fenetre le recap de la ligne
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' de la liste
        $this->bPagination=true;                       //bool Permet l'affichage de la pagination
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->bUseDelete=false;                       //bool Permet l'affichage de la pagination
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire)
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        //Entier
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        //String
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND')
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->sTitreCreationForm='Enregistrement d\'un';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='Enregistement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sMessageDuplicationSucces='Duplication de la ligne reussie';                           // sMessageDuplicationSucces Message affiché lorsquela duplication de la ligne s'est déroulé avec succès
        $this->sMessageDuplicationError='Erreur pendant la duplication de la ligne';                          //string sMessageDuplicationError Message affiché lorsque la duplication de la ligne s'est déroulé avec uen erreur
        $this->sTitreModificationForm='Modification d\'un thème';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='Modification reussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->sMessageSupprElem='Suppression réussie';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='Problème survenue pendant l\'enregistrement';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='Aj';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='Rechercher un';                      //string Titre de la section recherche
        $this->sTitreListe='';                          //string Titre de la liste
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->sLabelNbrLigne='nombre(s) sur votre sélection';                       //string Texte à coté du nombre de lignes
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->sUrlRetourConnexion='';                  //string Module-controleur-fonction spécifié pour le module de connexion à utiliser
        $this->sDirRetour='';                           //string Module-controleur-fonction spécifié pour le bouton 'Retour'
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sDirpagination='';                       //string Le module-controleur-fonction pour la pagination quand l'url est spécifique
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sListeTpl='documentation.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->sListeFitreNopDuplicate='';              //string sListeFitreNopDuplicate renvoi la liste des champs qu'on veut exclure de la duplication séparé par un point virgule
        //Array
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->aSelectSqlSuppl=array();                 //array Tableau Permettant de rajouter des champs dans le SELECT de la liste
        //Wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        //Peu utilisés
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        //inforamtion sur le menu automatique
        $this->sMenuIntitule='Accueil';
        $this->sMenuVisibile=true;
        $this->sMenuTarge=false;
        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module
        
        /* --------- $this->Ajout_champ() ici --------- */

        if(!empty($aTableauInfoRoute)) {
            $this->objSmarty->assign('sTitreForm', "Documentation sur la fonctionnalité<br>" . $aTableauInfoRoute[0]['intitule_menu_route']);
            $this->objSmarty->assign('route', $route);
            $this->objSmarty->assign('description', $aTableauInfoRoute[0]['description_route']);
        }else{
            $this->objSmarty->assign('sTitreForm', "Documentation sur la fonctionnalité<br>" );
            $this->objSmarty->assign('route', $route);
            $this->objSmarty->assign('description', "");
        }
        /* --------- fin $this->Ajout_champ() --------- */
        
        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli
        
        return $aTabTpl;
    }

}