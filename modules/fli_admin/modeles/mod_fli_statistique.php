<?php

/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 22/09/2017
 * Time: 11:21
 */
class mod_fli_statistique extends mod_form_list
{
    /**
     * methode  renvoi le dernier jour du mois
     * @author Danon Gnakouri
     * @since 3.2
     * @return la connexion
     */
    function Getdernierjourmois ($mois,$annee) {

        $variable =$mois."-".$annee;
        list ($month, $year) = explode ('-', $variable);
        $year = ((int)$month === 12)?$year+1:$year;
        $month = ((int)$month + 1 === 13)?1:$month+1;
        $lastDay = mktime (0, 0, 0, $month, 0,  $year);
        return strftime ('%d', $lastDay);
    }

    /**
     * methode renvoi nombre de jour entre deux date
     * @author Danon Gnakouri
     * @param login login de connexion
     * @param motdepass mot de pass
     *@param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */
    function nombrejourdate($datedebut,$datefin){
           $datetime1 = new DateTime($this->renvoi_date($datedebut,"eng"));
        $datetime2 = new DateTime($this->renvoi_date($datefin,"eng"));
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%a')+1;

    }

    public function renvoi_tableau_couleur(){
        $aTableauRetour=array();
        $aTableauRetour[]="#000000";
        $aTableauRetour[]="#7FFF00";
        $aTableauRetour[]="#008B8B";
        $aTableauRetour[]="#FF8C00";
        $aTableauRetour[]="#00CED1";
        $aTableauRetour[]="#B22222";
        $aTableauRetour[]="#DAA520";
        $aTableauRetour[]="#DAA520";
        $aTableauRetour[]="#4B0082";
        $aTableauRetour[]="#ADD8E6";
        $aTableauRetour[]="#FFA07A";
        $aTableauRetour[]="#9370D8";
        $aTableauRetour[]="#6B8E23";
        $aTableauRetour[]="#D87093";
        $aTableauRetour[]="#800080";
        $aTableauRetour[]="#2E8B57";
        $aTableauRetour[]="#0000FF";
        $aTableauRetour[]="#FF7F50";
        $aTableauRetour[]="#A9A9A9";
        $aTableauRetour[]="#8B0000";
        $aTableauRetour[]="#FFFF00";
        $aTableauRetour[]="#6A5ACD";
        $aTableauRetour[]="#C71585";
        $aTableauRetour[]="#8FBC8F";
        $aTableauRetour[]="#BDB76B";
        $aTableauRetour[]="#006400";
        $aTableauRetour[]="#E9967A";
        $aTableauRetour[]="#00BFFF";
        $aTableauRetour[]="#FF00FF";
        $aTableauRetour[]="#ADFF2F";
        $aTableauRetour[]="#E6E6FA";
        $aTableauRetour[]="#FAFAD2";
        $aTableauRetour[]="#800000";
        $aTableauRetour[]="#DA70D6";
        $aTableauRetour[]="#FFD700";
        $aTableauRetour[]="#CD5C5C";
        $aTableauRetour[]="#008080";
        $aTableauRetour[]="#D02090";
        $aTableauRetour[]="#F5DEB3";

        return $aTableauRetour;
    }
    /**
     * fonction tableau moi entre deux periode
     * @author Danon Gnakouri
     * @param email
     * @param motdepass mot de pass
     * @param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */
    function renvoi_mois_period($datedebut,$datefin){
        $aTableaumois=array("01"=>"JANVIER","02"=>"FEVRIER","03"=>"MARS","04"=>"AVRIL","05"=>"MAI","06"=>"JUIN","07"=>"JUILLET","08"=>"AOUT","09"=>"SEPTEMBRE","10"=>"OCTOBRE","11"=>"NOVEMBRE","12"=>"DECEMBRE");
        $aTableauRetour =array();
        $atmpdecouep =explode("-",$datedebut);
        $denierjour =$this->Getdernierjourmois($atmpdecouep[1],$atmpdecouep[0]);
        $aTableauRetour[] = array($datedebut,$atmpdecouep[0]."-".$atmpdecouep[1]."-".$denierjour,$aTableaumois[$atmpdecouep[1]]);

        $imoidebut = $atmpdecouep[1];
        $annedebut = $atmpdecouep[0];
        //nombre de jour entre le deux date
        $inbjour=$this->nombrejourdate($this->renvoi_date($datedebut,"fr"),$this->renvoi_date($datefin,"fr"));

        $nbrmois = ceil($inbjour/30);

        //echo $nbrmois."<br>";
        $atmpdecouep2 =explode("-",$datefin);
        $icount = $nbrmois-1;
        if($nbrmois>2){
            for ($i=1; $i<$icount; $i++) {

                $adatedebutemp = date('Y-m-d',mktime(0,0,0,$imoidebut+$i,1,$annedebut));
                $atmplemois =    date('m',mktime(0,0,0,$imoidebut+$i,1,$annedebut));
                $atmpleannee =    date('Y',mktime(0,0,0,$imoidebut+$i,1,$annedebut));
                $tmpdernierjour = $this->Getdernierjourmois($atmplemois,$atmpleannee);

                if($atmplemois!=$atmpdecouep2[1])
                    $aTableauRetour[] = array($adatedebutemp,$atmpleannee."-".$atmplemois."-".$tmpdernierjour,$aTableaumois[$atmplemois]);
            }
        }

        //affiche la fin

        $denierjour =$this->Getdernierjourmois($atmpdecouep[1],$atmpdecouep[0]);
        $aTableauRetour[] = array($atmpdecouep2[0]."-".$atmpdecouep2[1]."-01",$datefin,$aTableaumois[$atmpdecouep2[1]]);

        //echo"<pre>";print_r($aTableauRetour);echo"</pre>";

        return $aTableauRetour;

    }

    function renvoi_liste_table($base="",$bdebug=false){

        $aTableauRetour="";
        $sRequete_liste_table="show tables";
        $aTableauListtable = $this->renvoi_info_requete($sRequete_liste_table);
        

        if($bdebug){
            echo"<pre>";print_r($aTableauListtable);echo"</pre>";
        }
        if(!empty($aTableauListtable)){
            foreach($aTableauListtable as $valeur){
                $aTableauRetour[$valeur[$base]]=$valeur[$base];
            }
        }
        if($bdebug) {
            echo "<pre>";
            print_r($aTableauRetour);
            echo "</pre>";
        }
        return $aTableauRetour;
    }

    public function renvoi_champ_table($table,$filtre,$sVersion="",$bdebug=false){

        $aTableauRetour =array();


        $sRequete_jour="SHOW COLUMNS FROM ".$table;

        if($bdebug) {
            echo $sRequete_jour . "<br><br>";

        }
        $aTableauListe = $this->renvoi_info_requete($sRequete_jour);

        if($bdebug) {
            echo"<pre>";print_r($aTableauListe);echo"</pre>";
        }
        if(!empty($aTableauListe)){
            foreach($aTableauListe as $valeur){
                if($sVersion=="ajax"){
                    $aTableauRetour[]=array("id"=>$valeur['Field'],"libelle"=>$valeur['Field']);
                }else{
                    $aTableauRetour[$valeur['Field']] = $valeur['Field'];
                }
            }
        }
        return $aTableauRetour;

    }

    /**
     * @param $idstat
     * @param bool $bdebug
     * Renvoi les information sur les statistiques
     * @return array
     */
    public function renvoi_info_stat($idstat, $bdebug=false){

        $sRequete_info_stat="SELECT * FROM  generation_statistique WHERE id_statistique='".$idstat."'";

        if($bdebug)
            echo $sRequete_info_stat;

        $aTableauRetour = $this->renvoi_info_requete($sRequete_info_stat);
        if(!empty($aTableauRetour))
            return $aTableauRetour[0];
        else
            return array();

    }

    public function calcul_stat_simple($aTableauInfostat,$datedebut,$datefin){





            $sSuiteRequete="";
            $btoutitem=true;
            $aTableauItem=array();
            $sSupplogiqueExist="";
            $sFiltredateExist="";

            if(trim($aTableauInfostat['supplogiqueprincipal_statistique'])!="")
                $sSupplogiqueExist=" and ".$aTableauInfostat['tableprincipal_statistique'].".".$aTableauInfostat['supplogiqueprincipal_statistique']."='N'";

           if(trim($aTableauInfostat['champdateintervalle_statistique'])!="")
             $sFiltredateExist=" and ".$aTableauInfostat['tableprincipal_statistique'].".".$aTableauInfostat['champdateintervalle_statistique']." between '$datedebut' and '$datefin' ";

        if(trim($aTableauInfostat['flitresupplementaire_statistique'])!=""){
            $sSuiteRequete=str_replace("[","'",$aTableauInfostat['flitresupplementaire_statistique']);
            $sSuiteRequete=str_replace("]","'",$sSuiteRequete);
        }
        $sChaine="";
        if(trim($aTableauInfostat['tabexcluid_statistique'])!=""){
            $aTableaudecoupe =explode(";",$aTableauInfostat['tabexcluid_statistique']);

            if(!empty($aTableaudecoupe)){
                $sChaine.=" and ".$aTableauInfostat['tablevaleur_statistique'].".".$aTableauInfostat['idvaleur_statistique']." not in ('0'";
                foreach($aTableaudecoupe as $lesvaleur){
                    $sChaine.=",'".$lesvaleur."'";
                }
                $sChaine.=")";
            }

        }

            $sRequete_stat=" select count(*) as nombre, ".$aTableauInfostat['tablevaleur_statistique'].".".$aTableauInfostat['libelletableitem_statistique'].",
	 	".$aTableauInfostat['tablevaleur_statistique'].".".$aTableauInfostat['idvaleur_statistique']." from
	 	 ".$aTableauInfostat['tableprincipal_statistique']." 
	 	 inner join ".$aTableauInfostat['tablevaleur_statistique']." on ".$aTableauInfostat['tablevaleur_statistique'].".".$aTableauInfostat['idvaleur_statistique']
                ."=".$aTableauInfostat['tableprincipal_statistique'].".".$aTableauInfostat['identifiantcount_statistique']."
	 	 where 1 ".$sSupplogiqueExist."
	 	   ".$sFiltredateExist." ".$sSuiteRequete." ".$sChaine;



            $sRequete_stat.= "";




            $sRequete_stat.=" ".$aTableauInfostat['filtre_specifique_statisitque']." group by ".$aTableauInfostat['tableprincipal_statistique'].".".$aTableauInfostat['idvaleur_statistique']
                ."  order by ".$aTableauInfostat['tablevaleur_statistique'].".".$aTableauInfostat['libelletableitem_statistique'];

            if($aTableauInfostat['affrequete_statisque']=="1")
                echo $sRequete_stat."<br>";

            $Total =0;

            $aTableauResult = $this->renvoi_info_requete($sRequete_stat);
            if(!empty($aTableauResult)){
                foreach($aTableauResult as $valeur){
                    $Total=$Total+$valeur['nombre'];


                    $sLibelle = str_replace("'"," ",$valeur[$aTableauInfostat['libelletableitem_statistique']]);
                    $sLibelle = str_replace("«"," ",$sLibelle);
                    $sLibelle = str_replace("&"," ",$sLibelle);
                    $aTableauRetour[]=array($sLibelle,$valeur['nombre'],$valeur[$aTableauInfostat['idvaleur_statistique']]);
                    $aTableauItem[]=$valeur[$aTableauInfostat['idvaleur_statistique']];
                }

            }


            //***********************récupération des nm de la base de donnée
            $sRequete_nom_item="select ".$aTableauInfostat['idvaleur_statistique'].",".$aTableauInfostat['libelletableitem_statistique'].
                " from ".$aTableauInfostat['tablevaleur_statistique']." where ".$aTableauInfostat['supplogiquevaleur_statistique']."='N' 
                ".$sChaine." order by ".$aTableauInfostat['libelletableitem_statistique'];

            $aTableauNom = $this->renvoi_info_requete($sRequete_nom_item);
            $aTableauCouleur = $this->renvoi_tableau_couleur();
            $itotalcouleur = count($aTableauCouleur);
            $icouleur=0;
            if($btoutitem){
                if(!empty($aTableauNom)){
                    foreach ($aTableauNom as $valeuritem) {
                        $sLibelle = str_replace("'"," ",$valeuritem[$aTableauInfostat['libelletableitem_statistique']]);
                        $sLibelle = str_replace("«"," ",$sLibelle);
                        $sLibelle = str_replace("»"," ",$sLibelle);
                        $sLibelle = str_replace("&"," ",$sLibelle);
                        if(!in_array($valeuritem[$aTableauInfostat['idvaleur_statistique']],$aTableauItem)){
                            $aTableauRetour[]=array($sLibelle,"0",$valeuritem[$aTableauInfostat['idvaleur_statistique']],$aTableauCouleur[$icouleur]);
                        }

                    }
                }
            }

            $u=0;
            foreach($aTableauRetour  as $valeur){

                if($icouleur>=$itotalcouleur){
                    $icouleur=0;
                }else{
                    $icouleur++;
                }
                $aTableauRetour[$u][3]=$aTableauCouleur[$icouleur];
                $aTableauRetour[$u][4]="ok";
                $u++;

            }

            $aTableauRetour[]=array("TOTAL",$Total,"0","","nop");

        if($aTableauInfostat['affrequete_statisque']=="1"){
            echo"<pre>";print_r($aTableauRetour);echo"</pre>";
        }

        return $aTableauRetour;


    }


}