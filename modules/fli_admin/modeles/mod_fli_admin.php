<?php

/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 26/02/2016
 * Time: 12:03
 */
class mod_fli_admin extends mod_form_list
{
    public function get_sel_user($guid_user)
    {
        $aParam[] = $guid_user;

        $aResultRequete = $this->renvoi_info_requete("SELECT sel_user FROM " . $this->sPrefixeDb . "users WHERE supplogique_user='N' AND guid_user=?", $aParam);

        return $aResultRequete;
    }

    public function set_sel_user($sSel, $guid_user)
    {
        $aTab['sel_user'] = $sSel;
        $aTab['guid_user'] = $guid_user;

        $sql = "UPDATE " . $this->sPrefixeDb . "users SET sel_user=:sel_user WHERE guid_user=:guid_user";

        $this->insert_update_requete($sql, $aTab);
    }

    public function set_password_user($password, $guid)
    {

        $aTab['password_user'] = $password;
        $aTab['guid_user'] = $guid;

        $sql = "UPDATE " . $this->sPrefixeDb . "users SET password_user=:password_user WHERE guid_user=:guid_user";

        $this->insert_update_requete($sql, $aTab);
    }

    public function get_user($guid_user)
    {
        $aParam[] = $guid_user;

        $aResultRequete = $this->renvoi_info_requete("SELECT login_user FROM " . $this->sPrefixeDb . "users WHERE supplogique_user='N' AND guid_user=?", $aParam);

        return $aResultRequete;
    }

    public function get_info_menu($sroute)
    {
        $aParam[] = $sroute;
        $sRequeteInfoRoute = "SELECT intitule_menu_route,description_route FROM " . $this->sPrefixeDb . "routes WHERE  route_route='" . $sroute . "'";
        //echo $sRequeteInfoRoute."<br>";
        $aResultRequete = $this->renvoi_info_requete($sRequeteInfoRoute);
        return $aResultRequete;
    }


    public function set_doc_menu($sroute, $sdecription, $guid_user)
    {
        $sRequeteInfoRoute = "SELECT intitule_menu_route,description_route FROM " . $this->sPrefixeDb . "routes WHERE  route_route='" . $sroute . "'";
        $aResultRequete = $this->renvoi_info_requete($sRequeteInfoRoute);
        /*
         * Enregistrement de l'historique
         */
        if( !empty($aResultRequete) ) {
            $sRequete_histo = "Insert " . $this->sPrefixeDb . "historiquedoc SET description_historiquedoc='" . addslashes($aResultRequete[0]['description_route']) . "',
            guid_historiquedoc='" . class_helper::guid() . "',guid_user='" . class_fli::get_guid_user() . "'";
            $this->execute_requete($sRequete_histo);
        }


    }


    /**
     * Affectation d'un menu à un groupe
     * @param $tabmenu
     * @param $guidgroupe
     * @return array
     */
    public function affectation_menu_groupe($tabmenu,$guidgroupe,$sprefixe){
        $aTableauRetour=array();
        $aTableauRetour['message']="";

        //echo"test<pre>";print_r($tabmenu);echo"</pre>";

        if(!empty($tabmenu)){
            foreach($tabmenu as $valeur){

                //récupération des information sur le menu
                $sRequete_info_menu=" SELECT id_route,
                guid_route,
                intitule_menu_route
                  from  ".$sprefixe."routes where id_route='".$valeur."'";
                $aTableauInfoMneu= $this->renvoi_info_requete($sRequete_info_menu);

                $sRequete_affectation ="Insert  ".$sprefixe."groupes_routes set guid_groupe='".$guidgroupe."',
                guid_route='".$aTableauInfoMneu[0]['guid_route']."',
                ajout_groupe_route='1',
                modif_groupe_route='1',
                suppr_groupe_route='1',
                visu_groupe_route='1',
                switch_wizard_groupes_routes='0'
                ON DUPLICATE KEY UPDATE  supplogique_groupe_route='N',
                ajout_groupe_route='1',
                modif_groupe_route='1',
                suppr_groupe_route='1',
                visu_groupe_route='1',
                switch_wizard_groupes_routes='0'";
                //echo $sRequete_affectation."<br>";
                $bRresult= $this->execute_requete($sRequete_affectation);
                if($bRresult){
                    $aTableauRetour['message'].="Affectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." réussie<br>";
                }else{
                    $aTableauRetour['message'].=" Problème survenu pendant l'affectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." <br>";
                }
            }
        }

        return $aTableauRetour;

    }


    /**
     * Désaffectation d'un menu à un groupe
     * @param $tabmenu
     * @param $guidgroupe
     * @return array
     */
    public function desaffectation_menu_groupe($tabmenu, $guidgroupe,$sprefixe){
        $aTableauRetour=array();
        $aTableauRetour['message']="";

        //echo"test<pre>";print_r($tabmenu);echo"</pre>";

        if(!empty($tabmenu)){
            foreach($tabmenu as $valeur){

                //récupération des information sur le menu
                $sRequete_info_menu=" SELECT id_route,
                guid_route,
                intitule_menu_route
                  from  ".$sprefixe."routes where id_route='".$valeur."'";

                //echo $sRequete_info_menu."<br>";

                $aTableauInfoMneu= $this->renvoi_info_requete($sRequete_info_menu);

                $sRequete_affectation ="update  ".$sprefixe."groupes_routes 
                set supplogique_groupe_route='Y' where 
                guid_groupe='".$guidgroupe."' and guid_route='".$aTableauInfoMneu[0]['guid_route']."'";
                //echo $sRequete_affectation."<br>";

                $bRresult= $this->execute_requete($sRequete_affectation);

                if($bRresult){
                    $aTableauRetour['message'].=" Désaffectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." réussie<br>";
                }else{
                    $aTableauRetour['message'].=" Problème survenu pendant la désaffectation du menu ".$aTableauInfoMneu[0]['intitule_menu_route']." <br>";
                }
            }
        }

        return $aTableauRetour;

    }

    public function renvoi_menu_liste($identifiantpere,$prefixe=""){

        $sRequete_menu="select id_route,
        route_route,
        intitule_menu_route,
        pere_menu_route,
        afficher_menu_route,
        ordre_menu_route,
        target_menu_route,
        lien_menu_route,
        guid_route,
        supplogique_route
        from ".$prefixe."routes 
        where supplogique_route='N'
       and pere_menu_route='".$identifiantpere."' order by intitule_menu_route ";
        $aTabMenuFiltre=array();
        $k=0;



        //echo $sRequete_menu."<br>";

        $aTableauMenu = $this->renvoi_info_requete($sRequete_menu);

        if(!empty($aTableauMenu)){
            foreach($aTableauMenu as $valeur){
                $aTabMenuFiltre[$k]['id_route']=$valeur['id_route'];
                $aTabMenuFiltre[$k]['intitule_menu_route']=$valeur['intitule_menu_route'];
                $aTabMenuFiltre[$k]['route_route']=$valeur['route_route'];

                $aTtmp = $this->renvoi_menu_liste($valeur['route_route'],$prefixe);

                $aTabMenuFiltre[$k]['fils']=$aTtmp;
                if(empty($aTtmp)){
                    $aTabMenuFiltre[$k]['est_parent'] = false;
                }else {
                    $aTabMenuFiltre[$k]['est_parent'] = true;
                }

                $k++;
            }
        }



        return  $aTabMenuFiltre;

    }


    public function renvoi_menu_liste_groupe($identifiantpere,$guidgroupe,$prefixe=""){
        $sRequete_menu="select  id_route,
        route_route,
        intitule_menu_route,
        pere_menu_route,
        afficher_menu_route,
        ordre_menu_route,
        target_menu_route,
        lien_menu_route,
        ".$prefixe."groupes_routes.guid_route,
        supplogique_route,
        ajout_groupe_route,
        modif_groupe_route,
        suppr_groupe_route,
        switch_wizard_groupes_routes,
        visu_groupe_route,
        guid_groupe_route
        from ".$prefixe."routes 
        INNER JOIN  ".$prefixe."groupes_routes on ".$prefixe."groupes_routes.guid_route = ".$prefixe."routes .guid_route
        and supplogique_groupe_route='N'
        where supplogique_route='N' 
        and pere_menu_route='".$identifiantpere."' and ".$prefixe."groupes_routes.guid_groupe='".$guidgroupe."' 
        group by route_route
        order by intitule_menu_route ";
        $aTabMenuFiltre=array();
        $k=0;

        //echo $sRequete_menu."<br>";

        $aTableauMenu = $this->renvoi_info_requete($sRequete_menu);

        //echo $sRequete_menu."<br>";

        if(!empty($aTableauMenu)){
            foreach($aTableauMenu as $valeur){
                $aTabMenuFiltre[$k]['id_route']=$valeur['id_route'];
                $aTabMenuFiltre[$k]['guid_route']=$valeur['guid_route'];
                $aTabMenuFiltre[$k]['guid_groupe_route']=$valeur['guid_groupe_route'];
                $aTabMenuFiltre[$k]['intitule_menu_route']=$valeur['intitule_menu_route'];
                $aTabMenuFiltre[$k]['route_route']=$valeur['route_route'];
                $aTabMenuFiltre[$k]['ajout_groupe_route']=$valeur['ajout_groupe_route'];
                $aTabMenuFiltre[$k]['modif_groupe_route']=$valeur['modif_groupe_route'];
                $aTabMenuFiltre[$k]['suppr_groupe_route']=$valeur['suppr_groupe_route'];
                $aTabMenuFiltre[$k]['visu_groupe_route']=$valeur['visu_groupe_route'];
                $aTabMenuFiltre[$k]['switch_wizard_groupes_routes']=$valeur['switch_wizard_groupes_routes'];

                $aTtmp = $this->renvoi_menu_liste_groupe($valeur['route_route'],$guidgroupe,$prefixe);

                $aTabMenuFiltre[$k]['fils']=$aTtmp;
                if(empty($aTtmp)){
                    $aTabMenuFiltre[$k]['est_parent'] = false;
                }else {
                    $aTabMenuFiltre[$k]['est_parent'] = true;
                }

                $k++;
            }
        }



        return  $aTabMenuFiltre;

    }


    public function get_table()
    {
        $sRequeteTables = "Show Tables";

        $aTabRetourTable = $this->renvoi_info_requete($sRequeteTables);

        return $aTabRetourTable;

    }

    public function generateFichier($guid_page)
    {

        $sCheminBase = "/var/www/html/adv2016/modules/";

        $sRequeteInfoPage = "Select * from advnaj_page where guid_page = '$guid_page' and supplogique_page = 'N' ";
        $aTabInfoPage = $this->renvoi_info_requete($sRequeteInfoPage);

        $sRequeteInfoPageSave = "Select sTitreListe,sTitreForm,bLabelCreationElem,bAffMod,bAffSupp,sMenuIntitule,sMenuVisibile,sMenuTarge,sNomTable,sChampId,sChampGuid,sChampSupplogique,sListeTpl,sFormulaireTpl,sTplSortie,sListeFilsTpl,bAffFiche,setSJointureSup,aTableauSelectSupp
                                 From advnaj_page where  guid_page = '$guid_page' and supplogique_page = 'N' ";


        $aTabInfoPageSave = $this->renvoi_info_requete($sRequeteInfoPageSave);

        ;

        $aTabKeyInfoPage = array_keys($aTabInfoPageSave[0]);

        //echo"<pre>";print_r($aTabKeyInfoPage);echo"</pre>";

        $sRequeteInfoPageSupp = "select valeur_page_config_supp,nom_page_item_config
                                 from advnaj_page_config_supp
                                 INNER JOIN advnaj_page_item_config on advnaj_page_item_config.id_page_item_config = advnaj_page_config_supp.nom_page_config_supp AND supplogique_page_item_config = 'N'
                                 where guid_page = '$guid_page' and supplogique_page_config_supp = 'N' ";

        $aTabInfoPageSupp = $this->renvoi_info_requete($sRequeteInfoPageSupp);

        //echo"<pre>";print_r($aTabInfoPageSupp);echo"</pre>";

        $sRequeteInfoChampSave = "Select nom_type_champ as type_champ ,mapping_champ,nom_variable,text_label,aff_form,aff_liste,aff_recherche,type_recherche,traite_sql,id_page_champ,alias_champ
                            from advnaj_page_champ
                            INNER JOIN advnaj_page_type_champs on advnaj_page_type_champs.id_type_champ = advnaj_page_champ.type_champ and supplogique_type_champ= 'N'
                            where supplogique_page_champ = 'N' and guid_page = '$guid_page'";

        $aTabInfoInfoChampSave = $this->renvoi_info_requete($sRequeteInfoChampSave);


        $sRequeteMenuDeroulant = "Select href,javascript,key_replace,mapping_champ,attribut,intitule
                                 from advnaj_page_menuderoulant
                                 where guid_page = '$guid_page' and supplogique_page_menuderoulant = 'N'
                                 ";

        $aTabMenuDeroulant = $this->renvoi_info_requete($sRequeteMenuDeroulant);

        //echo"<pre>";print_r($aTabInfoInfoChampSave);echo"</pre>";

        $i=0;

        foreach($aTabInfoPageSupp as $eInfoSupp)
        {
            $aTabInfoPage[0]['infoSupp'][$i] = $eInfoSupp;
            $i++;
        }

        $sListeChampPage = "Select nom_type_champ,mapping_champ,nom_variable,text_label,aff_form,aff_liste,aff_recherche,type_recherche,traite_sql,type_champ,id_page_champ
                            from advnaj_page_champ
                            INNER JOIN advnaj_page_type_champs on advnaj_page_type_champs.id_type_champ = advnaj_page_champ.type_champ and supplogique_type_champ= 'N'
                            where supplogique_page_champ = 'N' and guid_page = '$guid_page'
                            ";

        $aTabListeChampPage = $this->renvoi_info_requete($sListeChampPage);

        $j=0;

        foreach($aTabListeChampPage as $eChampPage)
        {
            $sRequeteInfoChampSupp = "Select valeur_champ,nom_page_item_champ_supp
                                      from  lien_typechamp_itemchampsupp 
                                      INNER JOIN advnaj_page_item_champ_supp on advnaj_page_item_champ_supp.id_page_item_champ_supp = lien_typechamp_itemchampsupp.id_itempchampsupp AND advnaj_page_item_champ_supp.supplogique_page_item_champ_supp = 'N'
                                      where id_typechamp = '".$eChampPage['type_champ']."' and lien_typechamp_itemchampsupp.id_champ = '".$eChampPage['id_page_champ']."' and supplogique_lien_tcics = 'N' and lien_active = 'Y'
                                      ";

            $aTabInfoChampSupp = $this->renvoi_info_requete($sRequeteInfoChampSupp);

            $k = 0;

            foreach($aTabInfoChampSupp as $eChampSupp)
            {
                //echo"<pre>";print_r($eChampSupp);echo"</pre>";
                $aTabListeChampPage[$j]['champSupp'][$k] = $eChampSupp;

                $k++;
            }

            $aTabInfoPage[0]['champ'] = $aTabListeChampPage;

            $j++;



            //echo"<pre>";print_r($aTabInfoChampSupp);echo"</pre>";

        }


        $sCheminFichier =$sCheminBase.$aTabInfoPage[0]['nom_module']."/controleurs/".$aTabInfoPage[0]['nom_controleur'].".php";

        $sUrlControleur = $aTabInfoPage[0]['nom_module']."-".$aTabInfoPage[0]['nom_controleur']."-".$aTabInfoPage[0]['nom_fonction'];

        $sUrlSetRoute = $aTabInfoPage[0]['nom_module']."-".$aTabInfoPage[0]['nom_controleur']."-set_route";

        //echo"<pre>";print_r($sCheminFichier);echo"</pre>";



        $contenu = "<?php
        ";
        $contenu .="class " .$aTabInfoPage[0]['nom_controleur']. " extends class_form_list
        ";
        $contenu .="{
        ";
        $contenu .="public function " .$aTabInfoPage[0]['nom_fonction']."()
            ";
        $contenu .="{
            ";

        $contenu .= '$aVar = array( \'bdebug\');
$aVarVal = class_params::nettoie_get_post($aVar);

foreach( $aVar as $key => $value ) {
$$value = $aVarVal[$key];
}

$this->bDebugRequete = $bdebug;

';

        foreach($aTabKeyInfoPage as $eKey)
        {
            if(($eKey == "setSJointureSup" or $eKey == "aTableauSelectSupp") and !empty($aTabKeyInfoPage[0][$eKey]) )
            {
                if($eKey == "aTableauSelectSupp")
                {
                    $aTabSelectSupp = explode(";",$aTabInfoPageSave[0][$eKey]);

                    $contenu .= '$aTableauSelectSupp=array();
                            
                            ';

                    $i=0;

                    foreach($aTabSelectSupp as $eSelectSupp)
                    {
                        $contenu .= '$aTableauSelectSupp['.$i.']=';
                        $contenu .= '"'.$eSelectSupp.'";'

                        ;
                    }

                    $contenu .= '$this->aSelectSqlSuppl=$aTableauSelectSupp;
                            
                            ';

                }
                else
                {
                    $aTabInfoPageSave[0][$eKey] = str_replace('|',"'",$aTabInfoPageSave[0][$eKey]);

                    $contenu .= '$sJointure="'.$aTabInfoPageSave[0][$eKey].'"'.';
                        $this->setSJointureSup($sJointure);
                        ';
                }

            }

            else{
                $contenu .= '$this->'.$eKey.'= \'' .$aTabInfoPageSave[0][$eKey].'\';
                    ';
            }

        }
        foreach($aTabInfoPage[0]['infoSupp'] as $eInfoSupp)
        {
            $contenu .= '$this->'.$eInfoSupp['nom_page_item_config'].'=\''.$eInfoSupp['valeur_page_config_supp'].'\';
                    ';
        }
        $contenu .= '$this->chargement();';
        $contenu .= '
                
                ';

        foreach($aTabInfoInfoChampSave as $eChamp)
        {

            $aTabKeyChamp = array_keys($eChamp);

            $aTabKey = array_keys($aTabInfoInfoChampSave);

            //echo"<pre>";print_r($aTabKey);echo"</pre>";

            //echo"<pre>";print_r($aTabKeyChamp);echo"</pre>";

            $contenu .= '$this->ajout_champ(array(
                    ';

            $sRequeteInfoChampSuppSave = "Select valeur_champ,nom_page_item_champ_supp
                                      from  lien_typechamp_itemchampsupp 
                                      INNER JOIN advnaj_page_item_champ_supp on advnaj_page_item_champ_supp.id_page_item_champ_supp = lien_typechamp_itemchampsupp.id_itempchampsupp AND advnaj_page_item_champ_supp.supplogique_page_item_champ_supp = 'N'
                                      where lien_typechamp_itemchampsupp.id_champ = '".$eChamp['id_page_champ']."' and supplogique_lien_tcics = 'N' and lien_active = 'Y'";

            $aTabInfoChampSuppSave = $this->renvoi_info_requete($sRequeteInfoChampSuppSave);

            //echo"<pre>";print_r($aTabInfoChampSuppSave);echo"</pre>";

            foreach($aTabKeyChamp as $eKeyChamp)
            {

                if($eKeyChamp != 'id_page_champ')
                {
                    $contenu .= "'$eKeyChamp' => '$eChamp[$eKeyChamp]',
                        ";
                }

            }
            foreach($aTabInfoChampSuppSave as $eChampSupp)
            {
                $nom_champ_supp = $eChampSupp['nom_page_item_champ_supp'];
                $valeur_champ_supp = $eChampSupp['valeur_champ'];

                $contenu .= "'$nom_champ_supp' => '$valeur_champ_supp',
                        ";
            }





            $contenu .= '));
                    
                    ';
        }

        foreach($aTabMenuDeroulant as $eMenuDeroulant)
        {
            $contenu .= '$aTableauMenuDeroulant=Array();
                    ';
            $aTabKeyMenuDeroulant = array_keys($eMenuDeroulant);

            foreach($aTabKeyMenuDeroulant as $eKeyMenuDeroulant)
            {
                $contenu.= '$aTableauMenuDeroulant';
                $contenu.= "['$eKeyMenuDeroulant']=";
                $contenu.= '"';
                $contenu.= "$eMenuDeroulant[$eKeyMenuDeroulant]";
                $contenu.= '";';
                $contenu.= "
                        ";

            }
            $contenu .= '$this->set_menu_deroulant($aTableauMenuDeroulant);';
            $contenu .= '
                    
                    ';
        }



        $contenu .= '$aTabTpl[] = $this->run();
                
                ';

        $contenu .= 'return $aTabTpl;
                
                ';

        $contenu .="
            }";
        $contenu .="
        }";

        $contenu = str_replace("'false'",'false',$contenu);
        $contenu = str_replace("'true'",'true',$contenu);
        $contenu = str_replace("|","\'",$contenu);

        //echo"<pre>";print_r($aTabInfoPage);echo"</pre>";
        if(is_file($sCheminFichier))
        {
            $fichier = fopen($sCheminFichier,'r');
            $content = "";
            while(!feof($fichier))
            {
                $content .= fgets($fichier,1000);
            }
            fclose($fichier);

            if($content != $contenu)
            {
                $content = addslashes($content);

                $sRequeteInserVersioPage = "insert into adv_historique_version_page (chemin_page,text_fichier,guid_page) VALUES ('$sCheminFichier','$content','$guid_page')";

                $this->execute_requete($sRequeteInserVersioPage);

            }



        }




        foreach($aTabInfoPage[0]['champ'] as $eChamp)
        {
            if($eChamp['nom_type_champ'] == 'hidden' and isset($eChamp['champSupp']))
            {
                $sRequeteChampFiltre = "Select valeur_champ from lien_typechamp_itemchampsupp where id_itempchampsupp = 4 and id_champ = ".$eChamp['id_page_champ']." and lien_active = 'Y' and supplogique_lien_tcics = 'N' ";

                $aTabChampFiltre = $this->renvoi_info_requete($sRequeteChampFiltre);

                //echo"<pre>";print_r($aTabChampFiltre);echo"</pre>";

                foreach($aTabChampFiltre as $eChampFiltre)
                {
                    $sUrlSetRoute .= "&".$eChampFiltre['valeur_champ']."=1";
                }


            }



        }



        //echo"<pre>";print_r($sUrlSetRoute);echo"</pre>";

        $fichier = fopen($sCheminFichier,'w+');

        fwrite($fichier,$contenu);

        fclose($fichier);

        header("location:$sUrlSetRoute");

        //echo"<pre>";print_r($aTabListeChampPage);echo"</pre>";




    }

    public function  get_PageByIdChamp($id_champ)
    {
        $sRequetePageByIdChamp = "Select guid_page from advnaj_page_champ where id_page_champ = $id_champ and supplogique_page_champ = 'N'";

        $aTabPageByIdChamp = $this->renvoi_info_requete($sRequetePageByIdChamp);

        return $aTabPageByIdChamp;

    }

    public function updateMapping_Champ($id_champ,$mapping)
    {
        $sRequeteUpdateMapping = "update advnaj_page_champ set mapping_champ = '$mapping' where id_page_champ = $id_champ ";

        $this->execute_requete($sRequeteUpdateMapping);

    }

    public function get_column($table)
    {
        $sRequeteInTable = "SELECT COLUMN_NAME AS libelle FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table'  ";


        $aTabColumns = $this->renvoi_info_requete($sRequeteInTable);

        return $aTabColumns;

    }

    public function get_LienTypeChamps($idTypeChamps,$idchamp)
    {
        $sRequeteLienTypeChamps = "Select id_itempchampsupp 
                                   from lien_typechamp_itemchampsupp 
                                   where id_typechamp = $idTypeChamps and id_champ = 0 ";

        $aTabLienTypeChamps = $this->renvoi_info_requete($sRequeteLienTypeChamps);

        foreach($aTabLienTypeChamps as $eLienTypeChamps)
        {
            $sRequeteAddLien = "insert into lien_typechamp_itemchampsupp 
                                (id_typechamp,id_itempchampsupp,id_champ)
                                VALUES ($idTypeChamps,'".$eLienTypeChamps['id_itempchampsupp']."',$idchamp)
                                on duplicate key update supplogique_lien_tcics = 'N'
                                ";

            $this->execute_requete($sRequeteAddLien);

        }


    }


    public function get_ItemConfig()
    {
        $sRequeteItemConfig = "Select id_page_item_config,nom_page_item_config 
                               from advnaj_page_item_config 
                               where supplogique_page_item_config = 'N'";

        $aTabRequeteItempConfig = $this->renvoi_info_requete($sRequeteItemConfig);

        return $aTabRequeteItempConfig;

    }

    public function updateSelectValeur($aTabParam,$id_champs,$type_champ)
    {

        $sRequeteUpdateAll = "update lien_typechamp_itemchampsupp set lien_active = 'N' where id_champ = $id_champs  ";

        if(!empty($type_champ))
        {
            $sRequeteUpdateAll .= "and id_typechamp = $type_champ";
        }

        $this->execute_requete($sRequeteUpdateAll);

        foreach($aTabParam['iIds'] as $eLigne )
        {

            //echo"<pre>";print_r($aTabParam['tab'][$eLigne]);echo"</pre>";

            $sRequeteUpdate = "update lien_typechamp_itemchampsupp set valeur_champ = '".$aTabParam['tab'][$eLigne]."',lien_active = 'Y' where id_lien = $eLigne ";

            $this->execute_requete($sRequeteUpdate);


        }
    }

    public function add_ConfigSupp($id_page,$aTabParam)
    {
        $i = 0;
        foreach( $aTabParam['selectConfigSupp'] as $eParam ) {
            $aTabValeur[$i]['select'] = $eParam;

            $i++;
        }

        $i = 0;
        foreach( $aTabParam['valueConfigSupp'] as $eParam ) {

            $aTabValeur[$i]['value'] = $eParam;
            $i++;
        }

        foreach($aTabValeur as $eValeur)
        {
            $sRequeteAddConfigSupp = "Insert into advnaj_page_config_supp 
                                      (nom_page_config_supp,valeur_page_config_supp,guid_page) 
                                      VALUES ('".$eValeur['select']."','".$eValeur['value']."','$id_page')
                                      ON duplicate KEY update  valeur_page_config_supp = '".$eValeur['value']."' 
                                      ";

            echo"<pre>";print_r($sRequeteAddConfigSupp);echo"</pre>";

            $this->execute_requete($sRequeteAddConfigSupp);

        }
    }

    public function get_TableByGuidPage($guid)
    {
        $sRequeteTableByGuidPage = "Select sNomTable from advnaj_page where guid_page = '$guid' ";

        $aTabTableByGuidPage = $this->renvoi_info_requete($sRequeteTableByGuidPage);

        $sNomTable = $aTabTableByGuidPage[0]['sNomTable'];

        $aTabColumn = $this->get_column($sNomTable);

        return $aTabColumn;

    }
    /*
     * Renvoi liste des historiques
     */
    public function renvoi_documentation_route($route,$prefixe,$bdebug=false){


        $bdd = $prefixe."historiquedoc";
        $bjointureuser = $prefixe."users";

        $sRequete_histo="SELECT nom_user,prenom_user,date_format(datemodif_historiquedoc,'%d/%m/%Y %H:%i') as datemodif,guid_historiquedoc
        FROM ".$bdd." as doc
        LEFT  JOIN ".$bjointureuser." as userbdd on userbdd.guid_user = doc.guid_user
        WHERE route_route='".$route."'  order by datemodif_historiquedoc DESC";

        if($bdebug){
            echo $sRequete_histo."<br>";
        }

        return $this->renvoi_info_requete($sRequete_histo);
    }

    /*
    * Renvoi info des historiques
    */
    public function renvoi_documentation_historique($route,$prefixe,$bdebug=false){


        $bdd = $prefixe."historiquedoc";
        $bjointureuser = $prefixe."users";

        $sRequete_histo="SELECT nom_user,prenom_user,date_format(datemodif_historiquedoc,'%d/%m/%Y %H:%i') as datemodif,guid_historiquedoc,
        description_historiquedoc
        FROM ".$bdd." as doc
        LEFT  JOIN ".$bjointureuser." as userbdd on userbdd.guid_user = doc.guid_user
        WHERE guid_historiquedoc='".$route."'  order by datemodif_historiquedoc DESC";

        if($bdebug){
            echo $sRequete_histo."<br>";
        }

        return $this->renvoi_info_requete($sRequete_histo);
    }

    /*
     * Enregistrement documentation + plus historique
     *
     */

    public function enregistre_historique($route,$description,$prefixe,$guiduser,$bdebug=false){

        $bdd = $prefixe."historiquedoc";
        $guid = class_helper::guid();

        $sRequete_insert ="Insert ".$bdd." set description_historiquedoc='".addslashes($description)."',route_route='".$route."',
        guid_user='".$guiduser."',guid_historiquedoc='".$guid."'";
        if($bdebug){
            echo $sRequete_insert."<br>";
        }
        $this->execute_requete($sRequete_insert);

        //Mise a jour historioue
        $sRequete_update =" update  ".$bdd." set  guidpere_historiquedoc='".$guid."' where where route_route='".$route."' and guidpere_historiquedoc=''";
        if($bdebug){
            echo $sRequete_update."<br>";
        }
        $this->execute_requete($sRequete_update);

    }

    /*
     * Renvoi documentation actuelle
     */
    public function renvoi_documentation_actuelle($route,$prefixe,$bdebug=false){

        $bdd = $prefixe."historiquedoc";
        $bjointureuser = $prefixe."users";
        $sRequete_histo="SELECT nom_user,prenom_user,date_format(datemodif_historiquedoc,'%d/%m/%Y %H:%i') as datemodif,description_historiquedoc FROM ".$bdd." as doc
        LEFT  JOIN ".$bjointureuser." as userbdd on userbdd.guid_user = doc.guid_user
        WHERE route_route='".$route."' and  guidpere_historiquedoc='' order by datemodif_historiquedoc DESC";

        if($bdebug){
            echo $sRequete_histo."<br>";
        }

        return $this->renvoi_info_requete($sRequete_histo);
    }

}
