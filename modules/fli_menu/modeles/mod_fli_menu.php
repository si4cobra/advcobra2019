<?php
/**
 * Classe de connexion
 */

class mod_fli_menu extends mod_form_list{

    public function get_menu($guid_user,$sPere_menu,$sGroupe=''){
        $aTabElemMenu = array();

        $sRequete = "SELECT id_route,route_route,intitule_menu_route,pere_menu_route,ordre_menu_route,target_menu_route,lien_menu_route,
                      (route_route IN (SELECT pere_menu_route FROM ".$this->sPrefixeDb."routes)) AS est_parent,icone_routes
                      FROM ".$this->sPrefixeDb."routes
                      LEFT JOIN ".$this->sPrefixeDb."groupes_routes ON ".$this->sPrefixeDb."groupes_routes.guid_route=".$this->sPrefixeDb."routes.guid_route AND ".$this->sPrefixeDb."groupes_routes.supplogique_groupe_route='N'
                      LEFT JOIN ".$this->sPrefixeDb."users_routes ON ".$this->sPrefixeDb."users_routes.guid_route=".$this->sPrefixeDb."routes.guid_route AND ".$this->sPrefixeDb."users_routes.supplogique_user_route='N'
                      WHERE (".$this->sPrefixeDb."users_routes.guid_user='".$guid_user."' ".$sGroupe." AND
                      afficher_menu_route=TRUE AND ".$this->sPrefixeDb."routes.supplogique_route='N' AND
                      (".$this->sPrefixeDb."groupes_routes.ajout_groupe_route=TRUE OR ".$this->sPrefixeDb."groupes_routes.modif_groupe_route=TRUE OR ".$this->sPrefixeDb."groupes_routes.suppr_groupe_route=TRUE OR ".$this->sPrefixeDb."groupes_routes.visu_groupe_route=TRUE
                      OR ".$this->sPrefixeDb."users_routes.ajout_user_route=TRUE OR ".$this->sPrefixeDb."users_routes.modif_user_route=TRUE OR ".$this->sPrefixeDb."users_routes.suppr_user_route=TRUE OR ".$this->sPrefixeDb."users_routes.visu_user_route=TRUE)
                      AND pere_menu_route='".$sPere_menu."'
                      GROUP BY ".$this->sPrefixeDb."routes.id_route
                      ORDER BY ordre_menu_route,intitule_menu_route ASC";
        //echo $sRequete;
        $aTabElemMenu = $this->renvoi_info_requete($sRequete,array());
        //echo"<pre>";print_r($aTabElemMenu);echo"</pre>";

        return $aTabElemMenu;
    }

    /*
     *
     * Function qui recherche un nemu en focntion de l'utilisateur
     */

    public function get_MenuRecherche($recherche, $guid_user)
    {
        $sRequeteMenuRecherche = "Select intitule_menu_route,route_route 
                                  from ".$this->sPrefixeDb."routes 
                                  inner join ".$this->sPrefixeDb."groupes_routes on ".$this->sPrefixeDb."groupes_routes.guid_route = ".$this->sPrefixeDb."routes.guid_route and supplogique_groupe_route = 'N'
                                  inner join ".$this->sPrefixeDb."users_groupes on ".$this->sPrefixeDb."users_groupes.guid_groupe = ".$this->sPrefixeDb."groupes_routes.guid_groupe and supplogique_user_groupe = 'N'
                                  where ".$this->sPrefixeDb."users_groupes.guid_user = '$guid_user' and supplogique_route = 'N' and pere_menu_route != '' and afficher_menu_route = '1' and intitule_menu_route like '%$recherche%'
                                  ";

        //echo"<pre>";print_r($sRequeteMenuRecherche);echo"</pre>";


        $aTabMenuRecherche = $this->renvoi_info_requete($sRequeteMenuRecherche);


        return $aTabMenuRecherche;

    }
}