<?php

/**
 * Created by PhpStorm.
 * User: Yannick
 * Date: 10/12/2015
 * Time: 15:15
 */
class ctrl_fli_menu extends class_form_list
{
    private $guid_user;
    private $sGroupe;
    private $iProfondeur_menu=0;
    /**
     * @return mixed
     * Fonction qui renvoie le menu en fonction des différents modules présents dans le framework et selon le fichier install.php
     */
    public function renvoi_menu()
    {
        $tTplMenu = '';

        $aTabMenu = array();
        $aTabGroupesUser = array();


        //Récupération du guid_user et du/des guid_groupe(s)
       $aTabGroupesUser = explode(';', class_fli::get_aData('guid_groupes'));
        $this->guid_user = class_fli::get_aData('guid_user');
        
       
       //echo"<pre>";print_r($aTabGroupesUser);echo"</pre>";

        //Construction de la requête pour les groupes
        if( empty($this->guid_user) || $this->guid_user == '' ) {
            $this->guid_user = 0;
        }

        $this->sGroupe = '';
        if( count($aTabGroupesUser) > 1 || (count($aTabGroupesUser) == 1 && $aTabGroupesUser[0] != '') ) {
            foreach( $aTabGroupesUser as $aTabGroupesUserParc ) {
                $this->sGroupe .= " OR ".$this->sPrefixeDb."groupes_routes.guid_groupe='" . $aTabGroupesUserParc . "'";
            }
        } else {
            $this->sGroupe = " OR ".$this->sPrefixeDb."groupes_routes.guid_groupe='11111111-1111-1111-1111-111111111111'";
        }
        $this->sGroupe .= ")";

        //Architecture du menu
        $aTabMenu = $this->liste_menu('');

        //echo"<pre>";print_r($aTabMenu);echo"</pre>";;
        $sRepertoire="Flyvide";
        if(isset($_GET['fli_module'])){
            $sRepertoire =$_GET['fli_module'];
        }

        $i=0;
        //on regarde ici dans quelle module je suis via le link
        $bFindModule=false;
        if(!empty($aTabMenu)){
            foreach($aTabMenu as $valeur){

                if($valeur['route_route']==$sRepertoire){
                    $valeur['check']="ok";
                    $bFindModule=true;
                }else{
                    $valeur['check']="nop";
                }
                $aTabMenu[$i]=$valeur;
                $i++;
            }
            $bFindModule=true;
        }

        if(!$bFindModule) {
            $aTabMenu[0]['check'] = "ok";
            $aTabMenu[0]['est_parent'] = "";
            $aTabMenu[0]['route_route'] = "";
            $aTabMenu[0]['intitule_menu_route'] = "";
            $aTabMenu[0]['fils'] = array();
        }
        //echo"<pre>";print_r($aTabMenu);echo"</pre>";

        //Import du tpl du menu
        //$objSmarty = $objImport->import_vue('fli_menu', 'version1');
        
        //echo"<pre>";print_r($aTabMenu);echo"</pre>";

        $this->objSmarty->assign('iProfondeur_menu', $this->iProfondeur_menu);
        $this->objSmarty->assign('aTabMenu', $aTabMenu);

        $this->sListeTpl = 'menu.tpl';
        $this->sFormulaireTpl = 'menu.tpl';

        $this->chargement('fli_menu','version1');

        $tTplMenu = $this->run();

        return $tTplMenu;
    }

    /**
     * @param $sPere_menu
     * @return mixed
     */
    public function liste_menu($sPere_menu){
        $i=0;
        $aTabTmp = $this->objClassGenerique->get_menu($this->guid_user,$sPere_menu, $this->sGroupe);
        if(!empty($aTabTmp)){
            foreach($aTabTmp as $sValeur){
                $aTabTmp[$i]['fils'] = $this->liste_menu($sValeur['route_route']);
                $i++;
            }
            $this->iProfondeur_menu ++;
        }
        return $aTabTmp;
    }

}