<?php
/**
 * Created by PhpStorm.
 * User: Yann
 * Date: 04/09/2018
 * Time: 11:21
 */
ini_set("display_errors", "ON");
require_once $_SERVER["DOCUMENT_ROOT"].'/libs/initialise_framework.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/config/config.inc.php';

$aVar = array( 'recherche','guid_user', 'bdebug' );
$aVarVal = class_params::nettoie_get_post($aVar);

foreach( $aVar as $key => $value ) {
    $$value = $aVarVal[$key];
}


$guid_user =$_COOKIE[class_fli::get_nom_cookie()];


$objImport = new class_import();
$objImport->import_modele('fli_menu', 'mod_fli_menu');
$objMod = new mod_fli_menu();



$aTabListeMenu = $objMod->get_MenuRecherche($recherche,$guid_user);



echo json_encode($aTabListeMenu);