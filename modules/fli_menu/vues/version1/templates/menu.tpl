{if !empty($aTabMenu)}
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="javascript:void(0);">
                    <span class="glyphicon glyphicon-th-list"></span> Menu
                </a>
            </li>

            <li>
               <span style="color: white">Recherche : </span> <div><input style="height: 30px" type="text" name="rechercheMenu" id="rechercheMenu"></div>
            </li>

            {function menu level=0}
                {if $level==0}
                    <li id="menuAdv2016">
                {/if}
                {foreach from=$data item=entry}
                    {if $entry.est_parent}
                        <a data-toggle="collapse" href="#collapseChamp{$entry.id_route}" data-target="#collapseChamp{$entry.id_route}" aria-expanded="false" aria-controls="collapseChamp{$entry.id_route}" style="margin-left: {$level*20}px;">{$entry.intitule_menu_route}</a>
                    {else}
                        <a href="{$entry.route_route}" onclick="$('#wrapper').toggleClass('toggled');" style="margin-left: {$level*20}px;">{$entry.intitule_menu_route}</a>
                    {/if}
                    {if is_array($entry.fils)}
                        {if !empty($entry.fils)}
                            <div class="collapse" id="collapseChamp{$entry.id_route}">
                            {menu data=$entry.fils level=$level+1}
                            </div>
                        {/if}
                    {/if}
                {/foreach}
                {if $level==0}
                    </li>
                {/if}
            {/function}

            {menu data=$aTabMenu}

        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
</div>
{/if}