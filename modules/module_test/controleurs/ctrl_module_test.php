<?php

/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 16/12/2015
 * Time: 17:27
 */
class ctrl_module_test extends class_form_list
{

    public function install()
    {

        $this->raz_menu();
        $this->add_menu('Module de test', '', '', 'Menu test', 0, false, '');
        $this->add_menu('Module de test', 'fli_form_test', 'module_test', 'Formulaire test', 0, true, '');
        $this->add_menu('Module de test', 'fli_form_select', 'module_test', 'Liste select', 0, true, '');
    }

    public function desinstall()
    {

    }

    public function add_traitement_bdd()
    {
        $aTabRetour = array();
        $aTabRetour[] = "fli_form_test";
        $aTabRetour[] = "fli_form_select";
        return $aTabRetour;
    }

    public function pre_form_update_fli_form_test(){
        $aTableauPreAcion=array();
        $aTableauPreAcion['bResult']=true;
        $aTableauPreAcion['sMMessage']="Passe pas update";
        $aTableauPreAcion['Id']=0;
        $aTableauPreAcion['Guid']=0;
        $aTableauPreAcion['sSql']="";
        $aTableauPreAcion['sSql_Where']="";
        $aTableauPreAcion['bResult_Post']=true;
        $aTableauPreAcion['sMessage_Post']="";


        return $aTableauPreAcion;

    }

    public function pre_form_insert_fli_form_test(){
            $aTableauPreAcion=array();
            $aTableauPreAcion['bResult']=false;
            $aTableauPreAcion['sMMessage']="passe pas insert";
            $aTableauPreAcion['Id']=0;
            $aTableauPreAcion['Guid']=0;
            $aTableauPreAcion['Sql']="";
            $aTableauPreAcion['SqlWhere']="";
            $aTableauPreAcion['bResult_Post']=true;
            $aTableauPreAcion['sMessage_Post']="";


            return $aTableauPreAcion;

        }

     public function post_form_update_fli_form_test(){
         $aTableauPreAcion=array();
         $aTableauPreAcion['bResult_Post']=false;
         $aTableauPreAcion['sMessage_Post']= "<br>Je suis en post update";

        return $aTableauPreAcion;
     }



    public function fli_form_test()
    {
        $this->sNomTable = 'test_table';                            //string Nom de la table liée
        $this->sChampId = 'id_test';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = 'guid_test';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_test';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = true;                         //bool
        $this->bAffMod = true;                           //bool Permet la modification
        $this->bAffSupp = true;                          //bool Permet la suppression
        $this->bPagination = true;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = '';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = 'Formulaire de test';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = 'Enregistrement réussie';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = 'Modification du test';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = 'Modification Réussie';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = '';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = '';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = 'Ajouter un test';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = '';                      //string Titre de la section recherche
        $this->sTitreListe = 'Formulaire de test';                          //string Titre de la liste
        $this->sLabelNbrLigne = ' Ligne(s)';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = 'Modification';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = true;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'nom_test', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_test', // la valeur de l attribut "name" dans le formulaire
            'alias_champ' => 'nom_test', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'nom test', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => 'like \'%<champ>%\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'index_champ_sql' => 'KEY' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
            'type_champ' => 'color',
            'mapping_champ' => 'couleur_test', // le nom du champ dans la table selectionnee
            'alias_champ' => 'couleur_test', // la valeur de l attribut "name" dans le formulaire
            'nom_variable' => 'couleur_test', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'couleur test', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'index_champ_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        $this->ajout_champ(array(
            'type_champ' => 'date',
            'mapping_champ' => 'date_test', // le nom du champ dans la table selectionnee
            'alias_champ' => 'ladatetest', // le nom du champ dans la table selectionnee
            'nom_variable' => 'date_test', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'date test', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'date_format' => '%d/%m/%Y', // le format de la date a respecte
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'index_champ_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));

        $this->ajout_champ(array(
        	'type_champ' => 'time',
        	'mapping_champ' => 'time_test', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'time_test', // la valeur de l attribut "name" dans le formulaire
        	'text_label' => 'heure', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification

        ));

        $this->ajout_champ(array(
        	'type_champ' => 'select',
        	'mapping_champ' => 'idselect_test', // le nom du champ dans la table selectionnee
        	'nom_variable' => 'idselect_test', // la valeur de l attribut "name" dans le formulaire
        	'text_label_filtre' => 'Saisir un nom',
        	'text_label' => 'Select', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
        	'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
        	'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
        	'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
        	'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
        	'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
        	'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
        	'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
        	'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
        	'size_champ' => '', // taille max du champ
        	'style' => '', // ajout du style sur le champ
        	'tableau_attribut' => '', // ajout d attributs sur le champ
        	'fonction_javascript' => '', // ajout du javascript sur le champ
        	'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
        	'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        	'table_item' => 'test_select', // la table liee pour ce champ
        	'id_table_item' => 'guid_select', // le champ de la table liee qui sert de cle primaire
        	'affichage_table_item' => 'nom_select', // le champ de la table liee qui sert de label d affichage
        	'supplogique_table_item' => 'supplogique_select',
        	'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
        	'select_autocomplete' => '', //si on fait de l'autocompletion
        	'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
        	'bdebug' => ""

        ));

               $aTmpList = array();
               $aTmpList["Y"] = "OUI";
                $aTmpList["N"] ="NON";

               $this->ajout_champ(array(
                   'type_champ' => 'select',
                   'mapping_champ' => 'idselecttab_test', // le nom du champ dans la table selectionnee
                   'nom_variable' => 'idselecttab_test', // la valeur de l attribut "name" dans le formulaire
                   'text_label_filtre' => 'Saisir un nom',
                   'text_label' => 'Select tab', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
                   'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
                   'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
                   'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
                   'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
                   'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
                   'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
                   'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
                   'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
                   'size_champ' => '', // taille max du champ
                   'style' => '', // ajout du style sur le champ
                   'tableau_attribut' => '', // ajout d attributs sur le champ
                   'fonction_javascript' => '', // ajout du javascript sur le champ
                   'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
                   'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
                   'table_item' => '', // la table liee pour ce champ
                   'id_table_item' => '', // le champ de la table liee qui sert de cle primaire
                   'affichage_table_item' => 'concat(<bdd>.,\' \',<bdd>.)', // le champ de la table liee qui sert de label d affichage
                   'supplogique_table_item' => '',
                   'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
                   'select_autocomplete' => '', //si on fait de l'autocompletion
                   'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
                   'lesitem'=>$aTmpList,
                   'type_champ_sql'=>'Varchar (255)',
                   'bdebug' => ""
               ));


               $aTmpList = array();
               $aTmpList["Y"] = "RADIO";
               $aTmpList["N"] ="NON";

               $this->ajout_champ(array(
                   'type_champ' => 'radio',
                   'mapping_champ' => 'idradio_test', // le nom du champ dans la table selectionnee
                   'nom_variable' => 'idradio_test', // la valeur de l attribut "name" dans le formulaire
                   'text_label_filtre' => 'Saisir un nom',
                   'text_label' => 'radio tab', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
                   'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
                   'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
                   'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
                   'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
                   'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
                   'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
                   'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
                   'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
                   'size_champ' => '', // taille max du champ
                   'style' => '', // ajout du style sur le champ
                   'tableau_attribut' => '', // ajout d attributs sur le champ
                   'fonction_javascript' => '', // ajout du javascript sur le champ
                   'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
                   'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
                   'table_item' => '', // la table liee pour ce champ
                   'id_table_item' => '', // le champ de la table liee qui sert de cle primaire
                   'affichage_table_item' => 'concat(<bdd>.,\' \',<bdd>.)', // le champ de la table liee qui sert de label d affichage
                   'supplogique_table_item' => '',
                   'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
                   'select_autocomplete' => '', //si on fait de l'autocompletion
                   'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
                   'lesitem'=>$aTmpList,
                   'type_champ_sql'=>'Varchar (255)',
                   'bdebug' => ""
               ));
               //echo"<pre>";print_r($this->aListe);echo"</pre>";
               /* --------- fin $this->Ajout_champ() --------- */


        $this->ajout_champ(array(
            'type_champ' => 'file',
            'mapping_champ' => 'cheminfile_test', // le nom du champ dans la table selectionnee
            'nom_variable' => 'cheminfile_test', // la vafileleur de l attribut "name" dans le formulaire
            'text_label' => 'Fichier', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => '', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'prefix_file' => 'test',// faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'file_visu' => "upload/",
            'file_upload' => "upload/", // dossier de telechargement du fichier
            'file_aff_modif_form' => '',
            'file_aff_liste_taille' => '100px',
            'file_aff_modif_form_couleur_fond' => 'black',
            'file_compress' => 'ok',
            'file_taux_compress' => 70,
            'file_compress_min' => 'ok',
            'file_taux_compress_min' => 30
        ));

        $this->ajout_champ(array(
            'type_champ' => 'checkbox',
            'mapping_champ' => 'guid_test', // le nom du champ dans la table selectionnee
            'nom_variable' => 'guid_test', // la valeur de l attribut "name" dans le formulaire
            'alias_champ' => 'check1',
            'text_label_filtre' => 'Saisir un nom',
            'text_label' => 'Liste checkbox', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => 'ok', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'table_item' => 'test_select', // la table liee pour ce champ
            'id_table_item' => 'guid_select', // le champ de la table liee qui sert de cle primaire
            'affichage_table_item' => 'nom_select', // le champ de la table liee qui sert de label d affichage
            'supplogique_table_item' => 'supplogique_select',
            'type_table_join' => 'left join', // le champ de la table liee qui sert de label d affichage
            'select_autocomplete' => '', //si on fait de l'autocompletion
            'tabfiltre_autocomplete' => '', // "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=
            'table_lien' => array('test_lientable'), // la table qui fait le lien entre la table du formulaire et la table liee
            'supplogique_table_lien' => array('supplogique_lientable'),
            'id_table_lien' => array('guid_table'), // le nom du champ dans la table lien qui fait reference a la cle primaire de la table du formulaire
            'id_item_table_lien' => array('guid_select'), // le nom du champ dans la table lien qui fait reference a la cle primaire de la table liee
            'bdebug' => false

        ));

        $this->ajout_champ(array(
        	'type_champ' => 'injection_code_liste',
        	'mapping_champ' => '',
        	'nom_variable' => 'actioninjection',
        	'text_label' => 'Action programme',
        	'aff_liste' => 'ok',
        	'tab_replace_injection_code' => array('<champ>'=>'cheminfile_test'),
        	'injection_code'=>'<a href=\'main_v4.php?dir=&idprog={{$id_programme}}\'><champ></a>'
        ));
       /* */



        $aTabTpl[] = $this->run();
        //Le retour de run() contient le template déjà rempli
        return $aTabTpl;
    }




    public function fli_form_select()
    {
        $this->sNomTable = 'test_select';                            //string Nom de la table liée
        $this->sChampId = 'id_select';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid = 'guid_select';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique = 'supplogique_select';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete = false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche = false;                         //bool
        $this->bAffMod = false;                           //bool Permet la modification
        $this->bAffSupp = false;                          //bool Permet la suppression
        $this->bPagination = false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage = 50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe = null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste = '';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv = false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm = 'Liste champs select';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm = '';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm = '';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm = '';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre = true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect = '';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate = '';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement = array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe = '';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem = '';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm = '';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem = 'Ajouter un chmap select';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche = '';                      //string Titre de la section recherche
        $this->sTitreListe = 'Liste champ de test';                          //string Titre de la liste
        $this->sLabelNbrLigne = ' Ligne(s)';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler = true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl = 'liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm = 'liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne = '';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm = 'liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect = false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne = '';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl = 'formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm = '';                           //string Titre du formulaire
        $this->itemBoutonsForm = array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect = false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe = array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe = '';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe = array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect = false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup = false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl = 'liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils = array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne = '';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi = array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne = '';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy = '';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem = true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour = '';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour = false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche = true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier = '';                         //string Le type de fichier attendu
        $this->bRetourSpecifique = false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl = '';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem = '';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult = true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable = false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne = '';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert = '';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate = '';              //string Script executé après un UPDATE
        $this->sCategorieListe = '';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique = '';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert = '';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate = '';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination = '';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn = false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug = false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion = true;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie = 'principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'nom_select', // le nom du champ dans la table selectionnee
            'nom_variable' => 'nom_select', // la valeur de l attribut "name" dans le formulaire
            'alias_champ' => 'nom_select', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'nom select', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok', // faut il traite le champ dans les divers requetes sql selection, insertion, modification
            'index_champ_sql' => 'KEY' // faut il traite le champ dans les divers requetes sql selection, insertion, modification
        ));


        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();
        //Le retour de run() contient le template déjà rempli
        return $aTabTpl;
    }



//exemple avec fonction d'autorempliassage
    public function renvoi_affiche_liste_voir_planning(){
        $aTableauListe=array();
        $i=0;
        $larequete = $this->renvoi_requete();
        $aValeurBddListe = $aValeurBddListe = $this->objClassGenerique->renvoi_info_requete($larequete);


        $aTableauListe[$i]['ladate']="I am the winner";
        $aTableauListe[$i]['id_planning']="lejourid&ladate=ladate";
        $aTableauListe[$i]['id_planning']=$aValeurBddListe[0]['id_planning']."&ladate=ladate";
        $aTableauListe[$i]['prixkm_planning']=$aValeurBddListe[0]['prixkm_planning'];

        return $aTableauListe;
    }

    public function voir_planning(){
        $this->sNomTable='genpla_planning';                            //string Nom de la table liée
        $this->sChampId='id_planning';                             //string Nom du champ de la clé primaire de la table liée
        $this->sChampGuid='';                           //string Nom du champ GUID de la table liée
        $this->sChampSupplogique='supplogique_planning';                    //string Nom du champ supplogique dans la table liée
        $this->bDebugRequete=false;                     //bool Permet d'afficher les requêtes dans le form ou le liste
        $this->bAffFiche=false;                         //bool
        $this->bAffMod=true;                           //bool Permet la modification
        $this->bAffSupp=false;                          //bool Permet la suppression
        $this->bPagination=false;                       //bool Permet l'affichage de la pagination
        $this->iNombrePage=50;                          //int Nombre de lignes désirées par page
        $this->sRetourListe=null;                       //string Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
        $this->sFiltreliste='';                         //string Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
        $this->bCsv=false;                              //bool Permet d'afficher le bouton d'export en csv
        $this->sTitreCreationForm='';                   //string Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
        $this->sMessageCreationSuccesForm='';           //string Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
        $this->sTitreModificationForm='';               //string Titre de la page lorsque l'on se trouve sur le formulaire en Modification
        $this->sMessageModificationSuccesForm='';       //string Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
        $this->bAffTitre=true;                          //bool Permet d'afficher le titre de la liste
        $this->sDebugRequeteSelect='';                  //string La requête SELECT globale à afficher en debug
        $this->sDebugRequeteInsertUpdate='';            //string La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
        $this->aElement=array();                        //array Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
        $this->sLabelRetourListe='';                    //string Texte du bouton 'Retour' sur la liste
        $this->sMessageSupprElem='';                    //string Message à afficher en cas de succès lors de la suppression
        $this->sMessageErreurForm='';                   //string Message en cas d'erreur lors de l'envoi du formulaire
        $this->sLabelCreationElem='';                   //string Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
        $this->sLabelRecherche='';                      //string Titre de la section recherche
        $this->sTitreListe='Voir le planning';                          //string Titre de la liste
        $this->sLabelNbrLigne='';                       //string Texte à coté du nombre de lignes
        $this->bAffAnnuler=true;                        //bool Permet d'afficher le bouton 'Annuler' dans le formulaire
        $this->sListeTpl='liste.tpl';                   //string Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
        $this->sRetourValidationForm='liste';           //string Permet de spécifier où l'on doit revenir après la validation du formulaire
        $this->sRetourValidationFormExterne='';         //string Permet de spécifier où l'on doit revenir après la validation du formulaire externe
        $this->sRetourAnnulationForm='liste';           //string Permet de définir où l'on doit revenir après le formulaire
        $this->bCheckboxSelect=false;                   //bool Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->sRetourAnnulationFormExterne='';         //string Permet de spécifier l'url de l'annulation
        $this->sFormulaireTpl='formulaire.tpl';         //string Tpl affiché lorsque get['action']==form
        $this->sTitreForm='';                           //string Titre du formulaire
        $this->itemBoutonsForm=array();                 //array Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
        $this->bRadioSelect=false;                      //bool Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
        $this->itemBoutonsListe=array();                //array Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
        $this->sParametreWizardListe='';                //string Les paramètres de la liste du wizard
        $this->aParametreWizardListe=array();           //array Tableau contenant les paramètres du wizard
        $this->bActiveFormSelect=false;                 //bool Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
        $this->bFormPopup=false;                        //bool Permet d'afficher le formulaire dans une modal
        $this->sListeFilsTpl='liste_fils.tpl';          //string Tpl affiché par défaut pour la liste_fils
        $this->aListeFils=array();                      //array Tableau contenant les données de la liste fils
        $this->sRetourSuppressionFormExterne='';        //string Permet de spécifier l'url de retour après la suppression
        $this->aParamsNonSuivi=array();                 //array Tableau de paramètres spécifiés dans le constructeur pour le wizard
        $this->sUrlRetourSuppressionFormExterne='';     //string L'url de retour du formulaire dans le cas d'un retour externe après la suppression
        $this->sFiltrelisteOrderBy='';                  //string Permet de modifier l'ORDER BY original de la requête
        $this->bLabelCreationElem=true;                 //bool Permet d'afficher le bouton d'ajout
        $this->sDirRetour='';                           //string Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
        $this->bBtnRetour=false;                        //bool Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
        $this->bAffListeQuandPasRecherche=true;         //bool Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
        $this->sTypeFichier='';                         //string Le type de fichier attendu
        $this->bRetourSpecifique=false;                 //bool Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
        $this->sRetourElemUrl='';                       //string URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
        $this->sLabelFileRetourElem='';                 //string Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
        $this->bAffNombreResult=true;                   //bool Permet d'afficher le nombre de résultats renvoyés pour la liste
        $this->bLigneCliquable=false;                   //bool Permet de rendre les lignes de la liste cliquables
        $this->sLienLigne='';                           //string Lien sur la ligne si $bLigneCliquable vaut true
        $this->sScriptJavaSCriptInsert='';              //string Script executé après un INSERT
        $this->sScriptJavaSCriptUpdate='';              //string Script executé après un UPDATE
        $this->sCategorieListe='';                      //string La catégorie de la liste lors du wizard
        $this->sUrlRetourSpecifique='';                 //string Permet de spécifier l'url de retour si elle est spécifique
        $this->sSuiteRequeteInsert='';                  //string Permet de rajouter du SQL à la suite de la requête INSERT
        $this->sSuiteRequeteUpdate='';                  //string Permet de rajouter du SQL à la suite de la requête UPDATE
        $this->sDirpagination='';                       //string Le dir pour la pagination quand l'url est spécifique
        $this->bAffPrintBtn=false;                      //bool Afficher le bouton d'impression true ou false
        $this->bAffDebug=false;                         //bool Permet d'afficher la fenêtre de débuggage
        $this->bTraiteConnexion=false;                   //bool Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
        $this->sTplSortie='principal.tpl';              //string Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)

        $this->chargement();                            //Fonction obligatoire afin de prendre les valeurs des variables déclarées dans le module

        /* --------- $this->Ajout_champ() ici --------- */

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'ladate', // le nom du champ dans la table selectionnee
            'nom_variable' => 'ladate', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'La date', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => '' // faut il traite le champ dans les divers requetes sql selection, insertion, modification

        ));

        $this->ajout_champ(array(
            'type_champ' => 'text',
            'mapping_champ' => 'prixkm_planning', // le nom du champ dans la table selectionnee
            'nom_variable' => 'prixkm_planning', // la valeur de l attribut "name" dans le formulaire
            'text_label' => 'Prix kilometre', // l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...
            'ctrl_champ' => 'ok', // faut il controler le champ 'ok'|'wran'|''
            'valeur_variable' => '', // la valeur par defaut dans le formulaire lors de la creation
            'aff_liste' => 'ok', // faut il afficher le champ dans la liste 'ok'|''
            'aff_form' => 'ok', // faut il afficher le champ dans le formulaire 'ok'|''
            'aff_filtre' => 'ok', // faut il afficher le champ comme filtre 'ok'|''
            'aff_recherche' => '', // faut il afficher le champ dans la recherche 'ok'|''
            'type_recherche' => ' = \'<champ>\'', // type de la recherche 'ok'|''
            'aff_fiche' => 'ok', // faut il afficher le champ dans la fiche 'ok'|''
            'size_champ' => '', // taille max du champ
            'style' => '', // ajout du style sur le champ
            'tableau_attribut' => '', // ajout d attributs sur le champ
            'fonction_javascript' => '', // ajout du javascript sur le champ
            'mess_erreur' => 'Champ obligatoire', // message d erreur lorsque le controle n est pas valide
            'traite_sql' => 'ok' // faut il traite le champ dans les divers requetes sql selection, insertion, modification

        ));



        /* --------- fin $this->Ajout_champ() --------- */

        $aTabTpl[] = $this->run();                      //Le retour de run() contient le template déjà rempli

        return $aTabTpl;

    }



}