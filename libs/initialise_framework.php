<?php
/**
 * enregistre le timezone courant ici Paris UTC+1
 */
date_default_timezone_set('Europe/Paris');

/**
 * @author Sébastien Robert
 * @version 1
 * @method error_handler de gestion d'erreurs
 * @param sNumeroErreur numéro d'erreur
 * @param sMessageErreur message d'erreur
 * @param sFichierErreur fichier dans lequel l'erreur est apparue
 * @param sLigneErreur ligne dans le fichier dans lequel l'erreur est apparue
 */
function error_handler($sNumeroErreur, $sMessageErreur, $sFichierErreur, $sLigneErreur)
{
    throw new Exception(
        'Erreur:'. "\n" .
        "\n".'Numero: ' . $sNumeroErreur . "\n" .
        "\n".'Message: ' . $sMessageErreur . "\n" .
        "\n".'Fichier: ' . $sFichierErreur . "\n" .
        "\n".'Ligne: ' . $sLigneErreur . "\n"
    );
}

/**
 * enregistrement de la fonction error_handler en tant que fonction de gestion des erreurs PHP
 */
set_error_handler('error_handler');

/**
 * @author Sébastien Robert
 * @version 1
 * @method auto_load fonction d'autochargement de classes PHP, il n'est plus utile d'inclure des fichiers avec les commandes suivantes (include, include_once, require, require_once), cette fonction le fait dynamiquement
 * @param sClass classe à inclure
 * @see http://fr.php.net/autoload
 */
function auto_load($sClass)
{
    if( $sClass == 'Smarty' ) {
        $sClass = 'Smarty.class';
    }


    $path=$_SERVER["DOCUMENT_ROOT"]."";
    if (PHP_SAPI === 'cli') {
        $path = $_SERVER['OLDPWD'];
    }

    $sLastcaractere = substr($path,-1);

    if($sLastcaractere!="/")
        $path.="/";

    $aRepertoiresDesClasses = array( $path.'modeles', $path.'class', $path.'libs', $path.'modeles',$path.'class', $path.'libs' );

    foreach( $aRepertoiresDesClasses as $sRepertoire ) {
        $sChemin = $sRepertoire . '/' . $sClass . '.php';
        //echo $sChemin . '<br/>';
        if( file_exists($sChemin) ) {
            //echo $sChemin . '<br/>';
            include $sChemin;
            break;
        }
    }
}

/**
 * enregistrement de la fonction autoload en tant que fonction d'autochargement de classes PHP
 */
spl_autoload_register('auto_load');