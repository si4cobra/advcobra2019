<?php
/**
 * Created by PhpStorm.
 * User: Guy
 * Date: 24/05/2017
 * Time: 15:03
 */

ini_set("display_errors","On");
require_once '../libs/initialise_framework.php';
require_once '../config/config.inc.php';
include_once '../class/class_fli.php';
include_once '../modeles/mod_form_list.php';

$class_methods = get_class_methods(new class_fli());

$objPdo = new mod_form_list();

$aVariable = array("bdd", "identifiantlang", "slink","valueid","valuechamp");
for ($i=0;$i<count($aVariable);$i++) {
    if (isset($_GET[$aVariable[$i]])) {
        $$aVariable[$i] = $_GET[$aVariable[$i]];
    } else {
        $$aVariable[$i] = "";
    }
}

$aTableauRetour[0]['success'] = 'pasok';
$aTableauRetour[0]['message'] = 'erreur';
$aTableauRetour=array();

$sReqliste = "Select fr_langage,eng_langage,esp_langage,it_langage,all_langage,principal_langage,base_langage,champbase_langage
FROM ".class_fli::get_prefixe()."langage
WHERE identifiant_langage='".$identifiantlang."' and link_langage='".$slink."'";

//echo $sReqliste."<br>";
$sMessagePrincipal="<b>Traction prinicpal pour ce champ</b><br>";

$aTableauRetour = $objPdo->renvoi_info_requete($sReqliste);
if(!empty($aTableauRetour)) {
    if( $aTableauRetour[0]['principal_langage'] == "Y" ) {
        $sMessagePrincipal .= "FR : " . $aTableauRetour[0]['fr_langage'] . "<br>";
        $sMessagePrincipal .= "ENG : " . $aTableauRetour[0]['eng_langage'] . "<br>";
        $sMessagePrincipal .= "ESP : " . $aTableauRetour[0]['esp_langage'] . "<br>";
        $sMessagePrincipal .= "ALL : " . $aTableauRetour[0]['all_langage'] . "<br>";
    }
    $aTableauRetour[0]['infoprincipal'] = $sMessagePrincipal;
}
echo json_encode($aTableauRetour);
?>