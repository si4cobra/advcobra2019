<?php
header('Content-Type: text/html; charset=utf-8');
ini_set("display_errors","On");
/**
 * Fichier index.php du framework
 *
 */
if( !file_exists('config/config.inc.php') && file_exists('install.php') ) {
    header('Location: install.php');
}
if( file_exists('config/config.inc.php') && file_exists('install.php') ) {
    die('<div style="text-align: center;">
            <h1 style="font-size: 150px;color: rgb(162, 39, 38);">&#10008;</h1>
            <h4 style="font-size: 40px;">Veuillez supprimer le fichier install.php afin de continuer !</h4>
         </div>');
}

require_once 'libs/initialise_framework.php';
require_once 'config/config.inc.php';
//require_once 'controleurs/class_form_list.php';
require_once 'class/class_form_list.php';

$aVar = array( 'fli_module_debug', 'fli_fonction_debug' );
$aVarVal = class_params::nettoie_get_post($aVar);

foreach( $aVar as $key => $value ) {
    $$value = $aVarVal[$key];
}

if( empty($fli_module_debug) || empty($fli_fonction_debug) ) {
    $fli_module_debug = class_fli::get_module_defaut();
    $fli_fonction_debug = class_fli::get_fonction_defaut();
}

class_fli::set_fli_module($fli_module_debug);
class_fli::set_fli_fonction($fli_fonction_debug);

$objImport = new class_import();

$stabCle = $objImport->import_module('fli_session', '', 'session', '');
$aTabDroit = $objImport->import_module('fli_authentification', '', 'verifier_authentification', '');

$tTplMenu = $objImport->import_module('fli_menu', '', 'renvoi_menu', $objImport->import_vue());

$objSmartyImport = $objImport->import_vue();
$aTabTpl = $objImport->import_module($fli_module_debug, '', $fli_fonction_debug, $objSmartyImport);

class_fli::execution($aTabTpl, 'version1', $tTplMenu, true);

?>