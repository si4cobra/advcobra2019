<?php

class mod_form_list
{
    private $db;
    private $host;
    private $username;
    private $password;
    private $database;
    protected $sPrefixeDb;

    public function __construct($bypassdie = false)
    {
        $this->host = class_fli::get_host();
        $this->username = class_fli::get_username();
        $this->password = class_fli::get_password();
        $this->database = class_fli::get_database();
        $this->sPrefixeDb = class_fli::get_prefixe();

        try {
            $this->db = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->database, $this->username,
                $this->password, array(
                    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING

                ));

            $req = $this->db->prepare("SET sql_mode = 'NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'");
            $req->execute();

        } catch( PDOException $e ) {

            if($bypassdie == false){
                die('<h1>Impossible de se connecter &agrave; la base de donn&eacute;e ' . $this->database . ' !!</h1>');
            }else{
                echo 'Impossible de se connecter à la base de donnée ' . $this->database . ' !!';

            }
        }
    }

    public function tl_query($sql, $data = array())
    {
        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->fetchAll(PDO::FETCH_OBJ);
    }

    public function renvoi_info_requete($sql, $data = array())
    {

            //echo $sql;
            $req = $this->db->prepare($sql);
            $req->execute($data);
            return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Renvoi la liste des table de la base de donnée
     * @return array
     */
    public function renvoi_liste_table()
    {   $aTabRetour =array();
        $sql ="SHOW TABLES";
        $data=array();
        $req = $this->db->prepare($sql);
        $req->execute($data);
        $aTmp= $req->fetchAll(PDO::FETCH_ASSOC);

        $sDatabase = class_fli::get_database();

        if(!empty($aTmp)) {
            foreach( $aTmp as $item ) {
                $aTabRetour[] = $item['Tables_in_'.$sDatabase];
            }
        }

        return $aTabRetour;
    }

    public function renvoi_liste_champ_table($sNomTable){

        $sSqlChamp ="SHOW COLUMNS FROM ".$sNomTable;
        $req = $this->db->prepare($sSqlChamp);
        $req->execute();
        $aTmp= $req->fetchAll(PDO::FETCH_ASSOC);

        return $aTmp;
    }


    public function renvoi_nombreLigne_requete($sql, $data = array())
    {
        $req = $this->db->prepare($sql);
        $req->execute($data);
        return $req->rowCount();
    }

    /**
     * @param $sql
     */
    public function tl_exec($sql)
    {
        $req = $this->db->prepare($sql);
        $req->execute();
    }

    /**
     * Methode qui execute une requete en direct
     * @param $sql
     */
    public function execute_requete($sql)
    {
        //echo $sql."<br>";
        try {
            $req = $this->db->prepare($sql);
            return $req->execute();
        }catch(Exception $e){
            $aTabRetour=array();
            $aTabRetour['result']=false;
            $aTabRetour['message']="Probleme survenu ".$e;
            return $aTabRetour;
        }
    }

/*
    function bindArrayValue($req, $array, $typeArray = false)
    {
        if(is_object($req) && ($req instanceof PDOStatement))
        {
            foreach($array as $key => $value)
            {
                if($typeArray)
                    $req->bindValue(":$key",$value,$typeArray[$key]);
                else
                {

                    if($param)
                        $req->bindValue(":$key",$value,$param);
                }
            }
        }
    }*/


    /**
     * Methode qui execute une requete en direct
     * @param $sql
     */
    public function executionRequeteId($sql,$bdd="",$action="",$id="",$commentaire="")
    {
        //echo $sql."<br>";
        $req = $this->db->prepare($sql);
        //echo $sql."<br>"
        $bresult = $req->execute();
        // echo $sql."<br>";exit;
        if(preg_match('/\binsert/i',$sql)){
            $id= $this->db->lastInsertId();
            return $id;
        }
        else
            return $bresult;


    }


    public function prepare_execute_requete($sql,$aTab,$bdebug=false)
    {
        $req = $this->db->prepare($sql);

        if( $bdebug ) {
               echo $sql;
                echo "<pre>";
            print_r($aTab);
            echo "</pre>";
         }

        try {
            $bresult =  $req->execute($aTab);
            if( preg_match('/\binsert/i', $sql) ) {
                $id = $this->db->lastInsertId();
                return $id;
            } else
                return $bresult;
        }catch(Exception $e){
            $aTabRetour=array();
            $aTabRetour['result']=false;

            $sMessageErreur="";

            $aTableauerror= $req->errorInfo ();

            //echo"<pre>";print_r($aTableauerror);echo"</pre>";
            if($aTableauerror[0]=="23000"){
                $aTabRetour['message'] = "<br> Cette entrée est déjà dans la base";
            }else{
                $aTabRetour['message'] = "<br> erreur ".$aTableauerror[2]." ".$aTableauerror[0];
            }

            //echo"<pre>";print_r($aTableauerror);echo"</pre>";

            return $aTabRetour;
        }
    }

    public function insert_update_requete($sql,$aTab=array()){
        $req = $this->db->prepare($sql);
        //echo"<pre>";print_r($aTab);echo"</pre>";
        //echo $sql."<br>";
        if(!empty($aTab)) {
            foreach( $aTab as $key => $value ) {
                if(is_int($value))
                    $param = PDO::PARAM_INT;
                elseif(is_bool($value))
                    $param = PDO::PARAM_BOOL;
                elseif(is_null($value))
                    $param = PDO::PARAM_NULL;
                elseif(is_string($value))
                    $param = PDO::PARAM_STR;
                else
                    $param = FALSE;


                $req->bindValue(':' . $key, $value,$param);
            }
        }

        try {
            $bresult = $req->execute();
            if( preg_match('/\binsert/i', $sql) ) {
                $id = $this->db->lastInsertId();
                return $id;
            } else
                return $bresult;
        }catch(Exception $e){
                $aTabRetour=array();
                $aTabRetour['result']=false;

                 $sMessageErreur="";
                 
                 $aTableauerror= $req->errorInfo ();
                 
                 //echo"<pre>";print_r($aTableauerror);echo"</pre>";
                 if($aTableauerror[0]=="23000"){
                     $aTabRetour['message'] = "<br> Cette entrée est déjà dans la base";
                 }else{
                     $aTabRetour['message'] = "<br> erreur ".$aTableauerror[2]." ".$aTableauerror[0];
                 }
                 
                 //echo"<pre>";print_r($aTableauerror);echo"</pre>";

                return $aTabRetour;
            }
    }

    public function insert_historique($guiduser,$idbdd,$guidbdd,$bdd,$statut,$comment){



        $sRequete_histo="INSERT ".class_fli::get_prefixe()."historique set 
                           giud_historique='".class_helper::guid()."',
                            comment_historique='".addslashes($comment)."',
                            guid_user='".addslashes($guiduser)."',
                            id_bdd='".addslashes($idbdd)."',
                            guid_bdd='".addslashes($guidbdd)."',
                            bdd_historique='".addslashes($bdd)."',
                            statut_historique='".addslashes($statut)."'";
        $this->executionRequeteId($sRequete_histo);

    }


    public function renvoi_traduction($page,$langue){

        $aTableauRetour=array();
        $sRequete =" select ".$langue."_langage as textesimple,".$langue."_wysyg_langage as textewysig,identifiant_langage,guid_langage
        from  nec_langage where link_langage='".$page."'";

        $aTableauListe = $this->renvoi_info_requete($sRequete);
        if(!empty($aTableauListe)){
            foreach($aTableauListe as $valeur){
                $aTableauRetour[$valeur['identifiant_langage']]['textesimple']=$valeur['textesimple'];
                $aTableauRetour[$valeur['identifiant_langage']]['textewysig']=$valeur['textewysig'];
                $aTableauRetour[$valeur['identifiant_langage']]['guid']=$valeur['guid_langage'];
            }
        }

        return $aTableauRetour;

    }

    /*
     * Function duplique une ligne
     */
    public function copier_ligne_bdd($bdd,$filtre,$aTableauExcluAjout,$bdebug=false){


        $sRequete_info_ligne="SELECT * FROM ".$bdd." WHERE ".$filtre;
        $aTableauResult = $this->renvoi_info_requete($sRequete_info_ligne);
        $aTbakeys =array_keys($aTableauExcluAjout);
        
        echo"<pre>";print_r($aTbakeys);echo"</pre>";


        $sRequeteduplicate="Insert ".$bdd." SET";


        if($bdebug){
            echo $sRequete_info_ligne."<br>\n";
            echo"*******************************************************************<br>\n";
            //echo"<pre>";print_r($aTableauResult);echo"</pre>";
        }

        $bpass=false;
        if(!empty($aTableauResult)) {

            foreach( $aTableauResult[0] as $key => $value ) {
                if(!in_array($key, $aTbakeys) ) {
                    if($bpass)
                        $sRequeteduplicate .=",";
                    $sRequeteduplicate .= " " . $key . "='" . addslashes($value) . "'";
                    $bpass=true;
                }
            }


            if(!empty($aTableauExcluAjout)){
                foreach($aTableauExcluAjout as $keyexclut=>$valeurajout){

                    if(isset($valeurajout['ajout']) and  $valeurajout['ajout']=="ok"){
                        $sRequeteduplicate .=" ".$keyexclut."='".addslashes($valeurajout['valeur'])."'";
                    }
                }
            }

        }


        if($bdebug){
            echo $sRequeteduplicate."<br>";
        }


        //return $this->executionRequeteId($sRequeteduplicate);

    }

    public function close_connexion_pdo(){
        unset($this->db);
    }
}

?>