<?php
                    
    class_fli::set_nom_projet('advcobra');
    class_fli::set_nom_cookie('advcobra');

    //Chemins par défaut
    class_fli::set_chemin_acces_absolu('http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']));
    class_fli::set_chemin_acces_relatif(dirname($_SERVER['PHP_SELF']));
    class_fli::set_chemin_acces_server(dirname(dirname(__FILE__)));
    class_fli::set_chemin_acces_public('//'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/public');

    //Configuration du module d'authentification
    class_fli::set_chemin_module_gestion_connexion('modules/fli_authentification/controleurs/');
    class_fli::set_module_authentification('fli_authentification');
    class_fli::set_controleur_authentification('ctrl_fli_authentification');
    class_fli::set_fonction_authentification('verifier_authentification');

    //configuration du module de connexion
    class_fli::set_module_login('fli_connexion');
    class_fli::set_controleur_login('ctrl_fli_connexion');
    class_fli::set_fonction_login('connexion');
    //configuration de la page par defaut du site
    class_fli::set_module_defaut('module_hello_world');
    class_fli::set_controleur_defaut('ctrl_module_hello_world');
    class_fli::set_fonction_defaut('hello_world');    
    class_fli::set_fli_actif('N');
    class_fli::set_fli_supp('Y');
    class_fli::set_fli_actif_doc('Y');    
    class_fli::set_sous_repertoire('');

    //Base de données
    class_fli::set_host('advcobrasrv');
    class_fli::set_username('advcobra');
    class_fli::set_password('MLadhj6Tbjkdl');
    class_fli::set_database('advcobra');
    class_fli::set_prefixe('co_');    
    class_fli::set_fli_version_template('version1');

    //Logo
    class_fli::set_logo('images/flilogo.png');
    class_fli::set_logo_alternatif('images/flitxt.png');
    class_fli::set_favicon('images/flifavicon.png');

    //Par défaut l'utilisateur n'est pas connecté
    class_fli::set_fli_est_connecte(false);

    class_fli::set_aData_entier(array());
    ?>