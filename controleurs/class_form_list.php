<?php

/**
 * @name class_form_list classe qui génère le formulaire, la recherche, la liste et la fiche d'une table de bdd
 * @author
 * @version 1
 */
class class_form_list
{
    /**R
     * @var sNomTable nom de la table liée
     */
    public $sNomTable;

    /**
     * @var sChampId nom du champ de la clé primaire de la table liée
     */
    public $sChampId;

    /**
     * @var sChampSupplogique le nom du champ supplogique dans la table liée
     */
    public $sChampSupplogique;

    /**
     * @var bDebugRequete
     */
    public $bDebugRequete;

    /**
     * @var bAffMod si une entree du tableau peut etre modifiee
     */
    public $bAffFiche;

    /**
     * @var bAffMod si une entree du tableau peut etre modifiee
     */
    public $bAffMod;

    /**
     * @var bAffForm si une entree du tableau peut etre supprimee
     */
    public $bAffSupp;

    /**
     * @var bPagination attribut activation pagination pagination
     */
    public $bPagination;

    /**
     * @var iNombrepage attribut nombre de page pour une pagination
     */
    public $iNombrePage;

    /**
     * @var sRetourListe parametre de la methode run, squellette de l url de retour a partir de la liste
     */
    public $sRetourListe;

    /**
     * @var sFiltreliste filtre sur la liste
     */
    public $sFiltreliste;

    /**
     * @var sFiltrelisteOrderBy order by initial
     */
    public $sFiltrelisteOrderBy;

    /**
     * @var objSmarty instance de la classe Smarty
     */
    public $objSmarty;

    /**
     * @var sTitreCreationForm titre de la page lors de la creation d une entree dans le formulaire
     */
    public $sTitreCreationForm;

    /**
     * @var sMessageCreationSuccesForm le message a affiche quand le formulaire a ete rempli sans erreur en creation
     */
    public $sMessageCreationSuccesForm;

    /**
     * @var sTitreModificationForm titre de la page lors de la modification d une entree dans le formulaire
     */
    public $sTitreModificationForm;

    /**
     * @var sMessageModificationSuccesForm le message a affiche quand le formulaire a ete rempli sans erreur en modification
     */
    public $sMessageModificationSuccesForm;

    /**
     * @var sMessageModificationSuccesForm le message a affiche quand le formulaire a ete rempli sans erreur en modification
     */
    public $sParametreWizardListe;

    /**
     * @var iId identifiant de l'élément concerné
     */
    public $iId;

    /**
     * @var aElement les elements concernés par le formulaire ou la recherche ou la liste ou la fiche de la table liée
     */
    public $aElement;

    /**
     * @var objClassGenerique attribut objet connection bdd
     */
    public $objClassGenerique;

    /**
     * @var attribut message erreur à afficher
     */
    public $sMessageErreurAjoutChamp;

    /**
     * @var attribut message erreur à afficher
     */
    public $sMessageSuccesForm;

    /**
     * @var attribut message erreur à afficher
     */
    public $aParametreWizardListe;

    /**
     * @var attribut message erreur à afficher
     */
    public $itemBoutonsListe;

    /**
     * @var sBaseUrl la base des urls
     */
    public $sBaseUrl;

    /**
     * @var sParametreInterModule parametre inter module
     */
    public $sParametreInterModule;

    /**
     * @var sParametreRecherche parametres de recherche
     */
    public $sParametreRecherche;

    /**
     * @var sParametreFiltreListe parametre filtre de la liste
     */
    public $sParametreFiltreListe;

    /**
     * @var sParametreRetourValidationFormExterne parametre utilise pour le retour apres validation du formulaire
     */
    public $sParametreRetourValidationFormExterne;

    /**
     * @var sUrlRetourValidationFormExterne url de retour du formulaire dans le cas d'un retour externe apres validation du formulaire
     */
    public $sUrlRetourValidationFormExterne;

    /**
     * @var sParametreRetourAnnulationFormExterne parametre utilise pour le retour apres annulation du formulaire
     */
    public $sParametreRetourAnnulationFormExterne;

    /**
     * @var sUrlRetourAnnulationFormExterne url de retour du formulaire dans le cas d'un retour externe apres annulation du formulaire
     */
    public $sUrlRetourAnnulationFormExterne;

    /**
     * @var sParametreRetourListe parametre pour le retour a partir de la liste
     */
    public $sParametreRetourListe;

    /**
     * @var sParametreRetourListeTableau parametre pour le retour a partir de la liste sous forme de tableau
     */
    public $sParametreRetourListeTableau;

    /**
     * @var sUrlRetourListe url de retour a partir de la liste
     */
    public $sUrlRetourListe;

    /**
     * @var aEnteteListe l'entete de liste
     */
    public $aEnteteListe;

    /**
     * @var aListe la liste
     */
    public $aListe;

    /**
     * @var sDebugRequeteSelect la requete select global a affiche en debug
     */
    public $sDebugRequeteSelect;

    /**
     * @var sDebugRequeteInsertUpdate la requete insert/update de création ou modification d'un element a affiche en debug
     */
    public $sDebugRequeteInsertUpdate;

    /**
     * @var bLabelCreationElem si la creation d un nouvel element est possible
     */
    public $bLabelCreationElem;

    /**
     * @var bAffListeQuandPasRecherche si il y a affichage de la liste quant il n'y a pas de filtre sur le recherche
     */
    public $bAffListeQuandPasRecherche;

    /**
     * @var sTypeFichier activation type fichier
     */
    public $sTypeFichier;
    /**
     * @var sTypeFichier activation type fichier
     */
    public $bFormPopup;


    /**
     * @var bforceAfficheEntete permet de forcer l'affichage des messages resultat
     */
    public $bforceAfficheEntete = false;

    /**
     * @var bResultUpfate valeur de retour quand c'est il y a une modification
     */
    public $aTabResultUpdate;


    /**
     * @var bResultUpfate valeur de retour quand c'est il y a une insertion
     */
    public $aTabResultInsert;
    /**
     * @var bResultUpfate valeur de retour quand c'est il y a une insertion
     */
    public $aTabResultSupp;

    /**
     * @var bCsv permet d'afficher le bouton d'export en csv
     */
    public $bCsv;

    /**
     * @var bCsv permet d'afficher le bouton d'export en csv
     */
    public $aParamsNonSuivi;

    /**
     * @var permet d'avoir un formulaire depuis la liste
     */
    public $bActiveFormSelect;

    /**
     * @var boutons radio devant le form
     */
    public $bRadioSelect;

    /**
     * @var checkboxs devant le form
     */
    public $bCheckboxSelect;

    /**
     * @var script executé après une insertion
     */
    public $sScriptJavaSCriptInsert = "";

    /**
     * @var script executé après un update
     */
    public $sScriptJavaSCriptUpdate = "";

    /**
     * @var affichage bouton annuler formulaire
     */
    public $bAffAnnuler;

    /**
     * @var b retour vere un fichier spécifique
     */
    public $bRetourSpecifique;
    /**
     * @var URL de retour ver un fichier spécifique
     */
    public $RetourElemUrl;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $sLabelFileRetourElem;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $sDirRetour;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $bBtnRetour;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $bAffNombreResult;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $bAffTitre;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $bLigneCliquable;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $sLienLigne;
    /**
     * @var Label URL de retour ver un fichier spécifique
     */
    public $sUrlRetourSpecifique;
    /**
     * @var Afficher le bouton d'impression true ou false
     */
    public $bAffPrintBtn;
    /**
     * @var Label URL pour un pagination quand c'est spécifique
     */
    public $sDirpagination;
    /**
     * @var si on veut rajouter de sql dans la requete insert
     */
    public $sSuiteRequeteInsert;

    /**
     * @var si on veut rajouter du sql dans la requete update
     */
    public $sSuiteRequeteUpdate;

    public $fli_traiteConnexion;


    public function class_form_list($aParametre)
    {

        $aDescriptionParametre = array(
            array( 'sNomTable', '' ), array( 'sChampId', '' ), array( 'sChampSupplogique', '' ), array( 'bDebugRequete', false ), array( 'bAffFiche', false ), array( 'bAffMod', false ), array( 'bCsv', false ),
            array( 'bAffSupp', false ), array( 'bPagination', false ), array( 'iNombrePage', 0 ), array( 'sRetourListe', null ), array( 'sFiltreliste', '' ), array( 'objSmarty', null ),
            array( 'sTitreCreationForm', '' ), array( 'sMessageCreationSuccesForm', '' ), array( 'sTitreModificationForm', '' ), array( 'sMessageModificationSuccesForm', '' ), array( 'bAffTitre', true ),
            array( 'sDebugRequeteSelect', '' ), array( 'sDebugRequeteInsertUpdate', '' ), array( 'aElement', array() ), array( 'sLabelRetourListe', '' ), array( 'sMessageSupprElem', '' ),
            array( 'sMessageErreurForm', '' ), array( 'sLabelCreationElem', '' ), array( 'sLabelRecherche', '' ), array( 'sTitreListe', '' ), array( 'sLabelNbrLigne', '' ), array( 'bAffAnnuler', true ),
            array( 'sListeTpl', 'liste.tpl' ), array( 'sRetourValidationForm', 'liste' ), array( 'sRetourValidationFormExterne', '' ), array( 'sRetourAnnulationForm', 'liste' ), array( 'bCheckboxSelect', false ),
            array( 'sRetourAnnulationFormExterne', '' ), array( 'sFormulaireTpl', 'formulaire.tpl' ), array( 'sTitreForm', '' ), array( 'itemBoutonsForm', array() ), array( 'bRadioSelect', false ),
            array( 'sCategorieListe', '' ), array( 'itemBoutonsListe', array() ), array( 'sParametreWizardListe', '' ), array( 'aParametreWizardListe', array() ), array( 'bActiveFormSelect', false ),
            array( 'bFormPopup', false ), array( 'sListeFilsTpl', 'liste_fils.tpl' ), array( 'aListeFils', array() ), array( 'sRetourSuppressionFormExterne', '' ), array( 'aParamsNonSuivi', array() ),
            array( 'sUrlRetourSuppressionnFormExterne', '' ), array( 'sFiltrelisteOrderBy', '' ), array( 'bLabelCreationElem', true ), array( 'sDirRetour', '' ), array( 'bBtnRetour', false ),
            array( 'bAffListeQuandPasRecherche', true ), array( 'sTypeFichier', '' ), array( 'bRetourSpecifique', false ), array( 'RetourElemUrl', '' ), array( 'sLabelFileRetourElem', '' ),
            array( 'bAffNombreResult', true ), array( 'bLigneCliquable', false ), array( 'sLienLigne', '' ), array( 'sUrlRetourSpecifique', '' ), array( 'sSuiteRequeteInsert', '' ),
            array( 'sSuiteRequeteUpdate', '' ), array( 'sDirpagination', '' ), array( 'bAffPrintBtn', false ), array( 'fli_traiteConnexion', true )
        );


        foreach( $aDescriptionParametre as $objDescriptionParametre ) {
            $this->$objDescriptionParametre[0] = isset($aParametre[$objDescriptionParametre[0]]) ? $aParametre[$objDescriptionParametre[0]] : $objDescriptionParametre[1];
        }

        if( $this->fli_traiteConnexion && !class_fli::get_fli_est_connecte() ) {
            if( (isset($_GET['fli_module']) && $_GET['fli_module'] != class_fli::get_module_login()) || !isset($_GET['fli_module']) ) {
                header('Location: ' . class_fli::get_chemin_acces_absolu() . '/' . class_fli::get_module_login() . '-' . class_fli::get_fonction_login());
                exit;
            }
        }

        switch( $this->sTypeFichier ) {
            case 'csv':
                $this->sListeTpl = 'listeCsv.tpl';
                break;
            default:
                break;
        }

        //$this->objSmarty->assign('sNomTable', $this->sNomTable);

        $this->objClassGenerique = new mod_form_list();

        if( isset($_GET['action']) && $_GET['action'] == 'form' && isset($_GET[$this->sChampId]) ) {
            $this->iId = $_GET[$this->sChampId];
            //echo"jepaase=>".$this->iId ;
        } else if( isset($_GET['action']) && $_GET['action'] == 'supp' && isset($_GET[$this->sChampId]) ) {
            $this->iId = $_GET[$this->sChampId];
        } else {
            $this->iId = null;
        }

        if( $this->iId === null || (isset($_GET['wizard_type_etape']) && $_GET['wizard_type_etape'] == 'crea' && (!isset($_GET['action']) || $_GET['action'] != 'form')) ) {
            $this->sCategorieForm = $this->sTitreCreationForm;
            $this->sMessageSuccesForm = $this->sMessageCreationSuccesForm;
        } else {
            $this->sCategorieForm = $this->sTitreModificationForm;
            $this->sMessageSuccesForm = $this->sMessageModificationSuccesForm;
        }

        $this->aTabResultInsert = array();
        $this->aTabResultUpdate = array();
    }

    /**
     * fonction la liste des cles utilisable pour les champs
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */

    public function renvoi_liste_cles_champ()
    {

        $aTabRetour['type_champ'] = "Type de Champ";
        $aTabRetour['mapping_champ'] = "le nom du champ dans la table selectionnee";
        $aTabRetour['nom_variable'] = "la valeur de l attribut \"name\" dans le formulaire";
        $aTabRetour['text_label'] = "l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...";
        $aTabRetour['ctrl_champ'] = "faut il controler le champ 'ok'|'warn'|''";
        $aTabRetour['valeur_variable'] = "la valeur par defaut dans le formulaire lors de la creation";
        $aTabRetour['aff_liste'] = "faut il afficher le champ dans la liste 'ok'|''";
        $aTabRetour['aff_form'] = "faut il afficher le champ dans le formulaire 'ok'|''";
        $aTabRetour['aff_filtre'] = "faut il afficher le champ comme filtre 'ok'|''";
        $aTabRetour['aff_recherche'] = "faut il afficher le champ dans la recherche 'ok'|''";
        $aTabRetour['type_recherche'] = "exemple like \'<champ>%\'type de la recherche ";
        $aTabRetour['aff_fiche'] = "faut il afficher le champ dans la fiche 'ok'|''";
        $aTabRetour['size_champ'] = "taille max du champ";
        $aTabRetour['style'] = "ajout du style sur le champ";
        $aTabRetour['tableau_attribut'] = "ajout d attributs sur le champ";
        $aTabRetour['fonction_javascript'] = "ajout du javascript sur le champ (tableau clé=>valeur)";
        $aTabRetour['mess_erreur'] = "message d erreur lorsque le controle n est pas valide";
        $aTabRetour['traite_sql'] = "faut il traite le champ dans les divers requetes sql selection, insertion, modification";
        $aTabRetour['date_format'] = "le format de la date renvoyé ";
        $aTabRetour['date_conversion_enreg'] = "dans quelle type de conversion il faut mettre la date si eng on transformer dd/mm/YYYY en YYYY-mm-dd";
        $aTabRetour['date_now_creation'] = "si la date doit etre pre-rempli a la creation";
        $aTabRetour['date_now_modification'] = "si la date doit etre mise a jour a chaque modification";
        $aTabRetour['table_item'] = "la table liee pour ce champ ";
        $aTabRetour['id_table_item'] = "le champ de la table liee qui sert de cle primaire";
        $aTabRetour['id_liaison'] = "cle primaire de la table qui lie les champs dans un rapport n=>n (checkbox ou select multiple)";
        $aTabRetour['affichage_table_item'] = "le champ de la table lien qui sert de label d affichage exemple mon_client as nomitem <bdd> si nous voulon un affichage plus pousser";
        $aTabRetour['supplogique_table_item'] = "Outil supplogique sur la table item";
        $aTabRetour['type_table_join'] = "Type de la jointure ";
        $aTabRetour['table_lien'] = "la table qui fait le lien entre la table du formulaire et la table lien";
        $aTabRetour['id_table_lien'] = "le nom du champ dans la table lien qui fait reference a la cle primaire de la table du formulaire";
        $aTabRetour['id_item_table_lien'] = "le nom du champ dans lafile_uploadamp dans la table lien faisant reference a la derniere modificaton du lien dans la table";
        $aTabRetour['lesitem'] = "liste des items possibles choisies dans la table item avec pour cle l\identifiant de la table item et avec pour valeur la valeur que l\'on choisit";
        $aTabRetour['mapping_champ_label'] = "les possibles valeurs pour le type radio array('Y' => 'Oui', 'N' => 'Non')";
        $aTabRetour['url'] = "url de retour pour un bouton du formulaire, bouton annuler par exemple";
        $aTabRetour['file_upload'] = "dossier de telechargement du fichier";
        $aTabRetour['file_visu'] = "url de visualisation du fichier";
        $aTabRetour['file_aff_modif_form'] = 'si le fichier actuel doit etre affiche lors de la modification dans le formulaire, valeur ex: ok, type de fichier supporte: png, jpeg, gif, swf';
        $aTabRetour['file_aff_modif_form_taille'] = 'taille du fichier actuel affiche lors de la modification dans le formulaire';
        $aTabRetour['file_aff_modif_form_couleur_fond'] = 'couleur de fond du fichier affiche lors de la modification dans le formulaire';
        $aTabRetour['file_aff_liste_taille'] = 'taille du fichier actuel affiche dans la liste';
        $aTabRetour['file_aff_liste_couleur_fond'] = 'couleur de fond du fichier affiche dans la liste';
        $aTabRetour['recherche_intervalle_date'] = 'si le champ doit etre en recherche par intervalle';
        $aTabRetour['recherche_intervalle_date_label'] = 'tableau contenant 2 valeurs, les labels pour les dates a intervalle dans la recherche';
        $aTabRetour['transfert_inter_module'] = 'transfert la valeur de la variable a l\'interieur du module, la valeur doit exister et etre presente dans l\'url';
        $aTabRetour['injection_code'] = 'injection de code dans les cellules de la liste';
        $aTabRetour['recherche_intervalle_date'] = 'si il y a une recherche par intervalle pour la date dans la recherche, ex: ok';
        $aTabRetour['recherche_intervalle_date_label'] = 'les labels des inputs sur l\'intervalle de date dans la recherche, ex: array(\'min\', \'max\');';
        $aTabRetour['wysiwyg'] = 'si le textarea est utilise en wysiwyg dans le formulaire';
        $aTabRetour['recherche_filtre_actif'] = 'considère la recherche comme non active meme quand le champ est rempli';
        $aTabRetour['recherche_filtre_par_champ'] = 'si le champ doit etre filtre par un autre champ dans le formulaire de recherche, inscrivez le nom_variable du champ en question';
        $aTabRetour['recherche_affiche_non_filtre_par_champ'] = 'si la liste des possibilites est generee lorsque le champ n\'est pas encore filtre par un autre champ, valeurs possibles:true ou false, valeur par defaut false';
        //********************variable temporaire****************
        $aTabRetour['text_label_filtre'] = "dans le cas d'un champ pour remplir un select en bas";
        $aTabRetour['select_autocomplete'] = 'si l input est un select et que l on demande l auto completion sur la liste';
        $aTabRetour['tabfiltre_autocomplete'] = "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=";
        $aTabRetour['tabfiltre_autocomplete'] = "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=";
        $aTabRetour['alias_table_lien'] = " On defini un alias de le la base de donnée dans le cas de base intermédiare";
        $aTabRetour['requete_supplementaire_select'] = " on rajoute des info supplementaire dans la requete select ou radio ou chekbox en affichage";
        $aTabRetour['aff_debug'] = "On affiche la requete debug";
        $aTabRetour['alias_transfer'] = "Cles disponible pour le type transfert_inter_module pour remplacer le filtre par une alis sp&cifique";
        $aTabRetour['activ_form_select'] = "Activation d'un champ sur input, select, checkbox, radio dans le tableau afffichage list";
        $aTabRetour['double_password'] = "Double champ password";
        $aTabRetour['file_compress'] = "Active la compression pour les images jpeg,gif,png";
        $aTabRetour['file_taux_compress'] = "taux de compression 1-9 pour les images (jpeg,gif,png)  (100 = bonne qualité, 0 = très mauvaise qualité)";
        $aTabRetour['file_compress_min'] = "Active la compression pour les images jpeg,gif,png";
        $aTabRetour['file_taux_compress_min'] = "taux de compression 1-9 pour les images (jpeg,gif,png)  (100 = bonne qualité, 0 = très mauvaise qualité)";
        $aTabRetour['order_select'] = " orde dans le select";
        $aTabRetour['tags'] = "Activation de l'affichage sous forme de tags pour un checkbox";
        $aTabRetour['tags_ajax'] = "Activation de l'ajax pour les tags";
        $aTabRetour['tags_id'] = "id utilisé via ajax";
        $aTabRetour['tags_champ'] = "champ a afficher via ajax";
        $aTabRetour['tags_table'] = "table utilisé requete ajax";
        $aTabRetour['tags_champ_like'] = "Champ sur lequel le like est exécuté";
        $aTabRetour['tags_where'] = "Commencer avec AND absolument !";
        $aTabRetour['select_pere_fils'] = " Active une liste deroulante avec un decalage pere fils";
        $aTabRetour['Activdefaut'] = "On active le chemin de l'image par défaut a l'enregistremnt";
        $aTabRetour['filedefaut'] = "Chemin de l'image par défaut à l'enregistrement";
        $aTabRetour['type_encodage'] = "Type d'encodage du mot de passe (phpbb_hash,PASSWORD) par defaut: PASSWORD";
        $aTabRetour['datetime_format'] = "le format de la datetime renvoyé ";
        $aTabRetour['format_affiche_liste'] = "le format d'affichage dans la liste ";
        $aTabRetour['html_perso_th'] = "attribut perso html dans le <th> du liste.tpl";
        $aTabRetour['html_perso_td'] = "attribut perso html dans le <td> du liste.tpl";

        return $aTabRetour;
    }

    /**
     * Methode d'un champ dans les inputs
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function ajout_champ($objElement)
    {

        $aTableauLeschamp = $this->renvoi_liste_cles_champ();
        $aTableauCles = array_keys($aTableauLeschamp);
        $aTableauclesinput = array_keys($objElement);

        foreach( $aTableauCles as $sTableauCles ) {
            if( !in_array($sTableauCles, $aTableauclesinput) ) {
                $objElement[$sTableauCles] = '';
            }
        }

        $bPasse = false;

        if( in_array($objElement['type_champ'], array( 'select', 'selectmultiple' )) and empty($objElement['lesitem'])
            && ($objElement['table_item'] == '' || $objElement['id_table_item'] == ''
                || $objElement['affichage_table_item'] == '' || $objElement['supplogique_table_item'] == ''
                || $objElement['type_table_join'] == '')
        ) {

            $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Sur l\'ajout du champ  de type ' . $objElement['type_champ'] . ' label du <b>' . $objElement['text_label'] . '</b> manque cles<br>';
            $this->sMessageErreurAjoutChamp .= '-Cles tableau obligatoire pour un type select maquante:<br>';

            if( $objElement['table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles table_item<br>';
            }
            if( $objElement['id_table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles manquante <b>id_table_item</b><br>';
            }
            if( $objElement['affichage_table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '- cles manquante <b>affichage_table_item</b><br>';
            }
            if( $objElement['supplogique_table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles manquante <b>supplogique_table_item</b><br>';
            }
            if( $objElement['type_table_join'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles  manquante <b>type_table_join</b><br>';
            }

            $bPasse = true;
        }

        if( $objElement['aff_recherche'] == 'ok' && $objElement['type_recherche'] == '' ) {
            if( !$bPasse ) {
                $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Sur l\'ajout du champ  de type ' . $objElement['type_champ'] . ' label du <b>' . $objElement['text_label'] . '</b> manque cles<br>';
            }
            $this->sMessageErreurAjoutChamp .= '---- cles  manquante <b>type_recherche</b><br>';
        }

        if( $objElement['date_format'] == '' ) {
            $objElement['date_format'] = "%Y-%m-%d";
        }

        if( $objElement['datetime_format'] == '' ) {
            $objElement['datetime_format'] = "%Y-%m-%d %H:%i:%s";
        }

        $this->aElement[] = $objElement;

        return $this;
    }

    public function renvoi_base_url($type = "")
    {


        if( $type == "pagination" and $this->sDirpagination != "" ) {
            $sDir = $this->sDirpagination;
            // echo "pagination=>".$this->sDirpagination."=>".$this->sBaseUrl;
            $this->sBaseUrl = "?dir=" . $this->sDirpagination;
        }

        if( !isset($this->sBaseUrl) ) {
            $sModule = isset($_GET['fli_module']) ? $_GET['fli_module'] : '';
            $sModuleFonction = isset($_GET['fli_fonction']) ? $_GET['fli_fonction'] : '';

            //ici on  met un chemin different pour la pagination


            $this->sBaseUrl = $sModule . '-' . $sModuleFonction . '?';
        }

        return $this->sBaseUrl;
    }

    public function renvoi_parametre_inter_module()
    {
        if( !isset($this->sParametreInterModule) ) {
            $sParametreInterModule = '';

            foreach( $this->aElement as $objElement ) {
                if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' ) {
                    if( isset($_GET[$objElement['nom_variable']]) ) {
                        $sParametreInterModule .= '&' . $objElement['nom_variable'] . '=' . $_GET[$objElement['nom_variable']];
                    } else {
                        exit(0);
                    }
                }
            }

            $this->sParametreInterModule = $sParametreInterModule;
        }

        return $this->sParametreInterModule;
    }

    public function renvoi_parametre_recherche()
    {
        if( !isset($this->sParametreRecherche) ) {
            $sParametreRecherche = '';

            foreach( $this->aElement as $objElement ) {
                if( isset($objElement['aff_recherche']) && $objElement['aff_recherche'] == 'ok' ) {
                    if( isset($_GET[$objElement['nom_variable']]) ) {
                        if( !isset($objElement['transfert_inter_module']) || $objElement['transfert_inter_module'] != 'ok' ) {
                            if( is_array($_GET[$objElement['nom_variable']]) ) {
                                foreach( $_GET[$objElement['nom_variable']] as $sElement ) {
                                    $sParametreRecherche .= '&' . $objElement['nom_variable'] . '[]=' . $sElement;
                                }
                            } else {
                                $sParametreRecherche .= '&' . $objElement['nom_variable'] . '=' . $_GET[$objElement['nom_variable']];
                            }
                        }
                    }
                }
            }

            $this->sParametreRecherche = $sParametreRecherche;
        }

        return $this->sParametreRecherche;
    }

    public function renvoi_parametre_filtre_liste()
    {
        if( !isset($this->sParametreFiltreListe) ) {
            $sParametreFiltreListe = '';

            if( isset($_GET['filtreliste']) && isset($_GET['typefiltreliste']) ) {
                $sParametreFiltreListe = '&filtreliste=' . $_GET['filtreliste'] . '&typefiltreliste=' . $_GET['typefiltreliste'];
            }

            $this->sParametreFiltreListe = $sParametreFiltreListe;
        }

        return $this->sParametreFiltreListe;
    }

    public function renvoi_url_retour_suppression_form_externe()
    {
        if( !isset($this->sUrlRetourSuppressionFormExterne) ) {
            $sUrlRetourSuppressionFormExterne = $this->sRetourSuppressionFormExterne;

            if( $sUrlRetourSuppressionFormExterne != '' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourSuppressionFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sUrlRetourSuppressionFormExterne = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourSuppressionFormExterne);
                        }
                    }
                }
            }

            $this->sUrlRetourSuppressionFormExterne = $sUrlRetourSuppressionFormExterne;
        }

        return $this->sUrlRetourSuppressionFormExterne;
    }

    public function renvoi_parametre_retour_validation_form_externe()
    {
        if( !isset($this->sParametreRetourValidationFormExterne) ) {
            $sParametreRetourValidationFormExterne = '';

            if( $this->sRetourValidationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourValidationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sParametreRetourValidationFormExterne .= '&' . $sChamp . '=' . $_GET[$sChamp];
                        }
                    }
                }
            }

            $this->sParametreRetourValidationFormExterne = $sParametreRetourValidationFormExterne;
        }

        return $this->sParametreRetourValidationFormExterne;
    }

    public function renvoi_url_retour_validation_form_externe()
    {
        if( !isset($this->sUrlRetourValidationFormExterne) ) {
            $sUrlRetourValidationFormExterne = $this->sRetourValidationFormExterne;

            if( $this->sRetourValidationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourValidationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sUrlRetourValidationFormExterne = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourValidationFormExterne);
                        }
                    }
                }
            }

            $this->sUrlRetourValidationFormExterne = $sUrlRetourValidationFormExterne;
        }

        return $this->sUrlRetourValidationFormExterne;
    }

    public function renvoi_parametre_retour_annulation_form_externe()
    {
        if( !isset($this->sParametreRetourAnnulationFormExterne) ) {
            $sParametreRetourAnnulationFormExterne = '';

            if( $this->sRetourAnnulationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourAnnulationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sParametreRetourAnnulationFormExterne .= '&' . $sChamp . '=' . $_GET[$sChamp];
                        }
                    }
                }
            }

            $this->sParametreRetourAnnulationFormExterne = $sParametreRetourAnnulationFormExterne;
        }

        return $this->sParametreRetourAnnulationFormExterne;
    }

    public function renvoi_url_retour_annulation_form_externe()
    {
        if( !isset($this->sUrlRetourAnnulationnFormExterne) ) {
            $sUrlRetourAnnulationFormExterne = $this->sRetourAnnulationFormExterne;

            if( $this->sRetourAnnulationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourAnnulationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sUrlRetourAnnulationFormExterne = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourAnnulationFormExterne);
                        }
                    }
                }
            }

            $this->sUrlRetourAnnulationFormExterne = $sUrlRetourAnnulationFormExterne;
        }

        return $this->sUrlRetourAnnulationFormExterne;
    }

    public function renvoi_parametre_retour_liste()
    {
        if( !isset($this->sParametreRetourListe) ) {
            $sParametreRetourListe = '';

            $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourListe, $aMatches);
            if( $iNombreMatches > 0 ) {
                $aChamp = array_unique($aMatches[1]);
                foreach( $aChamp as $sChamp ) {
                    if( isset($_GET[$sChamp]) ) {
                        $sParametreRetourListe .= '&' . $sChamp . '=' . $_GET[$sChamp];
                    }
                }
            }

            $this->sParametreRetourListe = $sParametreRetourListe;
        }

        return $this->sParametreRetourListe;
    }

    public function renvoi_parametre_retour_liste_tableau()
    {
        if( !isset($this->sParametreRetourListeTableau) ) {
            $sParametreRetourListeTableau = null;

            $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourListe, $aMatches);
            if( $iNombreMatches > 0 ) {
                $aChamp = array_unique($aMatches[1]);
                foreach( $aChamp as $sChamp ) {
                    if( isset($_GET[$sChamp]) ) {
                        $sParametreRetourListeTableau[$sChamp] = $_GET[$sChamp];
                    }
                }
            }

            $this->sParametreRetourListeTableau = $sParametreRetourListeTableau;
        }

        return $this->sParametreRetourListeTableau;
    }

    public function renvoi_url_retour_liste()
    {
        if( !isset($this->sUrlRetourListe) ) {
            $sUrlRetourListe = $this->sRetourListe;

            $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourListe, $aMatches);
            if( $iNombreMatches > 0 ) {
                $aChamp = array_unique($aMatches[1]);
                foreach( $aChamp as $sChamp ) {
                    if( isset($_GET[$sChamp]) ) {
                        $sUrlRetourListe = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourListe);
                    }
                }
            }

            $this->sUrlRetourListe = $sUrlRetourListe;
        }

        return $this->sUrlRetourListe;
    }

    /**
     * @method renvoi_entete_liste calcul et renvoi les entetes de la liste
     * @author Sébastien Robert
     * @version 1
     * @use aff_liste aff_filtre nom_variable text_label filtreliste typefiltreliste
     */
    public function renvoi_entete_liste()
    {
        if( !isset($this->aEnteteListe) ) {
            /**
             * @var aEnteteListe tableau contenant les entetes de la liste pour l'affichage dans le template
             */
            $aEnteteListe = array();

            foreach( $this->aElement as $objElement ) {
                if( isset($objElement['aff_liste']) && $objElement['aff_liste'] == 'ok' ) {
                    $sUrl = $this->renvoi_base_url() . $this->renvoi_parametre_inter_module() . $this->renvoi_parametre_recherche() . $this->renvoi_parametre_retour_liste() . $this->sParametreWizardListe;
                    if( isset($objElement['text_label']) && is_string($objElement['text_label']) ) {
                        $sLabel = $objElement['text_label'];
                    } else {
                        echo 'Erreur: class_form_list::renvoi_entete_liste, l\'attribut "text_label" est manquant dans l\'element ci-dessous';
                        echo '<pre>';
                        print_r($objElement);
                        echo '</pre>';
                        exit(0);
                    }
                    if( isset($objElement['aff_filtre']) && $objElement['aff_filtre'] ) {
                        if( isset($objElement['nom_variable']) && is_string($objElement['nom_variable']) && $objElement['nom_variable'] != '' ) {
                            $sNomVariable = $objElement['nom_variable'];
                        } else {
                            echo 'Erreur: class_form_list::renvoi_entete_liste, l\'attribut "nom_variable" est manquant ou non rempli dans l\'element ci-dessous';
                            echo '<pre>';
                            print_r($objElement);
                            echo '</pre>';
                            exit(0);
                        }
                        $sTypeFiltre = (isset($_GET['filtreliste']) && $_GET['filtreliste'] == $objElement['nom_variable'] && isset($_GET['typefiltreliste']) && $_GET['typefiltreliste'] == 'asc') ? 'desc' : 'asc';
                        $sUrl .= '&filtreliste=' . $sNomVariable . '&typefiltreliste=' . $sTypeFiltre;
                    }
                    $aEnteteListe[] = array( 'sUrl' => $sUrl, 'sLabel' => $sLabel, 'objElement' => $objElement );
                }
            }

            $this->aEnteteListe = $aEnteteListe;
        }

        return $this->aEnteteListe;
    }

    public function renvoi_liste()
    {
        if( !isset($this->aListe) ) {
            $aListe = array();

            $larequete = $this->renvoi_requete();
            //echo $larequete."<br>";
            $aValeurBddListe = $this->objClassGenerique->renvoi_info_requete($larequete);

            if( !empty($aValeurBddListe) ) {
                $aEnteteListe = $this->renvoi_entete_liste();

                $sBaseUrl =
                    $this->renvoi_base_url()
                    . $this->renvoi_parametre_inter_module()
                    . $this->renvoi_parametre_recherche()
                    . $this->renvoi_parametre_filtre_liste()
                    . $this->renvoi_parametre_retour_liste()
                    . $this->renvoi_parametre_retour_validation_form_externe()
                    . $this->renvoi_parametre_retour_annulation_form_externe()
                    . $this->sParametreWizardListe;

                foreach( $aValeurBddListe as $objValeurBddListe ) {
                    foreach( $aEnteteListe as $objEnteteListe ) {
                        $objElement = $objEnteteListe['objElement'];
                        $sTypeChamp = $objElement['type_champ'];
                        if( !empty($this->sChampId) ) {
                            $aListe[$objValeurBddListe[$this->sChampId]]['id'] = $objValeurBddListe[$this->sChampId];
                        }

                        // ---------------- TEXT/TEXTAREA ----------------
                        if( in_array($sTypeChamp, array( 'text', 'hidden', 'textarea' )) and $objElement['traite_sql'] == "ok" ) {

                            // ici pour integrer de html dans le champ texte
                            if( $objElement['injection_code'] != "" ) {
                                $sChaineId = str_replace("<id>", $objValeurBddListe[$this->sChampId], $objElement['injection_code']);
                                $objValeurBddListe[$objElement['mapping_champ']] = str_replace("<value>", $objValeurBddListe[$objElement['mapping_champ']], $sChaineId);
                            }

                            $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe[$objElement['mapping_champ']];


                        } else if( in_array($sTypeChamp, array( 'select', 'radio' )) ) {

                            if( isset($objElement['table_lien']) && ($objElement['table_lien'] != '' || is_array($objElement['table_lien'])) && !$this->bActiveFormSelect ) {

                                //echo"titi ".$objElement['nom_variable']."\n";
                                //$aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $this->renvoi_requete_table_lien($objElement, $objValeurBddListe[$objElement['mapping_champ']]);
                                //$aListe[$objValeurBddListe[$this->sChampId]]["mon_".$objElement['nom_variable']] =  $objValeurBddListe[$objElement['mapping_champ']];
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe["mon_" . $objElement['nom_variable']];
                            } // ---------------- RADIO ----------------
                            else if( $sTypeChamp == 'radio' && isset($objElement['mapping_champ_label']) && is_array($objElement['mapping_champ_label']) ) {
                                if( !$this->bActiveFormSelect ) {
                                    if( isset($objElement['mapping_champ_label'][$objValeurBddListe[$objElement['mapping_champ']]]) ) {
                                        $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objElement['mapping_champ_label'][$objValeurBddListe[$objElement['mapping_champ']]];
                                    } else {
                                        $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe[$objElement['mapping_champ']];
                                    }
                                } else {
                                    foreach( $objElement['mapping_champ_label'] as $key => $mappingParc ) {
                                        $allElements['lesitem'][$key] = $mappingParc;
                                    }
                                    $sRequeteSelectChecked = "SELECT " . $objElement['mapping_champ'] . " FROM " . $this->sNomTable . " WHERE " . $this->sChampId . "='" . $objValeurBddListe[$this->sChampId] . "'";
                                    $aTabChecked = $this->objClassGenerique->renvoi_info_requete($sRequeteSelectChecked);

                                    //echo"<pre>";print_r($aNewTab);echo"</pre>";										
                                    $aListe[$objValeurBddListe[$this->sChampId]]['checked'] = $aTabChecked[0][$objElement['mapping_champ']];

                                    $this->objSmarty->assign('allElements', $allElements);
                                }
                            } else if( $sTypeChamp == 'radio' && $this->bActiveFormSelect ) {
                                if( empty($objElement['lesitem']) ) {
                                    if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                                        $objElement['affichage_table_item'] = str_replace('<bdd>', $objElement['table_item'], $objElement['affichage_table_item']);
                                    }

                                    if( !empty($objElement['table_lien'][0]) ) {
                                        $sRequeteChecked = "SELECT " . $objElement['id_table_item'] . " FROM " . $objElement['table_lien'][0] . "
										WHERE " . $objElement['supplogique_table_lien'][0] . "='N' AND " . $objElement['mapping_champ'] . "='" . $objValeurBddListe[$this->sChampId] . "'";
                                        $aTabChecked = $this->objClassGenerique->renvoi_info_requete($sRequeteChecked);

                                        $aNewTab = array();
                                        if( !empty($aTabChecked) ) {
                                            $k = 0;
                                            foreach( $aTabChecked as $aTabCheckedParc ) {
                                                $aNewTab[$k] = $aTabCheckedParc[$objElement['id_table_item']];
                                                $k++;
                                            }
                                        }
                                        //echo"<pre>";print_r($aNewTab);echo"</pre>";
                                        $aListe[$objValeurBddListe[$this->sChampId]]['tableauChecked'] = $aNewTab;
                                    }

                                    $sRequete_liste = '
										SELECT ' . $objElement['id_table_item'] . ' AS iditem,' . $objElement['affichage_table_item'] . ' AS nomitem 
										FROM ' . $objElement['table_item'] . ' 
										WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\'';

                                    /*if($objElement['bdebug']=="ok")
										echo	$sRequete_liste."<br>";*/

                                    $aTableauliste = $this->objClassGenerique->renvoi_info_requete($sRequete_liste);

                                    $objElement['lesitem'] = array();

                                    if( !empty($aTableauliste) ) {
                                        foreach( $aTableauliste as $objTableauliste ) {
                                            $objElement['lesitem'][$objTableauliste['iditem']] = $objTableauliste['nomitem'];
                                        }
                                    }
                                }

                                if( isset($objElement['mapping_champ_label'][$objValeurBddListe[$objElement['mapping_champ']]]) ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]]['checked'] = $objElement['mapping_champ_label'][$objValeurBddListe[$objElement['mapping_champ']]];
                                } else {
                                    $aListe[$objValeurBddListe[$this->sChampId]]['checked'] = $objValeurBddListe[$objElement['mapping_champ']];
                                }

                                //echo"<pre>";print_r($aListe);echo"</pre>";
                                //echo"<pre>";print_r($objElement['lesitem']);echo"</pre>";
                                //$aListe[$objValeurBddListe[$this->sChampId]]=$objElement['lesitem'];
                                //echo $objValeurBddListe[$this->sChampId];
                                $allElements = $objElement;
                                $this->objSmarty->assign('allElements', $allElements);
                            } // ---------------- SELECT ----------------
                            else if( $sTypeChamp == 'select' && isset($objElement['lesitem']) && is_array($objElement['lesitem']) ) {

                                if( isset($objElement['lesitem'][$objValeurBddListe[$objElement['mapping_champ']]]) ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objElement['lesitem'][$objValeurBddListe[$objElement['mapping_champ']]];
                                } else {
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe[$objElement['mapping_champ']];

                                }
                            } else if( $sTypeChamp == 'select' && $this->bActiveFormSelect ) {
                                $sRequete_liste = '
									SELECT ' . $objElement['id_table_item'] . ',' . $objElement['affichage_table_item'] . ' AS nomitem 
									FROM ' . $objElement['table_item'] . ' 
									WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\'';

                                if( $objElement['select_autocomplete'] == "ok" )
                                    $sRequete_liste .= ' and ' . $objElement['id_table_item'] . '=\'' . $objElement['<valeur_variable></valeur_variable>'] . '\'';

                                if( $objElement['bdebug'] == "ok" )
                                    echo $sRequete_liste . "<br>";

                                $aTableauliste = $this->objClassGenerique->renvoi_info_requete($sRequete_liste);

                                $objElement['lesitem'] = array();

                                if( !empty($aTableauliste) ) {
                                    foreach( $aTableauliste as $objTableauliste ) {
                                        $objElement['lesitem'][$objTableauliste[$objElement['id_table_item']]] = $objTableauliste['nomitem'];
                                    }
                                }
                                //echo"<pre>";print_r($objElement);echo"</pre>";															
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe["mon_" . $objElement['nom_variable']];
                                $this->objSmarty->assign('listeElemSelect', $objElement['lesitem']);
                            } else {

                                //echo $objElement['nom_variable']."\n";

                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe["mon_" . $objElement['nom_variable']];

                            }
                            // ---------------- DATE/DATETIME ----------------
                        } else if( in_array($sTypeChamp, array( 'datetime', 'date' )) ) {
                            if( $objValeurBddListe["date_" . $objElement['mapping_champ']] != "0000-00-00" ) {
                                if( $objElement['date_conversion_enreg'] == "eng" )
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $this->objClassGenerique->renvoi_date($objValeurBddListe["date_" . $objElement['mapping_champ']], "fr");
                                else {
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe["date_" . $objElement['mapping_champ']];
                                }
                            } else {
                                $aListe[$objValeurBddListe[$this->sChampId]]["date_" . $objElement['nom_variable']] = '';
                            }
                            // ---------------- CHECKBOX ----------------
                        } else if( in_array($sTypeChamp, array( 'time' )) ) {
                            if( strtotime($objValeurBddListe[$objElement['mapping_champ']]) != false ) {
                                //echo "toto";
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = substr($objValeurBddListe[$objElement['mapping_champ']], 0, 5);
                            } else {
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = '';
                            }
                            // ---------------- CHECKBOX ----------------
                        } else if( in_array($sTypeChamp, array( 'color' )) ) {

                            //echo "toto";
                            $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = "<div style='height:30px;width:50%;background-color:" . $objValeurBddListe[$objElement['mapping_champ']] . "'>" . $objValeurBddListe[$objElement['mapping_champ']] . "</div>";

                            // ---------------- CHECKBOX ----------------
                        } else if( in_array($sTypeChamp, array( 'checkbox', 'selectmultiple' )) && !$this->bActiveFormSelect ) {

                            $sChaineId = "";
                            if( $objElement['injection_code'] != "" ) {
                                $sChaineId = $objElement['injection_code'];
                            }


                            if( empty($objElement['lesitem']) ) {
                                /*echo "toto";
								exit();*/
                                //$aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $this->renvoi_requete_table_lien($objElement, $objValeurBddListe['ch_'.$objElement['nom_variable']],$sChaineId);
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $this->renvoi_requete_table_lien($objElement, $objValeurBddListe[$this->sChampId], $sChaineId);

                            } else {
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objValeurBddListe[$objElement['mapping_champ']];
                            }
                        } else if( in_array($sTypeChamp, array( 'checkbox', 'selectmultiple' )) && $this->bActiveFormSelect ) {
                            $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $this->renvoi_requete_table_lien($objElement, $objValeurBddListe[$objElement['mapping_champ']], $sChaineId);

                            //echo"<pre>";print_r($aListe[$objValeurBddListe[$this->sChampId]]);echo"</pre>";

                            $sRequeteChecked = "SELECT " . $objElement['id_table_item'] . " FROM " . $objElement['table_lien'][0] . "
							WHERE " . $objElement['supplogique_table_lien'][0] . "='N' AND " . $objElement['mapping_champ'] . "='" . $objValeurBddListe[$this->sChampId] . "'";
                            //echo"<pre>";print_r($sRequeteChecked);echo"</pre>";
                            $aTabChecked = $this->objClassGenerique->renvoi_info_requete($sRequeteChecked);

                            $aNewTab = array();
                            if( !empty($aTabChecked) ) {
                                $k = 0;
                                foreach( $aTabChecked as $aTabCheckedParc ) {
                                    $aNewTab[$k] = $aTabCheckedParc[$objElement['id_table_item']];
                                    $k++;
                                }
                            }
                            //echo"<pre>";print_r($aNewTab);echo"</pre>";
                            $aListe[$objValeurBddListe[$this->sChampId]]['tableauChecked'] = $aNewTab;

                            if( empty($objElement['lesitem']) ) {
                                if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                                    $objElement['affichage_table_item'] = str_replace('<bdd>', $objElement['table_item'], $objElement['affichage_table_item']);
                                }
                                $sRequete_liste = '
									SELECT ' . $objElement['id_table_item'] . ' AS iditem,' . $objElement['affichage_table_item'] . ' AS nomitem 
									FROM ' . $objElement['table_item'] . ' 
									WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\'';

                                /*if($objElement['bdebug']=="ok")
									echo	$sRequete_liste."<br>";*/

                                $aTableauliste = $this->objClassGenerique->renvoi_info_requete($sRequete_liste);

                                $objElement['lesitem'] = array();

                                if( !empty($aTableauliste) ) {
                                    foreach( $aTableauliste as $objTableauliste ) {
                                        $objElement['lesitem'][$objTableauliste['iditem']] = $objTableauliste['nomitem'];
                                    }
                                }
                            }

                            //echo"<pre>";print_r($objElement['lesitem']);echo"</pre>";
                            //$aListe[$objValeurBddListe[$this->sChampId]]=$objElement['lesitem'];
                            //echo $objValeurBddListe[$this->sChampId];
                            $allElements = $objElement;
                            $this->objSmarty->assign('allElements', $allElements);

                        } // ---------------- INJECTION ----------------
                        else if( in_array($sTypeChamp, array( 'injection_code_liste' )) ) {
                            if( isset($objElement['injection_code']) ) {
                                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $objElement['injection_code'], $aMatches);
                                if( $iNombreMatches > 0 ) {
                                    $aChamp = array_unique($aMatches[1]);
                                    $sInjectionCode = $objElement['injection_code'];
                                    foreach( $aChamp as $sChamp ) {
                                        $prefix_file = '';
                                        foreach( $this->aElement as $objElementForm ) {
                                            if( isset($objElementForm['mapping_champ']) && $objElementForm['mapping_champ'] == $sChamp ) {
                                                if( $objElementForm['type_champ'] == 'file' ) {
                                                    if( isset($objElementForm['file_visu']) ) {
                                                        $prefix_file = $objElementForm['file_visu'];
                                                    }
                                                }
                                                break;
                                            }
                                        }
                                        $sInjectionCode = str_replace('{{$' . $sChamp . '}}', $prefix_file . $objValeurBddListe[$sChamp], $sInjectionCode);
                                    }
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $sInjectionCode;
                                } else {
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $objElement['injection_code'];
                                }
                            } else {
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = '';
                            }
                        } else if( in_array($sTypeChamp, array( 'file' )) ) {
                            $sNomFichier = $objValeurBddListe[$objElement['mapping_champ']];

                            if( $sNomFichier != null && $sNomFichier != '' ) {
                                if( preg_match("/\.png$/", $sNomFichier) === 1 || preg_match("/\.jpeg$/", $sNomFichier) === 1 || preg_match("/\.gif$/", $sNomFichier) === 1 ) {
                                    if( file_exists($objElement['file_upload'] . 'min_' . $sNomFichier) ) {
                                        $chemin_image_up = $objElement['file_upload'] . 'min_' . $sNomFichier;
                                    } else {
                                        $chemin_image_up = $objElement['file_upload'] . $sNomFichier;
                                    }

                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = '
										<div style="
											border-radius:6px;
											border:1px solid #ACACAC;
											padding:3px;
											display:inline-block;
											vertical-align:middle;
											' . (isset($objElement['file_aff_liste_couleur_fond']) ? 'background-color:' . $objElement['file_aff_liste_couleur_fond'] : '') . '"
										>
											<a href="' . $objElement['file_upload'] . $sNomFichier . '"  target="_blank"><img
												src="' . $chemin_image_up . '"
												style="display:inline-block;vertical-align:middle;"
												' . (isset($objElement['file_aff_liste_taille']) ? 'width="' . $objElement['file_aff_liste_taille'] . '"' : '') . '
											></a>
										</div>';
                                } else if( preg_match("/\.swf$/", $sNomFichier) === 1 ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = '
										<div style="
											border-radius:6px;
											border:1px solid #ACACAC;
											padding:3px;
											display:inline-block;
											vertical-align:middle;
											' . (isset($objElement['file_aff_liste_couleur_fond']) ? 'background-color:' . $objElement['file_aff_liste_couleur_fond'] : '') . '"
										>
											<object
												type="application/x-shockwave-flash"
												data="' . $objElement['file_visu'] . $sNomFichier . '"
												' . (isset($objElement['file_aff_liste_taille']) ? 'width="' . $objElement['file_aff_liste_taille'] . '"' : '') . '
											>
												<param name="movie" value="' . $objElement['file_visu'] . $sNomFichier . '" />
												<param name="wmode" value="transparent" />
												<p>swf non affichable</p>
											</object>
										</div><br><a href="' . $objElement['file_visu'] . $sNomFichier . '" target="_blank">Voir</a><br>';
                                } else {
                                    $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = '<a target="_blank" href="' . $objElement['file_visu'] . $sNomFichier . '">Voir le fichier</a>';
                                }
                            }
                        }
                        if( isset($aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']]) ) {
                            if( is_array($aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']]) ) {
                                $sValeurTotale = '';
                                foreach( $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] as $sValeur ) {
                                    $sValeurTotale .= $sValeur . '<br>';
                                }
                                $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = $sValeurTotale;
                            }
                        } else {
                            $aListe[$objValeurBddListe[$this->sChampId]][$objElement['nom_variable']] = '';
                        }
                    }

                    if( $this->bAffFiche ) {
                        $aListe[$objValeurBddListe[$this->sChampId]]['sUrlFiche'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId] . '&action=fiche';
                    }
                    if( $this->bAffMod ) {
                        $aListe[$objValeurBddListe[$this->sChampId]]['sUrlForm'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId] . '&action=form';
                    }
                    if( $this->bAffSupp ) {
                        $aListe[$objValeurBddListe[$this->sChampId]]['sUrlSupp'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId] . '&action=supp';
                    }
                    if( $this->bActiveFormSelect ) {
                        $aListe[$objValeurBddListe[$this->sChampId]]['sUrlFormList'] = $sBaseUrl . '&action=formList';
                        $this->objSmarty->assign('urlFormList', $aListe[$objValeurBddListe[$this->sChampId]]['sUrlFormList']);
                    }
                }
            }

            $this->aListe = $aListe;
        }

        return $this->aListe;
    }

    /**
     * Methode d'un champ renvoi valeur base de données
     * @author Danon Gnakouri
     * @since  2.0
     */
    public function renvoi_valeur_bdd()
    {

        //echo "passe";

        if( $this->iId != null ) {
            $sReq = $this->renvoi_requete('AND principal.' . $this->sChampId . '=\'' . $this->iId . '\'');
            $aTableauInfo = $this->objClassGenerique->renvoi_info_requete($sReq);

            //echo $sReq;

            if( isset($aTableauInfo[0]) ) {
                foreach( $this->aElement as &$objElement ) {
                    if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' && $objElement['traite_sql'] == 'ok' ) {
                        if( !in_array($objElement['type_champ'], array( 'checkbox', 'selectmultiple' )) ) {

                            if( $objElement['date_conversion_enreg'] == "eng" )
                                $objElement['valeur_variable'] = $this->objClassGenerique->renvoi_date("date_" . $aTableauInfo[0][$objElement['mapping_champ']], "fr");
                            else if( $objElement['type_champ'] == "date" or $objElement['type_champ'] == "datetime" ) {
                                $objElement['valeur_variable'] = stripcslashes($aTableauInfo[0]["date_" . $objElement['mapping_champ']]);

                            } else
                                $objElement['valeur_variable'] = stripcslashes($aTableauInfo[0][$objElement['mapping_champ']]);

                        } else {

                            //on conmpte le nombre de table intermediare si superieur à deux on arrete peut pas enregistrer
                            $iCounTableline = count($objElement['table_lien']);

                            if( $iCounTableline == 1 ) {
                                $sRequete = '
									SELECT lien.' . $objElement['id_item_table_lien'][0] . ' 
									FROM ' . $objElement['table_lien'][0] . ' lien 
									WHERE lien.' . $objElement['supplogique_table_lien'][0] . ' = \'N\' 
									AND lien.' . $objElement['id_table_lien'][0] . ' = \'' . $this->iId . '\'';

                                $aResultat = $this->objClassGenerique->renvoi_info_requete($sRequete);
                                $aRetour = array();

                                if( $aResultat ) {
                                    foreach( $aResultat as $objResultat ) {
                                        $aRetour[] = $objResultat[$objElement['id_item_table_lien'][0]];
                                    }
                                }

                                $objElement['valeur_variable'] = $aRetour;
                            }
                        }
                    }
                }
                unset($objElement);
            }
        }
    }

    /**
     * Methode d'un champ renvoi valeur du formulaire
     * @author Sébastien Robert
     * @since  1
     */
    public function renvoi_valeur_form()
    {
        if( !empty($_POST) ) {
            foreach( $this->aElement as &$objElement ) {
                if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {
                    if( isset($_POST[$objElement['nom_variable']]) ) {
                        //echo"<pre>";print_r($_POST[$objElement['nom_variable']]);echo"</pre>";
                        $objElement['valeur_variable'] = $_POST[$objElement['nom_variable']];
                    }
                }
            }
            unset($objElement);
        }
    }

    /**
     * Methode d'un champ renvoi du formulaire
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function renvoi_formulaire()
    {
        if( $this->sMessageErreurAjoutChamp != '' ) {
            echo $this->sMessageErreurAjoutChamp;
            exit(0);
        }

        $aForm = array();

        foreach( $this->aElement as $objElement ) {
            if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {

                if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' ) {
                    if( isset($_GET[$objElement['nom_variable']]) ) {
                        $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];
                    }
                }

                if( in_array($objElement['type_champ'], array( 'text', 'textarea', 'datetime', 'date', 'hidden', 'file', 'time', 'color', 'category', 'modifpassword', 'textautocomplete' )) ) {

                    if( $objElement['type_champ'] == 'modifpassword' ) {
                        if( empty($_GET[$this->sChampId]) ) {
                            $objElement['type_champ'] = "";
                        } else {
                            if( $objElement['injection_code'] != "" ) {
                                $sChaineId = str_replace("<id>", $_GET[$this->sChampId], $objElement['injection_code']);
                                $objElement['injection_code'] = $sChaineId;
                            }
                        }

                    }

                    $aForm[] = $objElement;
                }

                //formaulaire pour une select

                if( in_array($objElement['type_champ'], array( 'select' )) ) {
                    if( empty($objElement['lesitem']) ) {
                        if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                            $objElement['affichage_table_item'] = str_replace('<bdd>', $objElement['table_item'], $objElement['affichage_table_item']);
                        }
                        if( $objElement['type_champ'] == 'radio' && isset($objElement['mapping_champ_label']) && is_array($objElement['mapping_champ_label']) ) {
                            $objElement['lesitem'] = array();
                            foreach( $objElement['mapping_champ_label'] as $sCle => $sValeur ) {
                                $objElement['lesitem'][$sCle] = $sValeur;
                            }
                        } else {
                            $sRequete_liste = '
								SELECT ' . $objElement['id_table_item'] . ',' . $objElement['affichage_table_item'] . ' AS nomitem 
								FROM ' . $objElement['table_item'] . ' 
								WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\' ';

                            if( $objElement['select_autocomplete'] == "ok" )
                                $sRequete_liste .= ' and ' . $objElement['id_table_item'] . '=\'' . $objElement['valeur_variable'] . '\'';

                            if( $objElement['select_pere_fils'] != "" )
                                $sRequete_liste .= ' and ' . $objElement['select_pere_fils'] . '=\'0\' ';

                            $sRequete_liste .= $objElement['order_select'];

                            if( $objElement['bdebug'] == "ok" )
                                echo $sRequete_liste . "<br>";

                            $aTableauliste = $this->objClassGenerique->renvoi_info_requete($sRequete_liste);

                            $objElement['lesitem'] = array();

                            if( !empty($aTableauliste) ) {
                                foreach( $aTableauliste as $objTableauliste ) {
                                    $objElement['lesitem'][$objTableauliste[$objElement['id_table_item']]] = $objTableauliste['nomitem'];
                                    //quand on veut crée un decalage dans la liste déroulante
                                    if( $objElement['select_pere_fils'] != "" ) {
                                        $sRequete_liste_fils = '
                                            SELECT ' . $objElement['id_table_item'] . ',' . $objElement['affichage_table_item'] . ' AS nomitem 
                                            FROM ' . $objElement['table_item'] . ' 
                                            WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\' ';

                                        if( $objElement['select_autocomplete'] == "ok" )
                                            $sRequete_liste_fils .= ' and ' . $objElement['id_table_item'] . '=\'' . $objElement['valeur_variable'] . '\'';

                                        if( $objElement['select_pere_fils'] != "" )
                                            $sRequete_liste_fils .= ' and ' . $objElement['select_pere_fils'] . '=\'' . $objTableauliste[$objElement['id_table_item']] . '\' ';

                                        $sRequete_liste_fils .= $objElement['order_select'];
                                        $aTableaulistefils = $this->objClassGenerique->renvoi_info_requete($sRequete_liste_fils);

                                        if( !empty($aTableaulistefils) ) {
                                            foreach( $aTableaulistefils as $objTableaulistefils ) {
                                                $objElement['lesitem'][$objTableaulistefils[$objElement['id_table_item']]] = "**********" . $objTableaulistefils['nomitem'];
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                    if( empty($objElement['id_table_lien']) )
                        $aForm[] = $objElement;
                }


                if( in_array($objElement['type_champ'], array( 'checkbox', 'radio', 'selectmultiple' )) ) {
                    if( empty($objElement['lesitem']) ) {
                        if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                            $objElement['affichage_table_item'] = str_replace('<bdd>', $objElement['table_item'], $objElement['affichage_table_item']);
                        }
                        if( $objElement['type_champ'] == 'radio' && isset($objElement['mapping_champ_label']) && is_array($objElement['mapping_champ_label']) ) {
                            $objElement['lesitem'] = array();
                            foreach( $objElement['mapping_champ_label'] as $sCle => $sValeur ) {
                                $objElement['lesitem'][$sCle] = $sValeur;
                            }
                        } else {
                            $sRequete_liste = '
								SELECT ' . $objElement['id_table_item'] . ',' . $objElement['affichage_table_item'] . ' AS nomitem 
								FROM ' . $objElement['table_item'] . ' 
								WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\'';

                            /*if($objElement['bdebug']=="ok")
								echo	$sRequete_liste."<br>";*/

                            $aTableauliste = $this->objClassGenerique->renvoi_info_requete($sRequete_liste);

                            $objElement['lesitem'] = array();

                            if( !empty($aTableauliste) ) {
                                foreach( $aTableauliste as $objTableauliste ) {
                                    $objElement['lesitem'][$objTableauliste[$objElement['id_table_item']]] = $objTableauliste['nomitem'];
                                }
                            }
                        }
                    }
                    $aForm[] = $objElement;
                }
            }
        }

        return $aForm;
    }

    /**
     * Methode renvoi du formulaire recherche
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function renvoi_recherche()
    {

        $aRech = array();

        foreach( $this->aElement as $objElement ) {


            if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' && $objElement['aff_recherche'] == 'ok' ) {
                if( isset($_GET[$objElement['nom_variable']]) ) {
                    $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];
                    $aRech[] = $objElement;
                    //echo "toto";
                }
            }


            if( isset($objElement['aff_recherche']) && $objElement['aff_recherche'] == 'ok' ) {
                if( in_array($objElement['type_champ'], array( 'text', 'textarea', 'datetime', 'date', 'hidden' )) ) {
                    if( isset($_GET[$objElement['nom_variable']]) ) {
                        $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];

                        $objElement['aff_recherche'] = "ok";
                    }
                    $aRech[] = $objElement;
                }

                if( in_array($objElement['type_champ'], array( 'select', 'checkbox', 'radio', 'selectmultiple' )) ) {

                    if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                        $objElement['affichage_table_item'] = str_replace('<bdd>', $objElement['table_item'], $objElement['affichage_table_item']);
                    }

                    if( !isset($objElement['select_autocomplete']) || $objElement['select_autocomplete'] != 'ok' ) {
                        if( ($objElement['type_champ'] == 'radio' || ($objElement['type_champ'] == 'select' && !empty($objElement['lesitem'])) || ($objElement['type_champ'] == 'checkbox' && !empty($objElement['lesitem']))) && isset($objElement['mapping_champ_label']) && is_array($objElement['mapping_champ_label']) ) {
                            $objElement['lesitem'] = array();
                            foreach( $objElement['mapping_champ_label'] as $sCle => $sValeur ) {
                                $objElement['lesitem'][$sCle] = $sValeur;
                            }
                        } else {
                            $sRequete_liste = '';
                            if( isset($objElement['recherche_filtre_par_champ']) && $objElement['recherche_filtre_par_champ'] != '' ) {
                                $objFiltreChamp = null;
                                foreach( $aRech as $objRech ) {
                                    if( $objRech['nom_variable'] == $objElement['recherche_filtre_par_champ'] ) {
                                        $objFiltreChamp = $objRech;
                                        break;
                                    }
                                }
                                if( $objFiltreChamp !== null ) {
                                    if( $objElement['recherche_affiche_non_filtre_par_champ']
                                        || (isset($objFiltreChamp['valeur_variable'])
                                            && ((is_string($objFiltreChamp['valeur_variable']) && $objFiltreChamp['valeur_variable'] != '')
                                                || (is_array($objFiltreChamp['valeur_variable']) && count($objFiltreChamp['valeur_variable']) > 0)))
                                    ) {
                                        if( isset($objFiltreChamp['table_lien']) ) {
                                            $sRequeteJoin = '';
                                            $sRequeteWhere = '';
                                            if( is_string($objFiltreChamp['table_lien']) && $objFiltreChamp['table_lien'] == $objElement['table_item'] ) {
                                                $sRequeteJoin .= ' ' . $objFiltreChamp['type_table_join'] . ' ' . $objFiltreChamp['table_item'] . ' item2 ON item2.' . $objFiltreChamp['id_table_item'] . ' = item.' . $objFiltreChamp['id_item_table_lien'];
                                            } else if( is_array($objFiltreChamp['table_lien']) && count($objFiltreChamp['table_lien']) > 0 ) {
                                                for( $i = 0; $i < count($objFiltreChamp['table_lien']); $i++ ) {
                                                    if( $objFiltreChamp['table_lien'][$i] == $objElement['table_item'] ) {
                                                        if( $i == count($objFiltreChamp['table_lien']) - 1 ) {
                                                            $sRequeteJoin .= ' ' . $objFiltreChamp['type_table_join'] . ' ' . $objFiltreChamp['table_item'] . ' item2 ON item2.' . $objFiltreChamp['id_table_item'] . ' = item.' . $objFiltreChamp['id_item_table_lien'][$i];
                                                        } else {
                                                            for( $j = $i + 1; $j < count($objFiltreChamp['table_lien']); $j++ ) {
                                                                if( $j == $i + 1 ) {
                                                                    $sRequeteJoin .= ' ' . $objFiltreChamp['type_table_join'] . ' ' . $objFiltreChamp['table_lien'][$j] . ' lien' . $j . ' ON lien' . $j . '.' . $objFiltreChamp['id_table_item'][$j] . ' = item.' . $objFiltreChamp['id_item_table_lien'][$j];
                                                                } else {
                                                                    $sRequeteJoin .= ' ' . $objFiltreChamp['type_table_join'] . ' ' . $objFiltreChamp['table_lien'][$j] . ' lien' . $j . ' ON lien' . $j . '.' . $objFiltreChamp['id_table_lien'][$j] . ' = lien' . ($j - 1) . '.' . $objFiltreChamp['id_item_table_lien'][$j - 1];
                                                                }
                                                                $sRequeteWhere .= ' AND lien' . $j . '.' . $objFiltreChamp['supplogique_table_lien'][$j] . ' = \'N\'';
                                                            }
                                                            $sRequeteJoin .= ' ' . $objFiltreChamp['type_table_join'] . ' ' . $objFiltreChamp['table_item'] . ' item2 ON item2.' . $objFiltreChamp['id_table_item'] . ' = lien' . ($j - 1) . '.' . $objFiltreChamp['id_item_table_lien'][$j - 1];
                                                        }
                                                        break;
                                                    }
                                                }
                                            }

                                            $sRequete_liste = '
												SELECT ' . $objElement['id_table_item'] . ',' . $objElement['affichage_table_item'] . ' AS nomitem 
												FROM ' . $objElement['table_item'] . ' item ' . $sRequeteJoin . '
												WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\'' . $sRequeteWhere . '
												AND item2.' . $objFiltreChamp['supplogique_table_item'] . ' = \'N\'
												AND item2.' . $objFiltreChamp['id_table_item'] . ' ' . str_replace('<champ>', (is_array($objFiltreChamp['valeur_variable']) ? implode(', ', $objFiltreChamp['valeur_variable']) : $objFiltreChamp['valeur_variable']), $objElement['type_recherche']);
                                        }
                                    }
                                }
                            } else {
                                $sRequete_liste = '
									SELECT ' . $objElement['id_table_item'] . ',' . $objElement['affichage_table_item'] . ' AS nomitem 
									FROM ' . $objElement['table_item'] . ' 
									WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\'';

                                if( $objElement['select_pere_fils'] != "" ) {
                                    $sRequete_liste .= ' and  ' . $objElement['select_pere_fils'] . '=0 ';
                                }

                                $sRequete_liste .= $objElement['order_select'];
                            }

                            $objElement['lesitem'] = array();

                            if( $sRequete_liste != '' ) {
                                $aTableauliste = $this->objClassGenerique->renvoi_info_requete($sRequete_liste);
                                if( !empty($aTableauliste) ) {
                                    foreach( $aTableauliste as $objTableauliste ) {
                                        $objElement['lesitem'][$objTableauliste[$objElement['id_table_item']]] = $objTableauliste['nomitem'];

                                        //quand on veut crée un decalage dans la liste déroulante
                                        if( $objElement['select_pere_fils'] != "" ) {
                                            $sRequete_liste_fils = '
                                            SELECT ' . $objElement['id_table_item'] . ',' . $objElement['affichage_table_item'] . ' AS nomitem 
                                            FROM ' . $objElement['table_item'] . ' 
                                            WHERE ' . $objElement['supplogique_table_item'] . ' = \'N\' ';

                                            if( $objElement['select_autocomplete'] == "ok" )
                                                $sRequete_liste_fils .= ' and ' . $objElement['id_table_item'] . '=\'' . $objElement['valeur_variable'] . '\'';

                                            if( $objElement['select_pere_fils'] != "" )
                                                $sRequete_liste_fils .= ' and ' . $objElement['select_pere_fils'] . '=\'' . $objTableauliste[$objElement['id_table_item']] . '\' ';

                                            $sRequete_liste_fils .= $objElement['order_select'];
                                            $aTableaulistefils = $this->objClassGenerique->renvoi_info_requete($sRequete_liste_fils);

                                            if( !empty($aTableaulistefils) ) {
                                                foreach( $aTableaulistefils as $objTableaulistefils ) {
                                                    $objElement['lesitem'][$objTableaulistefils[$objElement['id_table_item']]] = "**********" . $objTableaulistefils['nomitem'];
                                                }
                                            }

                                        }


                                    }
                                }
                            }
                        }
                    }

                    if( isset($_GET[$objElement['nom_variable']]) ) {
                        $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];
                    }

                    $aRech[] = $objElement;
                }
            }
        }

        //echo"<pre>";print_r($aRech);echo"</pre>";

        return $aRech;
    }

    /**
     * Methode renvoi d'une pagination
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function pagination()
    {

        $sBaseUrl =
            $this->renvoi_base_url("pagination")
            . $this->renvoi_parametre_inter_module()
            . $this->renvoi_parametre_recherche()
            . $this->renvoi_parametre_filtre_liste()
            . $this->renvoi_parametre_retour_liste()
            . $this->sParametreWizardListe;

        $iPosition = (isset($_GET['iposition']) ? $_GET['iposition'] : '');

        $iNombreLigne = $this->renvoi_nombreligne_requete();

        $iNombreLignePage = $this->iNombrePage;

        $aPagination = array();
        $aPaginationSelect = array();
        $aPaginationListe = array();

        if( $iNombreLignePage == '' || $iNombreLignePage == '0' ) {
            $iNombreLignePage = '1';
        }

        if( $iPosition == '' ) {
            $iPosition = 0;
        }

        $iNombrePage = ceil($iNombreLigne / $iNombreLignePage);

        if( $iNombrePage > 1 ) {
            $iNumeroPage = ceil($iPosition / $iNombreLignePage);

            for( $i = 0; $i < $iNombrePage; $i++ ) {
                $aPaginationSelect[] = array(
                    'active' => 'ok',
                    'numero' => $i + 1,
                    'url' => $sBaseUrl . '&iposition=' . ($i * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage,
                    'selected' => ($i == $iNumeroPage)
                );
            }

            $aPaginationListe['page_premiere'] = $sBaseUrl . '&iposition=0&nbrpage=' . $iNombreLignePage;
            $aPaginationListe['page_derniere'] = $sBaseUrl . '&iposition=' . (($iNombrePage - 1) * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage;

            if( $iNumeroPage > 0 ) {
                $aPaginationListe['page_precedente'] = $sBaseUrl . '&iposition=' . (($iNumeroPage - 1) * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage;
            } else {
                $aPaginationListe['page_precedente'] = $aPaginationListe['page_premiere'];
            }

            if( $iNumeroPage < $iNombrePage - 1 ) {
                $aPaginationListe['page_suivante'] = $sBaseUrl . '&iposition=' . (($iNumeroPage + 1) * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage;
            } else {
                $aPaginationListe['page_suivante'] = $aPaginationListe['page_derniere'];
            }

            $iIntervalle = 2;
            $aPaginationListe['page_liste'] = array();

            for( $i = $iNumeroPage - $iIntervalle; $i <= $iNumeroPage + $iIntervalle; $i++ ) {
                if( $i >= 0 && $i <= $iNombrePage - 1 ) {
                    $aPaginationListe['page_liste'][] = array(
                        'numero' => $i + 1,
                        'url' => $sBaseUrl . '&iposition=' . ($i * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage,
                        'selected' => ($i == $iNumeroPage),
                        'totale' => $iNombrePage
                    );
                }
            }

            $aPagination['select'] = $aPaginationSelect;
            $aPagination['liste'] = $aPaginationListe;
        } else {
            $aPagination = null;
        }

        return $aPagination;
    }

    /**
     * Methode renvoi d'une requete selection
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function renvoi_requete($sRequetePere = '', $bCount = true)
    {

        $sRequeteSelect = 'SELECT principal.' . $this->sChampId . ' ';
        $sRequeteJoint = '';
        $sRequeteOrderBy = '';
        $iIndexTable = 1;

        foreach( $this->aElement as $objElement ) {

            if( ($objElement['aff_liste'] == 'ok' || $objElement['aff_form'] == 'ok') and $objElement['traite_sql'] == 'ok' ) {

                if( in_array($objElement['type_champ'], array( 'text', 'textarea', 'datetime', 'date', 'file', 'hidden', 'time', 'color' )) ) {

                    if( $objElement['type_champ'] == 'date' ) {
                        $sRequeteSelect .= ',DATE_FORMAT(principal.' . $objElement['mapping_champ'] . ', "' . $objElement['date_format'] . '") AS ' . "date_" . $objElement['mapping_champ'];
                    } else if( $objElement['type_champ'] == 'datetime' ) {
                        $sRequeteSelect .= ',DATE_FORMAT(principal.' . $objElement['mapping_champ'] . ', "' . $objElement['datetime_format'] . '") AS date_' . $objElement['mapping_champ'];
                    } else {
                        $sRequeteSelect .= ',principal.' . $objElement['mapping_champ'];
                    }

                    if( isset($_GET['filtreliste']) && $_GET['filtreliste'] == $objElement['nom_variable'] ) {
                        $sRequeteOrderBy = 'ORDER BY principal.' . $objElement['mapping_champ'] . ' ' . $_GET['typefiltreliste'];
                    }
                }

                /*if(in_array($objElement['type_champ'], array('checkbox', 'selectmultiple'))){
						$sRequeteSelect .= ',\''.$objElement['mapping_champ'].'\'';
					}*/

                if( in_array($objElement['type_champ'], array( 'radio' )) ) {

                    if( $objElement['type_champ'] == 'radio' && isset($objElement['lesitem']) && is_array($objElement['lesitem']) ) {
                        $sRequeteSelect .= ',principal.' . $objElement['mapping_champ'];

                        if( isset($_GET['filtreliste']) && $_GET['filtreliste'] == $objElement['nom_variable'] ) {
                            $sRequeteOrderBy = 'ORDER BY principal.' . $objElement['mapping_champ'] . ' ' . $_GET['typefiltreliste'];
                        }
                    } else {
                        $sCompositionSelect = 'bd' . $iIndexTable . '.' . $objElement['affichage_table_item'];

                        if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                            $sCompositionSelect = str_replace('<bdd>', 'bd' . $iIndexTable, $objElement['affichage_table_item']);
                        } else {
                            if( $objElement['alias_table_lien'] != "" ) {
                                $sCompositionSelect = $objElement['alias_table_lien'] . '.' . $objElement['affichage_table_item'];
                            }
                        }

                        $sRequeteSelect .= ',principal.' . $objElement['mapping_champ'] . ', ' . $sCompositionSelect . ' AS mon_' . $objElement['nom_variable'];
                        $sRequeteJoint .= ' ' . $objElement['type_table_join'] . ' ' . $objElement['table_item'] . ' AS bd' . $iIndexTable . ' ON bd' . $iIndexTable . '.' . $objElement['id_table_item'] . ' = principal.' . $objElement['mapping_champ'];
                        $iIndexTable++;

                        if( isset($_GET['filtreliste']) && $_GET['filtreliste'] == $objElement['nom_variable'] ) {
                            $sRequeteOrderBy = 'ORDER BY mon_' . $objElement['nom_variable'] . ' ' . $_GET['typefiltreliste'];
                        }
                    }
                }

                if( in_array($objElement['type_champ'], array( 'checkbox' )) ) {
                    $sRequeteSelect .= ',\'ch_' . $objElement['nom_variable'] . '\'';
                }

                // creation de la requete liste quand c'est un champ select
                if( in_array($objElement['type_champ'], array( 'select' )) ) {

                    if( $objElement['alias_table_lien'] != "" ) {
                        $saliasBase = $objElement['alias_table_lien'];
                    } else {
                        $saliasBase = 'bd' . $iIndexTable;
                    }

                    $sCompositionSelect = $saliasBase . '.' . $objElement['affichage_table_item'];

                    if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                        $sCompositionSelect = str_replace('<bdd>', $saliasBase, $objElement['affichage_table_item']);
                    }


                    if( empty($objElement['table_lien']) ) {

                        if( empty($objElement['lesitem']) ) {
                            $sRequeteJoint .= ' ' . $objElement['type_table_join'] . ' ' . $objElement['table_item'] . ' AS ' . $saliasBase . ' 	  
							ON ' . $saliasBase . '.' . $objElement['id_table_item'] . ' = principal.' . $objElement['mapping_champ'];
                        }

                        //$sRequeteSelect .= ;

                        if( empty($objElement['lesitem']) )
                            $sRequeteSelect .= ',principal.' . $objElement['mapping_champ'] . ',' . $sCompositionSelect . ' AS mon_' . $objElement['nom_variable'];
                        else {
                            $sRequeteSelect .= ',principal.' . $objElement['mapping_champ'] . ' AS ' . $objElement['nom_variable'];
                        }

                    } else {
                        $ucomptetablelien = 0;
                        foreach( $objElement['table_lien'] as $valueitem ) {

                            if( $ucomptetablelien == 0 ) {
                                $sRequeteJoint .= ' ' . $objElement['table_type_table_join'][$ucomptetablelien] . ' ' . $valueitem . ' AS ' . $saliasBase . '_' . $ucomptetablelien . '
								 ON ' . $saliasBase . '_' . $ucomptetablelien . '.' . $objElement['id_table_lien'][$ucomptetablelien] . ' = principal.' . $objElement['mapping_champ'] .
                                    ' and ' . $objElement['supplogique_table_lien'][$ucomptetablelien] . '=\'N\'';
                            } else {
                                $identifiant_alias_precedent = $ucomptetablelien - 1;
                                $sRequeteJoint .= ' ' . $objElement['table_type_table_join'][$ucomptetablelien] . ' ' . $valueitem . ' AS ' . $saliasBase . '_' . $ucomptetablelien . '
								 ON ' . $saliasBase . '_' . $ucomptetablelien . '.' . $objElement['id_item_table_lien'][$ucomptetablelien] . ' = ' .
                                    $saliasBase . '_' . $identifiant_alias_precedent . '.' . $objElement['id_item_table_lien'][$identifiant_alias_precedent];
                            }
                            $ucomptetablelien++;
                        }

                        $identifiant_alias_precedent = $ucomptetablelien - 1;
                        $sRequeteJoint .= ' ' . $objElement['type_table_join'] . ' ' . $objElement['table_item'] . ' AS ' . $saliasBase . '
						 ON ' . $saliasBase . '.' . $objElement['id_table_item'] . ' = ' .
                            $saliasBase . '_' . $identifiant_alias_precedent . '.' . $objElement['id_item_table_lien'][$ucomptetablelien];

                        $sCompositionSelect = $saliasBase . '.' . $objElement['affichage_table_item'];

                        $sRequeteSelect .= ',principal.' . $objElement['mapping_champ'] . ', ' . $sCompositionSelect . ' AS mon_' . $objElement['nom_variable'];

                    }
                    $iIndexTable++;

                }


                if( in_array($objElement['type_champ'], array( 'injection_code_liste' )) ) {
                    if( isset($objElement['injection_code']) ) {
                        $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $objElement['injection_code'], $aMatches);
                        if( $iNombreMatches > 0 ) {
                            $aChamp = array_unique($aMatches[1]);
                            foreach( $aChamp as $sChamp ) {
                                $sRequeteSelect .= ',principal.' . $sChamp;
                            }
                        }
                    }
                }
            }
        }

        $sRequeteSelect .= ' FROM ' . $this->sNomTable . ' AS principal ' . $sRequeteJoint;
        $sRequeteSelect .= ' WHERE principal.' . $this->sChampSupplogique . ' = \'N\' ' . $sRequetePere;
        $sRequeteSelect .= ' ' . $this->renvoi_requete_filtrage_recherche();
        $sRequeteSelect .= ' ' . $this->sFiltreliste;

        if( $sRequeteOrderBy == '' ) {
            $sRequeteSelect .= ' ' . $this->sFiltrelisteOrderBy;
        } else {
            $sRequeteSelect .= $sRequeteOrderBy;
        }

        //echo $sRequeteOrderBy.'<br/>'.$sRequeteSelect.'<br/><br/>';

        if( $this->bPagination && $bCount ) {
            if( isset($_GET['iposition']) ) {
                $sRequeteSelect .= ' LIMIT ' . $_GET['iposition'] . ',' . $this->iNombrePage;
            } else {
                $sRequeteSelect .= " LIMIT 0," . $this->iNombrePage;
            }
        }

        if( $this->bDebugRequete ) {
            $this->sDebugRequeteSelect = $sRequeteSelect;
        }

        return $sRequeteSelect;
    }

    /**
     * Methode renvoi_requete_filtrage_recherche genere la requete sql pour le filtrage du formulaire de recherche
     * @author Sebastien Robert
     */
    public function renvoi_requete_filtrage_recherche()
    {

        $sSqlFiltre = '';

        foreach( $this->aElement as $objElement ) {


            $sAliasBaseDonne = "principal";


            if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' ) {

                if( $objElement['alias_transfer'] != "" )
                    $sAliasBaseDonne = $objElement['alias_transfer'];
            }

            if( $objElement['aff_recherche'] == 'ok' || (isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok') ) {

                $aValeurFiltrage = isset($_GET[$objElement['nom_variable']]) ? $_GET[$objElement['nom_variable']] : (isset($objElement['valeur_variable']) ? $objElement['valeur_variable'] : '');

                if( ($objElement['type_champ'] == 'date' || $objElement['type_champ'] == 'datetime') && isset($objElement['recherche_intervalle_date'])
                    && $objElement['recherche_intervalle_date'] == 'ok'
                ) {

                    if( isset($aValeurFiltrage[0]) && $aValeurFiltrage[0] != '' ) {

                        if( isset($aValeurFiltrage[1]) && $aValeurFiltrage[1] != '' ) {

                            if( $objElement['type_champ'] == 'date' ) {
                                $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' BETWEEN \'' . $aValeurFiltrage[0] . '\' AND \'' . $aValeurFiltrage[1] . '\'';
                            } else {
                                $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' BETWEEN \'' . $aValeurFiltrage[0] . ' 00:00:00\' AND \'' . $aValeurFiltrage[1] . ' 23:59:59\'';
                            }

                        } else {

                            $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' >= \'' . $aValeurFiltrage[0] . '\'';
                        }
                    } else if( isset($aValeurFiltrage[1]) && $aValeurFiltrage[1] != '' ) {

                        $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' <= \'' . $aValeurFiltrage[1] . '\'';
                    }
                } else if( $objElement['type_champ'] == 'date' and $aValeurFiltrage != '' ) {

                    $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' = \'' . $this->objClassGenerique->renvoi_date($aValeurFiltrage, "eng") . '\'';
                } else if( $objElement['type_champ'] == 'select' && $objElement['traite_sql'] == 'ok' ) {
                    if( isset($aValeurFiltrage[0]) && $aValeurFiltrage[0] != '' ) {
                        //si nous sommes sur un lien direct dans la base de donné						
                        if( empty($objElement['id_table_lien']) ) {
                            $sTypeRech = str_replace('<champ>', $aValeurFiltrage, $objElement['type_recherche']);
                            //echo $sTypeRech;
                            //$sSqlFiltre .= ' AND '.$sAliasBaseDonne.'.'.$objElement['mapping_champ'].'=\''.$aValeurFiltrage.'\'';
                            $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' ' . $sTypeRech;
                            //echo $sSqlFiltre.'<br>';
                        } else {
                            $sSqlFiltre .= ' AND ' . $objElement['alias_table_lien'] . '.' . $objElement['id_table_item'] . '=\'' . $aValeurFiltrage . '\'';
                        }
                    }
                } else {
                    if( isset($objElement['table_lien']) && ($objElement['table_lien'] != '' || is_array($objElement['table_lien'])) ) {
                        if( (is_array($aValeurFiltrage) && count($aValeurFiltrage)) || ((is_string($aValeurFiltrage) || is_int($aValeurFiltrage)) && $aValeurFiltrage != '') ) {
                            if( is_string($aValeurFiltrage) || is_int($aValeurFiltrage) ) {
                                $aValeurFiltrage = array( $aValeurFiltrage );
                            }
                            if( is_string($objElement['table_lien']) ) {
                                $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' ' .
                                    str_replace('<champ>', '
										SELECT ' . $objElement['mapping_champ'] . ' 
										FROM ' . $objElement['table_lien'] . ' 
										WHERE ' . $objElement['supplogique_table_lien'] . ' = \'N\' 
										AND ' . $objElement['id_item_table_lien'] . ' IN (' . implode(',', $aValeurFiltrage) . ')'
                                        , $objElement['type_recherche']);
                            } else {
                                if(
                                    is_array($objElement['table_lien'])
                                    && is_array($objElement['supplogique_table_lien'])
                                    && is_array($objElement['id_table_lien'])
                                    && is_array($objElement['id_item_table_lien'])
                                    && count($objElement['table_lien']) > 0
                                    && count($objElement['table_lien']) == count($objElement['supplogique_table_lien'])
                                    && count($objElement['supplogique_table_lien']) == count($objElement['id_table_lien'])
                                    && count($objElement['id_table_lien']) == count($objElement['id_item_table_lien'])
                                ) {
                                    $sRequete = 'SELECT ' . $objElement['mapping_champ'];

                                    for( $i = 0; $i < count($objElement['table_lien']); $i++ ) {
                                        if( $i == 0 ) {
                                            $sRequete .= ' FROM ' . $objElement['table_lien'][$i] . ' lien' . $i;
                                        } else {
                                            $sRequete .= ' JOIN ' . $objElement['table_lien'][$i] . ' lien' . $i . ' ON lien' . $i . '.' . $objElement['id_table_lien'][$i] . ' = lien' . ($i - 1) . '.' . $objElement['id_item_table_lien'][$i - 1];
                                        }
                                    }

                                    $sRequete .= ' WHERE lien' . (count($objElement['table_lien']) - 1) . '.' . $objElement['id_item_table_lien'][count($objElement['table_lien']) - 1] . ' IN (' . implode(',', $aValeurFiltrage) . ')';

                                    for( $i = 0; $i < count($objElement['table_lien']); $i++ ) {
                                        $sRequete .= " AND lien" . $i . "." . $objElement['supplogique_table_lien'][$i] . " = 'N'";
                                    }

                                    $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' ' . str_replace('<champ>', $sRequete, $objElement['type_recherche']);

                                    //echo $sSqlFiltre."<br>";
                                }
                            }
                        }
                    } else {
                        if( is_string($aValeurFiltrage) || is_int($aValeurFiltrage) ) {

                            if( $aValeurFiltrage != '' && $objElement['traite_sql'] == 'ok' ) {

                                $sSqlFiltre .= ' AND ' . $sAliasBaseDonne . '.' . $objElement['mapping_champ'] . ' ' . str_replace('<champ>', $aValeurFiltrage, $objElement['type_recherche']);
                            }
                        }
                    }
                }
            }
        }

        return $sSqlFiltre;
    }

    /**
     * Methode nombre de ligne sur la requete
     * @author Danon Gnakouri
     * @since  2.0
     * @return inombre ligne
     */
    public function renvoi_nombreligne_requete()
    {
        $sLarequete = $this->renvoi_requete('', false);
        $aCount = $this->objClassGenerique->renvoi_nombreLigne_requete($sLarequete);

        return $aCount;
    }

    public function renvoi_requete_table_lien($objElement, $id, $injectionHtml = "")
    {

        if( is_string($objElement['table_lien']) ) {
            $sRequete = '
				SELECT DISTINCT item.' . $objElement['affichage_table_item'] . ' 
				FROM ' . $objElement['table_item'] . ' item 
				JOIN ' . $objElement['table_lien'] . ' lien 
				ON lien.' . $objElement['id_item_table_lien'] . ' = item.' . $objElement['id_table_item'] . ' 
				WHERE lien.' . $objElement['supplogique_table_lien'] . ' = \'N\' 
				AND lien.' . $objElement['mapping_champ'] . ' = \'' . $id . '\' 
				AND item.' . $objElement['supplogique_table_item'] . ' = \'N\'';

            if( $this->bDebugRequete )
                echo $sRequete . "<br>\n";

            $aResultat = $this->objClassGenerique->renvoi_info_requete($sRequete);

            $aRetour = array();

            if( $aResultat ) {
                foreach( $aResultat as $objResultat ) {
                    $aRetour[] = $objResultat[$objElement['affichage_table_item']];
                }
            }
        } else {
            if(
                is_array($objElement['table_lien'])
                && is_array($objElement['supplogique_table_lien'])
                && is_array($objElement['id_table_lien'])
                && is_array($objElement['id_item_table_lien'])
                && count($objElement['table_lien']) > 0
                && count($objElement['table_lien']) == count($objElement['supplogique_table_lien'])
                && count($objElement['supplogique_table_lien']) == count($objElement['id_table_lien'])
                && count($objElement['id_table_lien']) == count($objElement['id_item_table_lien'])
            ) {


                $sAlias = 'item';

                if( $objElement['alias_table_lien'] != "" )
                    $sAlias = $objElement['alias_table_lien'];

                $sRequetsuiteidlien = "";
                if( $objElement['id_liaison'] != "" )
                    $sRequetsuiteidlien = ',' . $objElement['id_liaison'] . ' as lid ';


                if( preg_match("/<bdd>/", $objElement['affichage_table_item']) ) {
                    $objElement['affichage_table_item'] = str_replace("<bdd>", $sAlias, $objElement['affichage_table_item']);
                }

                $sRequeteSelectFrom = 'SELECT DISTINCT ' . $objElement['affichage_table_item'] . ' as nom ' . $sRequetsuiteidlien . ' FROM ' . $objElement['table_item'] . ' as ' . $sAlias . ' ';
                $sRequeteJoin = '';


                for( $i = count($objElement['table_lien']) - 1; $i >= 0; $i-- ) {
                    if( $i == count($objElement['table_lien']) - 1 ) {
                        $sRequeteJoin .= ' JOIN ' . $objElement['table_lien'][$i] . ' lien' . $i . ' ON lien' . $i . '.' . $objElement['id_item_table_lien'][$i] . ' = ' . $sAlias . '.' . $objElement['id_table_item'];
                    } else {
                        $sRequeteJoin .= ' JOIN ' . $objElement['table_lien'][$i] . ' lien' . $i . ' ON lien' . $i . '.' . $objElement['id_item_table_lien'][$i] . ' = lien' . ($i + 1) . '.' . $objElement['id_table_lien'][$i + 1];
                    }

                }

                $sRequeteJoin .= ' JOIN ' . $this->sNomTable . ' principal ON lien0.' . $objElement['id_table_lien'][0] . ' = principal.' . $objElement['mapping_champ'];

                $sRequeteWhere = ' WHERE principal.' . $objElement['id_table_lien'][0] . ' = \'' . $id . '\' AND ' . $sAlias . '.' . $objElement['supplogique_table_item'] . ' = \'N\'';

                for( $i = count($objElement['table_lien']) - 1; $i >= 0; $i-- ) {
                    $sRequeteWhere .= ' AND lien' . $i . '.' . $objElement['supplogique_table_lien'][$i] . ' = \'N\'';
                }

                $sRequete = $sRequeteSelectFrom . $sRequeteJoin . $sRequeteWhere;

                $sRequete .= $objElement['requete_supplementaire_select'];

                if( $objElement['aff_debug'] == true )
                    echo $sRequete . "<br>\n";

                $aResultat = $this->objClassGenerique->renvoi_info_requete($sRequete);


                if( $this->bDebugRequete )
                    echo $sRequete;


                $aRetour = array();

                if( $aResultat ) {


                    foreach( $aResultat as $objResultat ) {

                        if( $injectionHtml != "" ) {
                            $objResultat['nom'] = str_replace("<value>", $objResultat['nom'], $injectionHtml);
                            $objResultat['nom'] = str_replace("<id>", $objResultat['lid'], $objResultat['nom']);
                        }

                        $aRetour[] = $objResultat['nom'];
                    }
                }
            } else {
                $aRetour = array();
            }
        }

        return $aRetour;
    }

    public function controle_form()
    {

        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        //$aTableauRetour['message'][]="vide";

        $aValidationSupplementaireFormulaire = $this->validation_supplementaire_formulaire();

        if( is_array($aValidationSupplementaireFormulaire) && isset($aValidationSupplementaireFormulaire[0]) && isset($aValidationSupplementaireFormulaire[1]) ) {
            if( $aValidationSupplementaireFormulaire[1] != null && $aValidationSupplementaireFormulaire[1] != '' ) {
                if( $aValidationSupplementaireFormulaire[0] === true ) {
                    $this->sMessageSuccesForm = $aValidationSupplementaireFormulaire[1];
                } else {
                    $this->sMessageErreurForm = $aValidationSupplementaireFormulaire[1];
                }
            }
        }

        if( empty($_POST) ) {
            $aTableauRetour['result'] = false;
        }

        foreach( $this->aElement as $objElement ) {

            if( $objElement['double_password'] == 'ok' ) {
                if( !isset($_POST[$objElement['nom_variable']]) || ($_POST[$objElement['nom_variable']] != $_POST[$objElement['nom_variable'] . '_2']) ) {
                    $aTableauRetour['result'] = false;
                    $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                }
            }

            if( $objElement['aff_form'] == 'ok' ) {

                if( $objElement['type_champ'] == 'text' ) {
                    if( $objElement['ctrl_champ'] == 'ok' ) {

                        if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' ) {
                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }
                    }
                } elseif( $objElement['type_champ'] == 'textautocomplete' ) {
                    if( $objElement['ctrl_champ'] == 'ok' ) {

                        if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' ) {
                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }
                    }
                } else if( $objElement['type_champ'] == 'checkbox' or $objElement['type_champ'] == 'selectmultiple' ) {
                    if( $objElement['ctrl_champ'] == 'ok' ) {
                        if( !isset($_POST[$objElement['nom_variable']]) || count($_POST[$objElement['nom_variable']]) == 0 ) {
                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }
                    }
                    if( isset($_POST[$objElement['nom_variable']]) && (count($_POST[$objElement['nom_variable']]) > 0 && count($_POST[$objElement['nom_variable']]) != count($this->objClassGenerique->renvoi_info_requete('SELECT ' . $objElement['id_table_item'] . ' FROM ' . $objElement['table_item'] . ' WHERE ' . $objElement['id_table_item'] . ' IN (' . implode(',', $_POST[$objElement['nom_variable']]) . ')'))) ) {
                        $aTableauRetour['result'] = false;
                        $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                    }
                } else if( $objElement['type_champ'] == 'file' ) {
                    if( $objElement['ctrl_champ'] == 'ok' ) {
                        if( !isset($_FILES[$objElement['nom_variable']]) || !isset($_FILES[$objElement['nom_variable']]['tmp_name']) || $_FILES[$objElement['nom_variable']]['tmp_name'] == '' ) {
                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }
                    }
                } else if( $objElement['type_champ'] == 'date' ) {
                    if( $objElement['date_now_creation'] == '' && $objElement['date_now_modification'] == '' ) {
                        if( $objElement['ctrl_champ'] == 'ok' ) {
                            if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' || strtotime($_POST[$objElement['nom_variable']]) == false ) {
                                $aTableauRetour['result'] = false;
                                $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                            }
                        } else {
                            //convertir si la date passer et en français
                            if( $objElement['date_conversion_enreg'] == "eng" ) {
                                if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] != '' &&
                                    strtotime($this->objClassGenerique->renvoi_date($_POST[$objElement['nom_variable']], "eng")) == false
                                ) {
                                    $aTableauRetour['result'] = false;
                                    $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                                }
                            } else {
                                if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] != '' && strtotime($_POST[$objElement['nom_variable']]) == false ) {
                                    $aTableauRetour['result'] = false;
                                    $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                                }
                            }
                        }
                    }
                } else if( $objElement['type_champ'] == 'datetime' ) {
                    if( $objElement['date_now_creation'] == '' && $objElement['date_now_modification'] == '' ) {
                        if( $objElement['ctrl_champ'] == 'ok' ) {
                            if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' || strtotime($_POST[$objElement['nom_variable']]) == false ) {
                                $aTableauRetour['result'] = false;
                                $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                            }
                        } else {
                            if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] != '' && strtotime($_POST[$objElement['nom_variable']]) == false ) {
                                $aTableauRetour['result'] = false;
                                $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                            }
                        }
                    }
                } else if( $objElement['type_champ'] == 'select' ) {
                    if( $objElement['ctrl_champ'] == 'ok' ) {
                        if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' ) {
                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }

                        if( empty($objElement['lesitem']) ) {
                            if( !isset($_POST[$objElement['nom_variable']]) || ($_POST[$objElement['nom_variable']] != '' && !$this->objClassGenerique->renvoi_info_requete('SELECT ' . $objElement['id_table_item'] . ' FROM ' . $objElement['table_item'] . ' WHERE ' . $objElement['id_table_item'] . ' = \'' . $_POST[$objElement['nom_variable']] . '\'')) ) {
                                $aTableauRetour['result'] = false;
                                $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                            }
                        }
                    }
                } else if( $objElement['type_champ'] == 'radio' ) {
                    if( $objElement['ctrl_champ'] == 'ok' ) {
                        if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' ) {
                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }
                    }
                    if( isset($objElement['mapping_champ_label']) && is_array($objElement['mapping_champ_label']) ) {
                        if( !isset($_POST[$objElement['nom_variable']]) || ($_POST[$objElement['nom_variable']] != '' && !isset($objElement['mapping_champ_label'][$_POST[$objElement['nom_variable']]])) ) {

                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }
                    } else {
                        if( !isset($_POST[$objElement['nom_variable']]) || ($_POST[$objElement['nom_variable']] != '' && !$this->objClassGenerique->renvoi_info_requete('SELECT ' . $objElement['id_table_item'] . ' FROM ' . $objElement['table_item'] . ' WHERE ' . $objElement['id_table_item'] . ' = \'' . $_POST[$objElement['nom_variable']] . '\'')) ) {

                            $aTableauRetour['result'] = false;
                            $aTableauRetour['message'][] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
                        }
                    }
                } else {
                    $vide = "";
                }

            }
        }


        if( is_array($aValidationSupplementaireFormulaire) && isset($aValidationSupplementaireFormulaire[0]) ) {
            if( $aValidationSupplementaireFormulaire[0] === false ) {

                $aTableauRetour['result'] = false;
                $aTableauRetour['message'][] = $aValidationSupplementaireFormulaire[1];
            }
        }

        return $aTableauRetour;
    }

    public function enreg_modif_form($sRequeteplus = '')
    {
        $aKeyValue = array();
        $aTabLien = array();

        foreach( $this->aElement as &$objElement ) {
            if( $objElement['traite_sql'] == 'ok' ) {
                if( $objElement['aff_form'] == 'ok' ) {
                    if( isset($_POST[$objElement['nom_variable']]) ) {
                        $objElement['valeur_variable'] = $_POST[$objElement['nom_variable']];
                    } else if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' ) {
                        if( isset($_GET[$objElement['nom_variable']]) ) {
                            $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];
                        }
                    } else {
                        $objElement['valeur_variable'] = '';
                    }
                }

                if( $objElement['type_champ'] == 'checkbox' or $objElement['type_champ'] == 'selectmultiple' ) {
                    if( $this->iId ) {
                        $date_modification_table_lien = '';
                        if( isset($objElement['date_modification_table_lien']) && $objElement['date_modification_table_lien'] != '' ) {
                            $date_modification_table_lien = ', ' . $objElement['date_modification_table_lien'] . ' = \'' . date('Y-m-d H:i:s') . '\'';
                        }
                        $this->objClassGenerique->executionRequete('UPDATE ' . $objElement['table_lien'][0] . ' SET ' . $objElement['supplogique_table_lien'][0] . ' = \'Y\'' . $date_modification_table_lien . ' WHERE ' . $objElement['supplogique_table_lien'][0] . ' = \'N\' AND ' . $objElement['id_table_lien'][0] . '=\'' . $this->iId . '\'');
                    }
                    $aTabLien[] = $objElement;
                } else if( $objElement['type_champ'] == 'datetime' ) {
                    if( ($objElement['date_now_creation'] == 'ok' && !$this->iId) || ($objElement['date_now_modification'] == 'ok') ) {
                        $objElement['valeur_variable'] = date('Y-m-d H:i:s');
                        $aKeyValue[] = $objElement['mapping_champ'] . '=\'' . $objElement['valeur_variable'] . '\'';
                    } else if( $objElement['date_now_creation'] != 'ok' && $objElement['date_now_modification'] != 'ok' ) {
                        $aKeyValue[] = $objElement['mapping_champ'] . '=\'' . $objElement['valeur_variable'] . '\'';
                    }
                } else if( $objElement['type_champ'] == 'date' ) {
                    if( ($objElement['date_now_creation'] == 'ok' && !$this->iId) || ($objElement['date_now_modification'] == 'ok') ) {
                        $objElement['valeur_variable'] = date('Y-m-d');
                        $aKeyValue[] = $objElement['mapping_champ'] . '= \'' . $objElement['valeur_variable'] . '\'';

                    } else if( $objElement['date_now_creation'] != 'ok' && $objElement['date_now_modification'] != 'ok' ) {
                        if( $objElement['valeur_variable'] != '' ) {
                            if( $objElement['aff_form'] == 'ok' ) {
                                if( $objElement['date_conversion_enreg'] == 'eng' ) {
                                    $aKeyValue[] = $objElement['mapping_champ'] . '= \'' . $this->objClassGenerique->renvoi_date($objElement['valeur_variable'], "eng") . '\'';
                                } else {
                                    $aKeyValue[] = $objElement['mapping_champ'] . '= \'' . addslashes($objElement['valeur_variable']) . '\'';
                                }
                            }
                        } else {
                            if( $objElement['aff_form'] == 'ok' )
                                $aKeyValue[] = $objElement['mapping_champ'] . '= \'\'';
                        }
                    }
                } else if( ($objElement['type_champ'] == 'select' or $objElement['type_champ'] == 'hidden') and $objElement['aff_form'] == "ok" ) {

                    //echo "totoo =>".$objElement['text_label']."\n" ;

                    if( $objElement['valeur_variable'] != '' ) {
                        $aKeyValue[] = $objElement['mapping_champ'] . '=\'' . addslashes($objElement['valeur_variable']) . '\'';
                    } else {
                        $aKeyValue[] = $objElement['mapping_champ'] . '= \'\'';
                    }
                } else if( $objElement['type_champ'] == 'file' ) {
                    //echo"<pre>";print_r($_FILES);echo"</pre>";
                    if( isset($_FILES[$objElement['nom_variable']]) && isset($_FILES[$objElement['nom_variable']]['tmp_name']) && $_FILES[$objElement['nom_variable']]['tmp_name'] != '' ) {
                        $content_dir = $objElement['file_upload'];
                        $idrim = rand(1, 100);
                        $tmp_file = $_FILES[$objElement['nom_variable']]['tmp_name'];
                        $ext_file = '';
                        if( $_FILES[$objElement['nom_variable']]['type'] == 'text/html' ) {
                            $ext_file = '.html';
                        } else {
                            $aTableauDecoupe = getimagesize($tmp_file);
                            //print_r($tmp_file.'/'.$_FILES[$objElement['nom_variable']]['name']);
                            if( $aTableauDecoupe['mime'] == "application/x-shockwave-flash" ) {
                                $ext_file = ".swf";
                            } else if( $aTableauDecoupe['mime'] == "image/png" ) {
                                $ext_file = ".png";
                            } else if( $aTableauDecoupe['mime'] == "image/jpeg" ) {
                                $ext_file = ".jpeg";
                            } else if( $aTableauDecoupe['mime'] == "image/gif" ) {
                                $ext_file = ".gif";
                            } else if( $aTableauDecoupe['mime'] == "image/webp" ) {
                                $ext_file = ".webp";
                            } else if( $_FILES[$objElement['nom_variable']]['type'] == 'application/pdf' ) {
                                $ext_file = '.pdf';
                            }
                        }

                        $imageprincip = date('dmYHms') . "_" . $this->sNomTable . $idrim . $ext_file;
                        if( $ext_file == '.swf' || $ext_file == '.png' || $ext_file == '.jpeg' || $ext_file == '.gif' || $ext_file == '.webp' ) {
                            $imageprincipmin = 'min_' . date('dmYHms') . "_" . $this->sNomTable . $idrim . $ext_file;
                        }

                        move_uploaded_file($tmp_file, $content_dir . $imageprincip);
                        if( $ext_file == '.swf' || $ext_file == '.png' || $ext_file == '.jpeg' || $ext_file == '.gif' || $ext_file == '.webp' ) {
                            copy($content_dir . $imageprincip, $content_dir . $imageprincipmin);
                        }

                        //Compression
                        /* ----------------------------------- Miniature -------------------------------------*/
                        if( $objElement['file_compress_min'] == 'ok' ) {
                            if( !empty($objElement['file_taux_compress_min']) && is_numeric($objElement['file_taux_compress_min']) ) {
                                $iTauxCompress_min = $objElement['file_taux_compress_min'];
                            } else {
                                $iTauxCompress_min = 70;
                            }

                            if( $aTableauDecoupe['mime'] == "image/png" ) {
                                $iTauxCompress_min = round($iTauxCompress_min * 9 / 100);
                                $im_min = imagecreatefrompng($content_dir . $imageprincipmin);
                                imageAlphaBlending($im_min, true);
                                imageSaveAlpha($im_min, true);
                                imagepng($im_min, $content_dir . $imageprincipmin, $iTauxCompress_min);
                            } else if( $aTableauDecoupe['mime'] == "image/jpeg" ) {
                                imagejpeg(imagecreatefromjpeg($content_dir . $imageprincipmin), $content_dir . $imageprincipmin, $iTauxCompress_min);
                            }
                        }

                        /* ----------------------------------- Image originale -------------------------------------*/
                        if( $objElement['file_compress'] == 'ok' ) {
                            if( !empty($objElement['file_taux_compress']) && is_numeric($objElement['file_taux_compress']) ) {
                                $iTauxCompress = $objElement['file_taux_compress'];
                            } else {
                                $iTauxCompress = 90;
                            }

                            if( $aTableauDecoupe['mime'] == "image/png" ) {
                                $iTauxCompress = round($iTauxCompress * 9 / 100);
                                $im = imagecreatefrompng($content_dir . $imageprincip);
                                imageAlphaBlending($im, true);
                                imageSaveAlpha($im, true);
                                imagepng($im, $content_dir . $imageprincip, $iTauxCompress);
                            } else if( $aTableauDecoupe['mime'] == "image/jpeg" ) {
                                imagejpeg(imagecreatefromjpeg($content_dir . $imageprincip), $content_dir . $imageprincip, $iTauxCompress);
                            }
                        }

                        $aKeyValue[] = $objElement['mapping_champ'] . '=\'' . $imageprincip . '\'';
                    } else {
                        if( $objElement['Activdefaut'] == "ok" ) {
                            $aKeyValue[] = $objElement['mapping_champ'] . '=\'' . $objElement['filedefaut'] . '\'';
                        }
                    }

                } else {
                    if( $objElement['aff_form'] == "ok" and !in_array($objElement['type_champ'], array( 'checkbox', 'selectmultiple' )) ) {
                        if( $objElement['double_password'] == "ok" ) {
                            if( $objElement['type_encodage'] == "phpbb_hash" ) {
                                $aKeyValue[] = $objElement['mapping_champ'] . '=\'' . $this->objClassGenerique->phpbb_hash($objElement['valeur_variable']) . '\'';
                            } else {
                                $aKeyValue[] = $objElement['mapping_champ'] . '=PASSWORD(\'' . $objElement['valeur_variable'] . '\')';
                            }
                        } else {
                            $aKeyValue[] = $objElement['mapping_champ'] . '=\'' . addslashes($objElement['valeur_variable']) . '\'';
                        }
                    }
                }

            }
        }
        unset($objElement);

        //$aKeyValue[] = $this->sChampSupplogique.'=\'N\'';

        if( $this->iId ) {
            $sRequeteUpdate = '
				UPDATE ' . $this->sNomTable . ' 
				SET ' . implode(',', $aKeyValue) . ' ' . $this->sSuiteRequeteUpdate . ' 
				WHERE ' . $this->sChampId . '=\'' . $this->iId . '\'';

            $bresultat = $this->objClassGenerique->executionRequete($sRequeteUpdate);

            if( $bresultat ) {
                $this->aTabResultUpdate['id'] = $this->iId;
                $this->aTabResultUpdate['result'] = "ok";
                if( isset($this->sScriptJavascriptUpdate) && !empty($this->sScriptJavascriptUpdate) )
                    $this->objSmarty->assign('sScriptJavascriptUpdate', $this->sScriptJavascriptUpdate);
            }

            //echo $sRequeteUpdate."<br>";
            $iId = $this->iId;

            if( $this->bDebugRequete ) {
                $this->sDebugRequeteInsertUpdate = $sRequeteUpdate;
            }
        } else {
            $sRequeteInsert = '
				INSERT ' . $this->sNomTable . ' 
				SET ' . implode(',', $aKeyValue) . ' ' . $this->sSuiteRequeteInsert . ' on duplicate key update ' . $this->sChampSupplogique . '= \'N\' ';
            //echo $sRequeteInsert;
            $iId = $this->objClassGenerique->executionRequeteId($sRequeteInsert);

            if( $iId != false ) {
                $this->aTabResultInsert['id'] = $iId;
                $this->aTabResultInsert['result'] = "ok";
                //echo $this->sScriptJavascriptInsert;exit;
                if( isset($this->sScriptJavascriptInsert) && !empty($this->sScriptJavascriptInsert) )
                    $this->objSmarty->assign('sScriptJavascriptInsert', $this->sScriptJavascriptInsert);
            }

            if( $this->bDebugRequete ) {
                $this->sDebugRequeteInsertUpdate = $sRequeteInsert;
            }
        }

        foreach( $aTabLien as $objTabLien ) {
            if( is_array($objTabLien['valeur_variable']) && count($objTabLien['valeur_variable']) > 0 ) {
                foreach( $objTabLien['valeur_variable'] as $objValeur ) {
                    $date_modification_table_lien = '';
                    if( isset($objTabLien['date_modification_table_lien']) && $objTabLien['date_modification_table_lien'] != '' ) {
                        $date_modification_table_lien = ', ' . $objTabLien['date_modification_table_lien'] . ' = \'' . date('Y-m-d H:i:s') . '\'';
                    }

                    $sRequete_checkbox = "INSERT " . $objTabLien['table_lien'][0] . " SET " . $objTabLien['id_table_lien'][0] . "='" . $iId . "'
					, " . $objTabLien['id_item_table_lien'][0] . " = '" . $objValeur . "', " . $objTabLien['supplogique_table_lien'][0] . " = 'N'"
                        . $date_modification_table_lien . " ON DUPLICATE KEY update " . $objTabLien['supplogique_table_lien'][0] . " = 'N'"
                        . $date_modification_table_lien;

                    //echo"<pre>";print_r($sRequete_checkbox);echo"</pre>";

                    $this->objClassGenerique->executionRequete($sRequete_checkbox);

                }
            }
        }

        return $iId;
    }

    public function suppression_entree()
    {

        //echo "id=>".$this->iId;

        if( $this->iId == null ) {
            return false;
        }

        $sSuiterequetesupp = "";
        foreach( $this->aElement as $objElement ) {
            if( $objElement['traite_sql'] == 'ok' ) {
                if( $objElement['type_champ'] == 'checkbox' ) {
                    $date_modification_table_lien = '';
                    if( isset($objElement['date_modification_table_lien']) && $objElement['date_modification_table_lien'] != '' ) {
                        $date_modification_table_lien = $objElement['date_modification_table_lien'] . ' = \'' . date('Y-m-d H:i:s') . '\'';
                    }
                }
            }


            if( $objElement['date_now_modification'] == 'ok' ) {
                $objElement['valeur_variable'] = date('Y-m-d H:i:s');
                $sSuiterequetesupp = "," . $objElement['mapping_champ'] . '=\'' . $objElement['valeur_variable'] . '\'';

            }

        }


        $sRequete_supp = "UPDATE " . $this->sNomTable . " SET " . $this->sChampSupplogique . " = 'Y' " . $sSuiterequetesupp . " WHERE " . $this->sChampId . " = '" . $this->iId . "'";

        //echo "toto=>".$sRequete_supp."<br>";

        if( $this->objClassGenerique->executionRequete($sRequete_supp) ) {
            $this->aTabResultSupp['id'] = $this->iId;
            $this->iId = null;
            $this->aTabResultSupp['result'] = "ok";

            return true;
        }

        return false;
    }

    public function config_wizard($aWizard)
    {

        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            if( isset($aWizard['valider']) ) {

                $this->itemBoutonsForm['valider'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Valider',
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );

                $this->sRetourValidationForm = 'externe';
                $this->sRetourValidationFormExterne = $aWizard['valider'];
            }

            if( isset($aWizard['annuler']) ) {

                $this->itemBoutonsForm['annuler'] = array(
                    'type' => 'bouton',
                    'nom_variable' => 'btannuler',
                    'text_label' => 'Annuler',
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );

                $this->sRetourAnnulationForm = 'externe';
                $this->sRetourAnnulationFormExterne = $aWizard['annuler'];
                $this->itemBoutonsForm['annuler']['url'] = $this->renvoi_url_retour_annulation_form_externe();
            }

            if( isset($aWizard['precedent']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['precedent'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['precedent'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['precedent']);
                        }
                    }
                }

                $this->itemBoutonsForm['precedent'] = array(
                    'type' => 'bouton',
                    'nom_variable' => '',
                    'text_label' => 'Précédent',
                    'url' => $aWizard['precedent'],
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['suivant']) ) {
                $this->sRetourValidationForm = 'externe';
                $this->sRetourValidationFormExterne = $aWizard['suivant'];
                $this->itemBoutonsForm['suivant'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Suivant',
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }
        } else {

            $this->sParametreWizardListe = '';
            $this->aParametreWizardListe = array();

            $aBoutonLabel = array( 'valider', 'annuler', 'precedent', 'suivant' );

            $aParametreSuivi = array( 'wizard_type_etape', 'wizard_etape' );

            foreach( $aBoutonLabel as $sBoutonLabel ) {
                if( isset($aWizard[$sBoutonLabel]) ) {
                    $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard[$sBoutonLabel], $aMatches);
                    if( $iNombreMatches > 0 ) {
                        $aChamp = array_unique($aMatches[1]);
                        foreach( $aChamp as $sChamp ) {
                            if( $this->aParamsNonSuivi != $sChamp ) {
                                //if($sChamp!=$this->aParamsNonSuivi)
                                $aParametreSuivi[] = $sChamp;
                            }
                        }
                    }
                }
            }

            $aParametreSuivi = array_unique($aParametreSuivi);

            foreach( $aParametreSuivi as $sParametreSuivi ) {
                if( isset($_GET[$sParametreSuivi]) ) {
                    $this->sParametreWizardListe .= '&' . $sParametreSuivi . '=' . $_GET[$sParametreSuivi];
                    $this->aParametreWizardListe[$sParametreSuivi] = $_GET[$sParametreSuivi];
                }
            }

            if( isset($aWizard['valider']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['valider'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['valider'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['valider']);
                        }
                    }
                }

                $this->itemBoutonsListe['valider'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Valider',
                    'url' => $aWizard['valider'],
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['annuler']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['annuler'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['annuler'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['annuler']);
                        }
                    }
                }

                $this->itemBoutonsListe['annuler'] = array(
                    'type' => 'bouton',
                    'nom_variable' => 'btannuler',
                    'text_label' => 'Annuler',
                    'url' => $aWizard['annuler'],
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['precedent']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['precedent'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['precedent'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['precedent']);
                        }
                    }
                }

                $this->itemBoutonsListe['precedent'] = array(
                    'type' => 'bouton',
                    'nom_variable' => '',
                    'text_label' => 'Précédent',
                    'url' => $aWizard['precedent'],
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['suivant']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['suivant'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['suivant'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['suivant']);
                        }
                    }
                }

                $this->itemBoutonsListe['suivant'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Suivant',
                    'url' => $aWizard['suivant'],
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }
        }
    }

    public function obtenir_id()
    {
        return $this->iId;
    }

    public function obtenir_champ_id()
    {
        return $this->sChampId;
    }

    public function obtenir_categorie()
    {
        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            return $this->sCategorieForm;
        } else {
            return $this->sCategorieListe;
        }
    }

    public function config_wizard_categorie($sCategorie)
    {
        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            $this->sCategorieForm = $sCategorie;
        } else {
            $this->sCategorieListe = $sCategorie;
        }
    }

    public function config_wizard_desactivation_popup()
    {
        $this->bFormPopup = false;
    }

    public function add_fils($sDirFils, $sChampIdPere = null)
    {

        $sChemin = $this->objClassGenerique->renvoi_info_requete('SELECT chemin_listmenu AS chemin FROM menu_listemenu WHERE ledir_listmenu = \'' . $sDirFils . '\'');
        $sChemin = (isset($sChemin[0]) && $sChemin[0]['chemin']) ? $sChemin[0]['chemin'] : null;

        if( $sChemin !== null ) {
            if( $sChampIdPere === null ) {
                $sChampIdPere = $this->sChampId;
            }
            $this->aListeFils[] = array( 'sChemin' => $sChemin, 'sChampIdPere' => $sChampIdPere, 'sDir' => $sDirFils );
        }

        return $this;
    }

    public function renvoi_si_filtre_recherche_actif($aRech)
    {

        foreach( $aRech as $objRech ) {
            if( !isset($objRech['recherche_filtre_actif']) || $objRech['recherche_filtre_actif'] !== false ) {
                if( isset($objRech['valeur_variable']) ) {
                    if( is_string($objRech['valeur_variable']) && $objRech['valeur_variable'] != '' ) {
                        return true;
                    } else if( is_array($objRech['valeur_variable']) && count($objRech['valeur_variable']) > 0 ) {
                        foreach( $objRech['valeur_variable'] as $sValeurVariable ) {
                            if( $sValeurVariable != '' ) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public function validation_supplementaire_formulaire()
    {

        return array( true, null );
    }

    public function run()
    {
        //echo"<pre>";print_r($this);echo"</pre>";

        if( isset($_GET['action']) && $_GET['action'] == 'formList' ) {

        }

        if( isset($_GET['action']) && $_GET['action'] == 'liste_fils' ) {
            $aEnteteListe = $this->renvoi_entete_liste();
            $aListe = $this->renvoi_liste();

            if( count($this->aListeFils) > 0 ) {
                $sAction = $_GET['action'];
                $_GET['action'] = 'liste_fils';
                $objSmarty = $this->objSmarty;
                foreach( $aListe as $idObjListe => &$objListe ) {
                    foreach( $this->aListeFils as $objListeFils ) {
                        $sValeurChampIdPere = isset($_GET[$objListeFils['sChampIdPere']]) ? $_GET[$objListeFils['sChampIdPere']] : null;
                        $sDir = isset($_GET['dir']) ? $_GET['dir'] : null;

                        $_GET[$objListeFils['sChampIdPere']] = $idObjListe;
                        $_GET['dir'] = $objListeFils['sDir'];

                        $objListe['liste_fils'][] = include 'modules/' . $objListeFils['sChemin'];

                        $_GET[$objListeFils['sChampIdPere']] = $sValeurChampIdPere;
                        $_GET['dir'] = $sDir;
                    }
                    if( isset($objListe['liste_fils']) ) {
                        $iCount = count($objListe['liste_fils']);
                        for( $i = 0; $i < $iCount; $i++ ) {
                            if( $objListe['liste_fils'][$i] === '' ) {
                                unset($objListe['liste_fils'][$i]);
                            }
                        }
                    }
                }
                unset($objListe);
                reset($aListe);
                $_GET['action'] = $sAction;
            }

            $objSmartyFetch = clone $this->objSmarty;
            $objSmartyFetch->assign('iNombreColonneListe', count($aEnteteListe) + (($this->bAffFiche || $this->bAffMod || $this->bAffSupp) ? 1 : 0));
            $objSmartyFetch->assign('bAffFiche', $this->bAffFiche);
            $objSmartyFetch->assign('bAffMod', $this->bAffMod);
            $objSmartyFetch->assign('bAffSupp', $this->bAffSupp);
            $objSmartyFetch->assign('aEnteteListe', $aEnteteListe);
            $objSmartyFetch->assign('aListe', $aListe);
            $objSmartyFetch->assign('bCsv', $this->bCsv);

            return $objSmartyFetch->fetch($this->sListeFilsTpl);
        }

        $bMessageSuccesForm = false;
        $bMessageErreurForm = false;

        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            if( !empty($_POST) ) {

                $sTableauRetourControl = $this->controle_form();
                //echo"<pre>";print_r($sTableauRetourControl);echo"</pre>";

                $bMessageSuccesForm = $sTableauRetourControl['result'];
                $bMessageErreurForm = !$bMessageSuccesForm;

                if( !empty($sTableauRetourControl['message']) ) {
                    $this->sMessageErreurForm = "";
                    foreach( $sTableauRetourControl['message'] as $valuemessageerreur ) {
                        $this->sMessageErreurForm .= $valuemessageerreur . "<br>";
                    }
                }

                if( $bMessageSuccesForm ) {
                    $iId = $this->enreg_modif_form();

                    if( $this->iId === null ) {
                        foreach( $this->aElement as &$objElement ) {
                            if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {
                                $objElement['valeur_variable'] = '';
                            }
                        }
                        unset($objElement);
                    }

                    if( $this->sRetourValidationForm == 'externe' ) {
                        header('location:' . $this->renvoi_url_retour_validation_form_externe() . ($this->iId === null ? '&' . $this->sChampId . '=' . $iId : '') . $this->sParametreWizardListe);
                    }
                } else {
                    $this->renvoi_valeur_form();
                }
            } else {

                //echo "id=>".$this->iId;
                if( $this->iId !== null ) {
                    $this->renvoi_valeur_bdd();
                }
            }

            //echo $this->sParametreWizardListe;

            $sUrlForm =
                $this->renvoi_base_url()
                . $this->renvoi_parametre_inter_module()
                . $this->renvoi_parametre_recherche()
                . $this->renvoi_parametre_filtre_liste()
                . $this->renvoi_parametre_retour_liste()
                . $this->renvoi_parametre_retour_validation_form_externe()
                . $this->sParametreWizardListe
                . '&action=form';

            //echo "url".$sUrlForm;

            if( $this->renvoi_parametre_retour_validation_form_externe() != '' ) {
                $sUrlForm = '';
            }

            if( $this->iId != null && $this->renvoi_parametre_retour_validation_form_externe() == '' ) {
                $sUrlForm .= '&' . $this->sChampId . '=' . $this->iId;
            }

            $this->objSmarty->assign('url_form', $sUrlForm);
        }

        if( isset($_GET['action']) && $_GET['action'] == 'form' && ((!$bMessageSuccesForm && !$bMessageErreurForm) || ($bMessageSuccesForm && $this->sRetourValidationForm == 'form') || $bMessageErreurForm) && (!$this->bFormPopup || $this->iId !== null) ) {

            $bTelechargementFichier = false;

            foreach( $this->aElement as $objElement ) {
                if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {
                    if( $objElement['type_champ'] == 'file' ) {
                        $bTelechargementFichier = true;
                        break;
                    }
                }
            }

            $aForm = $this->renvoi_formulaire();

            if( count($this->itemBoutonsForm) == 0 ) {
                if( $this->sRetourAnnulationForm == 'externe' ) {
                    $sUrlAnnulerForm = $this->renvoi_url_retour_annulation_form_externe();
                } elseif( $this->sRetourAnnulationForm == 'url' ) {
                    $sUrlAnnulerForm = $this->sUrlRetourSpecifique;
                } else {
                    $sUrlAnnulerForm =
                        $this->renvoi_base_url()
                        . $this->renvoi_parametre_inter_module()
                        . $this->renvoi_parametre_recherche()
                        . $this->renvoi_parametre_filtre_liste()
                        . $this->renvoi_parametre_retour_liste()
                        . $this->renvoi_parametre_retour_annulation_form_externe()
                        . $this->sParametreWizardListe;
                }

                $this->itemBoutonsForm = array();

                $this->itemBoutonsForm['valider'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Valider',
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );

                $this->itemBoutonsForm['annuler'] = array(
                    'type' => 'bouton',
                    'nom_variable' => 'btannuler',
                    'text_label' => 'Annuler',
                    'url' => $sUrlAnnulerForm,
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            $this->objSmarty->assign('bTelechargementFichier', $bTelechargementFichier);
            $this->objSmarty->assign('sTitreForm', $this->sTitreForm);
            $this->objSmarty->assign('sCategorieForm', $this->sCategorieForm);
            $this->objSmarty->assign('bMessageSuccesForm', $bMessageSuccesForm);
            $this->objSmarty->assign('sMessageSuccesForm', $this->sMessageSuccesForm);
            $this->objSmarty->assign('bMessageErreurForm', $bMessageErreurForm);
            $this->objSmarty->assign('sMessageErreurForm', $this->sMessageErreurForm);
            $this->objSmarty->assign('bAffAnnuler', $this->bAffAnnuler);
            $this->objSmarty->assign('aForm', $aForm);
            $this->objSmarty->assign('itemBoutonsForm', $this->itemBoutonsForm);
            $this->objSmarty->assign('bDebugRequete', $this->bDebugRequete);
            $this->objSmarty->assign('sDebugRequeteSelect', $this->sDebugRequeteSelect);
            $this->objSmarty->assign('sDebugRequeteInsertUpdate', $this->sDebugRequeteInsertUpdate);
            $this->objSmarty->assign("pagedirection", $this->sFormulaireTpl);
            $this->objSmarty->assign("lapagedirection", $this->sFormulaireTpl);

            return $this->objSmarty->fetch($this->sFormulaireTpl);
        } else {

            if( $this->bFormPopup ) {
                $this->bFormPopup = false;
                $sAction = isset($_GET['action']) ? $_GET['action'] : null;
                $_GET['action'] = 'form';
                unset($_POST);
                $this->run();
                $this->bFormPopup = true;
                $_GET['action'] = $sAction;
            }

            if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
                foreach( $this->aElement as &$objElement ) {
                    if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {
                        $objElement['valeur_variable'] = '';
                    }
                }
                unset($objElement);
            }

            $bTelechargementFichier = false;

            $bMessageSupprElem = false;

            if( isset($_GET['action']) && $_GET['action'] == 'supp' ) {
                $bMessageSupprElem = $this->suppression_entree();

                //echo "action supps";

                if( $this->sRetourSuppressionFormExterne != '' ) {
                    header('location:' . $this->renvoi_url_retour_suppression_form_externe());
                }
            }

            $sLabelCreationElemUrl =
                $this->renvoi_base_url()
                . $this->renvoi_parametre_inter_module()
                . $this->renvoi_parametre_recherche()
                . $this->renvoi_parametre_filtre_liste()
                . $this->renvoi_parametre_retour_liste()
                . $this->sParametreWizardListe
                . '&action=form';

            $itemBoutons = array();

            $itemBoutons['valider'] = array(
                'type' => 'submit',
                'nom_variable' => 'btsubmit',
                'text_label' => 'Valider',
                'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
            );

            $itemBoutons['reset'] = array(
                'type' => 'bouton',
                'nom_variable' => 'btreset',
                'text_label' => 'Reset',
                'url' => $this->renvoi_base_url() . $this->renvoi_parametre_inter_module() . $this->renvoi_parametre_retour_liste() . $this->sParametreWizardListe,
                'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
            );


            $itemBoutons['csv'] = array(
                'type' => 'submit',
                'nom_variable' => 'btcsv',
                'text_label' => 'Télécharger le CSV',
                'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
            );

            $aRech = $this->renvoi_recherche();

            $bAffListe = ($this->bAffListeQuandPasRecherche || $this->renvoi_si_filtre_recherche_actif($aRech));

            if( $bAffListe ) {

                $aEnteteListe = $this->renvoi_entete_liste();
                $aListe = $this->renvoi_liste();
                $aPaginationListe = $this->pagination();

                if( count($this->aListeFils) > 0 ) {
                    $sAction = isset($_GET['action']) ? $_GET['action'] : null;
                    $_GET['action'] = 'liste_fils';
                    $objSmarty = $this->objSmarty;
                    foreach( $aListe as $idObjListe => &$objListe ) {
                        foreach( $this->aListeFils as $objListeFils ) {
                            $sValeurChampIdPere = isset($_GET[$objListeFils['sChampIdPere']]) ? $_GET[$objListeFils['sChampIdPere']] : null;
                            $sDir = isset($_GET['dir']) ? $_GET['dir'] : null;

                            $_GET[$objListeFils['sChampIdPere']] = $idObjListe;
                            $_GET['dir'] = $objListeFils['sDir'];

                            $objListe['liste_fils'][] = include 'modules/' . $objListeFils['sChemin'];

                            $_GET[$objListeFils['sChampIdPere']] = $sValeurChampIdPere;
                            $_GET['dir'] = $sDir;
                        }
                        if( isset($objListe['liste_fils']) ) {
                            $iCount = count($objListe['liste_fils']);
                            for( $i = 0; $i < $iCount; $i++ ) {
                                if( $objListe['liste_fils'][$i] === '' ) {
                                    unset($objListe['liste_fils'][$i]);
                                }
                            }
                        }
                    }
                    unset($objListe);
                    reset($aListe);
                    $_GET['action'] = $sAction;
                }
            } else {
                $aEnteteListe = array();
                $aListe = array();
                $aPaginationListe = array();
            }

            if( !$this->bforceAfficheEntete ) {

                $this->objSmarty->assign('bMessageSuccesForm', $bMessageSuccesForm);
                $this->objSmarty->assign('sMessageSuccesForm', $this->sMessageSuccesForm);
                $this->objSmarty->assign('bMessageErreurForm', $bMessageErreurForm);
                $this->objSmarty->assign('sMessageErreurForm', $this->sMessageErreurForm);

            }

            $this->objSmarty->assign('sLienLigne', $this->sLienLigne);
            $this->objSmarty->assign('bAffPrintBtn', $this->bAffPrintBtn);
            $this->objSmarty->assign('bLigneCliquable', $this->bLigneCliquable);
            $this->objSmarty->assign('bAffTitre', $this->bAffTitre);
            $this->objSmarty->assign('bAffNombreResult', $this->bAffNombreResult);
            $this->objSmarty->assign('bBtnRetour', $this->bBtnRetour);
            $this->objSmarty->assign('sDirRetour', $this->sDirRetour);
            $this->objSmarty->assign('bCheckboxSelect', $this->bCheckboxSelect);
            $this->objSmarty->assign('bRadioSelect', $this->bRadioSelect);
            $this->objSmarty->assign('bActiveFormSelect', $this->bActiveFormSelect);
            $this->objSmarty->assign('bAffListe', $bAffListe);
            $this->objSmarty->assign('bLabelCreationElem', $this->bLabelCreationElem);
            $this->objSmarty->assign('iNombreColonneListe', count($aEnteteListe) + (($this->bAffFiche || $this->bAffMod || $this->bAffSupp) ? 1 : 0));
            $this->objSmarty->assign('bFormPopup', $this->bFormPopup);
            $this->objSmarty->assign('aParametreWizardListe', $this->aParametreWizardListe);
            $this->objSmarty->assign('itemBoutonsListe', $this->itemBoutonsListe);
            $this->objSmarty->assign('sCategorieListe', $this->sCategorieListe);
            $this->objSmarty->assign('sLabelRetourListe', $this->sLabelRetourListe);
            $this->objSmarty->assign('sRetourListe', $this->renvoi_url_retour_liste());
            $this->objSmarty->assign('bMessageSupprElem', $bMessageSupprElem);
            $this->objSmarty->assign('sMessageSupprElem', $this->sMessageSupprElem);
            $this->objSmarty->assign('sLabelCreationElem', $this->sLabelCreationElem);
            $this->objSmarty->assign('sLabelCreationElemUrl', $sLabelCreationElemUrl);
            $this->objSmarty->assign('sLabelRecherche', $this->sLabelRecherche);
            $this->objSmarty->assign('sTitreListe', $this->sTitreListe);
            $this->objSmarty->assign('iTabListeCount', $this->renvoi_nombreligne_requete());
            $this->objSmarty->assign('sLabelNbrLigne', $this->sLabelNbrLigne);
            $this->objSmarty->assign('sChampId', $this->sChampId);
            $this->objSmarty->assign('bAffFiche', $this->bAffFiche);
            $this->objSmarty->assign('bAffMod', $this->bAffMod);
            $this->objSmarty->assign('bAffSupp', $this->bAffSupp);
            $this->objSmarty->assign('bCsv', $this->bCsv);
            $this->objSmarty->assign('aPaginationListe', $aPaginationListe);
            $this->objSmarty->assign('bPagination', $this->bPagination);
            $this->objSmarty->assign('itemBoutons', $itemBoutons);
            $this->objSmarty->assign('aRech', $aRech);
            $this->objSmarty->assign('aEnteteListe', $aEnteteListe);
            $this->objSmarty->assign('aListe', $aListe);
            $this->objSmarty->assign('sDirRech', (isset($_GET['dir']) ? $_GET['dir'] : ''));
            $this->objSmarty->assign('aRetourListe', $this->renvoi_parametre_retour_liste_tableau());
            $this->objSmarty->assign('bDebugRequete', $this->bDebugRequete);
            $this->objSmarty->assign('sDebugRequeteSelect', $this->sDebugRequeteSelect);
            $this->objSmarty->assign('sDebugRequeteInsertUpdate', $this->sDebugRequeteInsertUpdate);
            $this->objSmarty->assign('pagedirection', $this->sListeTpl);
            $this->objSmarty->assign('lapagedirection', $this->sListeTpl);
            $this->objSmarty->assign('bRetourSpecifique', $this->bRetourSpecifique);
            $this->objSmarty->assign('RetourElemUrl', $this->RetourElemUrl);
            $this->objSmarty->assign('sLabelFileRetourElem', $this->sLabelFileRetourElem);

            return $this->objSmarty->fetch($this->sListeTpl);
        }
    }

    /**
     * @param $sRoute
     * @param $sPereRoute
     * @param string $sLibeleMenu
     * @param int $iOrdre
     * @param bool $bLien
     * @param string $sTarget
     */
    public function add_menu($sNomModule='',$sRoute, $sPereRoute, $sLibeleMenu = '', $iOrdre=0, $bLien=false, $sTarget='')
    {
        if(!empty($sNomModule)){
            $aParam[] = $sNomModule;
            $sRequeteSelectModule = "SELECT installe_module FROM fli_modules WHERE nom_module=?";
            $aTabModule = $this->objClassGenerique->renvoi_info_requete($sRequeteSelectModule,$aParam);
        }else{
            die('<div style="text-align: center;"><h1 style="font-size: 150px;color: rgb(162, 39, 38);">&#10008;</h1>
                <h3>Veuillez indiquer le nom de votre module dans la fonction add_menu().</h3>
                <div><a href="fli_admin-modules">Retour à la gestion des modules</a></div></div>');
        }
        $aTab['route_route'] = $sRoute;
        $aTab['pere_menu_route'] = $sPereRoute;
        $aTab['intitule_menu_route'] = $sLibeleMenu;
        $aTab['ordre_menu_route'] = $iOrdre;
        $aTab['lien_menu_route'] = $bLien;
        $aTab['target_menu_route'] = $sTarget;
        $guid_route = class_helper::guid();
        $sRequeteInsertMenu = "INSERT fli_routes SET supplogique_route='N',guid_route='".$guid_route."',afficher_menu_route=1,route_route=:route_route,pere_menu_route=:pere_menu_route,
        intitule_menu_route=:intitule_menu_route,ordre_menu_route=:ordre_menu_route,lien_menu_route=:lien_menu_route,target_menu_route=:target_menu_route";
        //$this->objClassGenerique->insert_requete($sRequeteInsertMenu,$aTab);
        //Droits pour le groupe SuperAdmin
        $aParam = array();
        $aTabGroupe[0]['nom_groupe'] = "";
        $aParam[] = 'SuperAdmin';
        $sRequeteSelectModule = "SELECT guid_groupe FROM fli_groupes WHERE nom_groupe=?";
        $aTabGroupe = $this->objClassGenerique->renvoi_info_requete($sRequeteSelectModule,$aParam);
        if(!empty($aTabGroupe[0]['nom_groupe'])) {
            $sRequeteInsertGroupeRoute = "INSERT fli_groupes_routes SET supplogique_groupe_route='N',guid_route='" . $guid_route . "',ajout_groupe_route=1,modif_groupe_route=1,suppr_groupe_route=1,visu_groupe_route=1,
            guid_groupe='" . $aTabGroupe[0]['nom_groupe'] . "'";
            $this->objClassGenerique->insert_requete($sRequeteInsertGroupeRoute,array());
        }
    }
}