<?php header("Content-type: text/css"); ?>
<?php

	include('../class/class_generique.php');

	$objTest= new class_generique("","","");
	$bDroit = $objTest->verificationConnexion(0,$identifiant);
	$TableauResult=array();

	$sRequeteSelect = "SELECT valeur_configstock,identification_valeurconfig FROM routeurmail_valeurconfig WHERE identification_valeurconfig 
	IN ('nav-bg','nav-border','siderbar-wrapper','sidebar-liste','sidebar-liste-hover-color','sidebar-liste-hover-background','sidebar-titre',
		'btn-defaut-bg','btn-defaut-border','btn-defaut-color','nav-color','btn-defaut-hover-bg','btn-defaut-hover-color','btn-defaut-hover-border',
		'title-head-search-bg','title-head-search-color','text-head-search-bg','text-head-search-color','title-search-bg','title-search-color','text-search-bg',
		'text-search-color','sub-title-head-search-bg','sub-title-head-search-color','sub-text-head-search-bg','sub-text-head-search-color','title-table-head-bg','title-table-head-color')";
	$aTableauResultat = $objTest->renvoi_info_requete($sRequeteSelect);

	if(!empty($aTableauResultat)){
		foreach ($aTableauResultat as $valeur) {
			$TableauResult[$valeur['identification_valeurconfig']] = $valeur['valeur_configstock'];
		}
	}
	?>

	.navbar-default{
		background-color:<?php  echo $TableauResult['nav-bg'] ?>!important;
		border-color:<?php  echo $TableauResult['nav-border'] ?> !important;
	}
	.navbar-default .navbar-brand{
		color:<?php  echo $TableauResult['nav-color'] ?> !important;
	}

	#sidebar-wrapper{
		background-color:<?php  echo $TableauResult['siderbar-wrapper'] ?> !important;
	}
	.sidebar-nav li a{
		color:<?php  echo $TableauResult['sidebar-liste'] ?> !important;
	}
	.sidebar-nav li a:hover,.sidebar-nav li a:active,.sidebar-nav li a:focus{
		background-color:<?php  echo $TableauResult['sidebar-liste-hover-background'] ?> !important;
		color:<?php  echo $TableauResult['sidebar-liste-hover-color'] ?> !important;
	}
	.sidebar-nav > .sidebar-brand a{
		color:<?php  echo $TableauResult['sidebar-titre'] ?> !important;
	}

	.btn-default{
		background-color:<?php  echo $TableauResult['btn-defaut-bg'] ?> !important;
		border-color:<?php  echo $TableauResult['btn-defaut-border'] ?> !important;
		color:<?php  echo $TableauResult['btn-defaut-color'] ?> !important;
	}
	.btn-default:hover,.btn-default:active,.btn-default:focus{
		background-color:<?php  echo $TableauResult['btn-defaut-hover-bg'] ?> !important;
		border-color:<?php  echo $TableauResult['btn-defaut-hover-border'] ?> !important;
		color:<?php  echo $TableauResult['btn-defaut-hover-color'] ?> !important;
	}

	.title-head-search{
		background-color:<?php  echo $TableauResult['title-head-search-bg'] ?> !important;
		color:<?php  echo $TableauResult['title-head-search-color'] ?> !important;
	}
	.text-head-search{
		background-color:<?php  echo $TableauResult['text-head-search-bg'] ?> !important;
		color:<?php  echo $TableauResult['text-head-search-color'] ?> !important;
	}

	.title-search{
		background-color:<?php  echo $TableauResult['title-search-bg'] ?> !important;
		color:<?php  echo $TableauResult['title-search-color'] ?> !important;
	}
	.text-search{
		background-color:<?php  echo $TableauResult['text-search-bg'] ?> !important;
		color:<?php  echo $TableauResult['text-search-color'] ?> !important;
	}

	.sub-title-head-search{
		background-color:<?php  echo $TableauResult['sub-title-head-search-bg'] ?> !important;
		color:<?php  echo $TableauResult['sub-title-head-search-color'] ?> !important;
	}	

	.sub-text-head-search{
		background-color:<?php echo $TableauResult['sub-text-head-search-bg'] ?> !important;
		color:<?php echo $TableauResult['sub-text-head-search-color'] ?> !important;
	}

	.title-table-head{
		background:<?php echo $TableauResult['title-table-head-bg'] ?>  !important;
		color:<?php echo $TableauResult['title-table-head-color'] ?> !important;
	}