$(document).ready(function() {
	var currentImageIndex = null;
	var loadedFancybox = null;
	$("#image a").fancybox(
		{
			'hideOnContentClick': true,
			'titleShow': true,
			'titlePosition': 'inside',
			'overlayOpacity': 0.65,
			'overlayColor': '#000',
			'onCleanup': function(currentArray, currentIndex, currentOpts)
			{
				currentImageIndex = currentIndex;
			},
			'onClosed': function(currentArray, currentIndex, currentOpts)
			{
				currentImageIndex = null;
				loadedFancybox = null;

				var img_file = currentArray[currentIndex]
				img_file = get_img_name(img_file);
				omniture_tag($(this),'close_zoom',img_file);
			},
			'onStart': function(currentArray, currentIndex, currentOpts)
			{
				var img_file = currentArray[currentIndex]
				img_file = get_img_name(img_file);

				
/*				if ( loadedFancybox == null )
				{
					// Getting into the light box now.
					if ( currentIndex != 0 )
					{
						// Entering the lightbox through an additional image.
						omniture_tag($(this),'addtl_zoom',img_file);
					} else
					{
						omniture_tag($(this),'main_zoom',img_file);
					}
				}
				else
				{
					if ( currentIndex != 0 )
					{
						// Looking at an additional image from inside the fancybox.
						omniture_tag($(this),'gallery_addtl',img_file);
					}
					else
					{
						// Looking at the main image from inside the fancybox.	
						omniture_tag($(this),'gallery_main',img_file);

					}
				}
*/				loadedFancybox = 1;
				
			},
			'onComplete': function(currentArray, currentIndex, currentOpts)
			{
				// Find the relative image path
				var image = currentArray[currentIndex];
				image = '' + image;
				var regex = new RegExp( 'http.{0,1}:\/\/[^\/]+', 'i' );
				image = image.replace( regex, '');
				
				// Hide all the large images
				$('#image img').css('display', 'none');
				
				// Show the requested one
				var matching_image_src = null;
				$('#image a').each(
					function(index)
					{
						if ( $(this).attr('href') == image )
						{
							$("img", this).css('display', 'block');
							matching_image_src = $("img", this).attr('src');
						}
					}
				);
				
				// Fix thumbnails
				$('.thumb').removeClass('thumb_selected');
				if ( matching_image_src != null )
				{
					$('.image').each(
						function(index)
						{
							if ( $(this).attr('rel') == matching_image_src )
							{
								$("img", this).addClass('thumb_selected');
							}
						}
					);
				}
				
				if ( currentImageIndex != null )
				{
					if ( currentImageIndex - currentIndex > 0 )
					{
						// We clicked on the Gallery Navigate previous button to get to this image.
					}
					else
					{
						// We clicked on the Gallery Navigate next button to get to this image.
					}
				}
				
				// Make sure that when we move inside the fancybox, the newly selected
				// thumbnail is visible in the slider.
				// Note: -1 so that the selected thumbnail is centered.
				//var carousel = $('#image_slider').data('jcarousel');
				//carousel.scroll( currentIndex - 1 ); 
			}
		}
	);
	
	$(".image").click(
		function() {
			
			// Grab the URL of the image we want to show
			var image = $(this).attr("rel");
			
			// Hide the large image area
			$('#image').hide();
			
			// Hide all the large images
			$('#image img').css('display', 'none');
			
			// Show the requested one
			$('#image a').each(
				function(index)
				{
					if ( $("img", this).attr('src') == image )
					{
						$("img", this).css('display', 'block');
					}
				}
			);
			
			// Make the whole area visible again
			$('#image').fadeIn('fast');
			
			// Thumbnails management
			$('.thumb').removeClass('thumb_selected');
			$("img", this).addClass('thumb_selected');
			
			return false;
		}
	);
	
	var CAROUSEL_SIZE = 5;
	var slidersize = $('#image_slider li').length;
	$('#image_slider').jcarousel(
		{
			scroll: CAROUSEL_SIZE,
			initCallback: function( carousel, state )
			{
				if ( slidersize <= CAROUSEL_SIZE )
				{
					$('.jcarousel-next').hide();
					$('.jcarousel-prev').hide();
					$('.jcarousel-container').css('height', '56px');
					$('.jcarousel-container').css('width', '258px');
					$('.jcarousel-container').css('border', '1px solid #bdc3c7');
				}

				//if ( slidersize < 2 )
				//{
				//	$('.jcarousel-container').css('visibility', 'hidden');
				//	$('.prod_gallery').css('height', '300px');
				//	$('.prod_gallery').css('padding', '6px');
				//	$('.prod_gallery').css('border-color', '#dedede');
				//}

				// Only show the slider area once the slider has been built. Otherwise,
				// we'll sometime see a long list of unstyled LI elements until the JS
				// executes.
				$('.jcarousel-skin-tango').eq(0).css('visibility', 'visible');
			}
		}
	);
	
	
//	$('.jcarousel-prev-horizontal').click( function(){omniture_tag($(this),'slider_nav','Backward');}); 
//	$('.jcarousel-next-horizontal').click( function(){omniture_tag($(this),'slider_nav','Forward');}); 

});




//grab just the filename from the full image URL
function get_img_name(imgfile)
{
	var parts = new Array;
	//cast as a String by adding an empty string, in order to have access to String split() function
	parts = ('' + imgfile).split("/");
	return parts[parts.length - 1];	
}