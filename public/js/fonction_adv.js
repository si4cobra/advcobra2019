
//fucntion vÃ©rifant et rÃ©cupÃ©rant la bonne implÃ©mentation de l'objet ajax
function getXMLHTTP(){
  var xhr=null;
  if(window.XMLHttpRequest) // Firefox et autres
  xhr = new XMLHttpRequest();
  else if(window.ActiveXObject){ // Internet Explorer
    try {
      xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xhr = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e1) {
        xhr = null;
      }
    }
  }
  else { // XMLHttpRequest non supportÃ© par le navigateur
    alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
  }
  return xhr;
}


//********************************************
//* 
//********************************************
function update_info_fournisseur(idfourniseur,idproduit,idref,idurl,idprix){
	
	xhr_object = getXMLHTTP(); 
	xhr_object.open("POST","commande/info_fournisseur_prix.php", true);

	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var data ="idfournisseur="+idfourniseur+"&idproduit="+idproduit;
	xhr_object.send(data);
	xhr_object.onreadystatechange = function() {
		if((xhr_object.readyState == 4) && (xhr_object.status == 200)){
			result = xhr_object.responseText;

			atableaudecoupe = result.split("|");
			document.getElementById(idref).value=atableaudecoupe[0];
			document.getElementById(idurl).value=atableaudecoupe[1];
			document.getElementById(idprix).value=atableaudecoupe[2];
			//alert(result)
			return result;
		}
	}	
}


//********************************************
//* 
//********************************************
function update_categorie(code_postal){
	
	xhr_object = getXMLHTTP(); 
	xhr_object.open("POST","contrat/update_categorie.php", true);

	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var data ="code_postal="+code_postal;
	xhr_object.send(data);
	xhr_object.onreadystatechange = function() {
		if((xhr_object.readyState == 4) && (xhr_object.status == 200)){
			result = xhr_object.responseText;
			return result;
		}
	}	
}

function generer_pass(idchamp, idlogin){

	xhr_object = getXMLHTTP(); 
	xhr_object.open("POST","modules/client/controleurs/generer_mot_de_passe.php", true);
	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhr_object.send();
	xhr_object.onreadystatechange = function() {
		if((xhr_object.readyState == 4) && (xhr_object.status == 200)){
			result = xhr_object.responseText;
			var s_login = document.getElementById(idlogin).value;
			document.getElementById(idchamp).value=s_login+result;
		}
	}
}

function charger_modele(idmodele, idchamps, idclient){

	xhr_object = getXMLHTTP(); 
	xhr_object.open("POST","client/charger_modele.php", true);
	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	var data ="idmodele="+idmodele+"&idclient="+idclient;
	xhr_object.send(data);
	xhr_object.onreadystatechange = function() {
		if((xhr_object.readyState == 4) && (xhr_object.status == 200)){
			result = xhr_object.responseText;
			CKEDITOR.instances.txtareackeditor.insertHtml(result);
		}
	}
}

function include_variable(idvariable){
	CKEDITOR.instances.txtareackeditor.insertHtml(idvariable);
}

function affiche_div_module(idselect){   
	var idradio;
	if(document.getElementsByName('type_paiement')[0].checked==true){
		idradio = document.getElementsByName('type_paiement')[0].value;
	}
	if(document.getElementsByName('type_paiement')[1].checked==true){
		idradio = document.getElementsByName('type_paiement')[1].value;
	}
	if(document.getElementsByName('type_paiement')[2].checked==true){
		idradio = document.getElementsByName('type_paiement')[2].value;
	}

	//alert(idradio);

	xhr_object = getXMLHTTP();
	xhr_object.open("POST","facture/module_paiement.php", true);
	
	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
	var data = "idmodereglement="+idselect+"&idtypepaiement="+idradio;
	xhr_object.send(data);
	
	xhr_object.onreadystatechange = function(){
		if((xhr_object.readyState == 4) && (xhr_object.status == 200)){
			document.getElementById("id_block_html").style.display="block";
			document.getElementById("id_block_html").innerHTML = xhr_object.responseText;
		} 
	}
}

function CheckDate(d) {
	// Cette fonction vérifie le format JJ/MM/AAAA saisi et la validité de la date.
	// Le séparateur est défini dans la variable separateur
	var amin=1900; // année mini
	var amax=2115; // année maxi
	var separateur='"'; // separateur entre jour/mois/annee
	var j=(d.substring(0,2));
	var m=(d.substring(3,5));
	var a=(d.substring(6));
	var ok=1;
	if ( ((isNaN(j))||(j<1)||(j>31)) && (ok==1) ) {
		//alert("Le jour n'est pas correct."); 
		ok=0;
	}
	if ( ((isNaN(m))||(m<1)||(m>12)) && (ok==1) ) {
		//alert("Le mois n'est pas correct."); 
		ok=0;
	}
	if ( ((isNaN(a))||(a<amin)||(a>amax)) && (ok==1) ) {
		//alert("L'année n'est pas correcte.");
		ok=0;
	}
	if ( ((d.substring(2,3)!=separateur)||(d.substring(5,6)!=separateur)) && (ok==1) ) {
		//alert("Les séparateurs doivent être des "+separateur); 
		ok=0;
	}
	if (ok==1) {
		var d2=new Date(a,m-1,j);
		j2=d2.getDate();
		m2=d2.getMonth()+1;
		a2=d2.getFullYear();
		if (a2<=100) {a2=1900+a2}
		if ( (j!=j2)||(m!=m2)||(a!=a2) ) {
			alert("La date "+d+" n'existe pas !");
			ok=0;
		}
	}
	return ok;
}
//*******************************function liste nville************************
function affiche_liste_generique(base,id,nom,supplogique,tabfiltre,champ,idselect){
   
	//vider la liste dÃ©roulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","json/xml_generique.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data ="rech="+champ+"&base="+base+"&id="+id+"&nom="+nom+"&supplogique="+supplogique+"&"+tabfiltre;
	var idRic;
  	xhr_object.send(data);
  	//alert(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			//alert(xmldoc);
			var root_node = xmldoc.getElementsByTagName('item');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[1].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				document.getElementById(idselect).options[i] = nouvel_element;
			}
		} 
	};
} 


//*******************************function liste nville************************
function affiche_liste_prospect(nom,prenom,email,idprogramme,tel,idselect){
   
	//vider la liste dÃ©roulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","json/xml_prospect.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data ="nom="+document.getElementById(nom).value+"&prenom="+document.getElementById(prenom).value+"&email="+document.getElementById(email).value+"&tel="+document.getElementById(tel).value+"&idprogramme="+document.getElementById(idprogramme).value;
	var idRic;
  	xhr_object.send(data);
  	//alert(data);
  	console.log(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			//alert(xmldoc);
			var root_node = xmldoc.getElementsByTagName('item');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[1].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				document.getElementById(idselect).options[i] = nouvel_element;
			}
		} 
	};
} 


//****************************function liste nville
function affiche_liste_des_villes_simple(nom,lechamp,idselect){
   
	//vider la liste dÃ©roulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_ville.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
	var idRic;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				document.getElementById(idselect).options[i] = nouvel_element;
			}
		} 
	};
} 

//****************************function liste nville
function affiche_liste_des_ville(nom,lechamp,idselect){
   
	//vider la liste déroulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_ville.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
	var idRic;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				document.getElementById(idselect).options[i] = nouvel_element;
				if(i==0){
					idRic = valeur;
					affiche_liste_des_ric(idRic,'codepostal','idfrom_select_ric');
				}
			}
		} 
	};
} 

//****************************function liste nville
function affiche_liste_des_ville_rt(nom,lechamp,idselect){
   
	//vider la liste déroulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_ville.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
	var idRic;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				document.getElementById(idselect).options[i] = nouvel_element;
				if(i==0){
					idRic = valeur;
					affiche_liste_des_ric_rt(idRic,'codepostal','idfrom_select_ric');
				}
			}
		} 
	};
} 

//****************************function liste nville
function affiche_liste_des_ville_face_pub(nom,lechamp,idselect){
   
	//vider la liste déroulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_ville.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
	var idRic;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				document.getElementById(idselect).options[i] = nouvel_element;
				if(i==0){
					idRic = valeur;
					affiche_liste_type_mobilier(idRic,'codepostal','idfrom_select_type_mobilier');
				}
			}
		} 
	};
} 

//****************************function liste nville
function affiche_liste_des_ric(nom,lechamp,idselect){
	//vider la liste déroulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_ric.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				var tailleselect = document.getElementById(idselect).length;
				document.getElementById(idselect).options[i] = nouvel_element;
				
			}
		} 
	};
} 


//****************************function liste nville
function affiche_liste_des_ric_rt(nom,lechamp,idselect){
	//vider la liste déroulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_ric_rt.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				var tailleselect = document.getElementById(idselect).length;
				document.getElementById(idselect).options[i] = nouvel_element;
				
			}
		} 
	};
}


//****************************function liste nville
function affiche_liste_emplacement_dispo(nom,lechamp,idselect){
	//vider la liste déroulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_emplacement_dispo.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				var tailleselect = document.getElementById(idselect).length;
				document.getElementById(idselect).options[i] = nouvel_element;
				
			}
		} 
	};
} 


//****************************function liste nville
function affiche_liste_type_mobilier(nom,lechamp,idselect){
	//vider la liste dÃ©roulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_type_mobilier.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom;
	var idRic;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				var tailleselect = document.getElementById(idselect).length;
				document.getElementById(idselect).options[i] = nouvel_element;
				if(i==0){
					idRic = valeur;
					affiche_liste_num_mobilier(nom,'codepostal','idfrom_select_num_mobilier',idRic,'type_mobilier');
				}
			}
		} 
	};
} 



//****************************function liste nville
function affiche_liste_num_mobilier(nom,lechamp,idselect,nom2,lechamp2){
	//vider la liste dÃ©roulante
	document.getElementById(idselect).length = 0;
	
    xhr_object = new XMLHttpRequest(); 
  	xhr_object.open("POST","divers/affiche_liste_num_mobilier.php", true);	 
  	  
  	xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded"); 
  	var data =lechamp+"="+nom+"&"+lechamp2+"="+nom2;
  	xhr_object.send(data);
  	xhr_object.onreadystatechange = function() {
		if(xhr_object.readyState == 4){
			var xmldoc = xhr_object.responseXML;
			var root_node = xmldoc.getElementsByTagName('ville');
			itaille =root_node.length; 
			for(i=0;i<itaille;i++){
				champ=root_node.item(i).attributes[0].nodeValue;
				valeur=root_node.item(i).attributes[2].nodeValue;
				nouvel_element = new Option(champ,valeur,false,false);
				var tailleselect = document.getElementById(idselect).length;
				document.getElementById(idselect).options[i] = nouvel_element;
			}
		} 
	};
}

//fonction  de controle formulaire
function controle_form(aTableauChamp,aTableauText,aTableauType){

	iTailleTableau = aTableauChamp.length;
	bTest = true;
	sMessageRetour='';
	bsecuseul=true
	
	for(var i = 0; i < iTailleTableau; ++i){
	
		sNomChamp = aTableauChamp[i];
		sTextChamp = aTableauText[i];
		sTypeChamp = aTableauType[i];
		
		if(sTypeChamp=='input'){
			var chainevide=/^ *$/;
			if(chainevide.test(document.getElementById(sNomChamp).value)){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
				}
		}
		
		if(sTypeChamp=='cp'){
			var chainevide=/^[0-9][0-9][0-9][0-9][0-9]*$/;
			if(!chainevide.test(document.getElementById(sNomChamp).value)){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
				}
		}
		
		
		if(sTypeChamp=='textarea'){
			if(document.getElementById(sNomChamp).value.length==0){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
				}
		}
		
		if(sTypeChamp=='email'){
			var mail = /^[\w\-]+(\.[\w\-]+)*@[\w\-]+(\.[\w\-]+)*\.[\w\-]{2,}$/; 
			reponse=mail.test(document.getElementById(sNomChamp).value);
			if(reponse==false){
				bTest=false;
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
			}
		}
		
		
		if(sTypeChamp=='email2'){
			var mail = /^[\w\-]+(\.[\w\-]+)*@[\w\-]+(\.[\w\-]+)*\.[\w\-]{2,}$/; 
			var resultatII = /^ *$/;
			reponse=mail.test(document.getElementById(sNomChamp).value);
			reponse2=resultatII.test(document.getElementById(sNomChamp).value);
			if(reponse==false && reponse2==false){
				bTest=false;
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
			}
		}
		
		if(sTypeChamp=='select'){
			if(document.getElementById(sNomChamp).value==''){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
				}
		
		}
		
		/*if(sTypeChamp=='radioselect'){
			if(!document.getElementById(sNomChamp).checked ==true){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
			}
		}*/
		
		if(sTypeChamp=='date'){		
			if(!CheckDate(document.getElementById(sNomChamp).value)){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
			}
		}
		
		if(sTypeChamp=='inputnumero'){
			var sText =document.getElementById(sNomChamp).value;
			var resultat = sText.search(/0[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/);
			var resultatII = sText.search(/\+[0-9]{11}/);
			if(resultat==-1){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
			}
		}
		
		if(sTypeChamp=='inputnumeroport'){
			var sText =document.getElementById(sNomChamp).value;
			var resultat = sText.search(/06[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/);
			var resultatII = sText.search(/\+[0-9]{11}/);
			if(resultat==-1){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
			}
		}
		
		if(sTypeChamp=='inputnumeroport2'){
			var sText =document.getElementById(sNomChamp).value;
			var resultat = sText.search(/06[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/);
			var resultatII = sText.search(/^ *$/);
			if(resultat==-1 && resultatII){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
			}
		}
		
		if(sTypeChamp=='siren'){
			var sText =document.getElementById(sNomChamp).value;
			var resultat = sText.search(/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/);
			var resultatII = sText.search(/^ *$/);
			if(resultat==-1 && resultatII){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bTest=false;
			}
		}
		
		if(sTypeChamp=='secu'){		
			if(!isSecuValid(document.getElementById(sNomChamp).value)){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bsecuseul=false;			
			}
		}
		
		if(sTypeChamp=='inputconf' && bTest==true){			
			var chainevide=/^ *$/;
			if(chainevide.test(document.getElementById(sNomChamp).value)){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bsecuseul=false;
			}
		}
		
		if(sTypeChamp=='cmpcont' && bTest==true){		
			aTableaudecoupe =  sNomChamp.split(':');
			if(document.getElementById(aTableaudecoupe[0]).value != document.getElementById(aTableaudecoupe[1]).value){
				sMessageRetour = sTextChamp+"\n"+sMessageRetour;
				bsecuseul=false;
			}
		}
	}
	if(!bTest){
		alert(sMessageRetour);
		return bTest;
	}else{
	    if(!bsecuseul){
			sMessageRetour= sMessageRetour+"\nok pour valider votre saisie";
			bTest = confirm(sMessageRetour);
			return bTest
		}else{
			return bTest;
		}
	}
}

function save_edtable(nomtable,champid,champvalue,valueid,valuechamp){

    var message =nomtable+" "+champid+" "+champvalue+" "+valueid+" "+valuechamp;
    //alert(message);
    //console.log(message);

    $.ajax({
        url: 'ajax/save_editable.php',
        data: 'bdd='+nomtable+'&champid='+champid+'&champupdate='+champvalue+'&champupdate='+champvalue+'&valueid='+valueid+'&valuechamp='+valuechamp,
        dataType: 'json',
        success: function(json) {
        	//console.log(json.valeur)

			//console.log(champvalue+valueid);

			$("#"+valueid+champvalue).html(json.valeur);

		}
    });

    return false;
}

function maj_button_radio_on_off(nomtable,champid,champvalue,valueid,valuechamp){

    var message =nomtable+" "+champid+" "+champvalue+" "+valueid+" "+valuechamp;
    //alert(message);
    //console.log(message);

    $.ajax({
        url: 'ajax/save_buttonradio.php',
        data: 'bdd='+nomtable+'&champid='+champid+'&champupdate='+champvalue+'&champupdate='+champvalue+'&valueid='+valueid+'&valuechamp='+valuechamp,
        dataType: 'json',
        success: function(json) {}
    });


    return false;
}

function renvoi_info_traduction(identtifiantchamp,slink,lg,defaut,sbase,schmapbase){

    $('#fli_identifiant').val(identtifiantchamp);
    $('#fli_basedonne').val(sbase);
    $('#fli_champbasedonne').val(schmapbase);
    $('#fli_link').val(slink);
    $('#fli_desctipiton').html("");
    $('#fli_actifprinicpal').val("N").change();

    if(lg=="fr"){
        $('#fli_lgfr').val(defaut);
        $('#fli_lgeng').val("");
        $('#fli_lgall').val("");
        $('#fli_lgesp').val("");
	}

    if(lg=="eng"){
        $('#fli_lgfr').val("");
        $('#fli_lgeng').val(defaut);
        $('#fli_lgall').val("");
        $('#fli_lgesp').val("");
    }

    if(lg=="esp"){
        $('#fli_lgfr').val("");
        $('#fli_lgeng').val("");
        $('#fli_lgall').val("");
        $('#fli_lgesp').val(defaut);
    }

    if(lg=="all"){
        $('#fli_lgfr').val("");
        $('#fli_lgeng').val("");
        $('#fli_lgall').val(defaut);
        $('#fli_lgesp').val("");
    }

    $.ajax({
        cache: false,
        url: '../ajax/info_traduction.php',
        data: 'identifiantlang='+identtifiantchamp+'&slink='+slink,
        dataType: 'json',
        success: function(json) {
            $.each(json, function(index, value) {
                $('#fli_lgfr').val(value.fr_langage);
                $('#fli_lgeng').val(value.eng_langage);
                $('#fli_lgall').val(value.all_langage);
                $('#fli_lgesp').val(value.esp_langage);
                $('#fli_desctipiton').html(value.infoprincipal);
                $('#fli_actifprinicpal').val(value.principal_langage).change();
            });
        }
    });
}