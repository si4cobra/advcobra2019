<?php

class class_form extends class_list
{
    /**
     * Renvoi le code pour la requete insert et update
     * @param : $tabChzmp
     * @return : sReq
     */
    public function requete_enregistrer_modifier_champ($tabChzmp)
    {
        $sReq = "";


        return $sReq;
    }

    /**
     * methode qui nettoie les valeurs passé en parametre get ou post
     */
    public function nettoyer_formulaire()
    {


    }

    /**
     * contrôle les champ formulaire et renvoi tableau result et liste erreur
     */
    public function controler_formulaire()
    {


    }

    /**
     * renvoi la requete total d’enregistrement
     */
    public function requete_complete_enregistrer()
    {

    }

    /**
     * renvoi la requete total de modification
     */
    public function requete_complete_modifier()
    {

    }

    /**
     * methode qui renvoi un tableau de liste de requete de liaison avec la ligne qui est entrain d’être modifiée
     */
    public function requete_liaison()
    {

    }

    /**
     * Action qui est effectué après un enregistrement ou une modification
     */
    public function action_plus_formulaire()
    {

    }

    /**
     * Renvoi la donnée qui est dans la base ou en GET
     */
    public function renvoi_valeur_bdd_principal($aTableauInfo, $objElement)
    {

        $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] = stripcslashes($aTableauInfo[0][$objElement['mapping_champ']]);
        //echo"<pre>";print_r($aTableRetour);echo"</pre>";
        return $aTableRetour;
    }

    /**
     * Renvoi la donnée champ date qui est dans la base ou en GET
     */
    public function renvoi_valeur_bdd_date($aTableauInfo, $objElement)
    {

        $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] = $aTableauInfo[0][$objElement['alias_champ']];


        //echo "date ".$aTableRetour['value'];

        if($aTableRetour['value']=="" or $aTableRetour['value'] == null){
            if( $objElement['date_conversion_enreg'] == 'frtoeng' ){
                $aTableRetour['value']="00/00/0000";
            }else{
                $aTableRetour['value']="0000-00-00";
            }


        }
        //echo "date ".$aTableRetour['value'];


        if( $objElement['date_conversion_enreg'] == 'frtoeng' ) {
            $tab = explode('/', $aTableRetour['value']);
            $aTableRetour['value'] = $tab[2] . "-" . $tab[1] . "-" . $tab[0];
        }
        //echo"<pre>";print_r($aTableRetour);echo"</pre>";
        return $aTableRetour;
    }

    /**
     *   Renvoi la donnée champ date qui est dans la base ou en GET
     */

    public function renvoi_valeur_bdd_checkbox($aTableauInfo, $objElement)
    {

        //on conmpte le nombre de table intermediare si superieur à deux on arrete peut pas enregistrer
        $iCounTableline = count($objElement['table_lien']);

        //echo"<pre>";print_r($aTableauInfo);echo"</pre>";

        $idFiltre = $this->iId;
        if( $iCounTableline == 1 ) {

            // ici on est pour recupérer la bonne valeur de filtre
            if($objElement['utilise_cles_mapping']=="ok"){
                $idFiltre =$aTableauInfo[0][$objElement['mapping_champ']];
            }
            else if( strtolower($objElement['choix_id_lien']) == "guid" ) {
                $u = 0;
            } else {
                if( $this->sChampGuid != "" )
                    $idFiltre = $aTableauInfo[0]['prinicpalidtemp'];
            }

            $sRequete = "SELECT lien." . $objElement['id_item_table_lien'][0] . "
                         FROM " . $objElement['table_lien'][0] . " lien
                           WHERE lien." . $objElement['supplogique_table_lien'][0] . " = 'N'
                           AND lien." . $objElement['id_table_lien'][0] . " = '" . $idFiltre . "' ".$objElement['requete_supplementaire_select_lien'];

            //echo $sRequete."<br>";

            $aResultat = $this->objClassGenerique->renvoi_info_requete($sRequete);
            $aRetour = array();

            if( $aResultat ) {
                foreach( $aResultat as $objResultat ) {
                    $aRetour[] = $objResultat[$objElement['id_item_table_lien'][0]];
                }
            }

        }
        $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] = $aRetour;
        //echo"<pre>";print_r($aTableRetour);echo"</pre>";
        return $aTableRetour;
    }

    /**
     * Methode d'un champ renvoi valeur base de données par le mon d'une varable
     * @author Danon Gnakouri
     * @since  2.0
     */

    public function renvoi_valeur_bdd_par_nom_variable($sNomvariable){

        $sValeurRetour="";
        if( $this->iId != null or (isset($_GET['fi_keymutiple']) and $_GET['fi_keymutiple']=="modif")) {

            $sSuite = "";
            if( $this->bKeyMultiple ) {
                foreach( $this->aElement as &$objElement ) {
                    if( $objElement['is_keymultiple'] == 'ok' ) {

                        $sSuite .= " and principal." . $objElement['mapping_champ'] . "='" . $_GET[$objElement['nom_variable']] . "' ";

                    }
                }
                $sReq = $this->renvoi_requete($sSuite, true, false);
            } else {
                $sReq = $this->renvoi_requete('AND principal.' . $this->sChampId . '=\'' . $this->iId . '\'', true, false);
            }


            //echo $sSuite;
            $aTableauInfo = $this->objClassGenerique->renvoi_info_requete($sReq);
            echo"<pre>";print_r($aTableauInfo);echo"</pre>";

            if(!empty($aTableauInfo)){
                //$sValeurRetour=$aTableauInfo[0][$sNomvariable];
            }
        }

        return $sValeurRetour;
    }

    /**
     * Methode d'un champ renvoi valeur base de données
     * @author Danon Gnakouri
     * @since  2.0
     */
    public function renvoi_valeur_bdd()
    {

        //echo  "passe";


        if( $this->iId != null or (isset($_GET['fi_keymutiple']) and $_GET['fi_keymutiple']=="modif")) {

            $sSuite="";
            if($this->bKeyMultiple){
                foreach( $this->aElement as &$objElement ) {
                    if($objElement['is_keymultiple'] == 'ok' ){

                         $sSuite .= " and principal." . $objElement['mapping_champ'] . "='" . $_GET[$objElement['nom_variable']] . "' ";

                    }
                }
                $sReq = $this->renvoi_requete($sSuite, true, false);
            }else{
                $sReq = $this->renvoi_requete('AND principal.' . $this->sChampId . '=\'' . $this->iId . '\'', true, false);
            }


            //echo $sSuite;
            $aTableauInfo = $this->objClassGenerique->renvoi_info_requete($sReq);

            //echo $sReq;

            if( isset($aTableauInfo[0]) ) {
                foreach( $this->aElement as &$objElement ) {

                    //creation de la methode qui renvoi methode pour l'affichage de la valeur
                    $sTypeChamp = $objElement['type_champ'];
                    $sMethodeChampDonne = "renvoi_valeur_bdd_" . $sTypeChamp;

                    if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' && $objElement['traite_sql'] == 'ok' ) {
                        if( method_exists($this, $sMethodeChampDonne) ) {
                            $aTabRetour = $this->$sMethodeChampDonne($aTableauInfo, $objElement);
                        } else {
                            //sinon on appele la method prinicipal
                            $aTabRetour = $this->renvoi_valeur_bdd_principal($aTableauInfo, $objElement);
                        }

                        $objElement['valeur_variable'] = $aTabRetour['value'];
                    }

                }
            }
            unset($objElement);
        }
    }

    /**
     * Methode d'un champ renvoi valeur du formulaire
     */
    public function renvoi_valeur_form()
    {
        if( !empty($_POST) ) {
            foreach( $this->aElement as &$objElement ) {
                if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {
                    if( isset($_POST[$objElement['nom_variable']]) ) {
                        //echo"<pre>";print_r($_POST[$objElement['nom_variable']]);echo"</pre>";
                        $objElement['valeur_variable'] = $_POST[$objElement['nom_variable']];
                    }
                }
            }
            unset($objElement);
        }
    }

    /*
     * methode qui renvoi l'objet du formulaire
     */
    public function renvoi_formulaire_principal($objElement)
    {

        if( $objElement['type_champ'] == 'modifpassword' ) {
            if( empty($_GET[$this->sChampId]) ) {
                $objElement['type_champ'] = "";
            } else {
                if( $objElement['injection_code'] != "" ) {
                    $sChaineId = str_replace("<id>", $_GET[$this->sChampId], $objElement['injection_code']);
                    $objElement['injection_code'] = $sChaineId;
                }
            }

        } else {
            if( isset($objElement['valeur_forcer']) && $objElement['valeur_forcer'] != '' ) {
                $objElement['valeur_variable'] = $objElement['valeur_forcer'];
            }
        }


        return $objElement;
    }

    /*
     * methode qui renvoi l'objet formulaire pour select
     */
    public function renvoi_formulaire_select($objElement)
    {
        //echo"<pre>";print_r($objElement['lesitem']);echo"</pre>";
        if( (empty($objElement['lesitem']) or $objElement['lesitem'] == "") and  $objElement['table_item_aff_liste']!="nop") {
            if( preg_match('/<bdd>/', $objElement['affichage_table_item']) ) {
                $objElement['affichage_table_item'] = str_replace('<bdd>', $objElement['table_item'], $objElement['affichage_table_item']);

            }

            $objElement['affichage_table_item'] = str_replace("[bdd]", $objElement['table_item'], $objElement['affichage_table_item']);
            $bByPassSelect = false;
            if( $objElement['id_valeur_select'] == 'NaN' ) {
                $bByPassSelect = true;
            }
            //echo "toto";
            //echo"<pre>";print_r($objElement);echo"</pre>";
            if( $this->sEtatForm == 'update' ) {
                $objElement['id_valeur_select'] = $objElement['valeur_variable'];
            }

            if( $objElement['orderby_table_item']=="")
                $objElement['orderby_table_item']=" ORDER by ".$objElement['affichage_table_item'];


            $objElement['lesitem'] = $this->renvoi_tableau_liste($objElement['id_table_item'],
                $objElement['affichage_table_item'],
                $objElement['table_item'],
                $objElement['supplogique_table_item'],
                $objElement['select_pere_fils'],
                $objElement['orderby_table_item'],
                $objElement['select_value_pere_base'],
                array(),
                0,
                $objElement['id_valeur_select'],
                $bByPassSelect,
                 $objElement['requete_supplementaire_select'],
                 $objElement['alias_table_lien'],
                $objElement['requete_supplementaire_select_lien']
            );
        }

        if($objElement['table_item_aff_liste']=="nop"){

            /*
                $tmp =  $this->renvoi_tableau_liste($objElement['id_table_item'],
                $objElement['affichage_table_item'],
                $objElement['table_item'],
                $objElement['supplogique_table_item'],
                $objElement['select_pere_fils'],
                $objElement['orderby_table_item'],
                $objElement['select_value_pere_base'],
                array(),
                0,
                $objElement['id_valeur_select'],
                false,
                $objElement['requete_supplementaire_select'],
                $objElement['alias_table_lien']);
            */
            $tmp =array();
            $sAffiche = $objElement['valeur_variable'];
            $sRequete_info="";
            if($objElement['table_item']!="" and $objElement['affichage_table_item']!="" and $objElement['id_table_item']!=""){

                //récupération de la valeur selection quand nous somme en uddate
                if($this->sEtatForm=="update"){
                    $sRequete_info ="SELECT ".$objElement['affichage_table_item']." as nom  FROM ".$objElement['table_item']." 
                    WHERE ".$objElement['id_table_item']."='".$objElement['valeur_variable']."' ";

                    if($objElement['supplogique_table_item']!="")
                        $sRequete_info.=" and ".$objElement['supplogique_table_item']."='N'";

                    //echo $sRequete_info."<br>";

                    $aTableauInfo = $this->objClassGenerique->renvoi_info_requete($sRequete_info);
                    if(!empty($aTableauInfo)){
                        $sAffiche = $aTableauInfo[0]['nom'];
                    }
                }
            }

            //echo "requetenop ".$this->sEtatForm." ".$sRequete_info."<br>";

            $tmp[$objElement['valeur_variable']]=$sAffiche;
            $objElement['lesitem']=$tmp;
        }
        //echo"<pre>";print_r($objElement);echo"</pre>";
        return $objElement;
    }/*
 * Methode qui renvoi l'objet formulaire pour radio
 */
    public function renvoi_formulaire_radio($objElement)
    {

        return $this->renvoi_formulaire_select($objElement);

    }

    /*
     * Methode qui renvoi l'objet formulaire pour checkbox
     */
    public function renvoi_formulaire_checkbox($objElement)
    {

        return $this->renvoi_formulaire_select($objElement);

    }

    /*
     * Methode qui renvoi l'objet formulaire pour selectmultiple
     */
    public function renvoi_formulaire_selectmultiple($objElement)
    {

        return $this->renvoi_formulaire_select($objElement);

    }


    public function renvoi_tableau_liste($sID, $sNom, $sTable, $sSupplogique, $sIdPere, $sOrderseelct, $sIdligne, $aTableauRetour, $iDecalage,
                                         $iIdValeurSelect = '', $bByPassSelect = false,$sRequetefiltreSupplementaire="",$sAliasbase="",$RequeeSupplementiarelien="")
    {

        $aTableauListeRetour = array();
        if($sAliasbase!="")
            $sAliasbase=" as ".$sAliasbase;

        //echo "aff ".$sRequetefiltreSupplementaire." ".$sAliasbase;

        $sRequete_Liste = "SELECT " . $sID . "," . $sNom . "
         FROM " . $sTable .$sAliasbase. " WHERE " . $sSupplogique . "='N' " ;
        if( $sIdPere != "" ) {
            $sRequete_Liste .= " and " . $sIdPere . "='" . $sIdligne . "'";
        }
        if( $bByPassSelect ) {
            if( $iIdValeurSelect == 'NaN' || !empty($iIdValeurSelect) ) {
                $sRequete_Liste .= " and " . $sID . "='" . $iIdValeurSelect . "'";
            }
        }
        $sRequete_Liste .= $sRequetefiltreSupplementaire;

        if($sOrderseelct=="")
            $sOrderseelct= " order by ".$sNom;

        $sRequete_Liste .=" ".$sOrderseelct;

        //echo 'toto2'.$iIdValeurSelect.'<br/>';
        //echo $sRequete_Liste."<br>";


        $aTableauListeRetour = $this->objClassGenerique->renvoi_info_requete($sRequete_Liste);

        if( !empty($aTableauListeRetour) ) {
            foreach( $aTableauListeRetour as $valeurlist ) {
                $sDecalage = "";
                for( $i = 0; $i < $iDecalage; $i++ ) {
                    $sDecalage .= "****";
                }

                $aTableauRetour[$valeurlist[$sID]] = $sDecalage . stripslashes($valeurlist[$sNom]);

                if( $sIdPere != "" )
                    $aTableauRetour = $this->renvoi_tableau_liste($sID, $sNom, $sTable, $sSupplogique, $sIdPere, $sOrderseelct, $valeurlist[$sID], $aTableauRetour, $iDecalage + 1);
            }
        }

        return $aTableauRetour;

    }

    /**
     * Methode d'un champ renvoi du formulaire
     * @return d'un tableau cles valeurs
     */
    public function renvoi_formulaire()
    {

        //echo'totoform';
        if( $this->sMessageErreurAjoutChamp != '' ) {
            echo $this->sMessageErreurAjoutChamp;
            exit(0);
        }


        $aForm = array();
        //echo "<br><br>LE formulaire <br>";
        //echo"<pre>";print_r($this->aElement );echo"</pre>";

        $aTableauInfoUser = class_fli::get_aData_entier();
        //echo"<pre>";print_r(class_fli::get_aData_entier());echo"</pre>";
        if(!isset($aTableauInfoUser['langue_user'])){
            $aTableauInfoUser['langue_user']="fr";
        }

        $sLang =$aTableauInfoUser['langue_user'];
        $aTableauTraduction = $this->renvoi_tabtraduction();
        
        //echo"<pre>";print_r($aTableauTraduction);echo"</pre>";

        //recupération des différentes tradcution
        if(isset($aTableauTraduction['fli_titrefrom'][$sLang]) and $aTableauTraduction['fli_titrefrom'][$sLang]!=""){
            $this->sTitreForm = $aTableauTraduction['fli_titrefrom'][$sLang];
            //echo "test";
        }

        if(isset($aTableauTraduction['fli_catfrom'][$sLang]) and $aTableauTraduction['fli_catfrom'][$sLang]!=""){
            $this->sCategorieForm = $aTableauTraduction['fli_catfrom'][$sLang];
        }

        //recriture du titre;

       /**/

        //echo"<pre>";print_r($this->aElement);echo"</pre>";
        foreach( $this->aElement as $objElement ) {


            //echo "toto";
            if(isset($objElement['titre_inter_module'])) {
               // echo "toto";
                if( $objElement['titre_inter_module'] == "ok" ) {
                   // echo "toto";
                    $sIdfiltre ="";
                    if(isset($_GET[$objElement['nom_variable']])){
                        $sIdfiltre = $_GET[$objElement['nom_variable']];
                    }
                    if(isset($_POST[$objElement['nom_variable']])){
                        $sIdfiltre = $_GET[$objElement['nom_variable']];
                    }
                    if( isset($objElement['bbd_titre_inter_module']) && isset($objElement['champfiltre_titre_inter_module'])
                        && isset($objElement['champaffiche_titre_inter_module'])
                    ) {

                        $sRequete_info_titre = "SELECT " . $objElement['champaffiche_titre_inter_module'] . " as nom from " . $objElement['bbd_titre_inter_module'] . " where " . $objElement['champfiltre_titre_inter_module'] . "='" . $sIdfiltre . "'";

                        //echo $sRequete_info_titre . "<br>";

                        /*echo "<pre>";
                        print_r($objElement);
                        echo "</pre>";
                        */

                        $aTableauRetourTitre = $this->objClassGenerique->renvoi_info_requete($sRequete_info_titre);


                        //echo "stitre ".$sRequete_info_titre."<br>";

                        if( !empty($aTableauRetourTitre) ) {
                            $sMessageListe = sprintf($this->sTitreListe, $aTableauRetourTitre[0]['nom']);
                            $this->sTitreListe = $sMessageListe;
                            $sMessageForm = sprintf($this->sTitreForm, $aTableauRetourTitre[0]['nom']);
                            $this->sTitreForm = $sMessageForm;
                            $sMessageCreation = sprintf($this->sLabelCreationElem, $aTableauRetourTitre[0]['nom']);
                            $this->sLabelCreationElem = $sMessageCreation;
                            $sMessagesCategorieForm = sprintf($this->sCategorieForm, $aTableauRetourTitre[0]['nom']);
                            $this->sCategorieForm = $sMessagesCategorieForm;
                        }

                        //echo "stitre ". $this->sTitreListe."<br>";


                    }
                }
            }

            if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {

                if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' ) {
                    if( isset($_GET[$objElement['nom_variable']]) ) {
                        $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];
                    }
                }

                $sIdfiltre ="";
                /*if(isset($_GET[$objElement['nom_variable']])){
                    $sIdfiltre = $_GET[$objElement['nom_variable']];
                }
                if(isset($_POST[$objElement['nom_variable']])){
                    $sIdfiltre = $_GET[$objElement['nom_variable']];
                }

                if(isset($aTableauTraduction[$objElement['nom_variable']][$sLang]) and $aTableauTraduction[$objElement['nom_variable']][$sLang]!=""){
                    $objElement['text_label'] = $aTableauTraduction[$objElement['nom_variable']][$sLang];
                }*/

                //creation de la methode qui renvoi methode pour l'affichage de la valeur
                $sTypeChamp = $objElement['type_champ'];
                $sMethodeChampDonne = "renvoi_formulaire_" . $sTypeChamp;

                //echo " toto".$sMethodeChampDonne."<br>";

                if( method_exists($this, $sMethodeChampDonne) ) {
                    $aForm[] = $this->$sMethodeChampDonne($objElement);
                } else {
                    //sinon on appele la method prinicipal
                    $aForm[] = $this->renvoi_formulaire_principal($objElement);
                }

            }
        }

        return $aForm;
    }

    /**
     * contrôle champ prinicpal
     */
    public function controle_form_principal($objElement)
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        $aTableauRetour['message'] = "";

        if( $objElement['ctrl_champ'] == 'ok' ) {

            if( !isset($_POST[$objElement['nom_variable']]) || trim($_POST[$objElement['nom_variable']]) == '' ) {
                $aTableauRetour['result'] = false;
                $aTableauRetour['message'] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
            }
        }


        return $aTableauRetour;
    }

    /**
     * contrôle champ password
     */
    public function controle_form_password($objElement)
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        $aTableauRetour['message'] = "";

        if( $objElement['ctrl_champ'] == 'ok' ) {

            if( $objElement['double_password'] == 'ok' && $_POST[$objElement['nom_variable']] != $_POST[$objElement['nom_variable'] . '_2'] ) {
                $aTableauRetour['result'] = false;
                $aTableauRetour['message'] = "Les mots de passe ne correspondent pas.";
            }

            if( strlen(trim($_POST[$objElement['nom_variable']])) < $objElement['length_password'] ) {
                $aTableauRetour['result'] = false;
                $aTableauRetour['message'] = "Le mot de passe doit être constitué d'au moins " . $objElement['length_password'] . " caractères !";
            }

            if( !isset($_POST[$objElement['nom_variable']]) || trim($_POST[$objElement['nom_variable']]) == '' ) {
                $aTableauRetour['result'] = false;
                $aTableauRetour['message'] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
            }
        }

        return $aTableauRetour;
    }

    /**
     * contrôle champ checkbox
     */
    public function controle_form_checkbox($objElement)
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        $aTableauRetour['message'] = "";

        if( $objElement['ctrl_champ'] == 'ok' ) {

            if( !isset($_POST[$objElement['nom_variable']]) || empty($_POST[$objElement['nom_variable']]) ) {
                $aTableauRetour['result'] = false;
                $aTableauRetour['message'] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
            }
        }

        return $aTableauRetour;
    }

    /**
     * contrôle champ selectmultiple
     */
    public function controle_form_multisaisie($objElement)
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        $aTableauRetour['message'] = "";

        $aTableauRetour = $this->controle_form_checkbox($objElement);
        return $aTableauRetour;
    }

    /**
     * contrôle champ selectmultiple
     */
    public function controle_form_selectmultiple($objElement)
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        $aTableauRetour['message'] = "";

        $aTableauRetour = $this->controle_form_checkbox($objElement);
        return $aTableauRetour;
    }

    /**
     * contrôle champ date
     */
    public function controle_form_date($objElement)
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        $aTableauRetour['message'] = "";

        if( $objElement['ctrl_champ'] == 'ok' ) {
            if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' || strtotime($_POST[$objElement['nom_variable']]) == false ) {
                $aTableauRetour['result'] = false;
                $aTableauRetour['message'] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
            }
        }

        return $aTableauRetour;
    }

    /**
     * contrôle champ time
     */
    public function controle_form_time($objElement)
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;
        $aTableauRetour['message'] = "";

        if( !isset($_POST[$objElement['nom_variable']]) || $_POST[$objElement['nom_variable']] == '' || strtotime($_POST[$objElement['nom_variable']]) == false ) {
            $aTableauRetour['result'] = false;
            $aTableauRetour['message'] = $objElement['text_label'] . " " . sprintf($objElement['mess_erreur'], $objElement['text_label']);
        }
        return $aTableauRetour;
    }

    /*
     * Methode qui contrôle des champ qui sont en obligatiore
     */
    public function controle_form()
    {
        $aTableauRetour = array();
        $aTableauRetour['result'] = true;


        if( empty($_POST) ) {
            $aTableauRetour['result'] = false;
        }

        foreach( $this->aElement as $objElement ) {

            //creation de la méthode qui permet de faire le contrôle en fonction du type du champ
            $sTypeChamp = $objElement['type_champ'];
            $sMethodeChampDonne = "controle_form_" . $sTypeChamp;

            if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' && $objElement['traite_sql'] == 'ok' ) {
                if( method_exists($this, $sMethodeChampDonne) ) {
                    $aTabRetour = $this->$sMethodeChampDonne($objElement);
                } else {
                    //sinon on appele la method prinicipal
                    $aTabRetour = $this->controle_form_principal($objElement);
                }

                $aTableauRetour['message'][] = $aTabRetour['message'];

                if( !$aTabRetour['result'] )
                    $aTableauRetour['result'] = $aTabRetour['result'];
            }
        }

        return $aTableauRetour;
    }

    /*
     * Methode qui requete pour enregistre ou modifie principal date
     */
    public function enreg_modif_principal($objElement)
    {

        $sSql = $objElement['mapping_champ'] . "='" . addslashes($objElement['valeur_variable']) . "'";
        return $sSql;
    }

    /*
     * Methode qui requete pour enregistre ou modifie datetime
     */
    public function enreg_modif_datetime($objElement)
    {

        if( ($objElement['date_now_creation'] == 'ok' && !$this->iId) || ($objElement['date_now_modification'] == 'ok') ) {
            $objElement['valeur_variable'] = date('Y-m-d H:i:s');
            $sSql = $objElement['mapping_champ'] . "='" . $objElement['valeur_variable'] . "'";
        } else if( $objElement['date_now_creation'] != 'ok' && $objElement['date_now_modification'] != 'ok' ) {
            $sSql = $objElement['mapping_champ'] . " ='" . $objElement['valeur_variable'] . "'";
        }

        return $sSql;
    }

    /*
    * Methode qui requete pour enregistre ou modifie date
    */
    public function enreg_modif_date($objElement)
    {
        $sSql="";
        if( ($objElement['date_now_creation'] == 'ok' && !$this->iId) || ($objElement['date_now_modification'] == 'ok') ) {
            $objElement['valeur_variable'] = date('Y-m-d');
            $sSql = $objElement['mapping_champ'] . "='" . $objElement['valeur_variable'] . "'";
        } else if( $objElement['date_now_creation'] != 'ok' && $objElement['date_now_modification'] != 'ok' ) {
            if( $objElement['date_conversion_enreg']=="eng")
                $sSql = $objElement['mapping_champ'] . " ='" . class_helper::renvoi_date($objElement['valeur_variable'],"eng") . "'";
            else
                $sSql = $objElement['mapping_champ'] . " ='" . $objElement['valeur_variable'] . "'";
        }

        return $sSql;
    }

    /*
     * Methode qui requete pour enregistre ou modifie password
     */
    public function enreg_modif_password($objElement)
    {

        $objImport = new class_import();
        $objImport->import_modele('fli_admin', 'mod_fli_admin');

        $objModAdmin = new mod_fli_admin();

        $aSel = $objModAdmin->get_sel_user($this->iId);
        if( isset($aSel[0]['sel_user']) && !empty($aSel[0]['sel_user']) ) {
            $sSel = $aSel[0]['sel_user'];
        } else {
            $sSel = sha1(md5(uniqid(rand(), true)));
            $objModAdmin->set_sel_user($sSel, $this->iId);
        }

        $password = hash('sha512', $objElement['valeur_variable'] . $sSel);

        $sSql = $objElement['mapping_champ'] . "='" . $password . "'";

        return $sSql;
    }

    /*
     *methode renvoi checkbox vide
     */
    public function enreg_modif_checkbox()
    {
        $sSql = "";
        return $sSql;
    } /*
     *methode renvoi checkbox vide
     */
    public function enreg_modif_multichamp()
    {
        $sSql = "";
        return $sSql;

        $u = 0;

        foreach( $objElement['nom_variable'] as $valeur ) {

            $sRequete_multisaisie = "INSERT " . $objElement['table_lien'] . " SET " . $objElement['id_item_table_lien'][0] . "='" . $valeur . "'," .
                $objElement['id_table_lien'][0] . "='" . $this->iId . "'," . $objElement['id_item_table_lien'][0] . "='value_" . $objElement['nom_variable'][$u] . "'";


        }
    }

    /*
     *methode renvoi checkbox vide
     */
    public function enreg_modif_selectmulitple()
    {
        $sSql = "";
        return $sSql;
    }

    /*
     * Methode qui requete pour enregistre ou modifie file
     */
    public function enreg_modif_file($objElement)
    {

        $sSql = "";

        if( isset($_FILES[$objElement['nom_variable']]) && isset($_FILES[$objElement['nom_variable']]['tmp_name']) && $_FILES[$objElement['nom_variable']]['tmp_name'] != '' ) {
            $content_dir = $objElement['file_upload'];
            $idrim = rand(1, 100);
            $tmp_file = $_FILES[$objElement['nom_variable']]['tmp_name'];
            $ext_file = '';
            $bCompresse = true;
            //echo"<pre>";print_r($_FILES);echo"</pre>";
            if( $_FILES[$objElement['nom_variable']]['type'] == 'text/html' ) {
                $ext_file = '.html';
                $bCompresse=false;
            }else if($_FILES[$objElement['nom_variable']]['type'] == 'application/x-zip-compressed' ){
                $ext_file = '.zip';
                $bCompresse=false;
            } else if($_FILES[$objElement['nom_variable']]['type'] == 'application/vnd.oasis.opendocument.text' ){
                $ext_file = '.odt';
                $bCompresse=false;
            } else {
                $aTableauDecoupe = getimagesize($tmp_file);
               //echo"<pre>";print_r($aTableauDecoupe);echo"</pre>";
                // print_r($tmp_file.'/'.$_FILES[$objElement['nom_variable']]['name']);
                if( $aTableauDecoupe['mime'] == "application/x-shockwave-flash" ) {
                    $ext_file = ".swf";
                } else if( $aTableauDecoupe['mime'] == "image/png" ) {
                    $ext_file = ".png";
                } else if( $aTableauDecoupe['mime'] == "image/jpeg" ) {
                    $ext_file = ".jpeg";
                } else if( $aTableauDecoupe['mime'] == "image/gif" ) {
                    $ext_file = ".gif";
                } else if( $aTableauDecoupe['mime'] == "image/webp" ) {
                    $ext_file = ".webp";
                } else if( $aTableauDecoupe['mime'] == 'application/zip' ) {
                    $ext_file = '.zip';
                }else if( $_FILES[$objElement['nom_variable']]['type'] == 'application/pdf' ) {
                    $ext_file = '.pdf';
                }
            }

            $imageprincip = date('dmYHms') . "_" . $this->sNomTable . $idrim . $ext_file;
            if( $ext_file == '.swf' || $ext_file == '.png' || $ext_file == '.jpeg' || $ext_file == '.gif' || $ext_file == '.webp' || $ext_file == '.zip'  ) {
                $imageprincipmin = 'min_' . date('dmYHms') . "_" . $this->sNomTable . $idrim . $ext_file;
            }

            move_uploaded_file($tmp_file, $content_dir . $imageprincip);
            if( $ext_file == '.swf' || $ext_file == '.png' || $ext_file == '.jpeg' || $ext_file == '.gif' || $ext_file == '.webp'|| $ext_file == '.zip'   ) {
                copy($content_dir . $imageprincip, $content_dir . $imageprincipmin);
            }

            //Compression
            /* ----------------------------------- Miniature -------------------------------------*/
            if( $objElement['file_compress_min'] == 'ok' ) {
                if( !empty($objElement['file_taux_compress_min']) && is_numeric($objElement['file_taux_compress_min']) ) {
                    $iTauxCompress_min = $objElement['file_taux_compress_min'];
                } else {
                    $iTauxCompress_min = 70;
                }
                if($bCompresse) {
                    if( $aTableauDecoupe['mime'] == "image/png" ) {
                        $iTauxCompress_min = round($iTauxCompress_min * 9 / 100);
                        $im_min = imagecreatefrompng($content_dir . $imageprincipmin);
                        imageAlphaBlending($im_min, true);
                        imageSaveAlpha($im_min, true);
                        imagepng($im_min, $content_dir . $imageprincipmin, $iTauxCompress_min);
                    } else if( $aTableauDecoupe['mime'] == "image/jpeg" ) {
                        imagejpeg(imagecreatefromjpeg($content_dir . $imageprincipmin), $content_dir . $imageprincipmin, $iTauxCompress_min);
                    }
                }
            }

            /* ----------------------------------- Image originale -------------------------------------*/
            if( $objElement['file_compress'] == 'ok' and $bCompresse) {
                if( !empty($objElement['file_taux_compress']) && is_numeric($objElement['file_taux_compress']) ) {
                    $iTauxCompress = $objElement['file_taux_compress'];
                } else {
                    $iTauxCompress = 90;
                }

                if( $aTableauDecoupe['mime'] == "image/png" ) {
                    $iTauxCompress = round($iTauxCompress * 9 / 100);
                    $im = imagecreatefrompng($content_dir . $imageprincip);
                    imageAlphaBlending($im, true);
                    imageSaveAlpha($im, true);
                    imagepng($im, $content_dir . $imageprincip, $iTauxCompress);
                } else if( $aTableauDecoupe['mime'] == "image/jpeg" ) {
                    imagejpeg(imagecreatefromjpeg($content_dir . $imageprincip), $content_dir . $imageprincip, $iTauxCompress);
                }
            }

            $sSql = $objElement['mapping_champ'] . "='" . $imageprincip . "'";
        } else {
            if( $objElement['Activdefaut'] == "ok" ) {
                $sSql = $objElement['mapping_champ'] . "='" . $objElement['filedefaut'] . "'";
            }

        }
        return $sSql;
    }

    /*
    *  Methode d'enregistrement  des informations tablea intermeddiare
    */
    public function maj_table_intermediaire($iId, $sGUID)
    {


        foreach( $this->aElement as &$objElement ) {

            $idfiltre = $iId;

            if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' && $objElement['traite_sql'] == 'ok' && ($objElement['type_champ'] == "selectmultiple" || $objElement['type_champ'] == "checkbox") ) {


                //quand on active
                if($objElement['utilise_cles_mapping']=="ok"){
                    $sRequete_info_id = "SELECT " .$objElement['mapping_champ']."
                        FROM " . $this->sNomTable . "
                        WHERE " . $this->sChampId . "='" . $iId . "'";
                    //echo $sRequete_info_id."\n";

                    //echo "Requete ".$sRequete_info_id."<br>id : ".$idfiltre;
                    $aTableauInfo = $this->objClassGenerique->renvoi_info_requete($sRequete_info_id);
                    $idfiltre = $aTableauInfo[0][$objElement['mapping_champ']];
                    //exit();
                }
                else if( isset($objElement['choix_id_lien']) and strtolower($objElement['choix_id_lien']) == "guid" )
                    $idfiltre = $sGUID;
                else if( $this->sChampGuid == "" ) {
                    $u = 0;
                } else if( $this->sEtatForm == "insert" ) {
                    $u = 0;
                } else {
                    //ic on réciupère l'id primaire de la clae
                    $sRequete_info_id = "SELECT " . $this->sChampIdTmp . "
                    FROM " . $this->sNomTable . "
                    WHERE " . $this->sChampId . "='" . $iId . "'";
                    //echo $sRequete_info_id."\n";
                    $aTableauInfo = $this->objClassGenerique->renvoi_info_requete($sRequete_info_id);
                    $idfiltre = $aTableauInfo[0][$this->sChampIdTmp];


                }

                // Remise à zero de la ligne
                $sSqlRaz = "UPDATE " . $objElement['table_lien'][0] . " set " . $objElement['supplogique_table_lien'][0] . "='Y'
                where " . $objElement['id_table_lien'][0] . "='" . $idfiltre . "' ".$objElement['requete_supplementaire_select_lien'];

                //echo $sSqlRaz;

                $this->objClassGenerique->execute_requete($sSqlRaz);


                // on parcour la tableau des choix selectionné
                if( !empty($objElement['valeur_variable']) ) {
                    $u = 0;
                    $date_modification_table_lien = '';
                    if( isset($objElement['date_modification_table_lien']) && $objElement['date_modification_table_lien'] != '' ) {
                        $date_modification_table_lien = ', ' . $objElement['date_modification_table_lien'] . ' = \'' . date('Y-m-d H:i:s') . '\'';
                    }

                    // echo"<pre>";print_r($objElement['valeur_variable']);echo"</pre>";

                    foreach( $objElement['valeur_variable'] as $objValeur ) {

                        //rajout de la requete en plus quand il y a des champs supplementaires dands le lien
                        $sSqlAjoutChampSupplementaire = "";
                        if( !empty($objElement['item_table_champ_suplementaire']) ) {
                            foreach( $objElement['item_table_champ_suplementaire'] as $objAjoutSupp ) {
                                $sSqlAjoutChampSupplementaire .= "," . $objAjoutSupp['mapping_champ'] . "='" . $objAjoutSupp['valeur_variable'][$u] . "'";
                            }
                        }

                        $sSqlEnregModif = "INSERT " . $objElement['table_lien'][0] . " SET " . $objElement['id_table_lien'][0] . "='" . $idfiltre . "'
					, " . $objElement['id_item_table_lien'][0] . " = '" . $objValeur . "', " . $objElement['supplogique_table_lien'][0] . " = 'N'"
                            . $date_modification_table_lien . $sSqlAjoutChampSupplementaire . " ON DUPLICATE KEY update " . $objElement['supplogique_table_lien'][0] . " = 'N'"
                            . $date_modification_table_lien . $sSqlAjoutChampSupplementaire;

                        //echo $sSqlEnregModif."<br>\n";
                        $this->objClassGenerique->execute_requete($sSqlEnregModif);
                        $u++;
                    }
                }

            }
        }
    }

    /*
    *  Methode d'enregistrement  des informations
    */
    public function enreg_modif_form($sRequeteplus = '')
    {
        $aKeyValue = array();
        $aKeyValueUpdate = array();
        $aTabLien = array();
        $aTabKeysOnly = array();
        $sFiltreMultipleKey="";
        $sGUID="";

        foreach( $this->aElement as &$objElement ) {
            if( $objElement['traite_sql'] == 'ok' ) {
                if( $objElement['aff_form'] == 'ok' ) {

                    if( isset($_POST[$objElement['nom_variable']]) ) {
                        $objElement['valeur_variable'] = $_POST[$objElement['nom_variable']];
                    } else if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' ) {
                        if( isset($_GET[$objElement['nom_variable']]) ) {
                            $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];
                        }
                    } else {
                        $objElement['valeur_variable'] = '';
                    }
                }


                /*
                 * Ici en crée les filtre update pourle cles mutiple
                 */
                if($this->bKeyMultiple){
                    if($objElement['is_keymultiple']=="ok") {
                        if( $sFiltreMultipleKey != "" )
                            $sFiltreMultipleKey .=" and ";

                        $sFiltreMultipleKey.=$objElement['mapping_champ']."=:".$objElement['nom_variable']."";
                    }
                }

                //création de la méthode qui permet de faire le requete en fonction du type du champ
                $sTypeChamp = $objElement['type_champ'];
                $sMethodeChampDonne = "enreg_modif_" . $sTypeChamp;

                if( (isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' && $objElement['traite_sql'] == 'ok') ||  $objElement['date_now_creation'] == 'ok' || $objElement['date_now_modification'] == 'ok') {
                    if( method_exists($this, $sMethodeChampDonne) ) {

                        $sBoutRequeteSql = $this->$sMethodeChampDonne($objElement);

                    } else {

                        //sinon on appele la method prinicipal
                        $sBoutRequeteSql = $this->enreg_modif_principal($objElement);
                    }

                    if( trim($sBoutRequeteSql) != "" ){
                        $aKeyValue[] = $sBoutRequeteSql;
                        if($objElement['is_keymultiple']!="ok") {
                            $aKeyValueUpdate[]=$sBoutRequeteSql;
                        }
                    }


                }

            }


        }
        unset($objElement);

        //echo"<pre>";print_r($aKeyValue);echo"</pre>";

        //$aKeyValue[] = $this->sChampSupplogique.'=\'N\'';

        //Le tableau qui permet de recupéré les valeurs sur une action en pre update ou pre insert
        $aTableauPreAcion = array();
        $aTableauPreAcion['bResult'] = true;
        $aTableauPreAcion['sMMessage'] = "";
        $aTableauPreAcion['Id'] = 0;
        $aTableauPreAcion['Guid'] = "";
        $aTableauPreAcion['sSql'] = "";
        $aTableauPreAcion['sSql_Where'] = "";
        $aTableauPreAcion['bResult_Post'] = true;
        $aTableauPreAcion['sMessage_Post'] = "";

        //lavaleur du choix de la fonction en fonction insert ou  update
        $sTypeFunctionpost = "";

        //echo"<pre>";print_r($aKeyValue);echo"</pre>";

        if( $this->iId or (isset($_POST['fi_keymutiple']) and $_POST['fi_keymutiple']=="modif") ) {

            $sTypeFunctionpost = "update";
            foreach( $aKeyValue as $key => $value ) {
                $aTabTmp = explode('=', $value, 2);
                //$aTabKeyValueSql[trim($aTabTmp[0])] = trim($aTabTmp[1],'\'');
                $aTabKeyValueSql[trim($aTabTmp[0])] = trim($aTabTmp[1], '\'');
                //$aTabKeyValueSql[trim($aTabTmp[0])] = $aTabTmp[1];
                $aTabKeysOnly[] = trim($aTabTmp[0]) . '=:' . trim($aTabTmp[0]);
            }
            //echo 'TOTOTOTOTOTOTO<br/>';
            //echo"<pre>";print_r($aTabKeyValueSql);echo"</pre>";exit;
            $sNomMthodeActuelle = "pre_form_update_" . class_fli::get_fli_fonction();

            if( method_exists($this, $sNomMthodeActuelle) ) {
                $aTableauPreAcion = $this->$sNomMthodeActuelle();

            }
            //echo"<pre>";print_r($aTabKeyValueSql);echo"</pre>";

            if( $aTableauPreAcion['bResult'] ) {

                /*foreach( $aKeyValueUpdate as $key => $value ) {
                    $aTabTmp = explode('=', $value, 2);
                    //$aTabKeyValueSql[trim($aTabTmp[0])] = trim($aTabTmp[1],'\'');
                    $aTabKeyValueSql[trim($aTabTmp[0])] = trim($aTabTmp[1], '\'');
                    $aTabKeysOnly[] = trim($aTabTmp[0]) . '=:' . trim($aTabTmp[0]);
                }*/

                $sRequeteUpdate = "
				UPDATE " . $this->sNomTable . "
				SET " . implode(',', $aTabKeysOnly) . " " . $this->sSuiteRequeteUpdate . " " . $aTableauPreAcion['sSql'] . "
				 ";


                if($this->sChampSupplogique!="") {

                    if(!empty($aTabKeysOnly))
                        $sRequeteUpdate .=", ";
                    $sRequeteUpdate .= $this->sChampSupplogique."='".$this->sValONSupplogique."' ";
                }

                $sRequeteUpdate .= " WHERE ";
                if($this->bKeyMultiple){
                    $sRequeteUpdate .=$sFiltreMultipleKey;
                }else {
                    $sRequeteUpdate .= $this->sChampId . "=:" . $this->sChampId . " ";
                }
                $sRequeteUpdate.= $aTableauPreAcion['sSql_Where'];

                if($this->sChampId!="")
                    $aTabKeyValueSql[$this->sChampId] = $this->iId;
                //echo "requete_update ".$sRequeteUpdate."<br>";
                //echo"<pre>";print_r($aTabKeyValueSql);echo"</pre>";
                if( $this->bSansActionSQL ) {
                    $bresultat = $this->objClassGenerique->insert_update_requete($sRequeteUpdate, $aTabKeyValueSql);
                    //echo"<pre>";print_r($bresultat);echo"</pre>";
                } else {
                    $bresultat = true;
                }


                class_fli::set_debug("enreg_modif_form-requete_update", $sRequeteUpdate);

                if( $bresultat ) {
                    $this->aTabResultUpdate['id'] = $this->iId;
                    $this->aTabResultUpdate['result'] = "ok";
                    if( isset($this->sScriptJavascriptUpdate) && !empty($this->sScriptJavascriptUpdate) )
                        $this->objSmarty->assign('sScriptJavascriptUpdate', $this->sScriptJavascriptUpdate);
                }
            }


            $iId = $this->iId;
            $sGUID = $this->iId;

            if( $this->bDebugRequete ) {
                $this->sDebugRequeteInsertUpdate = $sRequeteUpdate;
            }


        } else {
            $sTypeFunctionpost = "insert";
            $iId = "";
            foreach( $aKeyValue as $key => $value ) {
                $aTabTmp = explode('=', $value, 2);
                //$aTabKeyValueSql[trim($aTabTmp[0])] = addslashes(trim($aTabTmp[1], '\''));
                $aTabKeyValueSql[trim($aTabTmp[0])] = (trim($aTabTmp[1], '\''));
                $aTabKeysOnly[] = trim($aTabTmp[0]) . '=:' . trim($aTabTmp[0]);
            }

            $sNomMthodeActuelle = "pre_form_insert_" . class_fli::get_fli_fonction();

            if( method_exists($this, $sNomMthodeActuelle) ) {
                $aTableauPreAcion = $this->$sNomMthodeActuelle();

            }
            if( $aTableauPreAcion['bResult'] ) {
                //echo"<pre>";print_r($aTabKeyValueSql);echo"</pre>";

                $sGUID = class_helper::guid();

                $sRequeteInsert = '
				INSERT ' . $this->sNomTable . '
				SET ' . implode(',', $aKeyValue) . ' ' . $this->sSuiteRequeteInsert . (!empty($this->sChampGuid) ? ' ,' . $this->sChampGuid . '=\'' . $sGUID . '\'' : '') . '
				 ' . $aTableauPreAcion['sSql'] ;

                /*
                 * si nous ne sommes en pas en situation de key multiple
                 */
                if(!$this->bKeyMultiple and $this->sChampSupplogique!="")
                     $sRequeteInsert.= ' on duplicate key update ' . $this->sChampSupplogique . '= \''.$this->sValONSupplogique.'\' ';


               // echo $sRequeteInsert."<br>";
                if( $this->bSansActionSQL ) {
                    $iId = $this->objClassGenerique->insert_update_requete($sRequeteInsert, $aTabKeyValueSql);
                    //echo"<pre>";print_r($iId);echo"</pre>";

                    if(is_array($iId)){
                        if(!$iId['result']){
                            $aTableauPreAcion['bResult']=false;
                            $aTableauPreAcion['sMMessage']=$iId['message'];
                        }
                    }

                } else {
                    $iId = $this->iIdCustom;
                }

                class_fli::set_debug("enreg_modif_form-requete_update", $sRequeteInsert);

                if( $this->bSansActionSQL ) {
                    if( $iId != false ) {
                        $this->aTabResultInsert['id'] = $iId;
                        $this->aTabResultInsert['guid'] = $sGUID;
                        $this->aTabResultInsert['result'] = "ok";
                        //echo $this->sScriptJavascriptInsert;exit;
                        if( isset($this->sScriptJavascriptInsert) && !empty($this->sScriptJavascriptInsert) )
                            $this->objSmarty->assign('sScriptJavascriptInsert', $this->sScriptJavascriptInsert);
                    }
                }
                if( $this->bDebugRequete ) {
                    $this->sDebugRequeteInsertUpdate = $sRequeteInsert;
                }
            }
        }

        //echo $sGUID."<br>".$iId;
        //******************************* si le tableau preaction est ok  ****************************
        if( $aTableauPreAcion['bResult'] ) {

            $this->maj_table_intermediaire($iId, $sGUID);
            $sNomMthodeActuelle = "post_form_" . $sTypeFunctionpost . "_" . class_fli::get_fli_fonction();
            if( method_exists($this, $sNomMthodeActuelle) ) {
                $aTmpAction = $this->$sNomMthodeActuelle();
                $aTableauPreAcion['bResult_Post'] = $aTmpAction['bResult_Post'];
                $aTableauPreAcion['sMessage_Post'] = $aTmpAction['sMessage_Post'];
            }
            $sSuivantvaleur = $iId;
            if( !empty($this->sChampGuid) ) {
                $sSuivantvaleur = $sGUID;
            }

            if( $this->get_parametre_bwizard() ) {
                header('location:' . $this->sParametreWizardSuivant . "?" . $this->sParametreWizardNomVariableSuivant . "=" . $sSuivantvaleur);
            }else{
                $sLabelCreationElemUrl =
                    $this->renvoi_base_url()
                    . $this->renvoi_parametre_inter_module()
                    . $this->renvoi_parametre_recherche()
                    . $this->renvoi_parametre_filtre_liste()
                    . $this->renvoi_parametre_retour_liste()
                    . $this->sParametreWizardListe;
                //header('location:' .$sLabelCreationElemUrl);

            }

        }

        $aTableauPreAcion['iId'] = $iId;
        $aTableauPreAcion['sGuid'] = $sGUID;

        return $aTableauPreAcion;
    }


    public function renvoi_url_retour_suppression_form_externe()
    {
        if( !isset($this->sUrlRetourSuppressionFormExterne) ) {
            $sUrlRetourSuppressionFormExterne = $this->sRetourSuppressionFormExterne;

            if( $sUrlRetourSuppressionFormExterne != '' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourSuppressionFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sUrlRetourSuppressionFormExterne = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourSuppressionFormExterne);
                        }
                    }
                }
            }

            $this->sUrlRetourSuppressionFormExterne = $sUrlRetourSuppressionFormExterne;
        }

        return $this->sUrlRetourSuppressionFormExterne;
    }

    public function renvoi_parametre_retour_validation_form_externe()
    {
        if( !isset($this->sParametreRetourValidationFormExterne) ) {
            $sParametreRetourValidationFormExterne = '';

            if( $this->sRetourValidationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourValidationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sParametreRetourValidationFormExterne .= '&' . $sChamp . '=' . $_GET[$sChamp];
                        }
                    }
                }
            }

            $this->sParametreRetourValidationFormExterne = $sParametreRetourValidationFormExterne;
        }

        return $this->sParametreRetourValidationFormExterne;
    }

    public function renvoi_url_retour_validation_form_externe()
    {
        if( !isset($this->sUrlRetourValidationFormExterne) ) {
            $sUrlRetourValidationFormExterne = $this->sRetourValidationFormExterne;

            if( $this->sRetourValidationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourValidationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sUrlRetourValidationFormExterne = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourValidationFormExterne);
                        }
                    }
                }
            }

            $this->sUrlRetourValidationFormExterne = $sUrlRetourValidationFormExterne;
        }

        return $this->sUrlRetourValidationFormExterne;
    }

    public function renvoi_parametre_retour_annulation_form_externe()
    {
        if( !isset($this->sParametreRetourAnnulationFormExterne) ) {
            $sParametreRetourAnnulationFormExterne = '';

            if( $this->sRetourAnnulationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourAnnulationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sParametreRetourAnnulationFormExterne .= '&' . $sChamp . '=' . $_GET[$sChamp];
                        }
                    }
                }
            }

            $this->sParametreRetourAnnulationFormExterne = $sParametreRetourAnnulationFormExterne;
        }

        return $this->sParametreRetourAnnulationFormExterne;
    }

    public function renvoi_url_retour_annulation_form_externe()
    {
        if( !isset($this->sUrlRetourAnnulationnFormExterne) ) {
            $sUrlRetourAnnulationFormExterne = $this->sRetourAnnulationFormExterne;

            if( $this->sRetourAnnulationForm == 'externe' ) {
                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourAnnulationFormExterne, $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $sUrlRetourAnnulationFormExterne = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourAnnulationFormExterne);
                        }
                    }
                }
            }

            $this->sUrlRetourAnnulationFormExterne = $sUrlRetourAnnulationFormExterne;
        }

        return $this->sUrlRetourAnnulationFormExterne;
    }
}
