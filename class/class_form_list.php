<?php

class class_form_list extends class_form_custom
{

    //TODO Rajout list erreur super admin quand par exemple le champ nom_variable n'est pa unique
    public function run()
    {


        if($this->bBypassRun or $this->bByPassConnect)
            return 0;

        if( isset($_GET['action']) && $_GET['action'] == 'formList' ) {


        }
        $sVersionTemplate = class_fli::get_fli_version_template();
        $bActivtraduction=false;

        $aTableauUserInfo = class_fli::get_aData_entier();

        //echo"<pre>";print_r($aTableauUserInfo);echo"</pre>";

        if(isset($aTableauUserInfo['autorisation_traduction']) and $aTableauUserInfo['autorisation_traduction']=="Y")
            $bActivtraduction=true;

        if(!isset($aTableauUserInfo['langue_user'])){
            $aTableauUserInfo['langue_user']="fr";
        }

        /*
         * On ffiche le tableau si las liste des actions sur le côté est remplis
         */
        if(!empty($this->aMenuDeroulant)){
            $this->baffAction=true;
        }

       /* $sModule =  class_fli::get_fli_module();
        $nomclass = get_class($this);
        $sLaRoute = $sModule."-".$nomclass;
        */



        //ici on renvoi le link appelé
        $sLink ="";
        if(isset($_GET['fli_module'])){
            $sLink.=$_GET['fli_module']."-";
        }
        if(isset($_GET['fli_controleur'])){
            $sLink.=$_GET['fli_controleur']."-";
        }
        if(isset($_GET['fli_fonction'])){
            $sLink.=$_GET['fli_fonction'];
        }

        if(isset($_GET['duplicate']) and $_GET['duplicate']=="ok"){
            $this->objSmarty->assign("bDuplicate",true);
        }else{
            $this->objSmarty->assign("bDuplicate",false);
        }


        $this->objSmarty->assign('sLaRoute',$sLink);
        $this->objSmarty->assign('sFli_LinkUrl',$sLink);
        $this->objSmarty->assign('sLangueUser', $aTableauUserInfo['langue_user']); //If ternaire dans le cas ou il n'y a pas de table dans le constructeur
        $this->objSmarty->assign('bActivtraduction', $bActivtraduction); //If ternaire dans le cas ou il n'y a pas de table dans le constructeur
        $this->objSmarty->assign('sNomTable', $this->sNomTable); //If ternaire dans le cas ou il n'y a pas de table dans le constructeur
        $this->objSmarty->assign('bdoc', class_fli::get_fli_actif_doc()); //If ternaire dans le cas ou il n'y a pas de table dans le constructeur

        $bisRecherche=false;
        

        if(isset($_GET["fli_rechercher"]) and $_GET["fli_rechercher"]=='ok'){
            $bisRecherche=true;
            //echo"<pre>";print_r($_GET);echo"</pre>";
        }
        $this->objSmarty->assign('bisRecherche', $bisRecherche);
        //*****************ici on rajoute le nom du filtre dans le titre***
        foreach( $this->aElement as &$objElement ) {
            if(isset($objElement['titre_inter_module'])){
                if($objElement['titre_inter_module']=="ok"){
                    //on rcupere l'identifiant
                    //echo"<pre>";print_r($objElement);echo"</pre>";
                    $sIdfiltre ="";
                    if(isset($_GET[$objElement['nom_variable']])){
                        $sIdfiltre = $_GET[$objElement['nom_variable']];
                    }
                    if(isset($_POST[$objElement['nom_variable']])){
                        $sIdfiltre = $_GET[$objElement['nom_variable']];
                    }

                    if(isset($objElement['bbd_titre_inter_module']) && isset($objElement['champfiltre_titre_inter_module'])  && isset($objElement['champaffiche_titre_inter_module'])){

                        $sRequete_info_titre ="SELECT ".$objElement['champaffiche_titre_inter_module']." as nom from ".$objElement['bbd_titre_inter_module']." where ".$objElement['champfiltre_titre_inter_module']."='".$sIdfiltre."'";

                        //echo $sRequete_info_titre."<br>";

                        $aTableauRetourTitre = $this->objClassGenerique->renvoi_info_requete($sRequete_info_titre);


                        //echo "stitre ".$sRequete_info_titre."<br>";
                        
                        //echo"<pre>";print_r($aTableauRetourTitre);echo"</pre>";

                        if(!empty($aTableauRetourTitre)){
                            $sMessageListe =sprintf($this->sTitreListe,$aTableauRetourTitre[0]['nom']);
                            $this->sTitreListe = $sMessageListe;
                            $sMessageForm =sprintf($this->sTitreForm,$aTableauRetourTitre[0]['nom']);
                            $this->sTitreForm = $sMessageForm;
                            $sMessageCreation =sprintf($this->sLabelCreationElem,$aTableauRetourTitre[0]['nom']);
                            $this->sLabelCreationElem = $sMessageCreation;
                            $sMessagesCategorieForm =sprintf($this->sCategorieForm,$aTableauRetourTitre[0]['nom']);
                            $this->sCategorieForm=$sMessagesCategorieForm;
                        }

                        //echo "stitre ". $this->sTitreListe."<br>";


                    }

                }
            }
        }
        
        //echo"<pre>";print_r($this->aTableauVariableDynamique);echo"</pre>";

        //echo"dd<pre>";print_r($this->aElement );echo"</pre>";

        //echo"guy<pre>";print_r($this->aListeFils);echo"</pre>";
        if( isset($_GET['action']) && $_GET['action'] == 'liste_fils' && $this->bEstModuleAppele ) {
            $aEnteteListe = $this->renvoi_entete_liste();

            $aListe = $this->renvoi_liste();

            $aTabTpl = array();
              //echo"tabguy<pre>";print_r($this->aListeFils);echo"</pre>";
            if( count($this->aListeFils) > 0 ) {
                $sAction = $_GET['action'];
                $_GET['action'] = 'liste_fils';

                $objSmarty = $this->objSmarty;
                foreach( $aListe as $idObjListe => &$objListe ) {
                    foreach( $this->aListeFils as $objListeFils ) {
                        $sValeurChampIdPere = isset($_GET[$objListeFils['sChampIdPere']]) ? $_GET[$objListeFils['sChampIdPere']] : null;

                        $sDir = isset($_GET['dir']) ? $_GET['dir'] : null;

                        $_GET[$objListeFils['sChampIdPere']] = $idObjListe;
                        $_GET['dir'] = $objListeFils['sDir'];
                        $_GET[$objListeFils['sFiltreChamp']] = $objListe[$objListeFils['sFiltreChamp']];
                        //echo"<pre>";print_r($_GET);echo"</pre>";
                            //-----------------------------------------------------------------------------
                        $objImport = new class_import();
                        $aTabTpl[] = $objImport->import_module($objListeFils['sDir'],$objListeFils['sControleur'],$objListeFils['sFonction'],$objSmarty);

                       //echo"<pre>";print_r($aTabTpl);echo"</pre>";

                        //$objListe['liste_fils'][] = include_once 'modules/' . $objListeFils['sChemin'];

                        $_GET[$objListeFils['sChampIdPere']] = $sValeurChampIdPere;
                        $_GET['dir'] = $sDir;
                    }
                    if( isset($objListe['liste_fils']) ) {
                        $iCount = count($objListe['liste_fils']);
                        for( $i = 0; $i < $iCount; $i++ ) {
                            if( $objListe['liste_fils'][$i] === '' ) {
                                unset($objListe['liste_fils'][$i]);
                            }
                        }
                    }
                }


                unset($objListe);
                reset($aListe);
                $_GET['action'] = $sAction;
            }


            $objSmartyFetch = clone $this->objSmarty;
            $objSmartyFetch->assign('iNombreColonneListe', count($aEnteteListe) + (($this->bAffFiche || $this->bAffMod || $this->bAffSupp) ? 1 : 0));
            $objSmartyFetch->assign('bAffFiche', $this->bAffFiche);
            $objSmartyFetch->assign('bAffMod', $this->bAffMod);
            $objSmartyFetch->assign('bAffSupp', $this->bAffSupp);
            $objSmartyFetch->assign('baffAction', $this->baffAction);
            $objSmartyFetch->assign('aEnteteListe', $aEnteteListe);
            $objSmartyFetch->assign('aListe', $aListe);
            $objSmartyFetch->assign('bCsv', $this->bCsv);


            $aTabTpl[] = $objSmartyFetch->fetch($this->sListeFilsTpl);

            return $aTabTpl;
        }




        //echo"<pre>";print_r($_GET);echo"</pre>";
        $bMessageSuccesForm = false;
        $bMessageErreurForm = false;

        if( isset($_GET['action']) && $_GET['action'] == 'form' && $this->bEstModuleAppele ) {
            //echo "passe";


            if( !empty($_POST) ) {
                $sTableauRetourControl=array();
                $sNomMthodeActuelle = "controle_form_".class_fli::get_fli_fonction();
                if( method_exists($this, $sNomMthodeActuelle) ) {
                    $sTableauRetourControl = $this->$sNomMthodeActuelle();
                }
               // echo"<pre>";print_r($sTableauRetourControl);echo"</pre>";
                //exit();
                // ici on lance le contrôle me controle sur  tous les objet
                foreach( $this->aElement as &$objElement ) {
                    if($objElement['ctrl_champ']=="ok" and $objElement['aff_form']=="ok" ){
                        $sNomMthodeCTRL = "controle_form_".$objElement['type_champ'];

                        //echo $sNomMthodeCTRL."<br>";
                        if( method_exists($this, $sNomMthodeCTRL) ) {
                            $sTableauRetourtmp = $this->$sNomMthodeCTRL($objElement);
                            if(!$sTableauRetourtmp['result']){
                                $sTableauRetourControl['result']=false;
                                $sTableauRetourControl['message'][]=$sTableauRetourtmp['message'];
                            }
                        }else{
                            $sTableauRetourtmp = $this->controle_form_principal($objElement);
                            if(!$sTableauRetourtmp['result']){
                                $sTableauRetourControl['result']=false;
                                $sTableauRetourControl['message'][]=$sTableauRetourtmp['message'];
                            }
                        }
                    }
                }


                if( !empty($sTableauRetourControl)) {
                    $this->sMessageErreurForm = "";
                    if(!$sTableauRetourControl['result']) {
                        if(!empty($sTableauRetourControl['message'])) {
                            foreach( $sTableauRetourControl['message'] as $valuemessageerreur ) {
                                if( !empty($valuemessageerreur) )
                                    $this->sMessageErreurForm .= $valuemessageerreur . "<br>";
                            }
                        }
                        $bMessageSuccesForm = false;
                        $bMessageErreurForm = true;
                    }
                    else
                    {
                        $bMessageSuccesForm = true;
                        $bMessageErreurForm = false;
                    }

                }else{
                    $bMessageSuccesForm = true;
                }



                if( $bMessageSuccesForm ) {


                    if(isset($_GET['duplicate']) and $_GET['duplicate']=="ok"){
                        $this->sEtatForm = "insert";
                        $this->iId = null;
                    }


                    $sFonctionenreg_modif="enreg_modif_form_".class_fli::get_fli_fonction();
                    if( method_exists($this, $sFonctionenreg_modif) ) {
                        $aTableauResult = $this->$sFonctionenreg_modif();
                    }else{
                        $aTableauResult = $this->enreg_modif_form();
                    }

                    $iId =$aTableauResult['iId'];

                    //echo"<pre>";print_r($aTableauResult);echo"</pre>";

                    //si tout est ok
                    if($aTableauResult['bResult']) {
                        if( $this->iId === null ) {
                            foreach( $this->aElement as &$objElement ) {
                                if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {
                                    $objElement['valeur_variable'] = '';
                                }
                            }

                            //header('location:' );
                            unset($objElement);
                        }

                        ///si il ya des message post enregeistrment
                        if($aTableauResult['sMessage_Post']!=""){

                            if($aTableauResult['bResult_Post']) {
                                $this->sMessageSuccesForm.= $aTableauResult['sMessage_Post'];
                                $bMessageSuccesForm=true;
                            }else{
                                $this->sMessageSuccesForm .= $aTableauResult['sMessage_Post'] . "<br>";
                                //$bMessageErreurForm =true;
                            }
                        }

                       /* $sModule =  class_fli::get_fli_module();
                        $nomclass = get_class($this);
                        $chemin = $sModule."-".$nomclass;
                        echo"<pre>";print_r($_SERVER);echo"</pre>";
                        echo "lechemin ". $chemin."<br>";
                       */
                       $ATableauDecoupeurl = explode("/",$_SERVER['REDIRECT_URL']);
                       //echo "urel ".$ATableauDecoupeurl[count($ATableauDecoupeurl)-1];

                        if($this->sEtatForm=='insert')
                            $sURlretour= $ATableauDecoupeurl[count($ATableauDecoupeurl)-1]."?bval=ok".$this->renvoi_base_url()
                                . $this->renvoi_parametre_inter_module()
                                . $this->renvoi_parametre_recherche()
                                . $this->renvoi_parametre_filtre_liste()
                                . $this->renvoi_parametre_retour_liste();
                        else
                            $sURlretour= $ATableauDecoupeurl[count($ATableauDecoupeurl)-1]."?bval=updatok&" . $this->renvoi_parametre_inter_module()
                                . $this->renvoi_parametre_recherche()
                                . $this->renvoi_parametre_filtre_liste()
                                . $this->renvoi_parametre_retour_liste();



                        if( $this->sRetourValidationForm == 'externe' ) {
                            header('location:' . $this->renvoi_url_retour_validation_form_externe() . ($this->iId === null ? '&' . $this->sChampId . '=' . $iId : '') . $this->sParametreWizardListe);
                        }
                        //echo $sURlretour;
                        if( !$this->get_parametre_bwizard() ) {
                            header('location:' . $sURlretour);
                        }

                    }else{
                        $this->renvoi_valeur_form();
                        $this->sMessageErreurForm .= $aTableauResult['sMMessage'] . "<br>";
                        $bMessageErreurForm =true;
                        $bMessageSuccesForm =false;
                    }



                } else {
                    $this->renvoi_valeur_form();
                }
            } else {

                //echo "id=>".$this->iId;
                if( $this->iId !== null or $this->bKeyMultiple) {
                    $this->renvoi_valeur_bdd();
                }
            }

            //echo $this->sParametreWizardListe;
            $sUrlForm =
                $this->renvoi_base_url()
                . $this->renvoi_parametre_inter_module()
                . $this->renvoi_parametre_recherche()
                . $this->renvoi_parametre_filtre_liste()
                . $this->renvoi_parametre_retour_liste()
                . $this->renvoi_parametre_retour_validation_form_externe()
                . $this->sParametreWizardListe
                . '&action=form';






            //echo "url".$sUrlForm;

            if( $this->renvoi_parametre_retour_validation_form_externe() != '' ) {
                $sUrlForm = '';

            }

            if( $this->iId != null && $this->renvoi_parametre_retour_validation_form_externe() == '' ) {
                $sUrlForm .= '&' . $this->sChampId . '=' . $this->iId;

            }

            $this->objSmarty->assign('url_form', $sUrlForm);

        }


        if(isset($_POST['fli_action']) and $_POST['fli_action']!=""){
            $_GET['action'] ="";
        }

        //echo"<pre>";print_r($this);echo"</pre>";
        if( isset($_GET['action']) && $_GET['action'] == 'form' && ((!$bMessageSuccesForm && !$bMessageErreurForm) || ($bMessageSuccesForm && $this->sRetourValidationForm == 'form') || $bMessageErreurForm) && (!$this->bFormPopup || $this->iId !== null) ) {
            $bTelechargementFichier = false;

            foreach( $this->aElement as $objtest ) {
                if( isset($objtest['aff_form']) && $objtest['aff_form'] == 'ok' ) {
                    //echo "toto";
                    if( $objtest['type_champ'] == 'file' ) {
                        $bTelechargementFichier = true;
                        break;
                    }

                }
            }

            //echo"ee<pre>";print_r($this->aElement );echo"</pre>";

            if(!empty($this->sNomTable)) {
                $aForm = $this->renvoi_formulaire();
                //echo 'toto';
                if( count($this->itemBoutonsForm) == 0 ) {
                    if( $this->sRetourAnnulationForm == 'externe' ) {
                        $sUrlAnnulerForm = $this->renvoi_url_retour_annulation_form_externe();
                    } elseif( $this->sRetourAnnulationForm == 'url' ) {
                        $sUrlAnnulerForm = $this->sUrlRetourSpecifique;
                    } else {
                        $sUrlAnnulerForm =
                            $this->renvoi_base_url()
                            . $this->renvoi_parametre_inter_module()
                            . $this->renvoi_parametre_recherche()
                            . $this->renvoi_parametre_filtre_liste()
                            . $this->renvoi_parametre_retour_liste()
                            . $this->renvoi_parametre_retour_annulation_form_externe()
                            . $this->sParametreWizardListe;
                    }

                    $this->itemBoutonsForm = array();

                    $this->itemBoutonsForm['valider'] = array(
                        'type' => 'submit',
                        'nom_variable' => 'btsubmit',
                        'text_label' => 'Valider',
                        'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                    );

                    $this->itemBoutonsForm['annuler'] = array(
                        'type' => 'bouton',
                        'nom_variable' => 'btannuler',
                        'text_label' => 'Annuler',
                        'url' => $sUrlAnnulerForm,
                        'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                    );
                }

                if(isset($_GET['sTypeSortie'])  and $_GET['sTypeSortie']=='visu') {
                    class_fli::set_tpl_sortie('principal_empty.tpl');
                    $this->sFormulaireTpl="recap_ligne.tpl";
                }

                if(isset($_GET['fi_keymutiple'])){
                    $this->objSmarty->assign('fi_keymutiple', $_GET['fi_keymutiple']);
                }else{
                    $this->objSmarty->assign('fi_keymutiple','');
                }

                //echo"<pre>";print_r($aForm);echo"</pre>";
                
                $this->objSmarty->assign('bTelechargementFichier', $bTelechargementFichier);
                $this->objSmarty->assign('sTitreForm', $this->sTitreForm);
                $this->objSmarty->assign('sCategorieForm', $this->sCategorieForm);
                $this->objSmarty->assign('bMessageSuccesForm', $bMessageSuccesForm);
                $this->objSmarty->assign('sMessageSuccesForm', $this->sMessageSuccesForm);
                $this->objSmarty->assign('bMessageErreurForm', $bMessageErreurForm);
                $this->objSmarty->assign('sMessageErreurForm', $this->sMessageErreurForm);
                $this->objSmarty->assign('bAffAnnuler', $this->bAffAnnuler);

                $this->objSmarty->assign('aForm', $aForm);
                $this->objSmarty->assign('itemBoutonsForm', $this->itemBoutonsForm);
                $this->objSmarty->assign('bDebugRequete', $this->bDebugRequete);
                $this->objSmarty->assign('sDebugRequeteSelect', $this->sDebugRequeteSelect);
                $this->objSmarty->assign('sDebugRequeteInsertUpdate', $this->sDebugRequeteInsertUpdate);
                $this->objSmarty->assign("pagedirection", $this->sFormulaireTpl);
                $this->objSmarty->assign("lapagedirection", $this->sFormulaireTpl);
                $this->objSmarty->assign('sLink',$sLink);


            }else{
                $aForm = array();
            }

           return $this->objSmarty->fetch($this->sFormulaireTpl);
        } else {
            $aTabTpl = array();
            //echo "tpl2 ";
            if( $this->bFormPopup ) {

                $this->bFormPopup = false;
                $sAction = isset($_GET['action']) ? $_GET['action'] : null;
                $_GET['action'] = 'form';
                unset($_POST);
                $this->run();
                $this->bFormPopup = true;
                $_GET['action'] = $sAction;
            }
            //echo"<pre>";print_r($this->aElement);echo"</pre>";
            if( isset($_GET['action']) && $_GET['action'] == 'form' ) {


                foreach( $this->aElement as &$objElement ) {
                    if( isset($objElement['aff_form']) && $objElement['aff_form'] == 'ok' ) {
                        $objElement['valeur_variable'] = '';
                    }
                }
                unset($objElement);
            }

            $bTelechargementFichier = false;

            $bMessageSupprElem = false;

            if( isset($_GET['action']) && $_GET['action'] == 'supp' ) {
                $bMessageSupprElem = $this->suppression_entree();
                $sMethodeChampDonne = "_action_" . $_GET['action'];
                //echo "action supps";

                if( $this->sRetourSuppressionFormExterne != '' ) {
                    header('location:' . $this->renvoi_url_retour_suppression_form_externe());
                }
            }

            /*
             * ICI la liste des form qu'on utilise déjà pour bypasser quand on lancement d'une fonction
             */
            $aTabAction =array();
            $aTabAction[]="supp";
            $aTabAction[]="form";
            if( isset($_GET['action']) and !in_array($_GET['action'],$aTabAction) and $this->bEstModuleAppele) {
                $sMethodeChampDonne = "fli_action_" . $_GET['action'];
                //echo " toto".$sMethodeChampDonne."<br>";

                if( method_exists($this, $sMethodeChampDonne) ) {
                    $aTableauretour = $this->$sMethodeChampDonne();

                    $this->sMessageSuccesForm=$aTableauretour['sMessge'];
                    $this->sMessageErreurForm=$aTableauretour['sMessge'];
                    $bMessageSuccesForm=$aTableauretour['bsucces'];
                    $bMessageErreurForm=$aTableauretour['berror'];
                    if( isset($aTableauretour['urlRetour']) and $aTableauretour['urlRetour']!=''){
                        header('location:' . $aTableauretour['urlRetour']);
                    }
                }
            }


            $sLabelCreationElemUrl =
                $this->renvoi_base_url()
                . $this->renvoi_parametre_inter_module()
                . $this->renvoi_parametre_recherche()
                . $this->renvoi_parametre_filtre_liste()
                . $this->renvoi_parametre_retour_liste()
                . $this->sParametreWizardListe
                . '&action=form';

            $itemBoutons = array();

            $itemBoutons['valider'] = array(
                'type' => 'submit',
                'nom_variable' => 'btsubmit',
                'text_label' => 'Valider',
                'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
            );

            $itemBoutons['reset'] = array(
                'type' => 'bouton',
                'nom_variable' => 'btreset',
                'text_label' => 'Reset',
                'url' => $this->renvoi_base_url() . $this->renvoi_parametre_inter_module() . $this->renvoi_parametre_retour_liste() . $this->sParametreWizardListe,
                'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
            );


            $itemBoutons['csv'] = array(
                'type' => 'submit',
                'nom_variable' => 'btcsv',
                'text_label' => 'Télécharger le CSV',
                'url' => $this->renvoi_base_url() . $this->renvoi_parametre_inter_module() . $this->renvoi_parametre_retour_liste() .$this->renvoi_parametre_recherche(). $this->sParametreWizardListe."&sTypeSortie=csv",
                'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
            );


            $itemBoutons['pdf'] = array(
                'type' => 'submit',
                'nom_variable' => 'btcsv',
                'text_label' => 'Télécharger le pdf',
                'url' => $this->renvoi_base_url() . $this->renvoi_parametre_inter_module() . $this->renvoi_parametre_retour_liste() .$this->renvoi_parametre_recherche(). $this->sParametreWizardListe."&sTypeSortie=csv",
                'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
            );

            $aRech = $this->renvoi_recherche();
            
            //echo"<pre>";print_r($aRech);echo"</pre>";
            if(count($aRech)==1 and $aRech[0]['transfert_inter_module']=="ok")
                $aRech=array();

            $bAffListe = ($this->bAffListeQuandPasRecherche || $this->renvoi_si_filtre_recherche_actif($aRech));
            //echo "table".$this->sNomTable;
            if( $bAffListe && !empty($this->sNomTable)) {  //Si on affiche la liste et qu'une table est précisée dans le constructeur


                //echo"<pre>";print_r($this->aListeFils);echo"</pre>";


                $aEnteteListe = $this->renvoi_entete_liste();
                //echo "passe";
                    $aListe = $this->renvoi_liste();
                    $aPaginationListe = $this->pagination();

                if( count($this->aListeFils) > 0 && $this->bEstModuleAppele ) {

                    $sAction = isset($_GET['action']) ? $_GET['action'] : null;
                    $_GET['action'] = 'liste_fils';
                    $objSmarty = $this->objSmarty;
                    //echo"test<pre>";print_r($aListe);echo"</pre>";
                    foreach( $aListe as $idObjListe => &$objListe ) {
                        foreach( $this->aListeFils as $objListeFils ) {
                            //echo"<pre>";print_r($objListe);echo"</pre>";
                            $sValeurChampIdPere = isset($_GET[$objListeFils['sChampIdPere']]) ? $_GET[$objListeFils['sChampIdPere']] : null;
                            $sDir = isset($_GET['dir']) ? $_GET['dir'] : null;

                            $_GET[$objListeFils['sChampIdPere']] = $idObjListe;
                            $_GET['dir'] = $objListeFils['sDir'];
                            //$_GET[$objListeFils['sFiltreChamp']] = $objListe[$objListeFils['sFiltreChamp']];
                            //echo"<pre>";print_r($_GET);echo"</pre>";
                            //echo"<pre>";print_r($objListeFils);echo"</pre>";
                            //-----------------------------------------------------------------------------
                            $objImport = new class_import();
                            $objListe['liste_fils'][] = $objImport->import_module($objListeFils['sDir'],$objListeFils['sControleur'],$objListeFils['sFonction'],$objSmarty);

                            //echo"<pre>";print_r($objListe);echo"</pre>";

                            //$objListe['liste_fils'][] = include_once 'modules/' . $objListeFils['sChemin'];
                            $_GET[$objListeFils['sChampIdPere']] = $sValeurChampIdPere;

                            //echo"<pre>";print_r($objListe['liste_fils']);echo"</pre>";

                            $_GET['dir'] = $sDir;
                        }
                        if( isset($objListe['liste_fils']) ) {
                            $iCount = count($objListe['liste_fils']);
                            for( $i = 0; $i < $iCount; $i++ ) {
                                if( $objListe['liste_fils'][$i] === '' ) {
                                    unset($objListe['liste_fils'][$i]);
                                }
                            }
                        }
                    }
                    unset($objListe);
                    reset($aListe);
                    $_GET['action'] = $sAction;
                }
            } else {
                $aEnteteListe = array();
                $aListe = array();
                $aPaginationListe = array();
            }

            if($this->bAffdebugListe){
                echo"<pre>";print_r($aListe);echo"</pre>";
                //echo "toto";
            }

            //echo"<pre>";print_r($objListe['liste_fils']);echo"</pre>";
            //************* on regarde s'il y a des message externe a rajouter
            if(isset($this->aTabResultExterne['bsucess']) and $this->aTabResultExterne['bsucess']){
                $bMessageSuccesForm =  $this->aTabResultExterne['bsucess'];
                $this->sMessageSuccesForm=$this->aTabResultExterne['sMessageSucess'];
            }

            if(isset($this->aTabResultExterne['berror']) and $this->aTabResultExterne['berror']){
                $bMessageErreurForm =  $this->aTabResultExterne['berror'];
                 $this->sMessageErreurForm=$this->aTabResultExterne['sMessagError'];
            }

            if(isset($this->aTabResultExterne['bsupp']) and $this->aTabResultExterne['bsupp']){
                $bMessageSupprElem =  $this->aTabResultExterne['bsupp'];
                $this->sMessageSupprElem=$this->aTabResultExterne['sMessageSupp'];
            }
            //********************************************************************************
            //echo"<pre>";print_r($aForm);echo"</pre>";
            //on active le message resulte en haut avec des messages ajouté succes ou error
            if(!empty($this->aTabResultMessageSuccessSup)){
                    $bMessageSuccesForm=true;
                    foreach($this->aTabResultMessageSuccessSup as $valeurMessageSucceSupp){
                            $this->sMessageSuccesForm=$valeurMessageSucceSupp;
                    }
            }

            if(!empty($this->aTabResultMessageErrosSup)){
                $bMessageErreurForm=true;
                foreach($this->aTabResultMessageErrosSup as $valeurMessageErrorSupp){
                    $this->sMessageErreurForm=$valeurMessageErrorSupp;
                }
            }

            //actiovation de la collone action
            $baffcolaction=false;

            if(!empty($this->aTableauVariableDynamique) && $this->bEstModuleAppele ){
                // echo"<pre>";print_r($this->aTableauVariableDynamique);echo"</pre>";
                $sMessageListe =sprintf($this->sTitreListe,$this->aTableauVariableDynamique['nom']);
                $this->sTitreListe = $sMessageListe;

                // echo  $this->sTitreListe ;
                $sMessageForm =sprintf($this->sTitreForm,$this->aTableauVariableDynamique['nom']);
                $this->sTitreForm = $sMessageForm;
                $sMessageCreation =sprintf($this->sLabelCreationElem,$this->aTableauVariableDynamique['nom']);
                $this->sLabelCreationElem = $sMessageCreation;
                $sMessagesCategorieForm =sprintf($this->sCategorieForm,$this->aTableauVariableDynamique['nom']);
                $this->sCategorieForm=$sMessagesCategorieForm;
            }


            if(!empty($this->aMenuDeroulant)){
                $baffcolaction=true;
                //echo "toto";
            }
            if(isset($_GET['bval']) and $_GET['bval']=="ok"){
                $bMessageSuccesForm=true;
            }
            if(isset($_GET['bval']) and $_GET['bval']=="updatok"){
                $bMessageSuccesForm=true;
                $this->sMessageSuccesForm=$this->sMessageModificationSuccesForm;
            }

            //********************ici en focntion <que les information son rempli on active les message d'allerte en haut
            if($this->sMessageModificationSuccesSupp!=""){
                $bMessageSuccesForm=true;
                $this->sMessageSuccesForm=$this->sMessageModificationSuccesSupp;
            }

            if($this->sMessageErrorsSupp!=""){
                $bMessageErreurForm=true;
                $this->sMessageErreurForm = $this->sMessageErrorsSupp;
            }

            //echo  $this->sTitreListe ;
            $this->objSmarty->assign('baffcolaction', $baffcolaction);
            $this->objSmarty->assign('bMessageSuccesForm', $bMessageSuccesForm);
            $this->objSmarty->assign('sMessageSuccesForm', $this->sMessageSuccesForm);
            $this->objSmarty->assign('bMessageErreurForm', $bMessageErreurForm);
            $this->objSmarty->assign('sMessageErreurForm', $this->sMessageErreurForm);
            $this->objSmarty->assign('sLienLigne', $this->sLienLigne);
            $this->objSmarty->assign('bAffPrintBtn', $this->bAffPrintBtn);
            $this->objSmarty->assign('bLigneCliquable', $this->bLigneCliquable);
            $this->objSmarty->assign('bAffTitre', $this->bAffTitre);
            $this->objSmarty->assign('bAffNombreResult', $this->bAffNombreResult);
            $this->objSmarty->assign('bBtnRetour', $this->bBtnRetour);
            $this->objSmarty->assign('sDirRetour', $this->sDirRetour. $this->renvoi_parametre_inter_module('retour') );
            $this->objSmarty->assign('bCheckboxSelect', $this->bCheckboxSelect);
            $this->objSmarty->assign('bRadioSelect', $this->bRadioSelect);
            $this->objSmarty->assign('bActiveFormSelect', $this->bActiveFormSelect);
            $this->objSmarty->assign('bAffListe', $bAffListe);
            $this->objSmarty->assign('bLabelCreationElem', $this->bLabelCreationElem);
            $this->objSmarty->assign('iNombreColonneListe', count($aEnteteListe) + (($this->bAffFiche || $this->bAffMod || $this->bAffSupp) ? 1 : 0));
            $this->objSmarty->assign('baffAction', $this->baffAction);
            $this->objSmarty->assign('bFormPopup', $this->bFormPopup);
            $this->objSmarty->assign('aParametreWizardListe', $this->aParametreWizardListe);
            $this->objSmarty->assign('itemBoutonsListe', $this->itemBoutonsListe);
            $this->objSmarty->assign('sCategorieListe', $this->sCategorieListe);
            $this->objSmarty->assign('sLabelRetourListe', $this->sLabelRetourListe);
            $this->objSmarty->assign('sRetourListe', $this->renvoi_url_retour_liste());
            $this->objSmarty->assign('bMessageSupprElem', $bMessageSupprElem);
            $this->objSmarty->assign('sMessageSupprElem', $this->sMessageSupprElem);
            $this->objSmarty->assign('sLabelCreationElem', $this->sLabelCreationElem);
            $this->objSmarty->assign('sLabelCreationElemUrl', $sLabelCreationElemUrl);
            $this->objSmarty->assign('sLabelRecherche', $this->sLabelRecherche);
            $this->objSmarty->assign('sTitreListe',$this->sTitreListe);
            $this->objSmarty->assign('bAffDup', $this->bAffDup);
            $this->objSmarty->assign('bAffRecapLigne', $this->bAffRecapLigne);


            if(!empty($this->sNomTable)) {
                try {
                    $iNbrLignes = $this->renvoi_nombreligne_requete();
                }catch(Exception $e){
                    $iNbrLignes = 0;
                }
            }else{
                $iNbrLignes = 0;
            }
             /*
              * Correction des variable statique qui se trouve dans url
              */
            $aTableauBloqueurl=array();
            $aTableauBloqueurl[]="iposition";
            $aTableauBloqueurl[]="nbrpage";
            $surlVarGET="";
            //on rajoute la suite dans l'url des ceuxi qui sont activé
            if($this->bPushVarUrl){
                $surltronue=  $_SERVER['REQUEST_URI']   ;
                //echo $surltronue."<br>";
                $aTabdecoupe = explode('?',$surltronue);
                if(isset($aTabdecoupe[1]) )
                    $surlVarGET="&".$aTabdecoupe[1];
                $surlVarGET= str_replace("fli_rechercher","fli_recherche_urlpass",$surlVarGET);

                $aTableaudecoupeurlfiltre= explode("&",$surlVarGET);

                if(!empty($aTableaudecoupeurlfiltre)){
                    $surlVarGET="";
                    foreach($aTableaudecoupeurlfiltre as $valeudecoupeurl){

                        $aDecoupeIIurl= explode("=",$valeudecoupeurl);

                        if(!in_array($aDecoupeIIurl[0],$aTableauBloqueurl)){
                            $surlVarGET.="&".$valeudecoupeurl;
                        }

                    }
                }
            }

            $this->objSmarty->assign('surlVarGET', $surlVarGET); //If ternaire dans le cas ou il n'y a pas de table dans le constructeur
            $this->objSmarty->assign('iTabListeCount', $iNbrLignes); //If ternaire dans le cas ou il n'y a pas de table dans le constructeur
            $this->objSmarty->assign('sLabelNbrLigne', $this->sLabelNbrLigne);
            $this->objSmarty->assign('sChampId', $this->sChampId);
            $this->objSmarty->assign('bAffFiche', $this->bAffFiche);
            $this->objSmarty->assign('bAffMod', $this->bAffMod);
            $this->objSmarty->assign('bAffSupp', $this->bAffSupp);
            $this->objSmarty->assign('baffAction', $this->baffAction);
            $this->objSmarty->assign('bCsv', $this->bCsv);
            $this->objSmarty->assign('bPdf', $this->bPdf);
            $this->objSmarty->assign('aPaginationListe', $aPaginationListe);
            $this->objSmarty->assign('bPagination', $this->bPagination);
            $this->objSmarty->assign('itemBoutons', $itemBoutons);
            
            //echo"<pre>";print_r($aListe);echo"</pre>";
            $this->objSmarty->assign('aRech', $aRech);
            $this->objSmarty->assign('aEnteteListe', $aEnteteListe);
            $this->objSmarty->assign('sDirRech', (isset($_GET['dir']) ? $_GET['dir'] : ''));
            $this->objSmarty->assign('aRetourListe', $this->renvoi_parametre_retour_liste_tableau());
            $this->objSmarty->assign('bDebugRequete', $this->bDebugRequete);
            $this->objSmarty->assign('sDebugRequeteSelect', $this->sDebugRequeteSelect);
            $this->objSmarty->assign('sDebugRequeteInsertUpdate', $this->sDebugRequeteInsertUpdate);
            $this->objSmarty->assign('pagedirection', $this->sListeTpl);
            $this->objSmarty->assign('lapagedirection', $this->sListeTpl);
            $this->objSmarty->assign('bRetourSpecifique', $this->bRetourSpecifique);
            $this->objSmarty->assign('sRetourElemUrl', $this->sRetourElemUrl);
            $this->objSmarty->assign('sLabelFileRetourElem', $this->sLabelFileRetourElem);


            //echo $this->sTypeSortie."=>sortie";


            switch($this->sTypeSortie){

                case 'html':
                    $this->objSmarty->assign('aListe', $aListe);
                    //echo "tpl222 ".$this->sListeTpl;echo"<pre>";print_r($this->objSmarty->getTemplateDir());echo"</pre>";;
                    return $this->objSmarty->fetch($this->sListeTpl);
                    break;
                case 'android':
                   // echo"{\"liste\":[{\"id\":\"7F3A9357-88B4-4EDE-AC33-68229BC832D5\",\"nom_categorie\":\"Coiffureguy32\",\"pere_categorie\":\"Vos commer\u00e7ants\"},{\"id\":\"232E374B-F6BF-4727-8722-85A4E403FA10\",\"nom_categorie\":\"Assurance\",\"pere_categorie\":\"Vos commer\u00e7ants\"},{\"id\":\"2ACE33F3-982A-4DC8-B61B-FADBD50A940F\",\"nom_categorie\":\"Immobilier\",\"pere_categorie\":\"Vos commer\u00e7ants\"},{\"id\":\"7AA41F03-9185-42EB-B628-2EC28DE3152A\",\"nom_categorie\":\"Services\",\"pere_categorie\":\"Vos commer\u00e7ants\"}]}";
                    //exit();

                    if(!empty($aListe)) {
                        $aTableuretour =array();
                        $atableaujson =array();
                        //$atableaujson['liste']=$aListe;

                        //echo"<pre>";print_r($aListe);echo"</pre>";
                        //exit();
                        $icount=0;
                        foreach($aListe as $item){

                            //echo"<pre>";print_r($item);echo"</pre>";

                            foreach($item as $key=>$value) {

                                if($key=="id") {
                                    $aTableuretour[$icount][$key] = $value;
                                }
                                elseif($key=="sUrlSupp" or $key=="sUrlForm"){
                                    $sVide="";
                                }
                                else{
                                    $aTableuretour[$icount][$key]=$value['value'];
                                }
                            }
                            $icount++;
                        }

                       /* echo"{\"liste\":[{\"id\":\"7F3A9357-88B4-4EDE-AC33-68229BC832D5\",\"nom_categorie\":\"Coiffureguy11\",\"pere_categorie\":\"Vos commer\u00e7ants\"},{\"id\":\"232E374B-F6BF-4727-8722-85A4E403FA10\",\"nom_categorie\":\"Assurance\",\"pere_categorie\":\"Vos commer\u00e7ants\"},{\"id\":\"2ACE33F3-982A-4DC8-B61B-FADBD50A940F\",\"nom_categorie\":\"Immobilier\",\"pere_categorie\":\"Vos commer\u00e7ants\"},{\"id\":\"7AA41F03-9185-42EB-B628-2EC28DE3152A\",\"nom_categorie\":\"Services\",\"pere_categorie\":\"Vos commer\u00e7ants\"}]}";
                        exit();
                        */
                        $atableaujson['liste']=$aTableuretour;
                        $sContenu=json_encode($atableaujson);
                        echo $sContenu;
                        $out = ob_get_clean();

                        echo $out; // éà
                       /* $out.= " <script>
                        console.log('".$sContenu."');
                        </script>";
                       */

                        exit();

                        class_fli::set_tpl_sortie('principal_empty.tpl');
                        $this->objSmarty->assign('aData', trim(json_encode($atableaujson)));
                        $this->objSmarty->setTemplateDir('vues/'.$sVersionTemplate.'/templates/');
                        $this->objSmarty->setCompileDir('vues/'.$sVersionTemplate.'/templates_c/');
                        return $this->objSmarty->fetch('json.tpl');
                    }else{
                        return null;
                    }
                    break;
                case 'pdf':
                    //echo"<pre>";print_r($aListe);echo"</pre>";
                    if(!empty($aListe)) {
                        $somFunction = $this->sFonctionGeneratePdf;
                        class_pdf::$somFunction($aListe,$aEnteteListe,$this->sTitreListe);
                    }
                    break;
                case 'csv':
                    if(!empty($aListe)) {
                        class_fli::set_tpl_sortie('principal_empty.tpl');
                        $this->objSmarty->assign('aListe', $aListe);
                        $this->objSmarty->setTemplateDir('vues/'.$sVersionTemplate.'/templates/');
                        $this->objSmarty->setCompileDir('vues/'.$sVersionTemplate.'/templates_c/');
                        return $this->objSmarty->fetch('csv.tpl');
                    }else{
                        class_fli::set_tpl_sortie('principal_empty.tpl');
                        $aListe=array();

                        $this->objSmarty->assign('aListe', $aListe);
                        $this->objSmarty->setTemplateDir('vues/'.$sVersionTemplate.'/templates/');
                        $this->objSmarty->setCompileDir('vues/'.$sVersionTemplate.'/templates_c/');
                        return $this->objSmarty->fetch('csv_vide.tpl');
                    }
                    break;
                case 'json':
                    if(!empty($aListe)) {
                        class_fli::set_tpl_sortie('principal_empty.tpl');
                        $this->objSmarty->assign('aData', trim(json_encode($aListe)));
                        $this->objSmarty->setTemplateDir('vues/'.$sVersionTemplate.'/templates/');
                        $this->objSmarty->setCompileDir('vues/'.$sVersionTemplate.'/templates_c/');
                        return $this->objSmarty->fetch('json.tpl');
                    }else{
                        return null;
                    }
                    break;
                 default:
                    $this->objSmarty->assign('aListe', $aListe);
                    return $this->objSmarty->fetch($this->sListeTpl);
                    break;
            }

        }
    }

    /**
     *Methode qui remet a zero le menu d'un module
     */
    public function raz_menu(){
        //récupération du chemin du script
        $aTableauCheminMethode = debug_backtrace();
        //echo "link ".$aTableauCheminMethode[0]['file']."<br>";
        $aTableauDecoupe = explode("/", $aTableauCheminMethode[0]['file']);
        $sRepertoireModule = $aTableauDecoupe[count($aTableauDecoupe)-3];
        $sRequete_RAZ = "UPDATE ".$this->sPrefixeDb."routes
        SET supplogique_route='Y'
        WHERE pere_menu_route = '".$sRepertoireModule."'";

        //echo $sRequete_RAZ."<br>";

        $this->objClassGenerique->execute_requete($sRequete_RAZ);
    }

    /**
     *Methode qui remet a zero le menu d'un module
     */
    public function raz_menu_module($sRepertoireModule,$nomclass,$aTabMethod){

        //récupération du chemin du script
        foreach($aTabMethod as $valeur) {

            if( $valeur != 'install' and preg_match('/^fli_./', $valeur) ) {

                $sRequete_RAZ = "UPDATE " . $this->sPrefixeDb . "routes
                SET supplogique_route='Y'
                WHERE route_route like '%" . $sRepertoireModule . "-".$nomclass."-".$valeur."%'";

                //echo $sRequete_RAZ."<br>\n";
                $this->objClassGenerique->execute_requete($sRequete_RAZ);
            }
        }
    }

    /**
     * @param $sRoute
     * @param $sPereRoute
     * @param string $sLibeleMenu
     * @param int $iOrdre
     * @param bool $bLien
     * @param string $sTarget
     */
    public function add_menu($sNomModule='',$sRoute, $sPereRoute, $sLibeleMenu = '', $iOrdre=0, $bLien=false, $sTarget='',$sAfficheMenu='0',$bblockrepertoire=false)
    {
        if(!empty($sNomModule)){
            $aParam[] = $sNomModule;
            $sRequeteSelectModule = "SELECT installe_module FROM ".$this->sPrefixeDb."modules WHERE nom_module=?";
            $aTabModule = $this->objClassGenerique->renvoi_info_requete($sRequeteSelectModule,$aParam);
        }else{
            die('<div style="text-align: center;"><img src="public/images/flilogo.png" alt="logo">
                <h3>Veuillez indiquer le nom de votre module dans la fonction add_menu().</h3>
                <div><a href="fli_admin-modules">Retour à la gestion des modules</a></div></div>');
        }

        //echo"<pre>";print_r(debug_backtrace());echo"</pre>";

       //récupération du chemin du script
        $aTableauCheminMethode = debug_backtrace();

        //echo "link ".$aTableauCheminMethode[0]['file']."<br>";

        $aTableauDecoupe = explode("/", $aTableauCheminMethode[0]['file']);

        $sRepertoireModule = $aTableauDecoupe[count($aTableauDecoupe)-3];

        //echo "Le repertoire ".$sRepertoireModule;
        $sRouteUnique="";
        //creation de la route de la fonctionnalité dans le menu
        if(!$bblockrepertoire) {
            $sRouteUnique = $sRepertoireModule;
            if( $sRoute != "" )
                $sRouteUnique .= "-" . $sRoute;
        }else{
            $sRouteUnique = $sRoute;
        }

        if($bLien)
            $bLien=1;
        else
            $bLien=0;

        if($sAfficheMenu=="")
            $sAfficheMenu=0;

        $aTab['route_route'] = $sRouteUnique;
        $aTab['pere_menu_route'] = $sPereRoute;
        $aTab['intitule_menu_route'] = $sLibeleMenu;
        $aTab['ordre_menu_route'] = $iOrdre;
        $aTab['lien_menu_route'] = $bLien;
        $aTab['target_menu_route'] = $sTarget;
        $aTab['afficher_menu_route'] = $sAfficheMenu;
        $guid_route = class_helper::guid();

        // ENregistrment ou modification de la route dans la base de donnée

       $sRequeteInsertMenu = "INSERT ".$this->sPrefixeDb."routes SET supplogique_route='N',
        guid_route='".$guid_route."',
        route_route=:route_route,
        pere_menu_route=:pere_menu_route,
        intitule_menu_route=:intitule_menu_route,
        ordre_menu_route=:ordre_menu_route,
        lien_menu_route=:lien_menu_route,
        target_menu_route=:target_menu_route,
        afficher_menu_route=:afficher_menu_route
        ON DUPLICATE KEY UPDATE supplogique_route='N',
        pere_menu_route=:pere_menu_route,
        ordre_menu_route=:ordre_menu_route,
        lien_menu_route=:lien_menu_route,
        target_menu_route=:target_menu_route,
        afficher_menu_route=:afficher_menu_route";


       /*
        $sRequeteInsertMenu = "INSERT ".$this->sPrefixeDb."routes SET supplogique_route='N',
        guid_route='".$guid_route."',
        route_route='".$sRouteUnique."',
        pere_menu_route='".$sPereRoute."',
        intitule_menu_route='".$sLibeleMenu."',
        ordre_menu_route='".$iOrdre."',
        lien_menu_route='".$bLien."',
        target_menu_route='".$sTarget."',
        afficher_menu_route='".$sAfficheMenu."'
        ON DUPLICATE KEY UPDATE supplogique_route='N',
        pere_menu_route='".$sPereRoute."',
        intitule_menu_route='".$sLibeleMenu."',
        ordre_menu_route='".$iOrdre."',
        lien_menu_route='".$bLien."',
        target_menu_route='".$sTarget."',
        afficher_menu_route='".$sAfficheMenu."'";*/


        //echo $sRequeteInsertMenu."<br>";
        //echo"<pre>";print_r($aTab);echo"</pre>";

        $this->objClassGenerique->insert_update_requete($sRequeteInsertMenu,$aTab);

        //Droits pour le groupe SuperAdmin
        $aParam = array();
        $aTabGroupe[0]['nom_groupe'] = "";
        $aParam[] = 'SuperAdmin';
        $sRequeteSelectModule = "SELECT guid_groupe FROM ".$this->sPrefixeDb."groupes WHERE nom_groupe=?";
        $aTabGroupe = $this->objClassGenerique->renvoi_info_requete($sRequeteSelectModule,$aParam);
        if(!empty($aTabGroupe[0]['guid_groupe'])) {
            $sRequeteInsertGroupeRoute = "INSERT ".$this->sPrefixeDb."groupes_routes SET supplogique_groupe_route='N',guid_route='" . $guid_route . "',ajout_groupe_route=1,modif_groupe_route=1,suppr_groupe_route=1,visu_groupe_route=1,
            guid_groupe='" . $aTabGroupe[0]['guid_groupe'] . "' ON DUPLICATE KEY UPDATE supplogique_groupe_route='N'";
            $this->objClassGenerique->insert_update_requete($sRequeteInsertGroupeRoute,array());
        }
    }

    public function add_fils($sModule, $sControleur, $sFonction, $sChampIdPere = null, $sFiltreChamp = '', $sTableFils=array())
    {



        if(empty($sModule)){
            class_fli::set_debug('class_form_list-add_fils','Le chemin spécifié ne peut pas être vide !');
            return 0;
        }

        if(empty($sFiltreChamp)){
            $sFiltreChamp = $sChampIdPere;
        }

        if(file_exists('modules/'.$sModule.'/controleurs/'.$sControleur.'.php')) {
            $sChemin = $sModule . '/controleurs/'.$sControleur.'.php';
            class_fli::set_debug('class_form_list-add_fils','Le controleur ayant pour chemin : '.$sModule . '/controleurs/'.$sControleur.'.php a été trouvé !');
        }elseif(file_exists('modules/'.$sModule . '/controleurs/'.$sControleur.'.php')){
            $sChemin = $sModule . '/controleurs/'.$sControleur.'.php';
            class_fli::set_debug('class_form_list-add_fils','Le controleur ayant pour chemin : '.$sModule . '/controleurs/'.$sControleur.'.php a été trouvé !');
        }else{
            class_fli::set_debug('class_form_list-add_fils','Le controleur ayant pour chemin : '.$sModule . '/controleurs/'.$sControleur.'.php n\'existe pas !');
            return 0;
        }

        if( $sChemin !== null ) {
            if( $sChampIdPere === null ) {
                $sChampIdPere = $this->sChampId;
            }
            self::$sFiltreChampFils = $sFiltreChamp;
            self::$sTableFils = $sTableFils;
            self::$sBaseUrlFils = $sModule.'-'.$sControleur.'-'.$sFonction;



            $this->aListeFils[] = array( 'sChemin' => $sChemin, 'sChampIdPere' => $sChampIdPere, 'sDir' => $sModule, 'sControleur' => $sControleur, 'sFonction' => $sFonction, 'sFiltreChamp' => $sFiltreChamp );
       
            //echo"<pre>";print_r($this->aListeFils);echo"</pre>";
        }

        return $this;
    }


    /**
     * Fonction qui affiche la page interdisant l'accès
     */
    public function fli_redirect_erreur(){
        class_fli::set_tpl_sortie('interdit.tpl');
    }

    /**
     * @return array
     */
    public function add_traitement_bdd(){
     $aTabRetour =array();

     return $aTabRetour;
 }


    public function mod_to_bdd(){

        $aTabMethod = $this->add_traitement_bdd();

        // on bloque le lance du run en passant la variable $this->bCreateTable à true
        $this->bBypassRun =true;

        $aTabChamp =array();
        $aTabChamp['date'] = "DATE";
        $aTabChamp['datetime'] = "DATETIME";
        $aTabChamp['text'] = "VARCHAR(255)";
        $aTabChamp['color'] = "VARCHAR(255)";
        $aTabChamp['file'] = "VARCHAR(255)";
        $aTabChamp['time'] = "TIME";
        $aTabChamp['hidden'] = "VARCHAR(255)";
        $aTabChamp['file'] = "VARCHAR(255)";
        $aTabChamp['textarea'] = "LONGTEXT";
        $aTabChamp['select'] = "BIGINT(20)";
        $aTabChamp['radio'] = "BIGINT(20)";
        $sValuedefault ="NOT NULL";
        $sValueAUTOIncrement ="BIGINT";
        $sCreationSqlKey="";


        $aListeTable = $this->objClassGenerique->renvoi_liste_table();
        //echo"<pre>";print_r($aListeTable);echo"</pre>";

        $aTableauAffiche =array();

        if(!empty($aTabMethod)){
            foreach($aTabMethod as $valeur){
                $sSqlcreate="";
                if(!preg_match ('/^entete_/',$valeur) and !preg_match ('/^install/',$valeur) ) {

                    $this->$valeur();
                    //echo $valeur;

                   /* echo "<pre>";
                    //print_r($this->aElement);
                    echo "</pre>";
                   */
                    // si la table n'est pas dans la base de donnée
                    if( !in_array($this->sNomTable,$aListeTable) ) {
                        $sSqlCreate = "CREATE TABLE " . $this->sNomTable . " ( " . $this->sChampId . " " . $sValueAUTOIncrement . " NOT NULL AUTO_INCREMENT, <br>";

                        if( !empty($this->aElement) ) {

                            if( $this->sChampGuid != "" ) {
                                $sCreationSqlKey .= ", UNIQUE KEY " . $this->sChampGuid . " (" . $this->sChampGuid . ")<br>";
                                $sSqlCreate .= $this->sChampGuid . " VARCHAR(80) NOT NULL,<br>";
                            }

                            foreach( $this->aElement as $sValeurSql ) {

                                $sTypeChampSql="";

                                if($sValeurSql['type_champ_sql']!=""){
                                    $sTypeChampSql=$sValeurSql['type_champ_sql'];
                                }else{
                                    $sTypeChampSql=$aTabChamp[$sValeurSql['type_champ']];
                                }

                                $sSqlCreate .= $sValeurSql['mapping_champ'] . " " .$sTypeChampSql. " " . $sValuedefault . ",<br>";

                                if( $sValeurSql['index_champ_sql'] != "" )
                                    $sCreationSqlKey .= ", " . $sValeurSql['index_champ_sql'] . " " . $sValeurSql['mapping_champ'] . " (" . $sValeurSql['mapping_champ'] . ")<br>";
                            }
                        }

                        $sSqlCreate .= $this->sChampSupplogique . " enum('".$this->sValONSupplogique."','".$this->sValOFFSupplogique."') default '".$this->sValONSupplogique."',<br>";
                        $sSqlCreate .= "PRIMARY KEY  (" . $this->sChampId . ")<br>";
                        $sSqlCreate .= $sCreationSqlKey;
                        $sSqlCreate .= ") ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ";

                        //echo $sSqlcreate."<br>----------------------------------<br>";

                         if($this->objClassGenerique->execute_requete(str_replace("<br>","",$sSqlCreate))){
                             $aTableauAffiche[]=array("keys"=>"bg-primary","contenu"=>"<b style='font-size: 20px;color:#4fbd4f;'><span class='glyphicon glyphicon-ok'></span> SUCCES</b><br>".$sSqlcreate);
                         }else{
                             $aTableauAffiche[]=array("keys"=>"bg-danger","contenu"=>"<b style='font-size: 20px;color:#ff0000'><span class='glyphicon glyphicon-remove'></span> ERREUR</b><br>".$sSqlcreate);
                         }
                       /* */
                    }else{

                        if( in_array($this->sNomTable,$aListeTable) ) {
                            $sSqlUpdateTable = "";
                            $sConfiltTable = "";
                            //si la table est déjà dans la base de données on regarde s'il y a un probleme sur les champs
                            $aListechamp = $this->objClassGenerique->renvoi_liste_champ_table($this->sNomTable);

                            if( !empty($this->aElement) ) {
                                foreach( $this->aElement as $sValeurSql ) {
                                    $bExiste = false;
                                    $bConflitChamp = false;
                                    $sTypeChampSql="";

                                    if($sValeurSql['type_champ_sql']!=""){
                                        $sTypeChampSql=$sValeurSql['type_champ_sql'];
                                    }else{
                                        $sTypeChampSql=$aTabChamp[$sValeurSql['type_champ']];
                                    }
                                    foreach( $aListechamp as $sValeurChamp ) {
                                        // on regarde si le champ est dans la base de données
                                        if( $sValeurSql['mapping_champ'] == $sValeurChamp['Field'] ) {
                                            $bExiste = true;
                                            // on regarde si le type du champ est coherent avec celui du formalaire

                                           // echo $sTypeChampSql."<br>\n";
                                            if( strtolower(str_replace(" ","",$sTypeChampSql)) != strtolower(str_replace(" ","",$sValeurChamp['Type'])) )
                                                $bConflitChamp = true;
                                        }

                                    }

                                    if( !$bExiste ) {
                                        $sSqlUpdateTable .= "ADD  " . $sValeurSql['mapping_champ'] . " " . $sTypeChampSql . " " . $sValuedefault . ",<br>";
                                    } elseif( $bConflitChamp ) {
                                        $sConfiltTable .= " Conflit sur le Type du champ " . $sValeurSql['mapping_champ'] . "<br>";
                                    }

                                }

                                if( $sSqlUpdateTable != "" )
                                    $sSqlUpdateTable = "ALTER TABLE " . $this->sNomTable . " " . $sSqlUpdateTable;

                                $sSqlUpdateTableEnvoi = rtrim(str_replace("<br>", "", $sSqlUpdateTable), ",") . ";";

                                if( $sSqlUpdateTable != "" and $this->objClassGenerique->execute_requete($sSqlUpdateTableEnvoi) )
                                    $aTableauAffiche[] = array( "keys" => "bg-primary", "contenu" => "<b style='font-size: 20px;color:#4fbd4f;'><span class='glyphicon glyphicon-ok'></span> SUCCES</b><br>" . $sSqlUpdateTable );
                                else {
                                    $aTableauAffiche[] = array( "keys" => "bg-danger", "contenu" => "<b style='font-size: 20px;color:#ff0000;'><span class='glyphicon glyphicon-remove'></span> ERREUR</b><br>Probleme pendant la mise à jour de la table" );
                                }

                                if( $sConfiltTable != "" )
                                    $aTableauAffiche[] = array( "keys" => "bg-danger", "contenu" => "<b style='font-size: 20px;color:#ff0000;'><span class='glyphicon glyphicon-remove'></span> SUCCES</b><br>" . $sConfiltTable );

                                if( $sSqlUpdateTable == "" and $sConfiltTable == "" )
                                    $aTableauAffiche[] = array( "keys" => "bg-primary", "contenu" => "<b style='font-size: 20px;color:#4fbd4f;'><span class='glyphicon glyphicon-ok'></span> ERREUR</b><br>Table " . $this->sNomTable . " Ok" );

                                $sSqlUpdateTable = rtrim(str_replace("<br>", "", $sSqlUpdateTable), ",") . ";";


                            }
                        }

                       //echo"<pre>";print_r($aListechamp);echo"</pre>";
                    }
                }

                if($valeur=='run'){
                    break;
                }
            }


            $this->objSmarty->assign('aResult',$aTableauAffiche);

            return $this->objSmarty->fetch('etat_lieux.tpl');


        }

        //echo"<pre>";print_r($aTabMethod);echo"</pre>";
    }

    public function set_route(){

        $aTabMethod = get_class_methods($this);

        $bIntallexist =false;
        foreach($aTabMethod as $valeur) {
            if($valeur=='install') {
                $bIntallexist = true;
                break;
            }
        }
        $aTableauAffiche=array();
            //echo"<pre>";print_r($aTabMethod);echo"</pre>";
        //echo"<pre>";print_r($aTabMethod);echo"</pre>";
        $sModule =  class_fli::get_fli_module();
        $nomclass = get_class($this);
        $this->bByPassConnect =true;
        $sLaRoute = $sModule."-".$nomclass;
        //echo "Info ".$sModule." ".$nomclass;
        if($bIntallexist){
            $this->install();
        }else{
            $this->raz_menu_module($sModule,$nomclass,$aTabMethod);
            $this->add_menu('Module '.$sModule, $sModule, '',$sModule, 0, false, '', 1,true);
        }
        
        //echo"<pre>";print_r($aTabMethod);echo"</pre>";

        foreach($aTabMethod as $valeur){

            if($valeur!='install' and preg_match('/^fli_./',$valeur)) {

                $this->sChampGuid='';
                $this->sChampIdTmp='';
                $this->sChampId='';
                $this->sJointureSup='';
                $this->sFiltreliste='';
                $this->sFiltrelisteOrderBy='';
                $this->aElement=array();

                //echo " test".$this->sChampGuid;
                $this->$valeur();
               /* echo "toto ".$valeur;
                exit();
               */
                $ivisibilite=1;

                $sLaRoute =$sModule."-".$nomclass."-".$valeur;
                $aTableauAffiche[] = array( "keys" => "bg-primary", "contenu" => "<b style='font-size: 20px;color:#4fbd4f;'><span class='glyphicon glyphicon-ok'>
                </span>Methode ".$valeur."</b><br>Intitule Menu : ".$this->sMenuIntitule."  visibilité : ".$this->sMenuVisibile." Target ".$this->sMenuTarget." la Route : ".$sLaRoute);

               if(!$this->sMenuVisibile)
                   $ivisibilite=0;

               $this->add_menu("Module ".$sModule,$sLaRoute,$sModule,$this->sMenuIntitule,0,false,$this->sMenuTarget,$ivisibilite,true);

                //$this->add_menu('Module frontend', 'ctrl_frontend-offre', 'frontend', 'offre', 0, false, '', $ivisibilite);
                //echo  $this->sNomTable."<br>";
            }

            if($valeur=='run'){
                break;
            }
        }


        $this->sListeTpl = 'etat_lieux.tpl';
        $this->sFormulaireTpl = 'etat_lieux.tpl';

        $this->sTplSortie="principal.tpl";

        $this->chargement();
        $this->bByPassConnect=false;
        $this->objSmarty->assign('aResult',$aTableauAffiche);
        $tTplMenu = $this->run();

        return $tTplMenu;
    }

    public function set_fonction_exception($sFonction=''){
        if(!empty($sFonction)){
            $this->aTabException[] = $sFonction;
        }
    }

    //TODO : Formulaire en ajax
    //TODO : Créer fiche.tpl
    //TODO : Finaliser les modules par défaut (droits, menu, ...)

}
