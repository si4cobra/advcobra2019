<?php

/**
 * Created by PhpStorm.
 * Date: 11/12/2015
 * Time: 16:36
 */
class class_helper
{
    /**
     * @return string Guid aléatoire
     */
    public static function guid()
    {
        if( function_exists('com_create_guid') === true ) {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public static function  random($car) {
        $string = "";
        $chaine = "abcdefghijklmnpqrstuvwxy";
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
        $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
        }

    /**
     * renvoi date
     * @param $Ladate
     * @param $langue
     * @param string $format
     * @return string
     */
    public static function renvoi_date($Ladate,$langue,$format="")
    {
        $Retour = $Ladate;

        if( $langue == "fr" ) {

            $recutableau = explode("-", $Ladate);
            $Retour = $recutableau[2] . "/" . $recutableau[1] . "/" . $recutableau[0];
        }

        if( $langue == "eng" ) {

            if(preg_match('#^([0-9]{2})/([0-9]{2})/([0-9]{4})$#',$Ladate)){
                $tableaudate = explode("/", $Ladate);
                $Retour = $tableaudate[2] . "-" . $tableaudate[1] . "-" . $tableaudate[0];
            }
        }

        if( $langue == "format" ) {
            $recutableau = explode("-", $Ladate);
            setlocale("LC_TIME", "fr");
            $Retour = strftime($format, mktime(0, 0, 0, $recutableau[1], $recutableau[2], $recutableau[0]));
        }

        return $Retour;
    }

    public static function renvoi_jour_literal($ladate){

        $aTableaudecoupe=explode("-",$ladate);
        $imktime=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2],$aTableaudecoupe[0]);
        $ijour=date('w',$imktime);
        $imois=date('n',$imktime);

        $jour = array( "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" );

        $mois = array( "", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" );

        $datefr = $jour[$ijour] . " " . $aTableaudecoupe[2] . " " . $mois[$imois] . " " . $aTableaudecoupe[0];


        return $datefr;
    }

    public static function renvoi_jour_semaine($ladate){

        $aTableauRetour=array();
        $aTableaudecoupe=explode("-",$ladate);
        $imktime=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2],$aTableaudecoupe[0]);
        $ijour=date('w',$imktime);

        if($ijour==1){
            $aTableauRetour[]=  date('Y-m-d',$imktime);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+6,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==2){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-1,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+5,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==3){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-2,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+4,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==4){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-3,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+3,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==5){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-4,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+2,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==6){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-5,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]+1,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else if($ijour==0){
            $imktimedebut=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2]-6,$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimedebut);
            $imktimefim=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2],$aTableaudecoupe[0]);
            $aTableauRetour[]=  date('Y-m-d',$imktimefim);
        }
        else {
            $aTableauRetour=array();
        }


        return $aTableauRetour;
    }

    /**
     * methode  renvoi le dernier jour du mois
     * @author Danon Gnakouri
     * @since 3.2
     * @return la connexion
     */
    public static function Getdernierjourmois ($mois,$annee) {

        $variable =$mois."-".$annee;
        list ($month, $year) = explode ('-', $variable);
        $year = ((int)$month === 12)?$year+1:$year;
        $month = ((int)$month + 1 === 13)?1:$month+1;
        $lastDay = mktime (0, 0, 0, $month, 0,  $year);
        return strftime ('%d', $lastDay);
    }

    public static  function get_lundi_vendredi_from_week($week,$year,$format="d/m/Y") {

        $firstDayInYear=date("N",mktime(0,0,0,1,1,$year));
        if ($firstDayInYear<5)
            $shift=-($firstDayInYear-1)*86400;
        else
            $shift=(8-$firstDayInYear)*86400;
        if ($week>1) $weekInSeconds=($week-1)*604800; else $weekInSeconds=0;
        $timestamp=mktime(0,0,0,1,1,$year)+$weekInSeconds+$shift;
        $timestamp_vendredi=mktime(0,0,0,1,7,$year)+$weekInSeconds+$shift;

        return array(date($format,$timestamp), date($format,$timestamp_vendredi));

    }

    /*
    * Mois literal
    */
    public static  function renvoi_mois_literal($ladate){

        $aTableaudecoupe=explode("-",$ladate);
        $imktime=mktime(0,0,0,$aTableaudecoupe[1],$aTableaudecoupe[2],$aTableaudecoupe[0]);
        $ijour=date('w',$imktime);
        $imois=date('n',$imktime);

        $jour = array( "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" );

        $mois = array( "", "Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre" );

        $datefr =  $mois[$imois] . " " . $aTableaudecoupe[0];


        return $datefr;
    }
    /**
     * methode  renvoi le  mois suivant
     * @author Danon Gnakouri
     * @since 3.2
     * @return la connexion
     */
    public static function renvoi_mois_suivant($mois,$annee){

        $mktimemoisprecedent = mktime(0,0,0,$mois+1,1,$annee);
        $mois= date('m',$mktimemoisprecedent);
        $annee = date('Y',$mktimemoisprecedent);

        $aTableauretour =array("mois"=>$mois,"annee"=>$annee);
        return $aTableauretour;
    }
    /**
     * methode  renvoi le  mois  precedent
     * @author Danon Gnakouri
     * @since 3.2
     * @return la connexion
     */
    public static function renvoi_mois_precedent($mois,$annee){

        $mktimemoisprecedent = mktime(0,0,0,$mois-1,1,$annee);

        $mois= date('m',$mktimemoisprecedent);
        $annee = date('Y',$mktimemoisprecedent);

        $aTableauretour =array("mois"=>$mois,"annee"=>$annee);
        return $aTableauretour;

    }
    /**
     * methode  renvoi le dernier jour du mois
     * @author Danon Gnakouri
     * @since 3.2
     * @return la connexion
     */

    public static function Getjoursemaine($ladate,$type){

        $atableau = explode("/",$ladate);

        //echo"<pre>";print_r($atableau);echo"</pre>";

        $lemktime = mktime(0,0,0,$atableau[1],$atableau[0],$atableau[2]);

        //on récupére le jour de la semaine
        $sLejoursemaine = date('w',$lemktime);

        //calcul du mktime du premier jour de la semaine
        $lemktimepremierjour = mktime(0,0,0,$atableau[1],$atableau[0]-$sLejoursemaine+1,$atableau[2]);

        $sPremierjour = date('d/m/Y',$lemktimepremierjour);

        //calcul du mktime dernier jour de la semaine
        $iIntev = 6-$sLejoursemaine;
        $lemktimedernierjour = mktime(0,0,0,$atableau[1],$atableau[0]+$iIntev+1,$atableau[2]);

        $sDernierjour = date('d/m/Y',$lemktimedernierjour);

        if($type==1)
            return $sPremierjour;
        else
            return $sDernierjour;

    }

    /**
     * methode renvoi tableau du moi
     * @author Danon Gnakouri
     * @param login login de connexion
     * @param motdepass mot de pass
     *@param $bdd mot de passe
     * @since 3.2
     * @return la connexion
     */
    public static  function renvoi_tableaumois($mois,$annee, $baffsuite=false,$bcomplexe=false){

        $k=0;
        $aTableauRetour=array();
        $iDernierjour = $this->Getdernierjourmois($mois,$annee);
        $ipremierjour = date('w',mktime(0,0,0,$mois,1,$annee));

        $moisprecedent =date('m',mktime(0,0,0,$mois-1,1,$annee));
        $anneeprecedent =date('Y',mktime(0,0,0,$mois-1,1,$annee));

        $moissuivant =date('m',mktime(0,0,0,$mois+1,1,$annee));
        $anneesuivant =date('Y',mktime(0,0,0,$mois+1,1,$annee));


        //echo $ipremierjour;
        if($ipremierjour==0)
            $ipremierjour=7;

        $ipremierjour = $ipremierjour-1;
        $bcount=false;

        for($i=0;$i<42;$i++){

            if($i==$ipremierjour) {
                $bcount = true;
                $ipremierpos = $i;
            }

            if($bcount){
                $k++;
            }else{
                $k=0;
            }
            $NumjourAff = $k;
            if($k<10)
                $NumjourAff = "0".$k;
            $aTableauRetour[$i]=array($k,$annee."-".$mois."-".$NumjourAff,$annee,$mois);

            if($k==$iDernierjour) {
                $bcount = false;
                $idernierpos=$i;
            }
        }

        if($baffsuite){

            $j=1;
            for($u=$idernierpos+1;$u<42;$u++){
                $NumjourAff = $j;
                if($j<10)
                    $NumjourAff = "0".$j;
                $aTableauRetour[$u]=array($j,$anneesuivant."-".$moissuivant."-".$NumjourAff,$anneesuivant,$moissuivant);
                $j++;
            }

            $iDernierjourprecedent = $this->Getdernierjourmois($mois-1,$annee);

            for($u=$ipremierpos-1;$u>=0;$u--){
                $NumjourAff = $iDernierjourprecedent;
                if($iDernierjourprecedent<10)
                    $NumjourAff = "0".$iDernierjourprecedent;
                $aTableauRetour[$u]=array($iDernierjourprecedent,$anneeprecedent."-".$moisprecedent."-".$NumjourAff,$anneeprecedent,$moisprecedent);
                $iDernierjourprecedent = $iDernierjourprecedent-1;
            }
        }

        return $aTableauRetour;

    }

    /**
     * methode renvoi tableau jour entre deux dates
     * @author Danon Gnakouri
     */
    public static function renvoi_tableau_date($datedeut,$datefin,$bdebug=false){

        $aTableaudeoucpedebut = explode('/',$datedeut);
        $aTableaudeoucpefin = explode('/',$datefin);
        $aTableauRetour=array();

        $mktimedatedebut = mktime(23,59,59,$aTableaudeoucpedebut[1],$aTableaudeoucpedebut[0] ,$aTableaudeoucpedebut[2]);
        $mktimedatefin = mktime(23,59,59,$aTableaudeoucpefin[1],$aTableaudeoucpefin[0] ,$aTableaudeoucpefin[2]);
        if($bdebug)
            echo $mktimedatedebut."<br>".$mktimedatefin;

        if($mktimedatedebut<$mktimedatefin){

            $bcompte=true;
            $aTableauRetour[]=date('Y-m-d',$mktimedatedebut);
            $u=1;
            while($bcompte){
                $mktimedatemp = mktime(23,59,59,$aTableaudeoucpedebut[1],$aTableaudeoucpedebut[0] +$u,$aTableaudeoucpedebut[2]);
                if($mktimedatemp>$mktimedatefin){
                    $bcompte=false;
                }else{
                    $aTableauRetour[]=date('Y-m-d',$mktimedatemp);
                }
                $u++;

                if($u>1000)
                    $bcompte=false;
            }
        }


        return $aTableauRetour;

    }





}