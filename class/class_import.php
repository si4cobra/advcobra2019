<?php

class class_import
{

    protected $sSourpertoire="";
    public function __construct()
    {

    }

    /**
     * fonction qui permet d'importer un modeles
     * @return : Injecte le modèle désiré
     *
     */
    public function import_modele($sModule, $sModele)
    {
        class_fli::set_debug('class_import-modeles',$sModule);
        //Si le modèle n'existe pas, on ajoute l'info dans le tableau de debug
        $path=$_SERVER["DOCUMENT_ROOT"]."".$this->sSourpertoire;

        $sSousRepertoire = class_fli::get_sous_repertoire();

        if($sSousRepertoire!="")
            $path.="/".$sSousRepertoire;

        if(!file_exists($path.'/modules/' . $sModule . '/modeles/' . $sModele . '.php')){
            class_fli::set_debug('class_import-modeles','Le modèle "'.$sModele.'" du module "'.$sModule.'" est introuvable !',1);
        }
        return require_once($path.'/modules/' . $sModule . '/modeles/' . $sModele . '.php');
    }

    /**
     * fonction qui permet de créer un objet Smarty
     * @return : objet Smarty
     *
     */
    public function import_vue(/*$sModule, $sVersionTemplate = 'version1'*/)
    {
        $objSmartyImport = new Smarty();
        class_fli::set_debug('class_import-vue','L\'objet Smarty a été correctement créé !');
        /*$objSmartyImport->setTemplateDir('modules/' . $sModule . '/vues/' . $sVersionTemplate . '/templates/');
        $objSmartyImport->setCompileDir('modules/' . $sModule . '/vues/' . $sVersionTemplate . '/templates_c/');*/
        return $objSmartyImport;
    }

    /**
     * fonction qui permet d'importer un module
     * @return : le ou les templates du/des module(s) appelé(s)
     *
     */
    public function import_module($sModule, $sControleur = '', $sFonction = '', $objSmartyImport = '')
    {
        //Si le controleur est vide, on rajoute 'ctrl_' devant le nom du module.
        if( empty($sControleur) ) {
            $sControleur = 'ctrl_' . $sModule;
        }

        class_fli::set_debug('class_import-module',$sModule);
        class_fli::set_debug('class_import-controleur',$sControleur);
        class_fli::set_debug('class_import-fonction',$sFonction);



        //Si le controleur existe, on l'inclut et on crée une instance de celui-ci. Sinon, on affiche la page 404
        $path=$_SERVER["DOCUMENT_ROOT"]."".$this->sSourpertoire;
        $sLastcaractere = substr($path,-1);
        if($sLastcaractere!="/")
            $path.="/";
        $sSousRepertoire = class_fli::get_sous_repertoire();

        if($sSousRepertoire!="")
            $path.="/".$sSousRepertoire;

        //echo $path."<br>";

        if(file_exists($path.'modules/' . $sModule . '/controleurs/' . $sControleur . '.php')) {

            require_once($path.'modules/' . $sModule . '/controleurs/' . $sControleur . '.php');



            $objControleur = new $sControleur($objSmartyImport,$sModule);

            //Si la fonction demandée existe dans le controleur, on l'exécute sinon on affiche la page 404
            if(method_exists($objControleur, $sFonction)){
                $aTabTpl = $objControleur->$sFonction();
                class_fli::set_debug('class_import','Le module "'.$sModule.'" a été correctement lancé !');
            }else{
                class_fli::set_tpl_sortie('404.tpl');
                $aTabTpl = array();
            }

              return $aTabTpl;
        }else{
            class_fli::set_tpl_sortie('404.tpl');
            return array();
        }
    }
}