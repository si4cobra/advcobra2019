<?php
/**
 * Created by PhpStorm.
 * User: Amory
 * Date: 15/12/2015
 * Time: 18:04
 */

class class_params
{
    /**
     * @param $aVar mixed Les paramètres POST/GET nettoyés
     * @return mixed Les paramètres POST/GET nettoyés
     */
    public static function nettoie_get_post($aVar){

        for ($i=0;$i<count($aVar);$i++) {
            if (isset($_POST[$aVar[$i]])) {
                $aVar[$i] = trim(filter_var($_POST[$aVar[$i]],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
            }elseif(!empty($_GET[$aVar[$i]])) {
                $aVar[$i] = trim(filter_var($_GET[$aVar[$i]],FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW));
            }else {
                $aVar[$i] = "";
            }
        }

        return $aVar;

    }

}