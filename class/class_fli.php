<?php

/**
 * Nom: Class fli
 * Description:
 * Date: 11/12/2015
 * Version:
 */
class class_fli
{
    //Variables de FLI Framwork
    private static $nomProjet;
    private static $nomCookie;
    private static $sousRepertoire='';

    //Chemins d'accès du framework
    private static $cheminAccesAbsolu;
    private static $cheminAccesRelatif = '';
    private static $cheminAccesServer;
    private static $cheminAccesPublic;

    //Variables de fonctionnement du framework
    private static $cheminModuleGestionConnexion;
    //BDD infos
    private static $host;
    private static $username;
    private static $password;
    private static $database;
    private static $prefixe;
    private static $fli_supp = '';
    private static $fli_actif = '';
    private static $fli_actif_doc = 'Y';

    //Booléen | Par défaut l'utilisateur est connecté ou non
    private static $fli_estConnecte;
    private static $sGUId;

    //Le module et la fonction exécutés par défaut (Défaut à l'installation : module_hello_world-hello_world)
    private static $module_defaut;
    private static $controleur_defaut;
    private static $fonction_defaut;
    //Le module de login et sa fonction
    private static $module_login;
    private static $controleur_login;
    private static $fonction_login;
    //Le module authentification et foonction
    private static $module_authentification;
    private static $controleur_authentification;
    private static $fonction_authentification;
    //Les variables contenant le chemin du logo, du logo alternatif et du favicon et texte d'accueil
    private static $logo = '';
    private static $logo_alternatif = '';
    private static $favicon = '';
    private static $html_home = '';
    //Variables de session et des droits relatifs à celle-ci (ajout/modification/suppression/visualisation)
    private static $session;
    private static $aDroits = array( 'm' => 0, 'a' => 0, 's' => 0, 'v' => 0 );
    //Le module et la fonction exécutés
    private static $fli_module = '';
    private static $fli_controleur = '';
    private static $fli_fonction = '';
    //L'utilisateur est-il SuperAdmin ?
    private static $bSuperAdmin = false;

    //Tableau contenant différentes informations telles que guid_user, guid_groupes, ...
    //Ajout dynamique de clés/valeurs
    private static $aData = array();

    //Tableaux de js,css,meta permettant leur ajout dans la balise <head> ou à la fin du <body> du tpl principal (assign dans class_fli::execution)
    private static $fli_js = array();
    private static $fli_css = array();
    private static $fli_meta = array();

    //Variables concernant le débuggage bdebug permet l'activation du débuggage, aDebug est le tableau contenant les informations de débuggage et le template par défaut est debug.tpl
    private static $bDebug = false;
    private static $aDebug = array();
    private static $sTplDebug = 'debug.tpl';

    //Template principal d'affiche, par défaut : principal.tpl
    private static $sTplSortie = 'principal.tpl';
    private static $sTplVersion = 'version1';

    /**
     * @param $aTabTpl array Le tableau de templates à afficher dans le principal
     * @param string $sVersionTemplate string Le dossier de version du template
     * @param string $tTplMenu string Le template du menu à afficher
     * @param bool $bDebug bool Permet l'affichage de la fenêtre de débuggage
     */
    public static function execution($aTabTpl, $sVersionTemplate = 'version1', $tTplMenu = '', $bDebug = false)
    {

        $aVar = array( 'action' );
        $aVarVal = class_params::nettoie_get_post($aVar);

        foreach( $aVar as $key => $value ) {
            $$value = $aVarVal[$key];
        }

        $objSmarty = new Smarty();
        $objSmarty->setTemplateDir('vues/' . $sVersionTemplate . '/templates/');
        $objSmarty->setCompileDir('vues/' . $sVersionTemplate . '/templates_c/');

        $objSmarty->assign('aTableauMenu', array());

        $objSmarty->assign('fli_action', $action);
        $objSmarty->assign('labdd', 'test');
        $objSmarty->assign('sTitreDeLaPage', self::get_nom_projet());
        $objSmarty->assign('titrehautprincipal', 'test');
        $objSmarty->assign('lasPageDirection', 'test');
        $objSmarty->assign('badminmain', 'test');
        $objSmarty->assign('sim', 'test');
        $objSmarty->assign('dir', self::$fli_module.'-'.self::$fli_controleur.'-'.self::$fli_fonction);
        $objSmarty->assign('bmodalstock', false);
        $objSmarty->assign('btypeformactionstock', '');

        $objSmarty->assign('cheminAccesAbsolu', self::$cheminAccesAbsolu);
        $objSmarty->assign('cheminAccesPublic', self::$cheminAccesPublic);

        $objSmarty->assign('logo', self::$logo);
        $objSmarty->assign('logo_alternatif', self::$logo_alternatif);
        $objSmarty->assign('favicon', self::$favicon);

        //ASSIGN CSS / JS / META des moules
        $objSmarty->assign('cssModules', self::$fli_css);
        $objSmarty->assign('jsModules', self::$fli_js);
        $objSmarty->assign('metaModules', self::$fli_meta);

        //Menu
        $objSmarty->assign('tTplMenu', $tTplMenu);

        //Connecté
        $objSmarty->assign('bEstConnecte', self::get_fli_est_connecte());

        $objSmarty->assign('pagedirection', $aTabTpl);
        //Debug personnalisé
        $objSmarty->assign('bDebug', self::$bDebug);
        $objSmarty->assign('fli_module_defaut', self::$module_defaut);
        $objSmarty->assign('fli_controleur_defaut', self::$controleur_defaut);
        $objSmarty->assign('fli_fonction_defaut', self::$fonction_defaut);
        $objSmarty->assign('fli_module', self::$fli_module);
        $objSmarty->assign('fli_controleur', self::$fli_controleur);
        $objSmarty->assign('fli_fonction', self::$fli_fonction);
        self::array_sort_by_column(self::$aDebug, "importance");
        $objSmarty->assign('aDebug', self::$aDebug);
        $objSmarty->assign('aData', self::$aData);

        if( !$bDebug ) {

            $objSmarty->display(self::$sTplSortie);
        } else {

            $objSmarty->display(self::$sTplDebug);
        }
    }

    //Permet de trier un tableau multidimensionnel sur une colonne
    public static function array_sort_by_column(&$arr, $col, $dir = SORT_ASC)
    {
        $sort_col = array();
        foreach( $arr as $key => $row ) {
            $sort_col[$key] = $row[$col];
        }

        array_multisort($sort_col, $dir, $arr);
    }

    /**
     * @return string
     */
    public static function get_html_home()
    {
        return self::$html_home;
    }

    /**
     * @return mixed
     */
    public static function get_controleur_login()
    {
        return self::$controleur_login;
    }

    /**
     * @return string
     */
    public static function get_fli_controleur()
    {
        return self::$fli_controleur;
    }
    /**
     * @return string
     */
    public static function get_fli_version_template()
    {
        return self::$sTplVersion;
    }

    /**
     * @return string
     */
    public static function set_fli_version_template($sversion)
    {
        self::$sTplVersion=$sversion;
    }


    /**
     * @return mixed
     */
    public static function get_controleur_defaut()
    {
        return self::$controleur_defaut;
    }

    /**
     * @return mixed
     */
    public static function get_prefixe()
    {
        return self::$prefixe;
    }

    /**
     * @return string
     */
    public static function get_chemin_acces_absolu()
    {
        return self::$cheminAccesAbsolu;
    }

    /**
     * @param string $cheminAccesAbsolu
     */
    public static function set_chemin_acces_absolu($cheminAccesAbsolu)
    {
        self::$cheminAccesAbsolu = $cheminAccesAbsolu;
    }

    /**
     * @return string
     */
    public static function get_chemin_acces_relatif()
    {
        return self::$cheminAccesRelatif;
    }

    /**
     * @param string $cheminAccesRelatif
     */
    public static function set_chemin_acces_relatif($cheminAccesRelatif)
    {
        self::$cheminAccesRelatif = $cheminAccesRelatif;
    }

    /**
     * @return string
     */
    public static function get_chemin_acces_serverr()
    {
        return self::$cheminAccesServer;
    }

    /**
     * @return boolean
     */
    public static function get_super_admin()
    {
        return self::$bSuperAdmin;
    }

    /**
     * @param mixed $prefixe
     */
    public static function set_prefixe($prefixe)
    {
        self::$prefixe = $prefixe;
    }

    /**
     * @param mixed $controleur_defaut
     */
    public static function set_controleur_defaut($controleur_defaut)
    {
        self::$controleur_defaut = $controleur_defaut;
    }

    /**
     * @param string $fli_controleur
     */
    public static function set_fli_controleur($fli_controleur)
    {
        self::$fli_controleur = $fli_controleur;
    }

    /**
     * @param mixed $controleur_login
     */
    public static function set_controleur_login($controleur_login)
    {
        self::$controleur_login = $controleur_login;
    }

    /**
     * @param string $html_home
     */
    public static function set_html_home($html_home)
    {
        self::$html_home = $html_home;
    }
    /**
     * @param mixed $prefixe
     */
    public static function set_sous_repertoire($sousRepertoire)
    {
        self::$sousRepertoire = $sousRepertoire;
    }
    /**
     * @param boolean $bSuperAdmin
     */
    public static function set_super_admin($bSuperAdmin)
    {
        self::$bSuperAdmin = $bSuperAdmin;
    }
    /**
     * @return mixed
     */
    public static function get_module_authentification()
    {
        return self::$module_authentification;
    }

    /**
     * @param mixed $module_authentification
     */
    public static function set_module_authentification($module_authentification)
    {
        self::$module_authentification = $module_authentification;
    }

    /**
     * @return mixed
     */
    public static function get_controleur_authentification()
    {
        return self::$controleur_authentification;
    }

    /**
     * @param mixed $controleur_authentification
     */
    public static function set_controleur_authentification($controleur_authentification)
    {
        self::$controleur_authentification = $controleur_authentification;
    }
    /**
     * @return mixed
     */
    public static function set_fonction_authentification($fonction_authentification)
    {
         self::$fonction_authentification = $fonction_authentification;
    }

    /**
     * @return mixed
     */
    public static function get_fonction_authentification()
    {
        return self::$fonction_authentification;
    }


    /**
     * @param mixed $favicon
     */
    public static function set_favicon($favicon)
    {
        self::$favicon = $favicon;
    }

    /**
     * @param mixed $logo
     */
    public static function set_logo($logo)
    {
        self::$logo = $logo;
    }

    /**
     * @param mixed $logo_alternatif
     */
    public static function set_logo_alternatif($logo_alternatif)
    {
        self::$logo_alternatif = $logo_alternatif;
    }

    /**
     * @param string $cheminAccesServer
     */
    public static function set_chemin_acces_server($cheminAccesServer)
    {
        self::$cheminAccesServer = $cheminAccesServer;
    }

    /**
     * @return string
     */
    public static function get_chemin_acces_public()
    {
        return self::$cheminAccesPublic;
    }

    /**
     * @param string $cheminAccesPublic
     */
    public static function set_chemin_acces_public($cheminAccesPublic)
    {
        self::$cheminAccesPublic = $cheminAccesPublic;
    }
    /**
     * @return mixed
     */
    public static function get_guid_user()
    {
        return self::$sGUId;
    }

    /**
     * @param mixed $sGUId
     */
    public static function set_guid_user($sGUId)
    {
        self::$sGUId = $sGUId;
    }

    /**
     * @return string
     */
    public static function get_chemin_module_gestion_connexion()
    {
        return self::$cheminModuleGestionConnexion;
    }

    /**
     * @param string $cheminModuleGestionConnexion
     */
    public static function set_chemin_module_gestion_connexion($cheminModuleGestionConnexion)
    {
        self::$cheminModuleGestionConnexion = $cheminModuleGestionConnexion;
    }

    /**
     * @return string
     */
    public static function get_host()
    {
        return self::$host;
    }

    /**
     * @param string $host
     */
    public static function set_host($host)
    {
        self::$host = $host;
    }

    /**
     * @return string
     */
    public static function get_username()
    {
        return self::$username;
    }

    /**
     * @param string $username
     */
    public static function set_username($username)
    {
        self::$username = $username;
    }

    /**
     * @return string
     */
    public static function get_password()
    {
        return self::$password;
    }

    /**
     * @param string $password
     */
    public static function set_password($password)
    {
        self::$password = $password;
    }

    /**
     * @return string
     */
    public static function get_database()
    {
        return self::$database;
    }

    /**
     * @param string $database
     */
    public static function set_database($database)
    {
        self::$database = $database;
    }

    /**
     * @return boolean
     */
    public static function get_fli_est_connecte()
    {
        return self::$fli_estConnecte;
    }

    /**
     * @param boolean $fli_estConnecte
     */
    public static function set_fli_est_connecte($fli_estConnecte)
    {
        self::$fli_estConnecte = $fli_estConnecte;
    }
    /**
     * @param string $fli_actif_doc
     */
    public static function set_fli_actif_doc($fli_actif_doc)
    {
        self::$fli_actif_doc = $fli_actif_doc;
    }
    /**
     * @return string
     */
    public static function get_module_defaut()
    {
        return self::$module_defaut;
    }

    /**
     * @return mixed
     */
    public static function get_module_login()
    {
        return self::$module_login;
    }

    /**
     * @param mixed $module_login
     */
    public static function set_module_login($module_login)
    {
        self::$module_login = $module_login;
    }

    /**
     * @return mixed
     */
    public static function get_fonction_login()
    {
        return self::$fonction_login;
    }
    /**
     * @return string
     */
    public static function get_fli_actif_doc()
    {
        return self::$fli_actif_doc;
    }

    /**
     * @param mixed $fonction_login
     */
    public static function set_fonction_login($fonction_login)
    {
        self::$fonction_login = $fonction_login;
    }

    /**
     * @param string $module_defaut
     */
    public static function set_module_defaut($module_defaut)
    {
        self::$module_defaut = $module_defaut;
    }

    /**
     * @return string
     */
    public static function get_fonction_defaut()
    {
        return self::$fonction_defaut;
    }

    /**
     * @param string $fonction_defaut
     */
    public static function set_fonction_defaut($fonction_defaut)
    {
        self::$fonction_defaut = $fonction_defaut;
    }

    /**
     * @return array
     */
    public static function get_session()
    {
        return self::$session;
    }

    /**
     * @param $aSession
     */
    public static function set_session($aSession)
    {
        self::$session = $aSession;
    }

    /**
     * @return array
     */
    public static function get_droits()
    {
        return self::$aDroits;
    }

    /**
     * @return mixed
     */
    public static function get_sous_repertoire()
    {
        return self::$sousRepertoire;
    }

    /**
     * @param $aTab
     */
    public static function set_droits($aTab)
    {


        self::$aDroits['m'] = $aTab['m'];
        self::$aDroits['a'] = $aTab['a'];
        self::$aDroits['s'] = $aTab['s'];
        self::$aDroits['v'] = $aTab['v'];
        self::$aDroits['w'] = $aTab['w'];
    }

    /**
     * @return mixed
     */
    public static function get_fli_js()
    {
        return self::$fli_js;
    }

    /**
     * @param mixed $fli_js
     */
    public static function set_fli_js($fli_js)
    {
        self::$fli_js = $fli_js;
    }

    /**
     * @param $js
     */
    public static function add_fli_js($aJs, $sNomModule)
    {
        if( !empty($aJs) ) {
            if( is_array($aJs) ) {
                foreach( $aJs as $sJs ) {
                    if( isset($sJs['bUrlExterne']) && $sJs['bUrlExterne'] ) {
                        array_push(self::$fli_js, $sJs['fichier']);
                    } else {
                        array_push(self::$fli_js, self::$cheminAccesAbsolu . 'modules/' . $sNomModule . '/js/' . $sJs['fichier']);
                    }
                }
            }
        }
    }

    /**
     * @return mixed
     */
    public static function get_fli_css()
    {
        return self::$fli_css;
    }

    /**
     * @param mixed $fli_css
     */
    public static function set_fli_css($fli_css)
    {
        self::$fli_css = $fli_css;
    }

    /**
     * @param $css
     */
    public static function add_fli_css($aCss, $sNomModule)
    {
        if( !empty($aCss) ) {
            if( is_array($aCss) ) {
                foreach( $aCss as $sCss ) {
                    if( isset($sCss['bUrlExterne']) && $sCss['bUrlExterne'] ) {
                        array_push(self::$fli_css, $sCss['fichier']);
                    } else {
                        array_push(self::$fli_css, self::$cheminAccesAbsolu . 'modules/' . $sNomModule . '/css/' . $sCss['fichier']);
                    }
                }
            }
        }
    }


    /**
     * @return mixed
     */
    public static function get_fli_meta()
    {
        return self::$fli_meta;
    }

    /**
     * @param mixed $fli_meta
     */
    public static function set_fli_meta($fli_meta)
    {
        self::$fli_meta = $fli_meta;
    }

    /**
     * @param $meta
     */
    public static function add_fli_meta($sCle, $sValeur)
    {
        self::$fli_meta[$sCle] = $sValeur;
    }

    /**
     * @return mixed
     */
    public static function get_nom_projet()
    {
        return self::$nomProjet;
    }

    /**
     * @param mixed $sNomProjet
     */
    public static function set_nom_projet($nomProjet)
    {
        self::$nomProjet = $nomProjet;
    }

    /**
     * @return mixed
     */
    public static function get_nom_cookie()
    {
        return self::$nomCookie;
    }

    /**
     * @param mixed $nomCookie
     */
    public static function set_nom_cookie($nomCookie)
    {
        self::$nomCookie = $nomCookie;
    }

    /**
     * @return array
     */
    public static function get_aData_entier()
    {
        return self::$aData;
    }

    /**
     * @return array
     */
    public static function get_aData($sCle)
    {
        return self::$aData[$sCle];
    }

    /**
     * @param array $aData
     */
    public static function set_aData_entier($aData)
    {
        self::$aData = $aData;
    }

    /**
     * @param array $aData
     */
    public static function set_aData($sCle, $sValeur)
    {
        self::$aData[$sCle] = $sValeur;
    }

    /**
     * @return string
     */
    public static function get_fli_module()
    {
        return self::$fli_module;
    }

    /**
     * @param string $fli_module
     */
    public static function set_fli_module($fli_module)
    {
        self::$fli_module = $fli_module;
    }

    /**
     * @return string
     */
    public static function get_fli_fonction()
    {
        return self::$fli_fonction;
    }

    /**
     * @param string $fli_fonction
     */
    public static function set_fli_fonction($fli_fonction)
    {
        self::$fli_fonction = $fli_fonction;
    }

    /**
     * @return array
     */
    public static function get_debug()
    {
        return self::$aDebug;
    }

    /**
     * @param array $aDebug
     */
    public static function set_debug($sKey, $sValue, $iImportance = 1000)
    {
        self::$aDebug[] = array( $sKey => $sValue, 'importance' => $iImportance );
    }

    /**
     * @return boolean
     */
    public static function get_bDebug()
    {
        return self::$bDebug;
    }

    /**
     * @param boolean $bDebug
     */
    public static function set_bDebug($bDebug)
    {
        if( !self::$bDebug && $bDebug ) {
            self::$bDebug = $bDebug;
        }
    }

    /**
     * @return string
     */
    public static function get_tpl_sortie()
    {
        return self::$sTplSortie;
    }

    /**
     * @param string $sTplSortie
     */
    public static function set_tpl_sortie($sTplSortie)
    {
        self::$sTplSortie = $sTplSortie;
    }

    /**
     * @return string
     */
    public static function get_tpl_debug()
    {
        return self::$sTplDebug;
    }

    /**
     * @param string $sTplDebug
     */
    public static function set_tpl_debug($sTplDebug)
    {
        self::$sTplDebug = $sTplDebug;
    }


    /**
     * @return string
     */
    public static function get_fli_supp()
    {
        return self::$fli_supp;
    }

    /**
     * @param string $fli_supp
     */
    public static function set_fli_supp($fli_supp)
    {
        self::$fli_supp = $fli_supp;
    }

    /**
     * @return string
     */
    public static function get_fli_actif()
    {
        return self::$fli_actif;
    }

    /**
     * @param string $fli_actif
     */
    public static function set_fli_actif($fli_actif)
    {
        self::$fli_actif = $fli_actif;
    }
    
}