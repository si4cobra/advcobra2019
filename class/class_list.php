<?php

class class_list
{
    /**
     * @var array aElement Tableau contenant les éléments à afficher par la liste, le formulaire, la recherche ou la fiche
     */
    protected $aElement = array();
    /**
     * @var bactiv order filtre pere enfant
     */
    protected $aActivorderfiltreperefils;

    /**
     * @var array aEnteteListe Tableau d'entêtes de la liste
     */
    protected $aEnteteListe;
    /**
     * @var array aListe Tableau contenant les données de la liste
     */
    protected $aListe;
    /**
     * @var array aListeFils Tableau contenant les données de la liste fils
     */
    protected $aListeFils = array();
    /**
     * @var array aListeFils Tableau contenant les données de la liste fils
     */
    protected $aMenuDeroulant = array();
    /**
     * @var array aParametreWizardListe Tableau contenant les paramètres du wizard
     */
    protected $aParametreWizardListe = array();
    /**
     * @var array aParamsNonSuivi Tableau de paramètres spécifiés dans le constructeur pour le wizard
     */
    protected $aParamsNonSuivi = array();
    /**
     * @var ajout des message de l'extenr erreur supp success
     */
    protected $aTabResultExterne = array();

    /**
     * @var $aTableau des champ selecte pour ne pas doubler les champs
     */
    protected $aTabChampSelect = array();

    /**
     * @var $aTableau des champ jointure pour  pour ne pas doubler les champs
     */
    protected $aTabChampJointure = array();

    /**
     * @var array aSelectSqlSuppl Permet de rajouter des champs dans le SELECT de la liste
     */
    protected $aSelectSqlSuppl = array();
    /**
     * @var array aTabException Tableau qui autorisent des fonctions qui ne sont pas en base de données
     */
    protected $aTabException = array( 'install', 'fli_redirect_erreur', 'renvoi_menu' );
    /**
     * @var array $aTableauParam Tableau contenant le variable à propager partout dans url (keys, value)
     */
    protected $aTableauParam=array();

    /**
     * @var array $aTableauVariableDynamique Tableau contenant le variable à propager dans les titres si on veut les mettre en dur
     */
    protected $aTableauVariableDynamique=array();
    /**
     * @var array $aTableauFunctionSupp Tableau contenant des menu supplémentaire à ajouter dans le menu deroulant (value,url)
     */
    protected $aTableauFunctionSupp=array();
    /**
     * @var array aTabResultInsert Tableau contenant le résultat après l'INSERT (ID et result étant les clés du tableau)
     */
    protected $aTabResultInsert;
    /**
     * @var array aTabResultSupp Tableau contenant le résultat après la suppression (ID et result étant les clés du tableau)
     */
    protected $aTabResultSupp;
    /**
     * @var array aTabResultUpdate Tableau contenant le résultat après l'UPDATE (ID et result étant les clés du tableau)
     */
    protected $aTabResultUpdate;
    /**
     * @var array $aTabFiltreColHtmlListe permet de mettre en place un habillage spécifique edans une collone de la list ene focnction de cles
     */
    protected $aTabFiltreColHtmlListe;

    /**
     * @var array aTabResultUpdate Tableau contenant des message à ajouter dans le resull tout c'est bien passé
     */
    protected $aTabResultMessageSuccessSup=array();
    /**
     * @var array aTabResultUpdate Tableau contenant des message à ajouter dans le resull tout c'est une erreur
     */
    protected $aTabResultMessageErrosSup=array();
    /**
     * @var array aVariable Tableau clé/valeur dynamique
     */
    private static $aVariable = array();
    /**
     * @var bool bActiveFormSelect Permet de transformer la liste en formulaire avec un bouton 'valider' en bas de la liste
     */
    protected $bActiveFormSelect = false;
    /**
     * @var bool bSansActionSQL Permet d'eviter les enregistrements et les updates
     */
    protected $bSansActionSQL = true;
    /**
     * @var bool bAffAnnuler Permet d'afficher le bouton 'Annuler' dans le formulaire
     */
    protected $bAffAnnuler = true;
    /**
     * @var bool bAffDebug Permet d'afficher la fenêtre de débuggage
     */
    protected $bAffDebug = false;

    /**
     * @var bool bAffFiche
     */
    protected $bAffFiche = false;

    /**
     * @var bool bAffListeQuandPasRecherche Permet de ne pas afficher la liste tant qu'il n'y a pas de recherche
     */
    protected $bAffListeQuandPasRecherche = true;
    /**
     * @var bool bPushvarurl Permet rajouter la suite de url dans toutes les lien
     */
    protected $bPushVarUrl = false;
    /**
     * @var bool $bAffdebugListe activation debuggae avec affiche de la liste renvoyé
     */
    protected $bAffdebugListe = false;

    /**
     * @var bool bAffMod Permet la modification
     */
    protected $bAffMod = false;
    /**
     * @var bool bAffDup Permet de dupliquer une ligne
     */
    protected $bAffDup = false;
    /**
     * @var bool bAffNombreResult Permet d'afficher le nombre de résultats renvoyés pour la liste
     */
    protected $bAffNombreResult = true;
    /**
     * @var bool bAffPrintBtn Afficher le bouton d'impression true ou false
     */
    protected $bAffPrintBtn = false;
    /**
     * @var bool bAffRecapLigne Permet d'affiche dans unenouvelle fenetre le recap de la ligne
     */
    protected $bAffRecapLigne = true;
    /**
     * @var bool $bUseDelete si ce champ est a true le delete sera totale on  utilise pas le supplogique
     */
    protected $bUseDelete = false;
    /**
    /**
     * @var bool bAffSupp Permet la suppression
     */
    protected $bAffSupp = false;
    /**
     * @var bool pour actiover la collone  action
     */
    protected $baffAction = false;
    /**
     * @var bool bAffTitre Permet d'afficher le titre de la liste
     */
    protected $bAffTitre = true;
    /**
     * @var bool bAffVisu Permet d'afficher la liste selon les droits
     */
    protected $bAffVisu = false;
    /**
     * @var bool bBtnRetour Permet d'afficher le bouton 'Retour' (basé sur un dir) de la liste
     */
    protected $bBtnRetour = false;
    /**
     * @var bool bCheckboxSelect Permet d'afficher des boutons checkbox (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
     */
    protected $bCheckboxSelect = false;
    /**
     * @var bool bCreateTable Permet de bypasser le run
     */
    protected $bBypassRun = false;
    /**
     * @var bool bCsv Permet d'afficher le bouton d'export en csv
     */
    protected $bCsv = false;

    /**
     * @var bool bPdf Permet d'afficher le bouton d'export en pdf
     */
    protected $bPdf = false;
    /**
     * @var bool bDebugRequete Permet d'afficher les requêtes dans le form ou le liste
     */
    protected $bDebugRequete = false;
    /**
     * @var bool bEstModuleAppele Permet de savoir dans la classe_list s'il s'agit du module appelé dans l'url ou non (true = module appelé dans l'url)
     */
    protected $bEstModuleAppele = false;
    /**
     * @var bool bFormPopup Permet d'afficher le formulaire dans une modal
     */
    protected $bFormPopup = false;
    /**
     * @var bool bLabelCreationElem Permet d'afficher le bouton d'ajout
     */
    protected $bLabelCreationElem = true;
    /**
     * @var bool bLigneCliquable Permet de rendre les lignes de la liste cliquables
     */
    protected $bLigneCliquable = false;
    /**
     * @var bool bPagination Permet l'affichage de la pagination
     */
    protected $bPagination = false;
    /**
     * @var bool $bByPassConnect  saut tout les contrôle
     */
    protected $bByPassConnect = false;

    /**
     * @var bool $bKeyMultiple  si cette fonction travail sur un key multiple
     */
    protected $bKeyMultiple = false;
    /**
     * @var bool bRadioSelect Permet d'afficher des boutons radio (contenant la valeur de la clé primaire de chaque ligne dans l'attribut 'value') lorsque la liste est transformée en formulaire
     */
    protected $bRadioSelect = false;
    /**
     * @var bool bRetourSpecifique Permet d'afficher le bouton 'Retour' de la liste avec une url spécifique ($sRetourElemUrl)
     */
    protected $bRetourSpecifique = false;


    /**
     * @var bool bSuperAdmin Indique si l'utilisateur fait parti du groupe SuperAdmin
     */
    protected $bSuperAdmin = false;
    /**
     * @var bool bTraiteConnexion Permet au module d'être visible sans que l'utilisateur soit connecté (si la variable vaut false)
     */
    protected $bTraiteConnexion = true;
    /**
     * @var int iId Identifiant relatif à l'élément correspondant
     */
    protected $iId;
    /**
     * @var int iIdCustom Identifiant relatif au module
     */
    protected $iIdCustom = true;
    /**
     * @var int iIdtemp quand guid existe Identifiant relatif à l'élément correspondant
     */
    protected $iIdtmp;
    /**
     * @var int iNombrepage Nombre de lignes désirées par page
     */
    protected $iNombrePage = 50;
    /**
     * @var array itemBoutonsForm Tableau contenant les boutons valider, annuler, suivant, précéent du formulaire
     */
    protected $itemBoutonsForm = array();
    /**
     * @var array itemBoutonsListe Tableau contenant les boutons valider, annuler, suivant, précéent du wizard
     */
    protected $itemBoutonsListe = array();
    /**
     * @var object objClassGenerique Instance du modèle permettant la connexion à la base de données
     */
    protected $objClassGenerique;
    /**
     * @var object objSmarty L'objet Smarty, du/des module(s) ou de l'index, passé en paramètre depuis la class_import lors de l'import d'un module
     */
    protected $objSmarty;
    /**
     * @var string sBaseUrl La base de l'url du framework
     */
    protected $sBaseUrl;
    /**
     * @var string sBaseUrlFils La base de l'url fils du framework
     */
    protected static $sBaseUrlFils = '';
    /**
     * @var string sCategorieListe La catégorie de la liste lors du wizard
     */
    protected $sCategorieListe = '';
    /**
     * @var string sChampGuid Nom du champ GUID de la table liée
     */
    protected $sChampGuid = '';
    /**
     * @var string sChampId Nom du champ de la clé primaire de la table liée
     */
    protected $sChampId = '';
    /**
     * @var string sChampId Nom du champ de la clé primaire temporairre quand le guid existe de la table liée
    */
    protected $sChampIdTmp = '';
    /**
     * @var string sChampId Nom du champ de la clé primaire temporairre quand le guid existe de la table liée
     */
    protected $sEtatForm = '';
    /**
     * @var string sChampSupplogique Nom du champ supplogique dans la table liée
     */
    protected $sChampSupplogique = '';
    /*
     * Champ pour le calcul du filtre order filtre champ fils pere
     */
    protected $sChampFiltreOrderperefils='';
    /**
     * @var valeur off  sChampSupplogique Nom du champ supplogique dans la table liée
     */
    protected $sValOFFSupplogique = 'Y';
    /**
     * @var valeur off  sChampSupplogique Nom du champ supplogique dans la table liée
     */
    protected $sValONSupplogique = 'N';
    /**
     * @var string sDebugRequeteInsertUpdate La requête INSERT/UPDATE de création ou de modification d'un élément à afficher en debug
     */
    protected $sDebugRequeteInsertUpdate = '';
    /**
     * @var string sDebugRequeteSelect La requête SELECT globale à afficher en debug
     */
    protected $sDebugRequeteSelect = '';
    /**
     * @var string sDirRetour Dir spécifié pour le bouton 'Retour' (basé sur un dir) sur la liste
     */
    protected $sDirRetour = '';
    /**
     * @var string sDirpagination Le dir pour la pagination quand l'url est spécifique
     */
    protected $sDirpagination = '';
    /**
     * @var string sFiltreChampFils Le champ sur lequel on veut filtrer le fils dans la liste
     */
    protected static $sFiltreChampFils = '';
    /**
     * @var string sFiltreliste Permet d'ajouter des filtres dans la requête SQL pour l'affichage de la liste (Doit commencer par : ' AND'
     */
    protected $sFiltreliste = '';
    /**
     * @var string sFiltrelisteOrderBy Permet de modifier l'ORDER BY original de la requête
     */
    protected $sFiltrelisteOrderBy = '';
    /**
     * @var string sFiltrelisteOrderBy Permet de modifier l'ORDER BY original de la requête
     */
    protected $sFiltrelisteOrderByPdf = '';
    /**
     * @var string sFiltrelisteOrderBy Permet de modifier l'ORDER BY original de la requête
     */
    protected $sFiltrelisteOrderbyCsv = '';
    /*
    * @var string sFonction_genate_pdf Tpl la fonction qui genere le pdf
    */
    protected $sFonctionGeneratePdf = 'generate_pdf';
    /**
    /**
     * @var string sFormulaireTpl Tpl affiché lorsque get['action']==form
     */
    protected $sFormulaireTpl = 'formulaire.tpl';
    /**
     * @var string sJointureSup  permet de rajouter des jointure supplémnentaire
     */
    protected $sJointureSup="";

    /**
     * @var string sLabelCreationElem Texte du bouton d'ajout de la liste s'il est activé (A activer avec $bLabelCreationElem)
     */

    protected $sLabelCreationElem = '';
    /**
     * @var string sLabelFileRetourElem Texte du bouton 'Retour' de la liste s'il est activé (A activer avec $bRetourSpecifique)
     */
    protected $sLabelFileRetourElem = '';
    /**
     * @var string sLabelNbrLigne Texte à coté du nombre de lignes
     */
    protected $sLabelNbrLigne = ' Résultats(s) ';
    /**
     * @var string sLabelRecherche Titre de la section recherche
     */
    protected $sLabelRecherche = '';
    /**
     * @var string sLabelRetourListe Texte du bouton 'Retour' sur la liste
     */
    protected $sLabelRetourListe = '';
    /**
     * @var string sLienLigne Lien sur la ligne si $bLigneCliquable vaut true
     */
    protected $sLienLigne = '';
    /**
     * @var string sListeFilsTpl Tpl affiché par défaut pour la liste_fils
     */
    protected $sListeFilsTpl = 'liste_fils.tpl';

    /**
     * @var string sListeFitreNopDuplicate renvoi la liste des champs qu'on veut exclure de la duplication
     */
    protected $sListeFitreNopDuplicate = '';
    /**
     * @var string sListeTpl Tpl affiché par défaut pour la liste (la liste est ce qui est affiché par défaut)
     */
    protected $sListeTpl = 'liste.tpl';

    /**
     * @var string sMenuIntitule intitulé du menu
     */
    protected $sMenuIntitule = '';
    /**
     * @var string sMenuVisibile visible ou pas dans le menu
     */
    protected $sMenuVisibile = true;
    /**
     * @var string sMenuVisibile visible ou pas dans le menu
     */
    protected $sMenuTarget = '';

    /**
     * @var string sMessageCreationSuccesForm Message affiché lorsque l'envoi du formulaire (Ajout) s'est déroulé avec succès
     */
    protected $sMessageCreationSuccesForm = '';
    /**
     * @var string sMessageDuplicationSucces Message affiché lorsquela duplication de la ligne s'est déroulé avec succès
     */
    protected $sMessageDuplicationSucces = '';
    /**
     * @var string sMessageDuplicationError Message affiché lorsque la duplication de la ligne s'est déroulé avec uen erreur
     */
    protected $sMessageDuplicationError = '';
    /**
     * @var string sMessageErreurAjoutChamp Message d'erreur relatif au champ à afficher
     */
    protected $sMessageErreurAjoutChamp;
    /**
     * @var string sMessageErreurForm Message en cas d'erreur lors de l'envoi du formulaire
     */
    protected $sMessageErreurForm = '';
    /**
     * @var string sMessageModificationSuccesForm Message affiché lorsque l'envoi du formulaire (Modification) s'est déroulé avec succès
     */
    protected $sMessageModificationSuccesForm = '';

    /**
     * @var string $sMessageModificationSuccesSupp Message affiché  dans la liste
     */
    protected $sMessageModificationSuccesSupp = '';
    /**
     * @var string $sMessageModificationSuccesSupp Message affiché  dans la liste
     */
    protected $sMessageErrorsSupp = '';

    /**
     * @var string sMessageSuccesForm Message à afficher en cas de succès lors de l'envoi du formulaire
     */
    protected $sMessageSuccesForm;
    /**
     * @var string sMessageSupprElem Message à afficher en cas de succès lors de la suppression
     */
    protected $sMessageSupprElem = '';
    /**
     * @var string sNomTable Nom de la table liée
     */
    protected $sNomTable = '';
    /**
     * @var string sParametreFiltreListe Les paramètres à passer lorsqu'on veut filtrer la liste
     */
    protected $sParametreFiltreListe;
    /**
     * @var string sParametreInterModule Les paramètres à passer entre les modules
     */
    protected $sParametreInterModule;
    /**
     * @var string sParametreRecherche Les paramètres GET lors de la recherche
     */
    protected $sParametreRecherche;
    /**
     * @var string sParametreRetourAnnulationFormExterne Les paramètres utilisés pour le retour apres l'annulation du formulaire
     */
    protected $sParametreRetourAnnulationFormExterne;
    /**
     * @var string sParametreRetourListe Les paramètres pour le retour vers la liste
     */
    protected $sParametreRetourListe;
    /**
     * @var string sParametreRetourListeTableau Les paramètres pour le retour vers la liste sous forme de tableau
     */
    protected $sParametreRetourListeTableau;
    /**
     * @var string sParametreRetourValidationFormExterne Les paramètre utilisés pour le retour après la validation du formulaire
     */
    protected $sParametreRetourValidationFormExterne;
    /**
     * @var string sParametreWizardListe Les paramètres de la liste du wizard
     */
    protected $sParametreWizardListe = '';
    protected $sParametrebWizard = false;
    protected $sParametreWizardNomVariablePrecedent = '';
    protected $sParametreWizardPrecedent = '';
    protected $sParametreWizardSuivant = '';
    protected $sParametreWizardNomVariableSuivant = '';
    protected $sParametrebWizardAnnule = false;
    protected $sParametrebWizardAnnuleFonction = '';
    /**
     * @var string sPrefixeDb Le prefixe des tables
     */
    protected $sPrefixeDb = '';
    /**
     * @var string sRetourAnnulationForm Permet de définir où l'on doit revenir après le formulaire
     */
    protected $sRetourAnnulationForm = 'liste';
    /**
     * @var string sRetourAnnulationFormExterne Permet de spécifier l'url de l'annulation
     */
    protected $sRetourAnnulationFormExterne = '';
    /**
     * @var string sRetourElemUrl URL de retour spécifique pour le bouton 'Retour' de la liste (A activer avec $bRetourSpecifique)
     */
    protected $sRetourElemUrl = '';
    /**
     * @var string sRetourListe Paramètre de la méthode run, squellette de l'url de retour à partir de la liste
     */
    protected $sRetourListe = null;
    /**
     * @var string sRetourSuppressionFormExterne Permet de spécifier l'url de retour après la suppression
     */
    protected $sRetourSuppressionFormExterne = '';
    /**
     * @var string sRetourValidationForm Permet de spécifier où l'on doit revenir après la validation du formulaire
     */
    protected $sRetourValidationForm = 'liste';
    /**
     * @var string sRetourValidationFormExterne Permet de spécifier où l'on doit revenir après la validation du formulaire externe
     */
    protected $sRetourValidationFormExterne = '';
    /**
     * @var string sScriptJavaSCriptInsert Script executé après un INSERT
     */
    protected $sScriptJavaSCriptInsert = '';
    /**
     * @var string sScriptJavaSCriptUpdate Script executé après un UPDATE
     */
    protected $sScriptJavaSCriptUpdate = '';
    /**
     * @var string sSuiteRequeteInsert Permet de rajouter du SQL à la suite de la requête INSERT
     */
    protected $sSuiteRequeteInsert = '';
    /**
     * @var string sSuiteRequeteUpdate Permet de rajouter du SQL à la suite de la requête UPDATE
     */
    protected $sSuiteRequeteUpdate = '';
    /**
     * @var string sTableFils Permet de spécifier la table de la liste fils
     */
    protected static $sTableFils = '';
    /**
     * @var string sTitreCreationForm Titre de la page lorsque l'on se trouve sur le formulaire en Ajout
     */
    protected $sTitreCreationForm = '';
    /**
     * @var string sTitreForm Titre du formulaire
     */
    protected $sTitreForm = '';
    /**
     * @var string sTitreListe Titre de la liste
     */
    protected $sTitreListe = '';
    /**
     * @var string sTitreModificationForm Titre de la page lorsque l'on se trouve sur le formulaire en Modification
     */
    protected $sTitreModificationForm = '';
    /**
     * @var string sTplSortie Permet de spécifier le template principal de sortie (par défaut : principal.tpl pour html et principal_empty.tpl pour json)
     */
    protected $sTplSortie = '';
    /**
     * @var string sTypeFichier Le type de fichier attendu
     */
    protected $sTypeFichier = '';
    /**
     * @var string sTypeSortie Permet de spécifier le type de sortie géré par le framework (html,json)
     */
    protected $sTypeSortie = 'html';
    /**
     * @var string sUrlRetourAnnulationFormExterne L'url de retour du formulaire dans le cas d'un retour externe après l'annulation du formulaire
     */
    protected $sUrlRetourAnnulationFormExterne;
    /**
     * @var string sUrlRetourConnexion L'url de retour  dans le cas l'utilisateur n'est pas connecter
     */
    protected $sUrlRetourConnexion;
    /**
     * @var string sUrlRetourListe L'url de retour vers la liste
     */
    protected $sUrlRetourListe;
    /**
     * @var string sUrlRetourSpecifique Permet de spécifier l'url de retour si elle est spécifique
     */
    protected $sUrlRetourSpecifique = '';
    /**
     * @var string sUrlRetourSuppressionFormExterne L'url de retour du formulaire dans le cas d'un retour externe après la suppression
     */
    protected $sUrlRetourSuppressionFormExterne = '';
    /**
     * @var string sUrlRetourValidationFormExterne L'url de retour du formulaire dans le cas d'un retour externe après la validation du formulaire
     */
    protected $sUrlRetourValidationFormExterne;

    /**
     * class_list constructor.
     * @param string $objSmarty
     */
    function __construct($objSmarty = '',$sModule='')
    {
        $this->objSmarty = $objSmarty;
        //$sModule = class_fli::get_fli_module();
        //On regarde ici si le fichier du modele spécifique au module existe
        if(file_exists("modules/".$sModule."/modeles/mod_".$sModule.".php")) {
            $objImport = new class_import();
            $NomClassMod = "mod_".$sModule;
            $objImport->import_modele($sModule, 'mod_' . $sModule);
            $this->objClassGenerique = new $NomClassMod();
        }else {
            $this->objClassGenerique = new mod_form_list();
        }


        $this->sPrefixeDb = class_fli::get_prefixe();
        
        //echo"<pre>";print_r($_GET);echo"</pre>";

        $aTypeSortieAccepte = array( 'html', 'json','android','ios','csv','pdf' );
        if( isset($_GET['sTypeSortie']) && !empty($_GET['sTypeSortie']) && in_array($_GET['sTypeSortie'], $aTypeSortieAccepte) ) {
            $this->sTypeSortie = $_GET['sTypeSortie'];
        } else {
            $this->sTypeSortie = 'html';
        }

       // echo "type sortie ".$this->sTypeSortie." le get => ".$_GET['sTypeSortie']." "; ;

        $this->bSuperAdmin = class_fli::get_super_admin();

    }

    /**
     * Methode d'un champ dans les inputs
     * @return object Tableau cles valeurs
     */
    public function ajout_champ($objElement)
    {

        $aTableauLeschamp = $this->renvoi_liste_cles_champ();
        $aTableauCles = array_keys($aTableauLeschamp);
        $aTableauclesinput = array_keys($objElement);

        foreach( $aTableauCles as $sTableauCles ) {
            if( !in_array($sTableauCles, $aTableauclesinput) ) {
                $objElement[$sTableauCles] = '';
            }
        }

        $bPasse = false;

        if( in_array($objElement['type_champ'], array( 'select', 'selectmultiple' )) and empty($objElement['lesitem'])
            && ($objElement['table_item'] == '' || $objElement['id_table_item'] == ''
                || $objElement['affichage_table_item'] == '' || $objElement['supplogique_table_item'] == ''
                || $objElement['type_table_join'] == '')
        ) {

            $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Sur l\'ajout du champ  de type ' . $objElement['type_champ'] . ' label du <b>' . $objElement['text_label'] . '</b> manque cles<br>';
            $this->sMessageErreurAjoutChamp .= '-Cles tableau obligatoire pour un type select maquante:<br>';

            if( $objElement['table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles table_item<br>';
            }
            if( $objElement['id_table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles manquante <b>id_table_item</b><br>';
            }
            if( $objElement['affichage_table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '- cles manquante <b>affichage_table_item</b><br>';
            }
            if( $objElement['supplogique_table_item'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles manquante <b>supplogique_table_item</b><br>';
            }
            if( $objElement['type_table_join'] == '' ) {
                $this->sMessageErreurAjoutChamp .= '---- cles  manquante <b>type_table_join</b><br>';

            }

            $bPasse = true;
        }


        if( $objElement['alias_champ'] == '' && ($objElement['type_champ'] == 'checkbox' or $objElement['type_champ'] == 'date' or $objElement['type_champ'] == 'datetime'  ) ) {
            if( !$bPasse ) {
                $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Sur l\'ajout du champ  de type ' . $objElement['type_champ'] . ' label du <b>' . $objElement['text_label'] . '</b> manque cles<br>';
            }
            $this->sMessageErreurAjoutChamp .= '---- cles  manquante  <b>alias_champ</b><br>';

            echo $this->sMessageErreurAjoutChamp;
            exit();
        }

        if($objElement['type_champ'] == 'selectdist'){

           // $berreurstop=false;

            if(count($objElement['table_type_join'])!=count($objElement['table_lien'])){
                $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Vous n\'avez pas le même nombre de valeur dans le tableau  ' . $objElement['type_champ'] . ' du label  <b>' . $objElement['text_label'] . '</b> <br>';
                echo $this->sMessageErreurAjoutChamp;
                exit();
            }

            if(count($objElement['table_type_join'])!=count($objElement['supplogique_table_lien'])){
                $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Vous n\'avez pas le même nombre de valeur dans le tableau  ' . $objElement['type_champ'] . ' label  <b>' . $objElement['text_label'] . '</b> <br>';
                echo $this->sMessageErreurAjoutChamp;
                exit();
            }


            if(count($objElement['table_type_join'])!=count($objElement['id_table_lien'])){
                $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Vous n\'avez pas le même nombre de valeur dans le tableau  ' . $objElement['type_champ'] . ' label  <b>' . $objElement['text_label'] . '</b> <br>';
                echo $this->sMessageErreurAjoutChamp;
                exit();
            }

            if(count($objElement['table_type_join'])!=count($objElement['id_item_table_lien'])){
                $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Vous n\'avez pas le même nombre de valeur dans le tableau  ' . $objElement['type_champ'] . ' label  <b>' . $objElement['text_label'] . '</b> <br>';
                echo $this->sMessageErreurAjoutChamp;
                exit();
            }



        }

        if( $objElement['aff_recherche'] == 'ok' && $objElement['type_recherche'] == '' ) {
            if( !$bPasse ) {
                $this->sMessageErreurAjoutChamp .= '-------------------------------------<br>Erreur: Sur l\'ajout du champ  de type ' . $objElement['type_champ'] . ' label du <b>' . $objElement['text_label'] . '</b> manque cles<br>';
            }
            $this->sMessageErreurAjoutChamp .= '---- cles  manquante <b>type_recherche</b><br>';
        }

        if( $objElement['date_format'] == '' ) {
            $objElement['date_format'] = "%Y-%m-%d";
        }

        if( $objElement['datetime_format'] == '' ) {
            $objElement['datetime_format'] = "%Y-%m-%d %H:%i:%s";
        }

        $this->aElement[] = $objElement;
        
        //echo"<pre>";print_r($objElement);echo"</pre>";
        //echo"<pre>";print_r($this->aElement);echo"</pre>";
        //echo" remplissage";
        //echo" remplissage";

        return $this;
    }

    /**
     * @param $keys
     * @param string $valeur
     */
    public function ajout_param($keys, $valeur=""){

        if($valeur==""){
            if(isset($_GET[$keys]))
                $valeur = $_GET[$keys];
        }

        $this->aTableauParam[]=array("keys"=>$keys,"value"=>$valeur);

    }

    /**
     * @return mixed fonction qui renvoi la liste des cles utilisable pour les champs
     */
    public function renvoi_liste_cles_champ()
    {
        $aTabRetour['type_champ'] = "Type de Champ";
        $aTabRetour['is_keymultiple'] = "ce champ fait partie de la cles multiple vide|ok";
        $aTabRetour['mapping_champ'] = "le nom du champ dans la table selectionnee";
        $aTabRetour['alias_champ'] = "alias du champ dans la table selectionnee";
        $aTabRetour['nom_variable'] = "la valeur de l attribut \"name\" dans le formulaire";
        $aTabRetour['text_label'] = "l intitule du champ utilise dans le formulaire, dans l entete de la liste, ...";
        $aTabRetour['ctrl_champ'] = "faut il controler le champ 'ok'|'warn'|''";
        $aTabRetour['valeur_variable'] = "la valeur par defaut dans le formulaire lors de la creation";
        $aTabRetour['aff_liste'] = "faut il afficher le champ dans la liste 'ok'|''";
        $aTabRetour['aff_pdf'] = "faut il afficher le champ dans le pdf 'ok'|''";
        $aTabRetour['aff_form'] = "faut il afficher le champ dans le formulaire 'ok'|''";
        $aTabRetour['aff_filtre'] = "faut il afficher le champ comme filtre 'ok'|''";
        $aTabRetour['aff_recherche'] = "faut il afficher le champ dans la recherche 'ok'|''";
        $aTabRetour['text_recherche'] = "permet de faire une recherche ar text sur une liste deroulante 'ok'|''";
        $aTabRetour['type_recherche'] = "exemple like \'<champ>%\'type de la recherche ";
        $aTabRetour['aff_fiche'] = "faut il afficher le champ dans la fiche 'ok'|''";
        $aTabRetour['activ_totaux'] = "Active le calcul des totaux ok|nop'";
        $aTabRetour['size_champ'] = "taille max du champ";
        $aTabRetour['style'] = "ajout du style sur le champ";
        $aTabRetour['style_pdf'] = "ajout du style dans le pdf";
        $aTabRetour['tableau_attribut'] = "ajout d attributs sur le champ";
        $aTabRetour['fonction_javascript'] = "ajout du javascript sur le champ (tableau clé=>valeur)";
        $aTabRetour['mess_erreur'] = "message d erreur lorsque le controle n est pas valide";
        $aTabRetour['traite_sql'] = "faut il traite le champ dans les divers requetes sql selection, insertion, modification";
        $aTabRetour['date_format'] = "le format de la date renvoyé ";
        $aTabRetour['date_conversion_enreg'] = "dans quelle type de conversion il faut mettre la date si eng on transformer dd/mm/YYYY en YYYY-mm-dd - nouvelle fonction frtoeng ";
        $aTabRetour['date_now_creation'] = "si la date doit etre pre-rempli a la creation";
        $aTabRetour['date_now_modification'] = "si la date doit etre mise a jour a chaque modification";
        $aTabRetour['table_item'] = "la table liee pour ce champ ";
        $aTabRetour['id_table_item'] = "le champ de la table liee qui sert de cle primaire";
        $aTabRetour['id_liaison'] = "cle primaire de la table qui lie les champs dans un rapport n=>n (checkbox ou select multiple)";
        $aTabRetour['actif_jointure'] = "cle permettant de ajouter la jointure dans la requete même s'il y a pas une recherche";
        $aTabRetour['affichage_table_item'] = "le champ de la table lien qui sert de label d affichage exemple mon_client as nomitem <bdd> si nous voulon un affichage plus pousser";
        $aTabRetour['supplogique_table_item'] = "Outil supplogique sur la table item";
        $aTabRetour['alias_champ_item'] = "alias du champ item dans la base la requete générale pour champ select et ou selectdist";
        $aTabRetour['orderby_table_item'] = "order by sur la table item";
        $aTabRetour['type_table_join'] = "Type de la jointure ";
        $aTabRetour['table_lien'] = "la table qui fait le lien entre la table du formulaire et la table lien";
        $aTabRetour['alias_table_item'] = "Alias du de la table dans la jointure ";
        $aTabRetour['choix_id_lien'] = "Choix des guid pour enregistrement dans la table de liaison Guid";
        $aTabRetour['item_table_champ_suplementaire'] = "Tableau des champ supplementaire a afficher et saisir dans une table de liaison";
        $aTabRetour['select_value_pere_base'] = "valeur de bas du pere dans un select avec hierachie";
        $aTabRetour['lesitem'] = "liste des items possibles choisies dans la table item avec pour cle l\identifiant de la table item et avec pour valeur la valeur que l\'on choisit";
        $aTabRetour['mapping_champ_label'] = "les possibles valeurs pour le type radio array('Y' => 'Oui', 'N' => 'Non')";
        $aTabRetour['table_type_join'] = "cles tableau des jointure dans un selectdist";
        $aTabRetour['table_lien'] = "la table qui fait le lien entre la table du formulaire et la table liee";
        $aTabRetour['id_table_lien'] = "le nom du champ dans la table lien qui fait reference a la cle primaire de la table du formulaire";
        $aTabRetour['id_item_table_lien'] = "le nom du champ dans la table lien qui fait reference a la cle primaire de la table liee";
        $aTabRetour['aff_item_literal'] = "Renvoi si c'est une date le champ en text exemple lundi 29 décembre pour un select";
        $aTabRetour['supplogique_table_lien'] = "supplogique entre les différente table de lien";
        $aTabRetour['table_item_aff_liste'] = " si a nop bloque l'affichage de la liste deroulante dans le formulaire";
        $aTabRetour['url'] = "url de retour pour un bouton du formulaire, bouton annuler par exemple";
        $aTabRetour['file_upload'] = "dossier de telechargement du fichier";
        $aTabRetour['activ_style_filte_liste'] = " value=ok Active la recherche d'un habillage spécifique sur le td en fonction de paramêtre passé dans le tableau aTabColList";
        $aTabRetour['file_visu'] = "url de visualisation du fichier";
        $aTabRetour['file_aff_modif_form'] = 'si le fichier actuel doit etre affiche lors de la modification dans le formulaire, valeur ex: ok, type de fichier supporte: png, jpeg, gif, swf';
        $aTabRetour['file_aff_modif_form_taille'] = 'taille du fichier actuel affiche lors de la modification dans le formulaire';
        $aTabRetour['file_aff_modif_form_couleur_fond'] = 'couleur de fond du fichier affiche lors de la modification dans le formulaire';
        $aTabRetour['file_aff_liste_taille'] = 'taille du fichier actuel affiche dans la liste';
        $aTabRetour['file_aff_liste_couleur_fond'] = 'couleur de fond du fichier affiche dans la liste';
        $aTabRetour['recherche_intervalle_date'] = 'si le champ doit etre en recherche par intervalle';
        $aTabRetour['recherche_intervalle_date_label'] = 'tableau contenant 2 valeurs, les labels pour les dates a intervalle dans la recherche';
        $aTabRetour['transfert_inter_module'] = 'transfert la valeur de la variable a l\'interieur du module, la valeur doit exister et etre presente dans l\'url';
        $aTabRetour['transfert_inter_module_filtre_suplementaire'] = 'si on veut rajouter un filtre supllementaire dans l\'intermodule';
        $aTabRetour['injection_code'] = 'injection de code dans les cellules de la liste';
        $aTabRetour['recherche_intervalle_date'] = 'si il y a une recherche par intervalle pour la date dans la recherche, ex: ok';
        $aTabRetour['recherche_intervalle_date_label'] = 'les labels des inputs sur l\'intervalle de date dans la recherche, ex: array(\'min\', \'max\');';
        $aTabRetour['wysiwyg'] = 'si le textarea est utilise en wysiwyg dans le formulaire';
        $aTabRetour['recherche_filtre_actif'] = 'considère la recherche comme non active meme quand le champ est rempli';
        $aTabRetour['recherche_filtre_par_champ'] = 'si le champ doit etre filtre par un autre champ dans le formulaire de recherche, inscrivez le nom_variable du champ en question';
        $aTabRetour['recherche_affiche_non_filtre_par_champ'] = 'si la liste des possibilites est generee lorsque le champ n\'est pas encore filtre par un autre champ, valeurs possibles:true ou false, valeur par defaut false';
        $aTabRetour['utilise_cles_mapping'] = ' si egale à ok dans un checkbox on recupere la valeur du champ maaping pour la jointure';
        //********************variable temporaire****************
        $aTabRetour['text_label_filtre'] = "dans le cas d'un champ pour remplir un select en bas";
        $aTabRetour['select_autocomplete'] = 'si l input est un select et que l on demande l auto completion sur la liste';
        $aTabRetour['tabfiltre_autocomplete'] = "tableaufiltre dans le requete autocompletion exemple tabfiltre[0]=champ|%<rech>%|like& tabfiltre[1]=champ|<rech>|=";
        $aTabRetour['id_valeur_select'] = "Valeur selectionnée pour un <select>";
        $aTabRetour['alias_table_lien'] = " On defini un alias de le la base de donnée dans le cas de base intermédiare";
        $aTabRetour['requete_supplementaire_select'] = " on rajoute des info supplementaire dans la requete select ou radio ou chekbox en affichage";
        $aTabRetour['requete_supplementaire_select_lien'] = " on rajoute des info supplementaire dans la requete sur le lien checkbox selectmultiple";
        $aTabRetour['aff_debug'] = "On affiche la requete debug";
        $aTabRetour['alias_transfer'] = "Cles disponible pour le type transfert_inter_module pour remplacer le filtre par une alis sp&cifique";
        $aTabRetour['activ_form_select'] = "Activation d'un champ sur input, select, checkbox, radio dans le tableau afffichage list";
        $aTabRetour['double_password'] = "Double champ password";
        $aTabRetour['length_password'] = 8;
        $aTabRetour['file_compress'] = "Active la compression pour les images jpeg,gif,png";
        $aTabRetour['file_taux_compress'] = "taux de compression 1-9 pour les images (jpeg,gif,png)  (100 = bonne qualité, 0 = très mauvaise qualité)";
        $aTabRetour['file_compress_min'] = "Active la compression pour les images jpeg,gif,png";
        $aTabRetour['file_taux_compress_min'] = "taux de compression 1-9 pour les images (jpeg,gif,png)  (100 = bonne qualité, 0 = très mauvaise qualité)";
        $aTabRetour['order_select'] = " orde dans le select";
        $aTabRetour['tags'] = "Activation de l'affichage sous forme de tags pour un checkbox";
        $aTabRetour['tags_ajax'] = "Activation de l'ajax pour les tags";
        $aTabRetour['tags_id'] = "id utilisé via ajax";
        $aTabRetour['tags_champ'] = "champ a afficher via ajax";
        $aTabRetour['tags_table'] = "table utilisé requete ajax";
        $aTabRetour['tags_champ_like'] = "Champ sur lequel le like est exécuté";
        $aTabRetour['tags_where'] = "Commencer avec AND absolument !";
        $aTabRetour['select_pere_fils'] = " Active une liste deroulante avec un decalage pere fils";
        $aTabRetour['Activdefaut'] = "On active le chemin de l'image par défaut a l'enregistremnt";
        $aTabRetour['filedefaut'] = "Chemin de l'image par défaut à l'enregistrement";
        $aTabRetour['type_encodage'] = "Type d'encodage du mot de passe (phpbb_hash,PASSWORD) par defaut: PASSWORD";
        $aTabRetour['datetime_format'] = "le format de la datetime renvoyé ";
        $aTabRetour['format_affiche_liste'] = "le format d'affichage dans la liste ";
        $aTabRetour['html_perso_form_th'] = "attribut perso html dans le <th> du formulaire.tpl";
        $aTabRetour['html_perso_form_td'] = "attribut perso html dans le <td > champ du formulaire.tpl";
        $aTabRetour['html_perso_th'] = "attribut perso html dans le <th> du liste.tpl";
        $aTabRetour['html_perso_td'] = "attribut perso html dans le <td> du liste.tpl";
        $aTabRetour['html_editable_td'] = "rend le td editable <td> du liste.tpl ok|''";
        $aTabRetour['html_editable_radio'] = "rajoute dans la liste un button on|off <td> du liste.tpl ok|''";
        $aTabRetour['html_editable_radio'] = "rend le td editable avec un chekbox oui|non <td> du liste.tpl ok|''";
        $aTabRetour['html_value_bouton_on_radio'] = "valeur a enregistrer quand on clique sur le bouton on";
        $aTabRetour['html_value_bouton_off_radio'] = "valeur a enregistrer quand on clique sur le bouton OFF";
        $aTabRetour['js_action_editable_radio'] = "Cles pour mettre la fonction que le clique vas appeller, par defaut s'il est vide c'est la fonction maj_radio dans le fichier fonctiob_adv.js";
        $aTabRetour['type_champ_sql'] = "le type de champ si on veut le spécifier";
        $aTabRetour['index_champ_sql'] = "l'index a appliquer sur ce champ";
        $aTabRetour['auto_champ_sql'] = "";
        $aTabRetour['valeur_champ_sql'] = "valeur du champ de choix si c'est un ";
        $aTabRetour['valeur_defaut_champ_sql'] = "valeur par defaut du champ";
        $aTabRetour['cles_champ_sql'] = "";
        $aTabRetour['titre_inter_module'] = " SI il est a ok il permet d'ecrire automatiquement le nom du filtre dans le menu haut";
        $aTabRetour['bbd_titre_inter_module'] = "La base de donné où on va chercher le titre";
        $aTabRetour['champfiltre_titre_inter_module'] = "Le champ qui permet le filtre dans la requete";
        $aTabRetour['champaffiche_titre_inter_module'] = "La valeur a afficher dans le titre";
        //Clés correspondant au menu
        $aTabRetour['nom_module_menu'] = "Le nom du module de l'élément du menu";
        $aTabRetour['route_menu'] = "";
        $aTabRetour['pere_route_menu'] = "";
        $aTabRetour['libelle_menu'] = "";
        $aTabRetour['ordre_menu'] = "";
        $aTabRetour['lien_menu'] = "";
        $aTabRetour['target_menu'] = "";
        $aTabRetour['valeur_forcer'] = "Permet de mettre une valeur de force fixe sur l'update ou l'ajout";
        $aTabRetour['supprimer_br'] = "Enleve le retour charito dans l'affichage";

        return $aTabRetour;
    }

    /**
     * @param string $sModule
     * @param string $sVersionTemplate
     */
    public function chargement($sModule = '', $sVersionTemplate = 'version1')
    {

        $sFliVersionTemplate = class_fli::get_fli_version_template();

        if( isset($this->sChampGuid) && !empty($this->sChampGuid) && !$this->bBypassRun ) {
            $this->sChampIdTmp =$this->sChampId;
            $this->sChampId = $this->sChampGuid;

        }

        if( (isset($this->sChampId) && !empty($this->sChampId)) or $this->bKeyMultiple ) {
            $this->bEstModuleAppele = true;
        }

        switch( $this->sTypeFichier ) {
            case 'csv':
                $this->sListeTpl = 'listeCsv.tpl';
                break;
            default:
                break;
        }

        $aDroits = class_fli::get_droits();

        //Gestion des filtres sur les accès de la fonction
        if($this->bTraiteConnexion) {
            $this->bLabelCreationElem = ($this->bLabelCreationElem) ? $aDroits['a'] : false;
            $this->bAffMod = ($this->bAffMod) ? $aDroits['m'] : false;
            $this->bAffSupp = ($this->bAffSupp) ? $aDroits['s'] : false;
            $this->bAffFiche = ($this->bAffFiche) ? $aDroits['v']:false;
        }
        $this->bAffVisu = ($this->bAffSupp || $this->bAffMod || $this->bLabelCreationElem || $this->bAffFiche || !$this->bTraiteConnexion)?true:false;
           //$this->bLabelCreationElem = ($this->bLabelCreationElem)?$aDroits['v']:false;

        class_fli::set_bDebug($this->bAffDebug);
        //$this->set_aVariable( 'fli_traiteConnexion', true);

        /*if($this->bByPassConnect)
            exit('sortie');
        */

        //echo"<pre>";print_r($_POST);echo"</pre>";
        //gestion du  système de tradcution
        if(isset($_POST['enreglangue']) and $_POST['enreglangue']=="ok"){

            $sFrlang="";
            $sEnglang="";
            $sEsplang="";
            $sAlllang="";
            $sActifprincipal="";
            $sBasedonne="";
            $champbasedonne="";
            if(isset($_POST['lg_fr'])){
                $sFrlang = $_POST['lg_fr'];
            }

            if(isset($_POST['lg_eng'])){
                $sEnglang = $_POST['lg_eng'];
            }

            if(isset($_POST['lg_esp'])){
                $sEsplang = $_POST['lg_esp'];
            }

            if(isset($_POST['lg_all'])){
                $sAlllang = $_POST['lg_all'];
            }

            if(isset($_POST['basedonne'])){
                $sBasedonne = $_POST['basedonne'];
            }

            if(isset($_POST['champbasedonne'])){
                $champbasedonne = $_POST['champbasedonne'];
            }

            if(isset($_POST['actifprinicpal'])){
                $sActifprincipal = $_POST['actifprinicpal'];
            }


            $sRequete_info_traduction="Insert ".$this->sPrefixeDb."langage set fr_langage='".addslashes($sFrlang)."',eng_langage='".addslashes($sEnglang)."',
            esp_langage='".addslashes($sEsplang)."',all_langage='".addslashes($sAlllang)."',identifiant_langage='".addslashes($_POST['identifianlg'])."'
            ,link_langage='".addslashes($_POST['idlinklg'])."',champbase_langage='".addslashes($champbasedonne)."',base_langage='".addslashes($sBasedonne)."',
            principal_langage='".$sActifprincipal."'
             ON DUPLICATE KEY update fr_langage='".addslashes($sFrlang)."',eng_langage='".addslashes($sEnglang)."',
            esp_langage='".addslashes($sEsplang)."',all_langage='".addslashes($sAlllang)."',champbase_langage='".addslashes($champbasedonne)."',base_langage='".addslashes($sBasedonne)."',
            principal_langage='".$sActifprincipal."'";

            if($this->bEstModuleAppele) {
               // echo"<pre>";print_r($_POST);echo"</pre>";
                //echo $sRequete_info_traduction . "<br>";
                $this->aTabResultExterne['bsucess']=true;
                $this->aTabResultExterne['sMessageSucess']="Traduction bien enrgistrée";
                //mettre ce texte en texte par défaut de ce champ pour cette table
                if($sActifprincipal=="Y"){
                    $sRequete_raz ="UPDATE ".$this->sPrefixeDb."langage set principal_langage='N' WHERE champbase_langage='".$champbasedonne."' 
                    and base_langage='".$sBasedonne."' and identifiant_langage<>'".$_POST['identifianlg']."'";

                    //echo $sRequete_raz;
                    $this->objClassGenerique->execute_requete($sRequete_info_traduction);
                }
                
                //echo"<pre>";print_r($this->aTabResultExterne);echo"</pre>";
                $this->objClassGenerique->execute_requete($sRequete_info_traduction);
            }

       }

        if( $this->bTraiteConnexion && !class_fli::get_fli_est_connecte() && $this->bEstModuleAppele && !$this->bByPassConnect) {
            if( (isset($_GET['fli_module']) && $_GET['fli_module'] != class_fli::get_module_login()) || !isset($_GET['fli_module']) ) {
                //si nous avons une  url de redirecttion quand on n'est pas connecté
                /*if(class_fli::get_fli_est_connecte())
                    echo "connect lui il est bon ";
                echo "Ca se passe ici";
                exit();
                */
                if(!empty($this->sUrlRetourConnexion)){
                    header('Location: '.class_fli::get_chemin_acces_absolu() . '' .$this->sUrlRetourConnexion);
                }else{
                    header('Location: ' . class_fli::get_chemin_acces_absolu() . '' . class_fli::get_module_login() . '-' . class_fli::get_controleur_login(). '-' . class_fli::get_fonction_login());
                }
                exit;
            }
        }

        /*$sSqlRoute = "SELECT * FROM ".$this->sPrefixeDb."routes WHERE route_route=?";
        $aParamRoute[] = class_fli::get_fli_module() . '-' . class_fli::get_fli_fonction();
        $aTabRoute = $this->objClassGenerique->renvoi_info_requete($sSqlRoute, $aParamRoute);
        */

        if(empty($this->sTplSortie) && $this->bEstModuleAppele){
            $this->sTplSortie = 'principal.tpl';
        }
        class_fli::set_tpl_sortie($this->sTplSortie);

        //Detecte le template a appelé
        $sTplAppele = '';
        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            $sTplAppele = $this->sFormulaireTpl;
        } elseif( isset($_GET['action']) && $_GET['action'] != 'form' ) {
            $sTplAppele = $this->sListeTpl;
        } elseif( !isset($_GET['action']) ) {
            $sTplAppele = $this->sListeTpl;
        }


        //echo $sTplAppele;

        if( isset($sModule) && !empty($sModule) && isset($sFliVersionTemplate ) && !empty($sFliVersionTemplate )
            && file_exists('modules/' . $sModule . '/vues/' . $sFliVersionTemplate  . '/templates/' . $sTplAppele)) {
            $this->objSmarty->setTemplateDir('modules/' . $sModule . '/vues/' . $sFliVersionTemplate  . '/templates/');
            $this->objSmarty->setCompileDir('modules/' . $sModule . '/vues/' . $sFliVersionTemplate  . '/templates_c/');
        } elseif( file_exists('vues/' . $sFliVersionTemplate  . '/templates/' . $sTplAppele) ) {
            //echo "Lepassage2 ".$sFliVersionTemplate;
            $this->objSmarty->setTemplateDir('vues/' . $sFliVersionTemplate  . '/templates/');
            $this->objSmarty->setCompileDir('vues/' . $sFliVersionTemplate  . '/templates_c/');
           // echo"smarty<pre>";print_r($this->objSmarty);echo"</pre>";
            //echo "le tpl ".$this->sTplSortie."<br>";
        } else {
            die('<div style="text-align: center;"><h1 style="font-size: 150px;color: rgb(162, 39, 38);">&#10008;</h1>
                <h3>Le template '.$sTplAppele.' n\'existe pas !</h3></div>');
        }

        //$this->objSmarty->assign('sNomTable', $this->sNomTable);
        //echo"<pre>";print_r($_GET);echo"</pre> ".$this->sChampId;

        /*
         * Si nçous sommes sur un fonctionnalité multi key
         */

        if($this->bKeyMultiple){

            if( isset($_GET['action']) && $_GET['action'] == 'form' && isset($_GET['fli_id']) ) {

                $this->iId = $_GET['fli_id'];
                $this->sEtatForm = "update";

            } elseif( isset($_GET['action']) && $_GET['action'] == 'form' && !isset($_GET['fli_id']) ) {
                $this->sEtatForm = "insert";
                $this->iId = null;
            } else if( isset($_GET['action']) && $_GET['action'] == 'supp' && isset($_GET['fli_id']) ) {
                $this->iId = $_GET['fli_id'];
            } else if( isset($_GET['action']) && $_GET['action'] == 'duplicate' && isset($_GET['fli_id']) ) {
                $this->iId = $_GET['fli_id'];
                //echo"toto";
            } else {
                $this->iId = null;
            }
        }else {

            if( isset($_GET['action']) && $_GET['action'] == 'form' && isset($_GET[$this->sChampId]) ) {

                $this->iId = $_GET[$this->sChampId];
                $this->sEtatForm = "update";

            } elseif( isset($_GET['action']) && $_GET['action'] == 'form' && !isset($_GET[$this->sChampId]) ) {
                $this->sEtatForm = "insert";
                $this->iId = null;
            } else if( isset($_GET['action']) && $_GET['action'] == 'supp' && isset($_GET[$this->sChampId]) ) {
                $this->iId = $_GET[$this->sChampId];
            } else if( isset($_GET['action']) && $_GET['action'] == 'duplicate' && isset($_GET[$this->sChampId]) ) {
                $this->iId = $_GET[$this->sChampId];
                //echo"toto";
            } else {
                $this->iId = null;
            }
        }


        //Fonction droits : redirige l'utilisateur vers la page d'erreur 13 s'il n'a pas l'accès
        //Droits en Ajout
        if(!$this->bByPassConnect) {
            if( isset($_GET['fli_module']) && $_GET['fli_module'] != class_fli::get_module_login() && isset($_GET['action']) && $_GET['action'] == 'form' && $this->bEstModuleAppele && !isset($this->iId) && !$this->bLabelCreationElem ) {
                         header('Location: ' . class_fli::get_chemin_acces_absolu() . '' . class_fli::get_fli_module() . '-ctrl_fli_redirect_erreur-fli_redirect_erreur');
                exit;
            }
            //Droits en Modification
            if( isset($_GET['fli_module']) && $_GET['fli_module'] != class_fli::get_module_login() && isset($_GET['action']) && $_GET['action'] == 'form' && $this->bEstModuleAppele && isset($this->iId) && !$this->bAffMod ) {
                header('Location: ' . class_fli::get_chemin_acces_absolu() . '' . class_fli::get_fli_module() . '-ctrl_fli_redirect_erreur-fli_redirect_erreur');
                exit;
            }
            //Droits en Suppression
            if( isset($_GET['fli_module']) && $_GET['fli_module'] != class_fli::get_module_login() && isset($_GET['action']) && $_GET['action'] == 'supp' && $this->bEstModuleAppele && isset($this->iId) && !$this->bAffSupp ) {
                header('Location: ' . class_fli::get_chemin_acces_absolu() . '' . class_fli::get_fli_module() . '-ctrl_fli_redirect_erreur-fli_redirect_erreur');
                exit;
            }
            //Droits en affichage
            if( isset($_GET['fli_module']) && $_GET['fli_module'] != class_fli::get_module_login() && $this->bEstModuleAppele && !$this->bAffVisu ) {
                header('Location: ' . class_fli::get_chemin_acces_absolu() . '' . class_fli::get_fli_module() . '-ctrl_fli_redirect_erreur-fli_redirect_erreur');
                exit;
            }
        }

        if( $this->iId === null || (isset($_GET['wizard_type_etape']) && $_GET['wizard_type_etape'] == 'crea' && (!isset($_GET['action']) || $_GET['action'] != 'form')) ) {
            $this->sCategorieForm = $this->sTitreCreationForm;
            $this->sMessageSuccesForm = $this->sMessageCreationSuccesForm;
        } else {
            $this->sCategorieForm = $this->sTitreModificationForm;
            $this->sMessageSuccesForm = $this->sMessageModificationSuccesForm;
        }
        $this->aTabResultInsert = array();
        $this->aTabResultUpdate = array();


    }

    public function config_wizard($aWizard)
    {

        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            if( isset($aWizard['valider']) ) {

                $this->itemBoutonsForm['valider'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Valider',
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );

                $this->sRetourValidationForm = 'externe';
                $this->sRetourValidationFormExterne = $aWizard['valider'];
            }

            if( isset($aWizard['annuler']) ) {

                $this->itemBoutonsForm['annuler'] = array(
                    'type' => 'bouton',
                    'nom_variable' => 'btannuler',
                    'text_label' => 'Annuler',
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );

                $this->sRetourAnnulationForm = 'externe';
                $this->sRetourAnnulationFormExterne = $aWizard['annuler'];
                $this->itemBoutonsForm['annuler']['url'] = $this->renvoi_url_retour_annulation_form_externe();
            }

            if( isset($aWizard['precedent']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['precedent'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['precedent'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['precedent']);
                        }
                    }
                }

                $this->itemBoutonsForm['precedent'] = array(
                    'type' => 'bouton',
                    'nom_variable' => '',
                    'text_label' => 'Précédent',
                    'url' => $aWizard['precedent'],
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['suivant']) ) {
                $this->sRetourValidationForm = 'externe';
                $this->sRetourValidationFormExterne = $aWizard['suivant'];
                $this->itemBoutonsForm['suivant'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Suivant',
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }
        } else {

            $this->sParametreWizardListe = '';
            $this->aParametreWizardListe = array();

            $aBoutonLabel = array( 'valider', 'annuler', 'precedent', 'suivant' );

            $aParametreSuivi = array( 'wizard_type_etape', 'wizard_etape' );

            foreach( $aBoutonLabel as $sBoutonLabel ) {
                if( isset($aWizard[$sBoutonLabel]) ) {
                    $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard[$sBoutonLabel], $aMatches);
                    if( $iNombreMatches > 0 ) {
                        $aChamp = array_unique($aMatches[1]);
                        foreach( $aChamp as $sChamp ) {
                            if( $this->aParamsNonSuivi != $sChamp ) {
                                //if($sChamp!=$this->aParamsNonSuivi)
                                $aParametreSuivi[] = $sChamp;
                            }
                        }
                    }
                }
            }

            $aParametreSuivi = array_unique($aParametreSuivi);

            foreach( $aParametreSuivi as $sParametreSuivi ) {
                if( isset($_GET[$sParametreSuivi]) ) {
                    $this->sParametreWizardListe .= '&' . $sParametreSuivi . '=' . $_GET[$sParametreSuivi];
                    $this->aParametreWizardListe[$sParametreSuivi] = $_GET[$sParametreSuivi];
                }
            }

            if( isset($aWizard['valider']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['valider'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['valider'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['valider']);
                        }
                    }
                }

                $this->itemBoutonsListe['valider'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Valider',
                    'url' => $aWizard['valider'],
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['annuler']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['annuler'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['annuler'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['annuler']);
                        }
                    }
                }

                $this->itemBoutonsListe['annuler'] = array(
                    'type' => 'bouton',
                    'nom_variable' => 'btannuler',
                    'text_label' => 'Annuler',
                    'url' => $aWizard['annuler'],
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['precedent']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['precedent'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['precedent'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['precedent']);
                        }
                    }
                }

                $this->itemBoutonsListe['precedent'] = array(
                    'type' => 'bouton',
                    'nom_variable' => '',
                    'text_label' => 'Précédent',
                    'url' => $aWizard['precedent'],
                    'style' => 'background-color:#777;border:1px solid #555;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }

            if( isset($aWizard['suivant']) ) {

                $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $aWizard['suivant'], $aMatches);
                if( $iNombreMatches > 0 ) {
                    $aChamp = array_unique($aMatches[1]);
                    foreach( $aChamp as $sChamp ) {
                        if( isset($_GET[$sChamp]) ) {
                            $aWizard['suivant'] = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $aWizard['suivant']);
                        }
                    }
                }

                $this->itemBoutonsListe['suivant'] = array(
                    'type' => 'submit',
                    'nom_variable' => 'btsubmit',
                    'text_label' => 'Suivant',
                    'url' => $aWizard['suivant'],
                    'style' => 'background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;'
                );
            }
        }
    }

    public function config_wizard_categorie($sCategorie)
    {
        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            $this->sCategorieForm = $sCategorie;
        } else {
            $this->sCategorieListe = $sCategorie;
        }
    }

    public function config_wizard_desactivation_popup()
    {
        $this->bFormPopup = false;
    }

    /**
     * @return array
     */
    public function get_menu_deroulant()
    {
        return $this->aMenuDeroulant;
    }

    /**
     * @return string
     */
    public function getSJointureSup()
    {
        return $this->sJointureSup;
    }

    /**
     * @return string
     */
    public function get_parametre_wizard_nom_variable_suivant()
    {
        return $this->sParametreWizardNomVariableSuivant;
    }

    /**
     * @return string
     */
    public function get_parametre_wizard_suivant()
    {
        return $this->sParametreWizardSuivant;
    }

    /**
     * @return boolean
     */
    public function get_parametre_bwizard()
    {
        return $this->sParametrebWizard;
    }


    public function renvoi_base_url($type = "")
    {


        if( $type == "pagination" and $this->sDirpagination != "" ) {
            $sDir = $this->sDirpagination;
            // echo "pagination=>".$this->sDirpagination."=>".$this->sBaseUrl;
            $this->sBaseUrl = "?dir=" . $this->sDirpagination;
        }

        if( !isset($this->sBaseUrl) ) {
            $sModule = isset($_GET['fli_module']) ? $_GET['fli_module'] : '';
            $sModuleControleur = isset($_GET['fli_controleur']) ? $_GET['fli_controleur'] : '';
            $sModuleFonction = isset($_GET['fli_fonction']) ? $_GET['fli_fonction'] : '';

            //ici on  met un chemin different pour la pagination


            $this->sBaseUrl = $sModule . '-'. $sModuleControleur . '-' . $sModuleFonction . '?';
        }

        return $this->sBaseUrl;
    }


    public function renvoi_parametre_inter_module($btype="")
    {

        //echo "<br>bretour =>".$btype;

       /* if($btype!="toto" ){
            echo $this->sParametreInterModule." lien ";
        }
       */

        if($btype=="retour" ){
            $sUrlPlus = "";

            if( !empty($this->aTableauParam) ) {
                foreach( $this->aTableauParam as $valeururl ) {

                    $sUrlPlus .= "&" . $valeururl['keys'] . "=" . $valeururl['value'];
                }
            }
            return $sUrlPlus;
        }
        if( !isset($this->sParametreInterModule) ) {
            $sParametreInterModule = '';

            foreach( $this->aElement as $objElement) {
                if( isset($objElement['transfert_inter_module']) && $objElement['transfert_inter_module'] == 'ok' && $btype!="retour" ) {
                    if( isset($_GET[$objElement['nom_variable']]) ) {
                        $sParametreInterModule .= '&' . $objElement['nom_variable'] . '=' . $_GET[$objElement['nom_variable']];
                    } else {
                        exit(0);
                    }
                }
            }


            $sUrlPlus = "";

            if( !empty($this->aTableauParam) ) {
                foreach( $this->aTableauParam as $valeururl ) {

                    $sUrlPlus .= "&" . $valeururl['keys'] . "=" . $valeururl['value'];
                }
            }



            $this->sParametreInterModule = $sParametreInterModule.$sUrlPlus;
        }

        return $this->sParametreInterModule;
    }



    public function renvoi_parametre_recherche()
    {
        if( !isset($this->sParametreRecherche) ) {


            $sParametreRecherche = '';

            if(isset($_GET['fli_rechercher'])){
                $sParametreRecherche="&fli_rechercher=".$_GET['fli_rechercher'];
                //echo "toto";
            }

            foreach( $this->aElement as $objElement ) {
                if( isset($objElement['aff_recherche']) && $objElement['aff_recherche'] == 'ok' ) {
                    if( isset($_GET["rech_".$objElement['nom_variable']]) ) {
                        if( !isset($objElement['transfert_inter_module']) || $objElement['transfert_inter_module'] != 'ok' ) {
                            if( is_array($_GET["rech_".$objElement['nom_variable']]) ) {
                                foreach( $_GET["rech_".$objElement['nom_variable']] as $sElement ) {
                                    $sParametreRecherche .= '&' . "rech_".$objElement['nom_variable'] . '[]=' . $sElement;
                                }
                            } else {
                                $sParametreRecherche .= '&' . "rech_".$objElement['nom_variable'] . '=' . $_GET["rech_".$objElement['nom_variable']];
                            }
                        }
                    }
                }
            }

            $this->sParametreRecherche = $sParametreRecherche;
        }

        return $this->sParametreRecherche;
    }

    public function renvoi_parametre_retour_liste()
    {
        if( !isset($this->sParametreRetourListe) ) {
            $sParametreRetourListe = '';

            $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourListe, $aMatches);
            if( $iNombreMatches > 0 ) {
                $aChamp = array_unique($aMatches[1]);
                foreach( $aChamp as $sChamp ) {
                    if( isset($_GET[$sChamp]) ) {
                        $sParametreRetourListe .= '&' . $sChamp . '=' . $_GET[$sChamp];
                    }
                }
            }

            $this->sParametreRetourListe = $sParametreRetourListe;
        }

        return $this->sParametreRetourListe;
    }

    /**
     * renvoi la valeur de la clé de aVariable
     */
    public static function get_aVariable($sCle)
    {
        return self::$aVariable[$sCle];
    }

    /**
     * défini la valeur de la clé de aVariable
     */
    public static function set_aVariable($sCle, $sValeur)
    {
        self::$aVariable[$sCle] = $sValeur;
    }

    public function obtenir_categorie()
    {
        if( isset($_GET['action']) && $_GET['action'] == 'form' ) {
            return $this->sCategorieForm;
        } else {
            return $this->sCategorieListe;
        }
    }

    public function obtenir_champ_id()
    {
        return $this->sChampId;
    }

    public function obtenir_id()
    {
        return $this->iId;
    }

    /**
     * Methode renvoi d'une pagination
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function pagination()
    {

       $sBaseUrl =
            $this->renvoi_base_url("pagination")
            . $this->renvoi_parametre_inter_module()
            . $this->renvoi_parametre_recherche()
            . $this->renvoi_parametre_filtre_liste()
            . $this->renvoi_parametre_retour_liste()
            . $this->sParametreWizardListe;

        $iPosition = (isset($_GET['iposition']) ? $_GET['iposition'] : '');

        $iNombreLigne = $this->renvoi_nombreligne_requete();

        $iNombreLignePage = $this->iNombrePage;

        $aPagination = array();
        $aPaginationSelect = array();
        $aPaginationListe = array();

        if( $iNombreLignePage == '' || $iNombreLignePage == '0' ) {
            $iNombreLignePage = '1';
        }

        if( $iPosition == '' ) {
            $iPosition = 0;
        }

        $iNombrePage = ceil($iNombreLigne / $iNombreLignePage);

        if( $iNombrePage > 1 ) {
            $iNumeroPage = ceil($iPosition / $iNombreLignePage);

            for( $i = 0; $i < $iNombrePage; $i++ ) {
                $aPaginationSelect[] = array(
                    'active' => 'ok',
                    'numero' => $i + 1,
                    'url' => $sBaseUrl . '&iposition=' . ($i * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage,
                    'selected' => ($i == $iNumeroPage)
                );
            }

            $aPaginationListe['page_premiere'] = $sBaseUrl . '&iposition=0&nbrpage=' . $iNombreLignePage;
            $aPaginationListe['page_derniere'] = $sBaseUrl . '&iposition=' . (($iNombrePage - 1) * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage;

            if( $iNumeroPage > 0 ) {
                $aPaginationListe['page_precedente'] = $sBaseUrl . '&iposition=' . (($iNumeroPage - 1) * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage;
            } else {
                $aPaginationListe['page_precedente'] = $aPaginationListe['page_premiere'];
            }

            if( $iNumeroPage < $iNombrePage - 1 ) {
                $aPaginationListe['page_suivante'] = $sBaseUrl . '&iposition=' . (($iNumeroPage + 1) * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage;
            } else {
                $aPaginationListe['page_suivante'] = $aPaginationListe['page_derniere'];
            }

            $iIntervalle = 2;
            $aPaginationListe['page_liste'] = array();

            for( $i = $iNumeroPage - $iIntervalle; $i <= $iNumeroPage + $iIntervalle; $i++ ) {
                if( $i >= 0 && $i <= $iNombrePage - 1 ) {
                    $aPaginationListe['page_liste'][] = array(
                        'numero' => $i + 1,
                        'url' => $sBaseUrl . '&iposition=' . ($i * $iNombreLignePage) . '&nbrpage=' . $iNombreLignePage,
                        'selected' => ($i == $iNumeroPage),
                        'totale' => $iNombrePage
                    );
                }
            }

            $aPagination['select'] = $aPaginationSelect;
            $aPagination['liste'] = $aPaginationListe;
        } else {
            $aPagination = null;
        }

        return $aPagination;
    }

    public function renvoi_parametre_filtre_liste()
    {
        if( !isset($this->sParametreFiltreListe) ) {
            $sParametreFiltreListe = '';

            if( isset($_GET['filtreliste']) && isset($_GET['typefiltreliste']) ) {
                $sParametreFiltreListe = '&filtreliste=' . $_GET['filtreliste'] . '&typefiltreliste=' . $_GET['typefiltreliste'];
            }

            $this->sParametreFiltreListe = $sParametreFiltreListe;
        }

        return $this->sParametreFiltreListe;
    }

    /**
     * Methode nombre de ligne sur la requete
     * @author Danon Gnakouri
     * @since  2.0
     * @return int inombre ligne
     */
    public function renvoi_nombreligne_requete()
    {
        $sLarequete = $this->renvoi_requete('', false);
        //echo 'la requete : '.$sLarequete.'<br/>';
        $aCount = $this->objClassGenerique->renvoi_nombreLigne_requete($sLarequete);

        return $aCount;
    }

    /**
     * renvoi champ a afficher dans le requete génral
     */
    public function renvoi_champ_requete_principal($objElement){
         $aTableRetour=array();
        if(!in_array($objElement['mapping_champ'],$this->aTabChampSelect)) {
            $aTableRetour['keys'] = $objElement['nom_variable'];
            $aTableRetour['value'] = 'principal.' . $objElement['mapping_champ'];
            //$aTableRetour['value'] = '';
            $this->aTabChampSelect[] = $objElement['mapping_champ'];
        }else{
            $aTableRetour['keys'] = $objElement['nom_variable'];
            $aTableRetour['value'] = '';
        }
        return $aTableRetour;
    }

    public function renvoi_champ_requete_select($objElement){


        $aTableRetour=array();
        if($objElement['alias_table_item']=="")
            $objElement['alias_table_item'] =class_helper::random(3);

        $aTableRetour['jointure']= "";

        if($objElement['alias_champ_item']==""){
            $objElement['alias_champ_item']=$objElement['nom_variable']."_".trim($objElement['table_item']);
        }

        $aTableRetour['keys'] = $objElement['nom_variable'];
        if(empty($objElement['lesitem'])) {

            $snomaffiche = $objElement['alias_table_item'] . "." . $objElement['affichage_table_item'];
            if(preg_match('/\[bdd\]/',$objElement['affichage_table_item'])){
                $snomaffiche=str_replace("[bdd]",$objElement['alias_table_item'] ,$objElement['affichage_table_item']);
            }
            if(!in_array($objElement['mapping_champ'],$this->aTabChampSelect)) {
                //echo $objElement['mapping_champ']."<br>";
                $aTableRetour['value'] = 'principal.' . $objElement['mapping_champ'] . ", " . $snomaffiche . "
             as " . $objElement['alias_champ_item'];
                $this->aTabChampSelect[]=$objElement['mapping_champ'];
            }else{
                $aTableRetour['value'] =  $snomaffiche . "
             as " . $objElement['alias_champ_item'];
            }
        }else
            $aTableRetour['value'] = 'principal.'.$objElement['mapping_champ']."";

        // si nous avons une recherche ou si nous voulons activer dans le push
        //if($objElement['aff_recherche']=="ok" || $objElement['actif_jointure']=="ok"){
        if(empty($objElement['lesitem'])){

            $identififantjointure=$objElement['table_item']."".$objElement['id_table_item']."principal".$objElement['mapping_champ'];
          //  if(!isset($this->aTabChampJointure['identifiant']) or !in_array($identififantjointure,$this->aTabChampJointure['identifiant'])) {

                $aTableRetour['jointure'] = " " . $objElement['type_table_join'] . " " . $objElement['table_item'] . " as " . $objElement['alias_table_item'] . "
             on " . $objElement['alias_table_item'] . "." . $objElement['id_table_item'] . " = principal." . $objElement['mapping_champ'];

                $this->aTabChampJointure['identifiant'][$identififantjointure]=$identififantjointure;
                $this->aTabChampJointure['alias'][$identififantjointure]=$objElement['alias_table_item'];
            /*}else{
                $aTableRetour['jointure'] = " ";
            }*/
        }

        return $aTableRetour;
    }


    public function renvoi_champ_requete_selectdist($objElement){
        $aTableRetour=array();


        $sAlisitem =$objElement['alias_table_item'];


        $snomaffiche = $sAlisitem . "." . $objElement['affichage_table_item'];
        if(preg_match('/\[bdd\]/',$objElement['affichage_table_item'])){
            $snomaffiche=str_replace("[bdd]",$sAlisitem ,$objElement['affichage_table_item']);
        }

        //echo $snomaffiche."<br>";

        $aTableRetour['keys'] = $objElement['nom_variable'];
        if(!in_array($objElement['mapping_champ'],$this->aTabChampSelect)) {
            $aTableRetour['value'] = 'principal.'.$objElement['mapping_champ'].",". $snomaffiche." as nom_".$sAlisitem;
        }else{
            $aTableRetour['value'] =  $snomaffiche." as nom_".$sAlisitem;
            $this->aTabChampSelect[]=$objElement['mapping_champ'];
        }
        $aTableRetour['jointure']="";
        // si nous avons une recherche ou si nous voulons activer dans le push
        if($objElement['aff_recherche']=="ok" || $objElement['actif_jointure']=="ok"){


            $t=0;
            $bddalias = class_helper::random(3)."";
            $identififantjointure = $objElement['table_type_join'][0]."".$objElement['id_table_lien'][0]."pincipal".$objElement['mapping_champ'];
           //if(!isset($this->aTabChampJointure['identifiant']) or !in_array($identififantjointure,$this->aTabChampJointure['identifiant'])) {
               $aTableRetour['jointure'] .= " " . $objElement['table_type_join'][0] . " " . $objElement['table_lien'][0] . " as " . $bddalias . "_0 on " . $bddalias . "_0." . $objElement['id_table_lien'][0] .
                   " = principal." . $objElement['mapping_champ'] . " and " . $bddalias . "_0." . $objElement['supplogique_table_lien'][0] . "='N'";

               $this->aTabChampJointure['identifiant'][$identififantjointure]=$identififantjointure;
               $this->aTabChampJointure['alias'][$identififantjointure]=$bddalias . "_0";
           //}
            //id_item_table_lien
            //foreach( $objElement['table_lien'] as $valeuritem){
            //echo"<pre>";print_r();echo"</pre>";
            for($u=1;$u<count($objElement['table_lien']);$u++){

                    $i=$u-1;
                $identififantjointure=$objElement['table_lien'][$u].$objElement['id_table_lien'][$u].$objElement['table_lien'][$i]."".$objElement['id_item_table_lien'][$i];
               // if(!isset($this->aTabChampJointure['identifiant']) or !in_array($identififantjointure,$this->aTabChampJointure['identifiant'])) {
                    $aTableRetour['jointure'] .= " " . $objElement['table_type_join'][$u] . " " . $objElement['table_lien'][$u] . " as " . $bddalias . "_" . $u . " on " . $bddalias . "_" . $u . "." . $objElement['id_table_lien'][$u] .
                        " = " . $bddalias . "_" . $i . "." . $objElement['id_item_table_lien'][$i] . " and " . $bddalias . "_" . $u . "." . $objElement['supplogique_table_lien'][$u] . "='N'";
                    $t++;
                    $this->aTabChampJointure['identifiant'][$identififantjointure]=$identififantjointure;
                    $this->aTabChampJointure['alias'][$identififantjointure]=$bddalias . "_".$u;
                    //echo"in<pre>";print_r($this->aTabChampJointure);echo"</pre>";
                /*}else{
                    echo"out<pre>";print_r($this->aTabChampJointure);echo"</pre>";
                    $aTableRetour['jointure'] .= " " . $objElement['table_type_join'][$u] . " " . $objElement['table_lien'][$u] . " as " . $bddalias . "_" . $u . " on " . $bddalias . "_" . $u . "." . $objElement['id_table_lien'][$u] .
                        " = " . $this->aTabChampJointure['alias'][$identififantjointure]. "." . $objElement['id_item_table_lien'][$i] . " and " . $bddalias . "_" . $u . "." . $objElement['supplogique_table_lien'][$u] . "='N'";
                    $t++;
                }*/ 
            }



            /*if($t>0)
                $i=$t-1;
            else */
                $i=$t;

            //echo $t."<br>";

            $aTableRetour['jointure'].= " ".$objElement['type_table_join']." ".$objElement['table_item']." as ".$sAlisitem." on ".$sAlisitem.".".$objElement['id_table_item'].
                " = ".$bddalias."_".$i.".".$objElement['id_item_table_lien'][$t]." and ".$bddalias."_".$i.".".$objElement['supplogique_table_lien'][$t]."='N'";



            /*if($objElement['alias_table_item']=="")
                $objElement['alias_table_item'] =class_helper::random(3);

            $aTableRetour['jointure']= $objElement['type_table_join']." ".$objElement['table_item']." as ". $objElement['alias_table_item']."
             on ".$objElement['alias_table_item'].".".$objElement['id_table_item']." = principal.".$objElement['mapping_champ'];
             */

            //echo $aTableRetour['jointure']."<br>";

        }

        return $aTableRetour;
    }
    
    public function renvoi_champ_requete_date($objElement){
        $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] = 'principal.'.$objElement['mapping_champ'].',DATE_FORMAT(principal.'.$objElement['mapping_champ'].',\''.$objElement['date_format'].'\') as '.$objElement['alias_champ'];
        return $aTableRetour;
    }


    
    public function renvoi_champ_requete_datetime($objElement){
        $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] = 'principal.'.$objElement['mapping_champ'].',DATE_FORMAT(principal.'.$objElement['mapping_champ'].',\''.$objElement['date_format'].'\') as '.$objElement['alias_champ'];
        return $aTableRetour;
    }



    public function renvoi_champ_requete_checkbox($objElement){
        $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] = 'principal.'.$objElement['mapping_champ'].' as '.$objElement['alias_champ'];
        return $aTableRetour;
    }
    /**
     * renvoi champ a afficher dans le requete date
     */

    /**
     * Methode renvoi d'une requete selection
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function renvoi_requete($sRequetePere = '', $bCount = true)
    {
        //echo"<pre>";print_r($_GET);echo"</pre>";
        //$sFiltreChampFils='';
       if(!empty(self::$sFiltreChampFils) && !$this->bEstModuleAppele){

            if(!empty(self::$sTableFils)){
                $sTableFils = self::$sTableFils.'.';
            }else{
                $sTableFils = 'principal.';
            }
            $sFiltreChampFils = $sTableFils.self::$sFiltreChampFils.' AS sFiltreChampFils,';
        }else{
            $sFiltreChampFils = '';
        }


        $sSelectSqlSuppl = '';
        if(!empty($this->aSelectSqlSuppl)){
            foreach($this->aSelectSqlSuppl as $key=>$value){
                $sSelectSqlSuppl .= $value.',';
            }
        }

        $this->aTabChampSelect[]= $this->sChampId;

        if($this->sChampId!="" and $this->sChampId!="fli_id") {
            $sRequeteSelect = 'SELECT ' . $sSelectSqlSuppl . $sFiltreChampFils . 'principal.' . $this->sChampId . ' ';

            if( trim($this->sChampGuid) != "" ) {
                $sRequeteSelect .= ' ,principal.' . $this->sChampGuid . ' ';
            }

        }else{
            $aTabKeymultiple = array();
            foreach( $this->aElement as $objElement ) {

                if($objElement['is_keymultiple']=="ok"){
                    $aTabKeymultiple[] =$objElement['mapping_champ'];
                }
            }


            $sIdchampSelect="";

            if(count($aTabKeymultiple)==1){
                $sIdchampSelect = "principal.".$aTabKeymultiple[0];
            }else{
                $sIdchampSelect="concat(";
                $ipassconcat=false;
                foreach($aTabKeymultiple as $valeurkeys){
                    if($ipassconcat)
                        $sIdchampSelect.=",'_',";

                    $sIdchampSelect.="principal.".$valeurkeys;


                    $ipassconcat=true;
                }

                $sIdchampSelect.=") ";
            }


             $sRequeteSelect= 'SELECT '.$sIdchampSelect.' as \'fli_id\' ';

            $this->sChampId="fli_id";


        }
        //si nous avons un chanmp guid
        if($this->sChampIdTmp!="" )
            $sRequeteSelect.= ', '.$sFiltreChampFils.'principal.' . $this->sChampIdTmp . ' as prinicpalidtemp ';



        $sRequeteJoint = '';
        $sRequeteOrderBy = '';
        $iIndexTable = 1;
        $bpassefiltre= false;
        $smapOrdebydefaut="";
        foreach( $this->aElement as $objElement ) {
            //$smapOrdebydefaut= $objElement['mapping_champ'];

            if($sRequeteOrderBy=="" and $this->sFiltrelisteOrderBy==""){
                if($objElement['type_champ']=="text") {
                    $sRequeteOrderBy = " order by principal." . $objElement['mapping_champ'];
                    //$smapOrdebydefaut = $objElement['mapping_champ'];
                }
            }

            if($objElement['type_champ']=="text" and !$bpassefiltre) {
                 $smapOrdebydefaut = $objElement['mapping_champ'];
                $bpassefiltre=true;
            }

            //renvoi la  liste des info dans  le seect et les jointure
            if( ($objElement['aff_liste'] == 'ok' || $objElement['aff_form'] == 'ok') && $objElement['traite_sql'] == 'ok' &&  $objElement['mapping_champ']!="") {
             
                $sMethodeChampEntete = "renvoi_champ_requete_".$objElement['type_champ'];

                 if(method_exists($this,$sMethodeChampEntete)){
                     $aTabRetour =  $this->$sMethodeChampEntete($objElement);
                 }else{
                     //sinon on appele la method prinicipal
                     $aTabRetour =  $this->renvoi_champ_requete_principal($objElement);
                 }

                 if($this->sFiltrelisteOrderBy==""){
                     if($objElement['type_champ'] =="text" and $objElement['traite_sql']=="ok"){
                         $this->sFiltrelisteOrderBy.=" order by principal.".$objElement['mapping_champ'];
                     }
                 }

                 if($aTabRetour['value']!="")
                    $sRequeteSelect .= ','.$aTabRetour['value'];


                 //on rajoute la jointure et le type de jointure
                 if(isset($aTabRetour['jointure'])){
                     $sRequeteJoint.=$aTabRetour['jointure'];
                 }
            }elseif($objElement['traite_sql'] == 'ok'){
                $sMethodeChampEntete = "renvoi_champ_requete_".$objElement['type_champ'];

                if(method_exists($this,$sMethodeChampEntete)){
                    $aTabRetour =  $this->$sMethodeChampEntete($objElement);
                }else{
                    //sinon on appele la method prinicipal
                    $aTabRetour =  $this->renvoi_champ_requete_principal($objElement);
                }

                if($aTabRetour['value']!="")
                    $sRequeteSelect .= ','.$aTabRetour['value'];


                //on rajoute la jointure et le type de jointure
                if(isset($aTabRetour['jointure'])){
                    $sRequeteJoint.=$aTabRetour['jointure'];
                }
            }else{
                $vide="";
            }

            if($objElement['transfert_inter_module'] == 'ok'){

                $sIdfiltre ="";
                if(isset($_GET[$objElement['nom_variable']])){
                    $sIdfiltre = $_GET[$objElement['nom_variable']];
                }
                if(isset($_POST[$objElement['nom_variable']])){
                    $sIdfiltre = $_GET[$objElement['nom_variable']];
                }


                //si on rajoute un filtre dans le filtre
                if($objElement['transfert_inter_module_filtre_suplementaire']!=""){
                    $sRequetePere.= " and ( principal.".$objElement['mapping_champ']."='".$sIdfiltre."' ".$objElement['transfert_inter_module_filtre_suplementaire'].")";
                }else{
                    $sRequetePere.= " and principal.".$objElement['mapping_champ']."='".$sIdfiltre."'";
                }

            }
        }




        //ici on calcul pour faire un ordre pere puis fils
        $sChampOderpereFils="";
        if($this->sChampFiltreOrderperefils!="")
            $sChampOderpereFils=" concat(if(principal.".$this->sChampFiltreOrderperefils."!='',concat(principal.".$this->sChampFiltreOrderperefils.",'_','b')
            ,concat(principal.".$this->sChampId.",'_','a')),'_',principal.".$this->sChampId.") as champorderperefils";

        if($sChampOderpereFils!="") {
            $sRequeteSelect .= ", " . $sChampOderpereFils;
            $this->sFiltrelisteOrderBy=" order by champorderperefils";
        }


        $sRequeteSelect .= ' FROM ' . $this->sNomTable . ' AS principal ' .$sRequeteJoint.' '.$this->sJointureSup;
        if($this->bUseDelete){
            $sRequeteSelect .= ' WHERE 1 ' . $sRequetePere;
        }else{
            $sRequeteSelect .= ' WHERE principal.' . $this->sChampSupplogique . ' = \''.$this->sValONSupplogique.'\' ' . $sRequetePere;
        }

        $sRequeteSelect .= ' ' . $this->renvoi_requete_filtrage_recherche();
        $sRequeteSelect .= ' ' . str_replace('&#39;','\'',$this->sFiltreliste);

        //echo "".$sRequeteSelect;
        if($this->sTypeSortie =="pdf"  and $this->sFiltrelisteOrderByPdf!="")
             $sRequeteSelect .= ' ' .$this->sFiltrelisteOrderByPdf;
         else
            $sRequeteSelect .= ' ' . $this->sFiltrelisteOrderBy;


        //echo $sRequeteOrderBy.'<br/>'.$sRequeteSelect.'<br/><br/>';
        class_fli::set_debug("class_list-renvoi_requete",$sRequeteSelect);

        if( $this->bPagination && $bCount && $this->sTypeSortie!="csv" && $this->sTypeSortie!="pdf") {
            if( isset($_GET['iposition']) ) {
                $sRequeteSelect .= ' LIMIT ' . $_GET['iposition'] . ',' . $this->iNombrePage;
            } else {
                $sRequeteSelect .= " LIMIT 0," . $this->iNombrePage;
            }
        }


        if( $this->bDebugRequete ) {
            $this->sDebugRequeteSelect = $sRequeteSelect;
        }

        //if( $this->bDebugRequete ) {echo $sRequeteSelect."<br>";}

        //echo $sRequeteSelect."<br>";
        /*
     ho"passe".$sRequeteSelect;
      exit();
     */

       // echo $sRequeteSelect;
        return $sRequeteSelect;
    }

    /*
     * Mtehode Renvoi filtre de la rercherche requete
     *
     */
    public function renvoi_requete_filtre_principal($objElement){
        $sAliasBaseDonne = "principal";
        $sRequeteRetour="";

        $aValeurFiltrage = isset($_GET["rech_".$objElement['nom_variable']]) ? $_GET["rech_".$objElement['nom_variable']] : (isset($objElement['valeur_variable']) ? $objElement['valeur_variable'] : '');
        if($objElement['aff_recherche'] == 'ok') {
            if( $aValeurFiltrage != '' && $objElement['traite_sql'] == 'ok' ) {
                if( isset($_GET['fli_rechercher']) ) {
                    if( $_GET['fli_rechercher'] == "ok" ) {
                        $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['mapping_champ'] . " " . str_replace("<champ>", addslashes($aValeurFiltrage), $objElement['type_recherche']);
                        //echo $sRequeteRetour;
                        //exit();
                    }
                }

            }
        }
         return $sRequeteRetour;
    }

    /*
    * Mtehode Renvoi filtre de la rercherche select
    *
    */
    public function renvoi_requete_filtre_select($objElement)
    {
        $sAliasBaseDonne = "principal";//$objElement['alias_table_item'];
        $sRequeteRetour = "";

        if($objElement['aff_recherche'] == 'ok') {
            $aValeurFiltrage = isset($_GET["rech_" . $objElement['nom_variable']]) ? $_GET["rech_" . $objElement['nom_variable']] : '';
            if( $aValeurFiltrage != '' && $objElement['traite_sql'] == 'ok' ) {

                if($objElement['text_recherche'] == 'ok'){
                    $sAliasBaseDonne = $objElement['alias_table_item'];
                   $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['affichage_table_item'] ." ". str_replace("<champ>",$aValeurFiltrage,$objElement['type_recherche']);
               }else {
                   $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['mapping_champ'] . "='" . $aValeurFiltrage . "'";
               }

            }
        }
        return $sRequeteRetour;
  }

    /*
    * Mtehode Renvoi filtre de la rercherche selectdist
    *
    */
    public function renvoi_requete_filtre_selectdist($objElement)
    {
        $sAliasBaseDonne = $objElement['alias_table_item'];
        $sRequeteRetour = "";

        if($objElement['aff_recherche'] == 'ok') {
            $aValeurFiltrage = isset($_GET["rech_" . $objElement['nom_variable']]) ? $_GET["rech_" . $objElement['nom_variable']] : '';
            if( $aValeurFiltrage != '' && $objElement['traite_sql'] == 'ok' ) {
                if($objElement['text_recherche'] == 'ok'){
                    $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['affichage_table_item'] . str_replace("<champ>",$aValeurFiltrage,$objElement['type_recherche']);
                }else {
                    $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['id_table_item'] . "='" . $aValeurFiltrage . "'";
                }
            }
        }

        //echo $sRequeteRetour;
        return $sRequeteRetour;
    }


    /*
    * Mtehode Renvoi filtre de la rercherche select
    *
    */
    public function renvoi_requete_filtre_checkbox($objElement)
    {
        $sAliasBaseDonne = "principal";
        $sRequeteRetour = "";

        $aValeurFiltrage = isset($_GET["rech_".$objElement['nom_variable']]) ? $_GET["rech_".$objElement['nom_variable']] : (isset($objElement['valeur_variable']) ? $objElement['valeur_variable'] : '');
        if( !empty($aValeurFiltrage) && $objElement['traite_sql'] == 'ok' && isset($_GET['fli_rechercher']) && $_GET['fli_rechercher']=="ok") {

            $sRequeteSqlIn="";
            //echo"<pre>";print_r($_GET);echo"</pre>";
            $sIdfiltre="(''";
            if(is_array($aValeurFiltrage)) {
                foreach( $aValeurFiltrage as $iLesIdFiltre ) {
                    $sIdfiltre .= ",'" . $iLesIdFiltre . "'";
                }

            $sIdfiltre.")";
            $sRequeteSqlIn="( select ".$objElement['id_table_lien'][0]." FROM ".$objElement['table_lien'][0]." as lien0";
            $u=0;
            for( $i = 0; $i < count($objElement['table_lien'])-1; $i++ ) {
                $sRequeteSqlIn .= " JOIN " . $objElement['table_lien'][$i] . " lien" . $i . " ON lien" . $i . "." . $objElement['id_table_lien'][$i] .
                    " = lien" . ($i - 1) . "." . $objElement['id_item_table_lien'][$i - 1]." and ".$objElement['supplogique_table_lien'][$i]."='N'";

                $u++;
            }

            $sRequeteSqlIn .= " JOIN ".$objElement['table_item']." on ".$objElement['table_item'].".".$objElement['id_table_item']." = lien".$u.".".$objElement['id_item_table_lien'][$u].
                " and ".$objElement['supplogique_table_lien'][$u]."='N'";

            $sRequeteSqlIn.= " where ".$objElement['table_item'].".".$objElement['id_table_item']." in ".$sIdfiltre."))";
            $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['mapping_champ'] ." in " .$sRequeteSqlIn."";
            }
        }


        return $sRequeteRetour;
    }

    public function renvoi_requete_filtre_date($objElement)
    {
        $sAliasBaseDonne = "principal";
        $sRequeteRetour = '';
        //echo"<pre>";print_r($_GET);echo"</pre>";
        $aValeurFiltrage = isset($_GET["rech_" . $objElement['nom_variable']]) ? $_GET["rech_" . $objElement['nom_variable']] : (isset($objElement['valeur_variable']) ? $objElement['valeur_variable'] : '');

        if( $aValeurFiltrage != '' && $objElement['traite_sql'] == 'ok' ) {
            if( isset($_GET['fli_rechercher']) ) {
                if( $_GET['fli_rechercher'] == "ok" ) {
                    if($objElement['recherche_intervalle_date']!="ok"){
                        $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['mapping_champ'] . " " . str_replace("<champ>", $aValeurFiltrage, $objElement['type_recherche']);
                    }else{
                        $aTabFiltre = $_GET["rech_" . $objElement['nom_variable']];

                        if($aTabFiltre[0]!="" and $aTabFiltre[1]!="")
                            if($objElement['date_conversion_enreg']=="frtoeng")
                                $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['mapping_champ'] . " between '".class_helper::renvoi_date($aTabFiltre[0],"eng")." 00:00:00' and '".class_helper::renvoi_date($aTabFiltre[1],"eng")." 23:59:59'";
                            else
                                $sRequeteRetour = " AND " . $sAliasBaseDonne . "." . $objElement['mapping_champ'] . " between '".$aTabFiltre[0]." 00:00:00' and '".$aTabFiltre[1]." 23:59:59'";

                    }
                }
            }


        }
        return $sRequeteRetour;
    }
    /**
     * Methode renvoi_requete_filtrage_recherche genere la requete sql pour le filtrage du formulaire de recherche
     * @author Sebastien Robert
     */
    public function renvoi_requete_filtrage_recherche()
    {

        $sSqlFiltre = '';

        foreach( $this->aElement as $objElement ) {

            $sMethodeChampEntete = "renvoi_requete_filtre_".$objElement['type_champ'];

            if(method_exists($this,$sMethodeChampEntete)){
                $sSqlFiltre .=  $this->$sMethodeChampEntete($objElement);
            }else{
                //sinon on appele la method prinicipal
                $sSqlFiltre .=   $this->renvoi_requete_filtre_principal($objElement);
            }

        }

        //echo $sSqlFiltre;
        return $sSqlFiltre;
    }
    /**
     * renvoi le format d’affichage dans la cellule
     */
    public function renvoi_affiche_cellule_principal($objElement, $objValeurBddListe)
    {

        $aTableRetour = array();
        if( $objElement['injection_code'] != "" ) {
            $sChaineId = str_replace("<id>", $objValeurBddListe[$this->sChampId], $objElement['injection_code']);
            $objValeurBddListe[$objElement['mapping_champ']]['value'] = str_replace("<value>", $objValeurBddListe[$objElement['mapping_champ']]['value'], $sChaineId);
        }

        $objValeurBddListe[$objElement['mapping_champ']]['value']=stripcslashes($objValeurBddListe[$objElement['mapping_champ']]['value']);
        $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] = $objValeurBddListe[$objElement['mapping_champ']];

        return $aTableRetour;
    }


    /**
     * renvoi le format d’affichage dans la cellule type injection_code_liste
     */
    public function renvoi_affiche_cellule_injection_code_liste($objElement, $objValeurBddListe)
    {

        $aTableRetour = array();
        $aTableRetour['keys'] = $objElement['nom_variable'];

        if($this->sTypeSortie=="json" or $this->sTypeSortie=="android"){
            $aTableRetour['keys'] = $objElement['nom_variable'];
            $aTableRetour['value'] ="";
            return $aTableRetour;
        }

        if(!empty($objElement['tab_replace_injection_code'])){

            $atmp=array();
             $atmp['value']=$objElement['injection_code'];
             
            foreach ($objElement['tab_replace_injection_code'] as $key => $value) {

                $atmp['value'] = str_replace($key,$objValeurBddListe[$value]['value'],$atmp['value']);
                //echo"<pre>";print_r($objElement);echo"</pre>";
            }
             $objElement['injection_code']=$atmp;
        }

        $aTableRetour['value'] = $objElement['injection_code'];
        return $aTableRetour;
    }
    /**
     * renvoi le format d’affichage dans la cellule pour e type color
     */
   public function  renvoi_affiche_cellule_color($objElement,$objValeurBddListe){
        
        $aTableRetour=array();
        //echo"<pre>";print_r($objValeurBddListe);echo"</pre>";
       $objValeurBddListe[$objElement['mapping_champ']]['value'] = "<span style='background-color:".$objValeurBddListe[$objElement['mapping_champ']]['value']."'>&nbsp;&nbsp;&nbsp;&nbsp;".$objValeurBddListe[$objElement['mapping_champ']]['value']."</span>";
       $aTableRetour['keys'] = $objElement['nom_variable'];
        $aTableRetour['value'] =$objValeurBddListe[$objElement['mapping_champ']];
           

         return $aTableRetour;
    }

    /**
     * renvoi le format d’affichage dans la cellule pour e type color
     */
   public function  renvoi_affiche_cellule_date($objElement,$objValeurBddListe){
        
        $aTableRetour=array();
         //echo"<pre>";print_r($objValeurBddListe);echo"</pre>";
         $aTableRetour['keys'] = $objElement['alias_champ'];
         $aTableRetour['value'] = $objValeurBddListe[$objElement['alias_champ']];

         return $aTableRetour;
    }


     /**
     * renvoi le format d’affichage dans la cellule pour e type color
     */
   public function  renvoi_affiche_cellule_datetime($objElement,$objValeurBddListe){
        
        $aTableRetour=array();
         //echo"<pre>";print_r($objValeurBddListe);echo"</pre>";
        if(isset($objElement['alias_champ'])){
             $aTableRetour['keys'] = $objElement['alias_champ'];
             $aTableRetour['value'] = $objValeurBddListe[$objElement['alias_champ']];
         }else{
               $aTableRetour['keys'] = $objElement['nom_variable'];
               $aTableRetour['value'] = $objValeurBddListe[$objElement['mapping_champ']];
         }  

         return $aTableRetour;
    }
   /**
    * renvoi le format d’affichage dans la cellule pour e type select
    */
   public function  renvoi_affiche_cellule_time($objElement,$objValeurBddListe){

       if( $objElement['injection_code'] != "" ) {
           $sChaineId = str_replace("<id>", $objValeurBddListe[$this->sChampId]['value'], $objElement['injection_code']);
           $objValeurBddListe[$objElement['mapping_champ']]['value'] = str_replace("<value>", $objValeurBddListe[$objElement['mapping_champ']]['value'], $sChaineId);
       }
       //echo"<pre>";print_r($objValeurBddListe);echo"</pre>";
       //echo"<pre>";print_r($objElement);echo"</pre>";
       $objValeurBddListe[$objElement['mapping_champ']]['value']=substr($objValeurBddListe[$objElement['mapping_champ']]['value'],0,5);

       $aTableRetour['keys'] = $objElement['nom_variable'];
       $aTableRetour['value'] = $objValeurBddListe[$objElement['mapping_champ']];

       return $aTableRetour;
   }


   public function renvoi_affiche_cellule_selectdist($objElement,$objValeurBddListe){

       $objValeurBddListe[$objElement['mapping_champ']]['value']=stripcslashes($objValeurBddListe['nom_'.$objElement['alias_table_item']]['value']);
       $aTableRetour['keys'] = $objElement['nom_variable'];
       $aTableRetour['value'] = $objValeurBddListe[$objElement['mapping_champ']];

       return $aTableRetour;
   }


   /**
     * renvoi le format d’affichage dans la cellule pour e type select
     */
    public function  renvoi_affiche_cellule_select($objElement,$objValeurBddListe){

        if( $objElement['injection_code'] != "" ) {
            $sChaineId = str_replace("<id>", $objValeurBddListe[$this->sChampId]['value'], $objElement['injection_code']);
            $objValeurBddListe[$objElement['mapping_champ']]['value'] = str_replace("<value>", $objValeurBddListe[$objElement['mapping_champ']]['value'], $sChaineId);
        }

          $objElement['affichage_table_item'] = str_replace("[bdd]", $objElement['table_item'], $objElement['affichage_table_item']);





        //echo"<pre>";print_r($objElement);echo"</pre>";
        //si le tableau est vide est que on pointe sur une base de donnée

        if(empty($objElement['lesitem']) ){

            $aTableRetour['keys'] = $objElement['nom_variable'];
            if($objElement['alias_champ_item']==""){
                $objElement['alias_champ_item']=$objElement['nom_variable']."_".trim($objElement['table_item']);
            }

            $objValeurBddListe[$objElement['mapping_champ']]['value'] = $objValeurBddListe[$objElement['alias_champ_item']]['value'];

            if(isset($objElement['aff_item_literal']) and $objElement['aff_item_literal']=="ok") {
                $objValeurBddListe[$objElement['mapping_champ']]['value'] = class_helper::renvoi_jour_literal($objValeurBddListe[$objElement['alias_champ_item']]['value']);
            }


            $aTableRetour['value'] =$objValeurBddListe[$objElement['mapping_champ']];
            //class_fli::set_debug("class_list-renvoi_affiche_cellule_select",$sRequete_renvoi_info);
        }else{
            $aTableRetour['keys'] = $objElement['nom_variable'];
            //si le champ de la table n'est pas vide et que la valeur est compris dans le tableau de selection
            if($objValeurBddListe[$objElement['mapping_champ']]['value']!="" and in_array($objValeurBddListe[$objElement['mapping_champ']]['value'],array_keys($objElement['lesitem']))) {

                $objValeurBddListe[$objElement['mapping_champ']]['value'] = $objElement['lesitem'][$objValeurBddListe[$objElement['mapping_champ']]['value']];
                //echo $objElement['lesitem'][$objValeurBddListe[$objElement['mapping_champ']]['value']]."<br>";
                //echo $objValeurBddListe[$objElement['mapping_champ']]['value']."<br>";

                $aTableRetour['value'] = $objValeurBddListe[$objElement['mapping_champ']];

            }else{
                $aTableRetour['value'] =$objValeurBddListe[$objElement['mapping_champ']];
            }
            //$aTableRetour['value'] =$objValeurBddListe[$objElement['mapping_champ']];

        }
        return $aTableRetour;
    }

    /**
     * renvoi le format d’affichage dans la cellule pour type select
     */
    public function  renvoi_affiche_cellule_radio($objElement,$objValeurBddListe){

              return $this->renvoi_affiche_cellule_select($objElement, $objValeurBddListe);
    }

    /**
    * renvoi le format d’affichage dans la cellule pour e type fichier
    */
    public function  renvoi_affiche_cellule_file($objElement,$objValeurBddListe)
    {

        if( $objElement['injection_code'] != "" ) {
            $sChaineId = str_replace("<id>", $objValeurBddListe[$this->sChampId], $objElement['injection_code']);
            $objValeurBddListe[$objElement['mapping_champ']]['value'] = str_replace("<value>", $objValeurBddListe[$objElement['mapping_champ']]['value'], $sChaineId);
        }

        if($this->sTypeSortie=="json" or $this->sTypeSortie=="android"){
            $aTableRetour['keys'] = $objElement['nom_variable'];
            $aTableRetour['value'] =$objValeurBddListe[$objElement['mapping_champ']];
            return $aTableRetour;
        }

        $sNomFichier = $objValeurBddListe[$objElement['mapping_champ']]['value'];
        if( $sNomFichier != null && $sNomFichier != '' ) {

            // si le fichier est une image
            if( preg_match("/\.png$/", $sNomFichier) === 1 || preg_match("/\.jpeg$/", $sNomFichier) === 1 || preg_match("/\.jpg$/", $sNomFichier) === 1 || preg_match("/\.gif$/", $sNomFichier) === 1 ) {

                if( file_exists($objElement['file_upload'] . 'min_' . $sNomFichier) ) {
                    $chemin_image_up = $objElement['file_upload'] . 'min_' . $sNomFichier;
                } else {
                    $chemin_image_up = $objElement['file_upload'] . $sNomFichier;
                }

                $objValeurBddListe[$objElement['mapping_champ']]['value']='
                                 <div style="
                                     border-radius:6px;
                                     border:1px solid #ACACAC;
                                     padding:3px;
                                     display:inline-block;
                                     vertical-align:middle;
                                     ' . (isset($objElement['file_aff_liste_couleur_fond']) ? 'background-color:' . $objElement['file_aff_liste_couleur_fond'] : '') . '"
                                 >
                                     <a href="' . $objElement['file_upload'] . $sNomFichier . '"  target="_blank"><img
                                         src="' . $chemin_image_up . '"
                                         style="display:inline-block;vertical-align:middle;"
                                         ' . (isset($objElement['file_aff_liste_taille']) ? 'width="' . $objElement['file_aff_liste_taille'] . '"' : '') . '
                                     ></a>
                                 </div>';
                $aTableRetour['value']  =$objValeurBddListe[$objElement['mapping_champ']];
            }else if( preg_match("/\.swf$/", $sNomFichier) === 1 ) {

                $objValeurBddListe[$objElement['mapping_champ']]['value']= '
                                 <div style="
                                     border-radius:6px;
                                     border:1px solid #ACACAC;
                                     padding:3px;
                                     display:inline-block;
                                     vertical-align:middle;
                                     ' . (isset($objElement['file_aff_liste_couleur_fond']) ? 'background-color:' . $objElement['file_aff_liste_couleur_fond'] : '') . '"
                                 >
                                     <object
                                         type="application/x-shockwave-flash"
                                         data="' . $objElement['file_visu'] . $sNomFichier . '"
                                         ' . (isset($objElement['file_aff_liste_taille']) ? 'width="' . $objElement['file_aff_liste_taille'] . '"' : '') . '
                                     >
                                         <param name="movie" value="' . $objElement['file_visu'] . $sNomFichier . '" />
                                         <param name="wmode" value="transparent" />
                                         <p>swf non affichable</p>
                                     </object>
                                 </div><br><a href="' . $objElement['file_visu'] . $sNomFichier . '" target="_blank">Voir</a><br>';

                $aTableRetour['value']  =$objValeurBddListe[$objElement['mapping_champ']];
            }else{
               $objValeurBddListe[$objElement['mapping_champ']]['value']='<a target="_blank" href="' . $objElement['file_visu'] . $sNomFichier . '">Voir le fichier</a>';
               $aTableRetour['value']  =$objValeurBddListe[$objElement['mapping_champ']];
            }

            $aTableRetour['keys'] = $objElement['nom_variable'];

        }else{
            $aTableRetour['keys'] = $objElement['nom_variable'];
            $aTableRetour['value'] =$objValeurBddListe[$objElement['mapping_champ']];
        }



        return $aTableRetour;
    }

/**
 * renvoi le format d’affichage dans la cellule pour e type checkbox
 */
public function  renvoi_affiche_cellule_checkbox($objElement,$objValeurBddListe)
{
    $sChaineId="";
    if( $objElement['injection_code'] != "" ) {
        $sChaineId = $objElement['injection_code'];
    }

    $aTableRetour['keys'] = $objElement['alias_champ'];
    $aTmp = array();
    $sChaineAffichage ="";
    $aTmp=$this->renvoi_requete_table_lien($objElement, $objValeurBddListe[$objElement['alias_champ']]['value'],$sChaineId);
    
    //echo"<pre>";print_r($aTmp);echo"</pre>";
    if(!empty($aTmp)){
        foreach($aTmp as $sValeurtmp){
            if($objElement['supprimer_br']){
                $sChaineAffichage.=$sValeurtmp;
            }else{
                $sChaineAffichage.=$sValeurtmp."<br>";
            }
        }
    }

    //echo $sChaineAffichage;
    $objValeurBddListe[$objElement['mapping_champ']]['value']=$sChaineAffichage;
    $aTableRetour['value'] =$objValeurBddListe[$objElement['mapping_champ']];

    //echo"<pre>";print_r($aTableRetour);echo"</pre>";
    return $aTableRetour;
}


    public function renvoi_affiche_liste(){

        $larequete = $this->renvoi_requete();

        if($this->bDebugRequete)
            echo $larequete."<br>";

        $aTableauRetour =array();
        $aValeurBddListe = $aValeurBddListe = $this->objClassGenerique->renvoi_info_requete($larequete);
        
        //echo"<pre>";print_r($aValeurBddListe);echo"</pre>";

        if(!empty($aValeurBddListe)){
            $i=0;
            foreach($aValeurBddListe as $valeubdd){
                foreach ($valeubdd as $key => $value) {
                    $aTableauRetour[$i][$key]=array("value"=>$value,"contenu"=>"");
                }
                $i++;
            }
        }
        //echo"<pre>";print_r($aTableauRetour);echo"</pre>";
        return $aTableauRetour;
    }

    public function renvoi_tabtraduction(){

        $aTableauRetour=array();
        $sNomfonction = class_fli::get_fli_fonction();
        $sModule =  class_fli::get_fli_module();
        $nomclass = get_class($this);

        $sFiltre = $sModule."-".$nomclass."-".$sNomfonction;

        $sRequete_liste_traduction ="SELECT fr_langage,eng_langage,esp_langage,all_langage,identifiant_langage,esp_langage
        from ".class_fli::get_prefixe()."langage where link_langage='".$sFiltre."'";
        $aTableauliste= $this->objClassGenerique->renvoi_info_requete($sRequete_liste_traduction);

        //echo $sRequete_liste_traduction."<br>";

        if(!empty($aTableauliste)){
            foreach($aTableauliste as $valeur){
                $aTableauRetour[$valeur['identifiant_langage']]['fr']=$valeur['fr_langage'];
                $aTableauRetour[$valeur['identifiant_langage']]['eng']=$valeur['eng_langage'];
                $aTableauRetour[$valeur['identifiant_langage']]['all']=$valeur['all_langage'];
                $aTableauRetour[$valeur['identifiant_langage']]['esp']=$valeur['esp_langage'];
            }
        }

        return $aTableauRetour;
    }


    /**
     * @return array
     */
    public function renvoi_liste()
    {
        $aListe = array();

        $iTotaux=array();
        $aTabkeychamp=array();

        if( !isset($this->aListe) ) {

            $sNomMethodeActuelle = "renvoi_affiche_liste_".class_fli::get_fli_fonction();

            if( method_exists($this, $sNomMethodeActuelle) ) {
                $aValeurBddListe = $this->$sNomMethodeActuelle();
            }else {
                $aValeurBddListe = $this->renvoi_affiche_liste();
            }
            //echo $larequete."<br>";

            $this->objSmarty->assign('urlFormList', '');

            if( !empty($aValeurBddListe) ) {
                $aEnteteListe = $this->renvoi_entete_liste();

                $sBaseUrl =
                    $this->renvoi_base_url()
                    . $this->renvoi_parametre_inter_module()
                    . $this->renvoi_parametre_recherche()
                    . $this->renvoi_parametre_filtre_liste()
                    . $this->renvoi_parametre_retour_liste()
                    . $this->renvoi_parametre_retour_validation_form_externe()
                    . $this->renvoi_parametre_retour_annulation_form_externe()
                    . $this->sParametreWizardListe;

                //echo"<pre>";print_r($aValeurBddListe);echo"</pre>";
                $aTableauTraduction = $this->renvoi_tabtraduction();
                $aTableauInfoUser = class_fli::get_aData_entier();
                //echo"<pre>";print_r(class_fli::get_aData_entier());echo"</pre>";
                if(!isset($aTableauInfoUser['langue_user'])){
                    $aTableauInfoUser['langue_user']="fr";
                }
                $sLang =$aTableauInfoUser['langue_user'];


                foreach( $aValeurBddListe as $objValeurBddListe ) {
                    $sLinkKeysMulitple="fi_keymutiple=modif&";
                    foreach( $aEnteteListe as $objEnteteListe ) {

                        //echo"<pre>";print_r($objEnteteListe);echo"</pre>";
                        $objElement = $objEnteteListe['objElement'];
                        $sTypeChamp = $objElement['type_champ'];
                        $sMappingChamp = $objElement['mapping_champ'];

                        if($objElement['type_champ']=="date")
                            $sMappingChamp = $objElement['alias_champ'];
                        $bTotaux = $objElement['activ_totaux'];

                        //réecriture de l'url form si  nus somme ene multiple keys
                        if($this->bKeyMultiple){

                            if($objElement['is_keymultiple']=="ok") {
                                $sLinkKeysMulitple .= $objElement['mapping_champ'] . "=" . $objValeurBddListe[$objElement['mapping_champ']]['value'] . "&";
                            }
                        }

                        if( !empty($this->sChampId) ) {
                            $aListe[$objValeurBddListe[$this->sChampId]['value']]['id'] = $objValeurBddListe[$this->sChampId]['value'];
                        }

                        if(!isset($objValeurBddListe[$objElement['mapping_champ']]) and !isset($objValeurBddListe[$objElement['alias_champ']])   and !in_array($objElement['type_champ'], array( 'injection_code_liste')) ){

                           if($objElement['traite_sql']=="")
                               $objValeurBddListe[$objElement['mapping_champ']]="vide";
                            else
                                echo "Il manque la clé : " . $objElement['mapping_champ'];
                            //TODO: Si valeur retournée par la BDD == NULL --> BUG
                        } else {


                            //creation de la methode qui renvoi methode pour l'affichage de la cellule
                            $sMethodeChampEntete = "renvoi_affiche_cellule_" . $sTypeChamp;

                            //echo"<pre>";print_r($objValeurBddListe);echo"</pre>";

                            if( method_exists($this, $sMethodeChampEntete) ) {
                                $aTabRetour = $this->$sMethodeChampEntete($objElement, $objValeurBddListe);
                            } else {
                                //sinon on appele la method prinicipal
                                $aTabRetour = $this->renvoi_affiche_cellule_principal($objElement, $objValeurBddListe);
                            }

                            //echo"<pre>";print_r($aTabRetour);echo"</pre>";
                            if( $bTotaux=="ok" ) {
                                if(!isset($iTotaux[$sMappingChamp]))
                                    $iTotaux[$sMappingChamp] = $aTabRetour['value']['value'];
                                else
                                    $iTotaux[$sMappingChamp] = $iTotaux[$sMappingChamp] + $aTabRetour['value']['value'];
                            }
                            $aTabkeychamp[$sMappingChamp] = $sMappingChamp;
                            $aTabRetour['value']['html_td']="";
                            //Ici on regarde si il ya une fonction pour l'hanbillge du tr déclaré pour cette focntion
                            $sMethodeHabillagecellule = "renvoi_habillage_cellule_tr_" .class_fli::get_fli_fonction();
                            if( method_exists($this, $sMethodeHabillagecellule) ) {
                                 $aTabRetour['value']['html_td']=$this->$sMethodeHabillagecellule($objElement, $objValeurBddListe);
                            }
                            //Ici on regarde si il ya une fonction pour l'hanbillge du td déclaré en fonction tu type de champ
                            $sMethodeHabillagecelluletd = "renvoi_habillage_cellule_td_" .$sTypeChamp;
                            if( method_exists($this, $sMethodeHabillagecelluletd) ) {
                                $aTabRetour['value']['html_td'].=$this->$sMethodeHabillagecelluletd($objElement, $objValeurBddListe);
                            }
                            //echo"<pre>";print_r($this->aMenuDeroulant);echo"</pre>";

                            $aListe[$objValeurBddListe[$this->sChampId]['value']][$aTabRetour['keys']] = $aTabRetour['value'];

                            $aTmpMenueroulant=array();
                            if(!empty($this->aMenuDeroulant)){
                                foreach($this->aMenuDeroulant as $valeurlistederoulante){
                                    $aTableauDecoupeUrl  = explode("?",$valeurlistederoulante['href']);

                                    //recupération des différentes tradcution
                                    if(isset($aTableauTraduction[$aTableauDecoupeUrl[0]][$sLang]) and $aTableauTraduction[$aTableauDecoupeUrl[0]][$sLang]!=""){
                                        $valeurlistederoulante['intitule']  = $aTableauTraduction[$aTableauDecoupeUrl[0]][$sLang];
                                    }

                                    /*
                                     * Ici on rregarde s'il y a plusieurs variable à remplacer
                                     */
                                    $aTableauExplodeMenuDeroulant = explode(";",$valeurlistederoulante['key_replace']);
                                    $sUrlMenuDeroulant="";

                                    if(count($aTableauExplodeMenuDeroulant)==1) {
                                        $valeurlistederoulante['href'] = str_replace("[" . $valeurlistederoulante['key_replace'] . "]", $objValeurBddListe[$valeurlistederoulante['mapping_champ']]['value'], $valeurlistederoulante['href']);
                                        $sUrlMenuDeroulant = str_replace("[" . $valeurlistederoulante['key_replace'] . "]", $objValeurBddListe[$valeurlistederoulante['mapping_champ']]['value'], $valeurlistederoulante['javascript']);
                                    }else{
                                        //$valeurlistederoulante['href']="";
                                        $aTableauExplodeMenuDeroulantvariale = explode(";",$valeurlistederoulante['mapping_champ']);
                                        $icountMenuderoulant=0;
                                        //echo"<pre>";print_r($aTableauExplodeMenuDeroulant);echo"</pre>";
                                        // echo"<pre>";print_r($aTableauExplodeMenuDeroulantvariale);echo"</pre>";
                                        foreach($aTableauExplodeMenuDeroulant as $valeurmenu){

                                            if(isset($aTableauExplodeMenuDeroulantvariale[$icountMenuderoulant])) {
                                                $valeurlistederoulante['href'] = str_replace("[" . $valeurmenu . "]", $objValeurBddListe[$aTableauExplodeMenuDeroulantvariale[$icountMenuderoulant]]['value'], $valeurlistederoulante['href']);;
                                                $sUrlMenuDeroulant= str_replace("[" . $valeurmenu . "]", $objValeurBddListe[$aTableauExplodeMenuDeroulantvariale[$icountMenuderoulant]]['value'], $valeurlistederoulante['javascript']);;
                                            }
                                            $icountMenuderoulant++;
                                        }




                                    }
                                    //echo"toto<br><br><br>".$sUrlMenuDeroulant;
                                    //$valeurlistederoulante['javascript'] =  str_replace("[".$valeurlistederoulante['mapping_champ']."]", $objValeurBddListe[$valeurlistederoulante['mapping_champ']]['value'],$valeurlistederoulante['javascript']);
                                    $valeurlistederoulante['javascript'] =  $sUrlMenuDeroulant;


                                    $valeurlistederoulante['id'] =$aTableauDecoupeUrl[0];


                                    //Mise en place de blogage sur la ligne pour un menu
                                    if( isset($valeurlistederoulante['bloque']) ) {

                                        $aTabCleBloquant = array_keys($valeurlistederoulante['bloque']);

                                        //echo"<pre>";print_r($aTabCleBloquant);echo"</pre>";

                                        foreach( $aTabCleBloquant as $eCleBloquante ) {
                                            if( isset($aListe[$objValeurBddListe[$this->sChampId]['value']][$eCleBloquante]) ) {



                                                if( $aListe[$objValeurBddListe[$this->sChampId]['value']][$eCleBloquante]['value'] == $valeurlistederoulante['bloque'][$eCleBloquante] ) {

                                                    $valeurlistederoulante['isbloque'] = "y";
                                                    //echo"<pre>";print_r('test');echo"</pre>";
                                                }

                                            } else {
                                                $valeurlistederoulante['isbloque'] = "y";
                                            }

                                            //echo"<pre>";print_r($objValeurBddListe);echo"</pre>";
                                        }


                                    }

                                    $aTmpMenueroulant[] = $valeurlistederoulante;


                                }
                                $aListe[$objValeurBddListe[$this->sChampId]['value']]['aMenuDeroulant']=$aTmpMenueroulant;
                            }
                            
                            //echo"<pre>";print_r($aListe);echo"</pre>";

                            //echo self::$sFiltreChampFils."toto<br>";
                            /*if(!empty(self::$sFiltreChampFils)){
                                $aListe[$objValeurBddListe[$this->sChampId]][self::$sFiltreChampFils] = $objValeurBddListe['sFiltreChampFils'];
                            }*/



                            if(isset($_GET['action']) && $_GET['action'] == 'liste_fils' && !empty(self::$sBaseUrlFils)){
                                $sBaseUrl = self::$sBaseUrlFils.'?';
                            }


                            //echo $sBaseUrl."<br>";

                            if($this->bKeyMultiple){
                                if( $this->bAffFiche ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlFiche'] = $sBaseUrl . '&' . $sLinkKeysMulitple . 'action=fiche';
                                }
                                if( $this->bAffMod ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlForm'] = $sBaseUrl . '&' . $sLinkKeysMulitple . 'action=form';
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlDuplicate'] = $sBaseUrl . '&' . $sLinkKeysMulitple . 'action=form&duplicate=ok';
                                }
                                if( $this->bAffSupp ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlSupp'] = $sBaseUrl . '&' . $sLinkKeysMulitple . '&action=supp';
                                }
                                if( $this->bAffDup ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlDuplicate'] = $sBaseUrl . '&' . $sLinkKeysMulitple . '&action=duplicate';
                                }
                                if( $this->bAffRecapLigne ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlRecapLigne'] = $sBaseUrl . '&' . $sLinkKeysMulitple . '&action=form&sTypeSortie=visu';
                                }
                                if( $this->bActiveFormSelect ) {
                                    //$aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlFormList'] = $sBaseUrl . '&action=formList';
                                    //$this->objSmarty->assign('urlFormList', $aListe[$objValeurBddListe[$this->sChampId]]['sUrlFormList']);
                                    $this->objSmarty->assign('urlFormList', $sBaseUrl . '&action=formList');
                                }
                            }else {

                                if( $this->bAffFiche ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlFiche'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId]['value'] . '&action=fiche';
                                }
                                if( $this->bAffMod ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlForm'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId]['value'] . '&action=form';
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlDuplicate'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId]['value'] . '&action=form&duplicate=ok';
                                }
                                if( $this->bAffSupp ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlSupp'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId]['value'] . '&action=supp';
                                }
                                if( $this->bAffDup ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlDuplicate'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId]['value'] . '&action=duplicate';
                                }
                                if( $this->bAffRecapLigne ) {
                                    $aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlRecapLigne'] = $sBaseUrl . '&' . $this->sChampId . '=' . $objValeurBddListe[$this->sChampId]['value'] . '&action=form&sTypeSortie=visu';
                                }
                                if( $this->bActiveFormSelect ) {
                                    //$aListe[$objValeurBddListe[$this->sChampId]['value']]['sUrlFormList'] = $sBaseUrl . '&action=formList';
                                    //$this->objSmarty->assign('urlFormList', $aListe[$objValeurBddListe[$this->sChampId]]['sUrlFormList']);
                                    $this->objSmarty->assign('urlFormList', $sBaseUrl . '&action=formList');
                                }
                            }

                        }




                    }
                }

            }
        }
        //echo"<pre>";print_r($iTotaux);echo"</pre>";
        //echo"<pre>";print_r($aTabkeychamp);echo"</pre>";

        //on rajouter les totaux dans la liste quand elle n'est pas vide
        if(!empty($iTotaux)){
            $aListe['totaux']=array();
            $aListe['totaux']['sUrlForm']="";
            $aListe['totaux']['sUrlDuplicate']="";
            $aListe['totaux']['sUrlSupp']="";
            $aListe['totaux']['id']="totaux";
            foreach( $aTabkeychamp as $askey ) {
                $aListe['totaux'][$askey]=array();
                $aListe['totaux'][$askey]['contenu']="";
                $aListe['totaux'][$askey]['html_td']="";

                if(isset($iTotaux[$askey]))
                    $aListe['totaux'][$askey]['value']=$iTotaux[$askey];
                else
                    $aListe['totaux'][$askey]['value']="";

            }
        }
        //echo"<pre>";print_r($aListe);echo"</pre>";

        $this->aListe = $aListe;
        return $this->aListe;
    }


    public function entete_principal($objElement)
    {

        $aTabRetour = array();
        
        //echo"<pre>";print_r($objElement);echo"</pre>";

        if( isset($objElement['aff_liste']) && $objElement['aff_liste'] == 'ok' ) {
            $sUrl = $this->renvoi_base_url() . $this->renvoi_parametre_inter_module() . $this->renvoi_parametre_recherche() . $this->renvoi_parametre_retour_liste() . $this->sParametreWizardListe;
            if( isset($objElement['text_label']) && is_string($objElement['text_label']) ) {
                $sLabel = $objElement['text_label'];
            } else {
                echo 'Erreur: class_form_list::renvoi_entete_liste, l\'attribut "text_label" est manquant dans l\'element ci-dessous';
                echo '<pre>';
                print_r($objElement);
                echo '</pre>';
                exit(0);
            }

            if( isset($objElement['aff_filtre']) && $objElement['aff_filtre'] ) {
                if( isset($objElement['nom_variable']) && is_string($objElement['nom_variable']) && $objElement['nom_variable'] != '' ) {
                    $sNomVariable = $objElement['nom_variable'];
                } else {
                    echo 'Erreur: class_form_list::renvoi_entete_liste, l\'attribut "nom_variable" est manquant ou non rempli dans l\'element ci-dessous';
                    echo '<pre>';
                    print_r($objElement);
                    echo '</pre>';
                    exit(0);
                }
                $sTypeFiltre = (isset($_GET['filtreliste']) && $_GET['filtreliste'] == $objElement['nom_variable'] && isset($_GET['typefiltreliste']) && $_GET['typefiltreliste'] == 'asc') ? 'desc' : 'asc';
                $sUrl .= '&filtreliste=' . $sNomVariable . '&typefiltreliste=' . $sTypeFiltre;
            }

            $aTabRetour = array( 'sUrl' => $sUrl, 'sLabel' => $sLabel, 'objElement' => $objElement, 'sAction' => 'href' );

        }



        return $aTabRetour;
    }

    /**
     * @method renvoi_entete_liste calcul et renvoi les entetes de la liste
     * @use aff_liste aff_filtre nom_variable text_label filtreliste typefiltreliste
     */
    public function renvoi_entete_liste()
    {
        if( !isset($this->aEnteteListe) ) {
            /**
             * @var aEnteteListe tableau contenant les entetes de la liste pour l'affichage dans le template
             */
            $aEnteteListe = array();
            $aTableauInfoUser = class_fli::get_aData_entier();
            //echo"<pre>";print_r(class_fli::get_aData_entier());echo"</pre>";
            if(!isset($aTableauInfoUser['langue_user'])){
                $aTableauInfoUser['langue_user']="fr";
            }

            $sLang =$aTableauInfoUser['langue_user'];
            $aTableauTraduction = $this->renvoi_tabtraduction();

            //echo"<pre>";print_r($aTableauTraduction);echo"</pre>";
            $aTableauRetourTitre=array();
            foreach( $this->aElement as $objElement ) {

                //echo $objElement['nom_variable']."<br>\n";

                if(isset($aTableauTraduction[$objElement['nom_variable']][$sLang]) and $aTableauTraduction[$objElement['nom_variable']][$sLang]!=""){
                    $objElement['text_label'] = $aTableauTraduction[$objElement['nom_variable']][$sLang];
                }

                if(($objElement['aff_liste']=="ok" ) and $objElement['transfert_inter_module']!="ok"  ) {
                    //ici on ecrit la composition de la fonction entete specifique au champ
                    $sMethodeChampEntete = "entete_" . $objElement['type_champ'];
                    if( method_exists($this, $sMethodeChampEntete) ) {
                        $aEnteteListe[] = $this->$sMethodeChampEntete($objElement);
                    } else {
                        //sinon on appele la method prinicipal
                        $aEnteteListe[] = $this->entete_principal($objElement);
                    }
                }

                if( isset($objElement['titre_inter_module']) ) {
                    if( $objElement['titre_inter_module'] == "ok" ) {
                        //on rcupere l'identifiant
                        $sIdfiltre = "";
                        if( isset($_GET[$objElement['nom_variable']]) ) {
                            $sIdfiltre = $_GET[$objElement['nom_variable']];
                        }
                        if( isset($_POST[$objElement['nom_variable']]) ) {
                            $sIdfiltre = $_GET[$objElement['nom_variable']];
                        }
                        $sIdfiltre = "";
                        if( isset($_GET[$objElement['nom_variable']]) ) {
                            $sIdfiltre = $_GET[$objElement['nom_variable']];
                        }
                        if( isset($_POST[$objElement['nom_variable']]) ) {
                            $sIdfiltre = $_GET[$objElement['nom_variable']];
                        }
                        if( isset($objElement['bbd_titre_inter_module']) && isset($objElement['champfiltre_titre_inter_module']) && isset($objElement['champaffiche_titre_inter_module']) ) {

                            $sRequete_info_titre = "SELECT " . $objElement['champaffiche_titre_inter_module'] . " as nom from " . $objElement['bbd_titre_inter_module'] . " where " . $objElement['champfiltre_titre_inter_module'] . "='" . $sIdfiltre . "'";
                            //echo $sRequete_info_titre . "<br>";
                            $aTableauRetourTitre = $this->objClassGenerique->renvoi_info_requete($sRequete_info_titre);
                        }
                    }
                }
            }



            if(isset($aTableauTraduction['fli_titreliste'][$sLang]) and $aTableauTraduction['fli_titreliste'][$sLang]!=""){
                $this->sTitreListe = $aTableauTraduction['fli_titreliste'][$sLang];
                if(!empty($aTableauRetourTitre)){
                    $this->sTitreListe =sprintf($this->sTitreListe,$aTableauRetourTitre[0]['nom']);
                }
            }


            if(isset($aTableauTraduction['fli_titrerech'][$sLang]) and $aTableauTraduction['fli_titrerech'][$sLang]!=""){
                $this->sLabelRecherche = $aTableauTraduction['fli_titrerech'][$sLang];
                if(!empty($aTableauRetourTitre)){
                    $this->sLabelRecherche =sprintf($this->sLabelRecherche,$aTableauRetourTitre[0]['nom']);
                }
            }


            if(isset($aTableauTraduction['fli_textnbrligne'][$sLang]) and $aTableauTraduction['fli_textnbrligne'][$sLang]!=""){
                $this->sLabelNbrLigne = $aTableauTraduction['fli_textnbrligne'][$sLang];
                if(!empty($aTableauRetourTitre)){
                    $this->sLabelNbrLigne =sprintf($this->sLabelNbrLigne,$aTableauRetourTitre[0]['nom']);
                }
            }


            if(isset($aTableauTraduction['fli_btaj'][$sLang]) and $aTableauTraduction['fli_btaj'][$sLang]!=""){
                $this->sLabelCreationElem = $aTableauTraduction['fli_btaj'][$sLang];
                if(!empty($aTableauRetourTitre)){
                    $this->sLabelCreationElem =sprintf($this->sLabelCreationElem,$aTableauRetourTitre[0]['nom']);
                }
            }

            //$aEnteteListe[] = array( 'sUrl' => $sUrl, 'sLabel' => $sLabel, 'objElement' => $objElement );
            $this->aEnteteListe = $aEnteteListe;
        }
        // echo"<pre>";print_r($this->aEnteteListe);echo"</pre>";
        return $this->aEnteteListe;
    }


    /**
     * @param $objElement
     * @param $id
     * @param string $injectionHtml
     * @return array
     */
    public function renvoi_requete_table_lien($objElement, $id, $injectionHtml = "")
    {

        if( is_string($objElement['table_lien']) ) {
            $sRequete = '
				SELECT DISTINCT item.' . $objElement['affichage_table_item'] . '
				FROM ' . $objElement['table_item'] . ' item
				JOIN ' . $objElement['table_lien'] . ' lien
				ON lien.' . $objElement['id_item_table_lien'] . ' = item.' . $objElement['id_table_item'] . '
				WHERE lien.' . $objElement['supplogique_table_lien'] . ' = \'N\'
				AND lien.' . $objElement['mapping_champ'] . ' = \'' . $id . '\'
				AND item.' . $objElement['supplogique_table_item'] . ' = \'N\'';

            if($objElement['bdebug'] )
                echo $sRequete . "<br>\n";

            $aResultat = $this->objClassGenerique->renvoi_info_requete($sRequete);

            $aRetour = array();

            if( $aResultat ) {
                foreach( $aResultat as $objResultat ) {
                    $aRetour[] = $objResultat[$objElement['affichage_table_item']];
                }
            }
        } else {
            if(
                is_array($objElement['table_lien'])
                && is_array($objElement['supplogique_table_lien'])
                && is_array($objElement['id_table_lien'])
                && is_array($objElement['id_item_table_lien'])
                && count($objElement['table_lien']) > 0
                && count($objElement['table_lien']) == count($objElement['supplogique_table_lien'])
                && count($objElement['supplogique_table_lien']) == count($objElement['id_table_lien'])
                && count($objElement['id_table_lien']) == count($objElement['id_item_table_lien'])
            ) {


                $sAlias = 'item';

                if( $objElement['alias_table_lien'] != "" )
                    $sAlias = $objElement['alias_table_lien'];

                $sRequetsuiteidlien = "";
                if( $objElement['id_liaison'] != "" )
                    $sRequetsuiteidlien = ',' . $objElement['id_liaison'] . ' as lid ';


                /*if( preg_match("/<bdd>/", $objElement['affichage_table_item']) ) {
                    $objElement['affichage_table_item'] = str_replace("<bdd>", $sAlias, $objElement['affichage_table_item']);
                }*/



                //if( preg_match("/\[bdd\]/", $objElement['affichage_table_item']) ) {
                    $objElement['affichage_table_item'] = str_replace("[bdd]", $sAlias, $objElement['affichage_table_item']);

                    //echo   "totot".$objElement['affichage_table_item'] ."<br>\n"; exit();
                //}
                // on rajoutre les champ suplemetaire à afficher s'il y en a
                $sLesChampSuppItem="";
                if(!empty($objElement['item_table_champ_suplementaire'] )){
                    foreach($objElement['item_table_champ_suplementaire'] as $sValeurItemSup){
                        $sLesChampSuppItem.= ",".$sValeurItemSup['mapping_champ'];
                    }
                }

                $sRequeteSelectFrom = 'SELECT DISTINCT '.$sAlias."." . $objElement['affichage_table_item'] . ' as nom ' . $sLesChampSuppItem.$sRequetsuiteidlien . ' FROM ' . $objElement['table_item'] . ' as ' . $sAlias . ' ';
                $sRequeteJoin = '';


                for( $i = count($objElement['table_lien']) - 1; $i >= 0; $i-- ) {
                    if( $i == count($objElement['table_lien']) - 1 ) {
                        $sRequeteJoin .= ' JOIN ' . $objElement['table_lien'][$i] . ' lien' . $i . ' ON lien' . $i . '.' . $objElement['id_item_table_lien'][$i] . ' = ' . $sAlias . '.' . $objElement['id_table_item'];
                    } else {
                        $sRequeteJoin .= ' JOIN ' . $objElement['table_lien'][$i] . ' lien' . $i . ' ON lien' . $i . '.' . $objElement['id_item_table_lien'][$i] . ' = lien' . ($i + 1) . '.' . $objElement['id_table_lien'][$i + 1];
                    }

                }

                $sRequeteJoin .= ' JOIN ' . $this->sNomTable . ' principal ON lien0.' . $objElement['id_table_lien'][0] . ' = principal.' . $objElement['mapping_champ'];

                $sRequeteWhere = ' WHERE principal.' . $objElement['mapping_champ']. ' = \'' . $id . '\' AND ' . $sAlias . '.' . $objElement['supplogique_table_item'] . ' = \'N\'';

                for( $i = count($objElement['table_lien']) - 1; $i >= 0; $i-- ) {
                    $sRequeteWhere .= ' AND lien' . $i . '.' . $objElement['supplogique_table_lien'][$i] . ' = \'N\'';
                }

                $sRequete = $sRequeteSelectFrom . $sRequeteJoin . $sRequeteWhere;

                $sRequete .= $objElement['requete_supplementaire_select']." ".$objElement['requete_supplementaire_select_lien'];

                if($objElement['bdebug'])
                    echo $sRequete . "<br>\n";

                if( $this->bDebugRequete )
                    echo $sRequete;

                $aResultat = $this->objClassGenerique->renvoi_info_requete($sRequete);

                $aRetour = array();

                if( $aResultat ) {


                    foreach( $aResultat as $objResultat ) {


                        if(!empty($objElement['item_table_champ_suplementaire'] )) {
                            foreach( $objElement['item_table_champ_suplementaire'] as $sValeurItemSup ) {

                                if(isset($sValeurItemSup['aff_liste']) and $sValeurItemSup['aff_liste']=="ok")
                                     $objResultat['nom'].=" ". $objResultat[$sValeurItemSup['mapping_champ']];
                            }
                        }

                        if( $injectionHtml != "" ) {
                            $objResultat['nom'] = str_replace("<value>", $objResultat['nom'], $injectionHtml);
                            $objResultat['nom'] = str_replace("<id>", $objResultat['lid'], $objResultat['nom']);
                        }

                        $aRetour[] = $objResultat['nom'];
                    }
                }
            } else {
                $aRetour = array();
            }
        }

        class_fli::set_debug("class_listrenvoi_requete_table_lien",$sRequete);
        //echo"<pre>";print_r($aRetour);echo"</pre>";
        return $aRetour;
    }

    /**
     * @return null|string
     */
    public function renvoi_parametre_retour_liste_tableau()
    {
        if( !isset($this->sParametreRetourListeTableau) ) {
            $sParametreRetourListeTableau = null;

            $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $this->sRetourListe, $aMatches);
            if( $iNombreMatches > 0 ) {
                $aChamp = array_unique($aMatches[1]);
                foreach( $aChamp as $sChamp ) {
                    if( isset($_GET[$sChamp]) ) {
                        $sParametreRetourListeTableau[$sChamp] = $_GET[$sChamp];
                    }
                }
            }

            $this->sParametreRetourListeTableau = $sParametreRetourListeTableau;
        }

        return $this->sParametreRetourListeTableau;
    }
    /*
     * methode renvoi obj recherche principal
     */
    public function renvoi_recherche_principal($objElement){
        return $objElement;
    }
    /*
     * Methode renvoi obj rech select
     */
    public function renvoi_recherche_select($objElement){

        if(empty($objElement['lesitem'])) {


            //echo  "ddd".$objElement['requete_supplementaire_select'];

             $objElement['affichage_table_item'] = str_replace("[bdd]", $objElement['table_item'], $objElement['affichage_table_item']);
             $objElement['lesitem'] = $this->renvoi_tableau_liste($objElement['id_table_item'],
                $objElement['affichage_table_item'],
                $objElement['table_item'],
                $objElement['supplogique_table_item'],
                $objElement['select_pere_fils'],
                "",
                0,
                array(),
                0,
                 "",
                 "",
                 $objElement['requete_supplementaire_select'],
                 $objElement['alias_table_lien']
            );
        }
        return $objElement;
    }

    public function renvoi_recherche_selectdist($objElement){
        return $this->renvoi_recherche_select($objElement);
    }
    /*
     * Methode renvoi obj rech checkbox
     */

    public function renvoi_recherche_checkbox($objElement){
        return $this->renvoi_recherche_select($objElement);
    }

    /*
    * Methode renvoi obj rech checkbox
    */

    public function renvoi_recherche_selectmultiple($objElement){
        return $this->renvoi_recherche_select($objElement);
    }
    /**
     * Methode renvoi du formulaire recherche
     * @author Danon Gnakouri
     * @since  2.0
     * @return d'un tableau cles valeurs
     */
    public function renvoi_recherche()
    {

        $aRech = array();
        $aTableauInfoUser = class_fli::get_aData_entier();

        if(isset($aTableauInfoUser['langue_user']) and $aTableauInfoUser['langue_user']!="")
            $sLang =$aTableauInfoUser['langue_user'];
        else
            $sLang="fr";
        $aTableauTraduction = $this->renvoi_tabtraduction();

        foreach( $this->aElement as $objElement ) {


            if( (isset($objElement['aff_recherche']) && $objElement['aff_recherche'] == 'ok') or $objElement['transfert_inter_module']=="ok") {

                if( isset($_GET["rech_".$objElement['nom_variable']]) ) {

                    $objElement['valeur_variable'] = $_GET["rech_".$objElement['nom_variable']];

                }

                if(isset($aTableauTraduction[$objElement['nom_variable']][$sLang]) and $aTableauTraduction[$objElement['nom_variable']][$sLang]!=""){
                    $objElement['text_label'] = $aTableauTraduction[$objElement['nom_variable']][$sLang];
                }





                //creation de la methode qui renvoi methode pour l'affichage de la valeur
                $sTypeChamp = $objElement['type_champ'];
                $sMethodeChampDonne = "renvoi_recherche_".$sTypeChamp;
                if($objElement['transfert_inter_module']!="ok"){
                    if( method_exists($this, $sMethodeChampDonne) ) {
                        $aTmp = $this->$sMethodeChampDonne($objElement);
                        $aTmp['nom_variable'] = "rech_" . $aTmp['nom_variable'];
                        $aRech[] = $aTmp;
                    } else {
                        //sinon on appelle la method prinicipal
                        $aTmp = $this->renvoi_recherche_principal($objElement);
                        $aTmp['nom_variable'] = "rech_" . $aTmp['nom_variable'];
                        $aRech[] = $aTmp;
                    }
                }
                //$aRech[] = $objElement;
                //echo "toto";




            if($objElement['transfert_inter_module']=="ok"){
                if( isset($_GET[$objElement['nom_variable']]) ) {
                    $objElement['valeur_variable'] = $_GET[$objElement['nom_variable']];
                }
                $aRech[]=$objElement;
            }
        }


        }

        //echo"<pre>";print_r($aRech);echo"</pre>";

        return $aRech;
    }

    public function renvoi_si_filtre_recherche_actif($aRech)
    {

        foreach( $aRech as $objRech ) {
            if( !isset($objRech['recherche_filtre_actif']) || $objRech['recherche_filtre_actif'] !== false ) {
                if( isset($objRech['valeur_variable']) ) {
                    if( is_string($objRech['valeur_variable']) && $objRech['valeur_variable'] != '' ) {
                        return true;
                    } else if( is_array($objRech['valeur_variable']) && count($objRech['valeur_variable']) > 0 ) {
                        foreach( $objRech['valeur_variable'] as $sValeurVariable ) {
                            if( $sValeurVariable != '' ) {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Method qui renvoi le tableau à un champ précis
     * @param $sMapping
     * @return array
     */
    public function renvoi_tab_mapping_champ($sMapping){
        $aTableauRetour=array();

        $this->renvoi_valeur_bdd();
       $aTableauvaleur = $this->renvoi_formulaire();
       
      //echo"<pre>";print_r($aTableauvaleur);echo"</pre>";
        ////echo $sMapping;
       if(!empty($aTableauvaleur)){
           foreach($aTableauvaleur as $valeur){

               if($valeur['nom_variable']==$sMapping){

                   $aTableauRetour=$valeur;
               }
           }
       }

        return $aTableauRetour;
    }

    /**
     * renvoi le tableau de pagination ainsi que  l’endroit ou on se trouve
     */
    public function renvoi_tableau_pagination()
    {

    }

    public function renvoi_url_retour_liste()
    {
        if( !isset($this->sUrlRetourListe) ) {
            $sUrlRetourListe = $this->sRetourListe;

            $iNombreMatches = preg_match_all('/{{\$([^\${}]+)}}/', $sUrlRetourListe, $aMatches);
            if( $iNombreMatches > 0 ) {
                $aChamp = array_unique($aMatches[1]);
                foreach( $aChamp as $sChamp ) {
                    if( isset($_GET[$sChamp]) ) {
                        $sUrlRetourListe = str_replace('{{$' . $sChamp . '}}', $_GET[$sChamp], $sUrlRetourListe);
                    }
                }
            }

            $this->sUrlRetourListe = $sUrlRetourListe;
        }

        return $this->sUrlRetourListe;
    }

    /**
     * @param string $sJointureSup
     */
    public function setSJointureSup($sJointureSup)
    {
        $this->sJointureSup = $sJointureSup;
    }

    /**
     * @param array $aMenuDeroulant tableau passé avec cles href, javascript, intitule, attribut
     */
    public function set_menu_deroulant($aMenuDeroulant)
    {
        $this->aMenuDeroulant[] = $aMenuDeroulant;
    }

    /**
     * @param array $aMenuDeroulant tableau passé avec cles href, javascript, intitule, attribut
     */
    public function set_filtre_col_liste($aTabColList)
    {
        $this->aTabFiltreColHtmlListe[] = $aTabColList;
    }


    /**
     * @param string $sParametreWizardNomVariableSuivant
     */
    public function set_parametre_wizard_nom_variable_suivant($sParametreWizardNomVariableSuivant)
    {
        $this->sParametreWizardNomVariableSuivant = $sParametreWizardNomVariableSuivant;
    }

    /**
     * @param string $sParametreWizardSuivant
     */
    public function set_parametre_wizard_suivant($sParametreWizardSuivant)
    {
        $this->sParametreWizardSuivant = $sParametreWizardSuivant;
    }

    /**
     * @param boolean $sParametrebWizard
     */
    public function set_parametre_bwizard($sParametrebWizard)
    {
        $this->sParametrebWizard = $sParametrebWizard;
    }


    public function suppression_entree()
    {
        //echo"<pre>";print_r($this);echo"</pre>";
        //echo "id=>".$this->iId;

        if( $this->iId == null and !$this->bKeyMultiple ) {
            return false;
        }

        $sSuiterequetesupp = "";
        $sFiltre="";
        $bpassand=false;
        foreach( $this->aElement as $objElement ) {

            if( $objElement['traite_sql'] == 'ok' ) {
                if( $objElement['type_champ'] == 'checkbox' ) {
                    $date_modification_table_lien = '';
                    if( isset($objElement['date_modification_table_lien']) && $objElement['date_modification_table_lien'] != '' ) {
                        $date_modification_table_lien = $objElement['date_modification_table_lien'] . ' = \'' . date('Y-m-d H:i:s') . '\'';
                    }
                }

                if($this->bKeyMultiple){
                    if($objElement['is_keymultiple']=="ok"){

                        if($bpassand)
                            $sFiltre.=" and ";

                        $sFiltre.=$objElement['mapping_champ']." ='".$_GET[$objElement['mapping_champ']]."' ";
                        $bpassand=true;
                    }
                }

            }


            if( $objElement['date_now_modification'] == 'ok' ) {
                $objElement['valeur_variable'] = date('Y-m-d H:i:s');
                $sSuiterequetesupp = "," . $objElement['mapping_champ'] . '=\'' . $objElement['valeur_variable'] . '\'';

            }

        }

        if($this->bKeyMultiple){
            if( $this->bUseDelete ) {
                $sRequete_supp = "DELETE FROM " . $this->sNomTable . " WHERE " . $sFiltre . "";
            } else {
                $sRequete_supp = "UPDATE " . $this->sNomTable . " SET " . $this->sChampSupplogique . " = '".$this->sValOFFSupplogique."' " . $sSuiterequetesupp . " 
                WHERE " .$sFiltre . "";
            }
        }else {
            if( $this->bUseDelete ) {
                $sRequete_supp = "DELETE FROM " . $this->sNomTable . " WHERE " . $this->sChampId . " = '" . $this->iId . "'";
            } else {
                $sRequete_supp = "UPDATE " . $this->sNomTable . " SET " . $this->sChampSupplogique . " = '".$this->sValOFFSupplogique."' " . $sSuiterequetesupp . " 
                WHERE " . $this->sChampId . " = '" . $this->iId . "'";
            }
        }
        //echo "toto=>".$sRequete_supp."<br>";

        if( $this->objClassGenerique->tl_exec($sRequete_supp) ) {
            $this->aTabResultSupp['id'] = $this->iId;
            $this->iId = null;
            $this->aTabResultSupp['result'] = "ok";

            return true;
        }

        return false;
    }

    /*
 * Function permettant de duplicate une  ligne
 */

    public function fli_action_duplicate(){

        $aTableauretour=array();
        $aTableauretour['sMessge']="";
        $aTableauretour['urlRetour']="";
        $aTableauretour['bsucces']=false;
        $aTableauretour['berror']=false;
        //echo "passe duplicate ".$this->iId." ".$this->sChampId." ".$this->sChampGuid." ".$this->sChampIdTmp;
        if( $this->iId == null ) {
            return false;
        }

        $sRequete_liste ="SELECT * FROM  ";
        $sRequete_liste.= $this->sNomTable ." where ";
        $sRequete_liste.=$this->sChampId . " = '";
        $sRequete_liste.= $this->iId . "'";

        //echo $sRequete_liste."<br>";

        /*
         * On traite ici la liste des
         */
        $aTableauFilte=array();
        if($this->sListeFitreNopDuplicate!=""){
            $aTableauFilte =  explode(";",$this->sListeFitreNopDuplicate);
        }

        $aTabRetour = $this->objClassGenerique->renvoi_info_requete($sRequete_liste);

        //echo"<pre>";print_r($aTabRetour);echo"</pre>";
        $sRequeteduplicate="Insert ".$this->sNomTable." SET ".$this->sChampId."='".class_helper::guid()."'";
        if(!empty($aTabRetour)){
            $aTabtmp = $aTabRetour[0];
            foreach($aTabtmp as $key=>$value){
               if($key!=$this->sChampGuid and $key!=$this->sChampIdTmp and !in_array($key,$aTableauFilte)){
                    $sRequeteduplicate.=",".$key."='".addslashes($value)."'";
                }
            }

            //echo $sRequeteduplicate."<br>";
            if( $this->objClassGenerique->tl_exec($sRequeteduplicate) ) {
                $this->aTabResultSupp['id'] = $this->iId;
                $this->iId = null;
               $aTableauretour['sMessge'] = $this->sMessageDuplicationSucces;
                $aTableauretour['bsucces']=true;
            }else{
                $aTableauretour['sMessge'] = $this->sMessageDuplicationError;
                $aTableauretour['berror']=true;
            }
        }

        return  $aTableauretour;
    }


    public function validation_supplementaire_formulaire()
    {

        return array( true, null );
    }



}