<!doctype html> <!--
paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <!-- DNS prefetch -->
    <link rel=dns-prefetch href="//fonts.googleapis.com">
    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
    More info: h5bp.com/b/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width,initial-scale=1">
    {if !empty($metaModules)}
        {foreach from=$metaModules item=metaModule key=cle}
            <meta name="{$cle}" content="{$metaModule}">
        {/foreach}
    {/if}
    <title>{$sTitreDeLaPage}</title>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory:
    mathiasbynens.be/notes/touch-icons -->

    <!-- CSS -->
    <link rel="stylesheet" href="vues/version2/css/960gs/fluid.css"> <!-- 960.gs Grid System -->
    <!-- The HTML5-Boilerplate CSS styling -->
    <link rel="stylesheet" href="vues/version2/css/h5bp/normalize.css"> <!-- RECOMMENDED: H5BP reset styles -->
    <link rel="stylesheet" href="vues/version2/css/h5bp/non-semantic.helper.classes.css"> <!-- RECOMMENDED: H5BP helpers (e.g. .clear or .hidden) -->
    <link rel="stylesheet" href="vues/version2/css/h5bp/print.styles.css"> <!-- OPTIONAL: H5BP print styles -->
    <!-- The main styling -->
    <link rel="stylesheet" href="vues/version2/css/sprites.css"> <!-- STRONGLY RECOMMENDED: Basic sprites (e.g. buttons, jGrowl) -->
    <link rel="stylesheet" href="vues/version2/css/header.css"> <!-- REQUIRED: Header styling -->
    <link rel="stylesheet" href="vues/version2/css/navigation.css"> <!-- REQUIRED: Navigation styling -->
    <link rel="stylesheet" href="vues/version2/css/sidebar.css"> <!-- OPTIONAL: Sidebar -->
    <link rel="stylesheet" href="vues/version2/css/content.css"> <!-- REQUIRED: Content styling -->
    <link rel="stylesheet" href="vues/version2/css/footer.css"> <!-- REQUIRED: Footer styling -->
    <link rel="stylesheet" href="vues/version2/css/typographics.css"> <!-- REQUIRED: Typographics -->
    <link rel="stylesheet" href="vues/version2/css/ie.fixes.css"> <!-- OPTIONAL: Fixes for IE7 -->
    <!-- Sprites styling -->
    <link rel="stylesheet" href="vues/version2/css/sprite.forms.css"> <!-- SPRITE: Forms styling -->
    <link rel="stylesheet" href="vues/version2/css/sprite.lists.css"> <!-- SPRITE: Lists styling -->
    <link rel="stylesheet" href="vues/version2/css/icons.css"> <!-- Icons -->
    <link rel="stylesheet" href="vues/version2/css/sprite.tables.css"> <!-- SPRITE: Tables styling -->
    <!-- Styling of JS plugins -->
    <link rel="stylesheet" href="vues/version2/css/external/jquery-ui-1.8.16.custom.css">	<!-- PLUGIN: jQuery UI styling -->

    <script type="text/javascript" src="vues/version2/js/tooltip.js"></script>

    <!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
    <link rel="stylesheet" href="vues/version2/css/sidebar.css"> <!-- TODO: Remove this line! -->
    <!-- All JavaScript at the bottom, except this Modernizr build incl. Respond.js
    Respond is a polyfill for min/max-width media queries. Modernizr enables HTML5
    elements & feature detects;
    for optimal performance, create your own custom Modernizr build:
    www.modernizr.com/download/ -->
    <script src="vues/version2/js/libs/modernizr-2.0.6.min.js"></script>
</head>
<body>
<!-- Begin of #height-wrapper -->
<div id="height-wrapper">
    <!-- Begin of header -->
    <header>
        <div id="header_toolbar">
            <div class="container_12">
                <h1 class="grid_8">{$sTitreDeLaPage}</h1>
                <!-- Start of right part -->
                <div class="grid_4">

                    <!-- A large toolbar button -->
                    <div class="toolbar_large">
                        <div class="toolbutton">
                            {if $bEstConnecte}
                            <div class="toolicon">
                                <img src="vues/version2/img/icons/16x16/user.png" width="16" height="16" alt="user" >
                            </div>
                            <div class="toolmenu">
                                <div class="toolcaption">
                                    <span>Administration</span>
                                </div>
                                <!-- Start of menu -->
                                <div class="dropdown">
                                    <ul>
                                        <li>
                                            <a href="main.php?dir=config">configuration</a>
                                        </li>

                                        <li>
                                            <a href="deco.php">Logout</a>
                                        </li>
                                    </ul>
                                </div> <!-- End of menu -->
                            </div>
                            {/if}
                        </div>
                    </div> <!-- End of large toolbar button -->
                </div>
                <!-- End of right part -->
            </div>

        </div>
        <!-- Start of the main header bar -->
        <nav id="header_main">
            {if isset($tTplMenu)}
                {$tTplMenu}

                {/if}


        </nav>
        <div id="nav_sub"></div>


    </header>
    {if isset($pagedirection)}
        {if $pagedirection|is_array}
            {foreach from=$pagedirection item=tabTpl}
                {foreach from=$tabTpl item=tpl}
                    {$tpl}
                {/foreach}
            {/foreach}
        {else}
            {$pagedirection}
        {/if}
    {/if}
</div>

<!-- Start of footer-->
<footer>
    <div class="container_12">
        Gnakouri danon dgnakouri@gmail.com.
        <div id="button_bar">
            <ul>
                <li>
                    <span><a href="dashboard.html">Accueil</a></span>
                </li>
                <li>
                    <span><a href="#">Configuration</a></span>
                </li>
            </ul>
        </div>
    </div>
</footer>
<!-- End of footer-->

{literal}
    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Grab Google CDN's jQuery + jQueryUI, with a protocol relative URL; fall back to local -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
    <script>window.jQuery || document.write('<script src="js/libs/jquery-1.7.1.js"><\/script>')</script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
    <script>window.jQuery.ui || document.write('<script src="js/libs/jquery-ui-1.8.16.min.js"><\/script>')</script>

    <!-- scripts concatenated and minified via build script -->
    <script defer src="vues/version2/js/plugins.js"></script> <!-- REQUIRED: Different own jQuery plugnis -->
    <script defer src="vues/version2/js/mylibs/jquery.ba-resize.js"></script> <!-- RECOMMENDED when using sidebar: page resizing -->
    <script defer src="vues/version2/js/mylibs/jquery.easing.1.3.js"></script> <!-- RECOMMENDED: box animations -->
    <script defer src="vues/version2/js/mylibs/jquery.ui.touch-punch.js"></script> <!-- RECOMMENDED: touch compatibility -->
    <script defer src="vues/version2/js/mylibs/jquery.chosen.js"></script>
    <script defer src="vues/version2/js/mylibs/jquery.validate.js"></script>
    <script defer src="vues/version2/js/mylibs/jquery.dataTables.js"></script>
    <script defer src="vues/version2/js/mylibs/jquery.checkbox.js"></script>
    <script defer src="vues/version2/js/mylibs/jquery.ui.timepicker.js"></script> <!-- Needed for <input type=date/datetime/time> -->

    <script defer src="vues/version2/js/script.js"></script> <!-- REQUIRED: Generic scripts -->
                                               <!-- end scripts -->
    <script>
        $(window).load(function() {
            $('#accordion').accordion();
            $(window).resize();
        });
    </script>

    <script defer>
        $(window).load(function() {
            $('#table-example').dataTable();
            $(window).resize();
        });

    </script>

    <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to
    support IE 6.
    chromium.org/developers/how-tos/chrome-frame-getting-started -->
    <!--[if lt IE 7 ]>
    <script defer
            src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script
            defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
    <![endif]-->
{/literal}

</body>
</html>