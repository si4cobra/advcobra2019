<div role="main" class="container_12" id="content-wrapper">

    <div id="main_content" style="overflow:visible">

        <h2 class="grid_12">{$sTitreForm}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_titrefrom','{$sFli_LinkUrl}','{$sLangueUser}','{$sTitreForm|replace:'\'':'\\\''}','{$sNomTable}','fli_titrefrom');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</h2>


        {if isset($bMessageSuccesForm) and $bMessageSuccesForm}
            <div class="alert success">
                <span class="icon"></span><strong>Success</strong><br>
                {if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}
            </div>
        {/if}
        {if isset($bMessageErreurForm) and $bMessageErreurForm}
            <div class="alert error">
                <span class="icon"></span><strong>Error</strong><br>
                {if isset($sMessageErreurForm)}{$sMessageErreurForm}{/if}
            </div>
        {/if}

        <div class="clean"></div>
        {if isset($itemBoutonsForm.annuler) and $bAffAnnuler}
        <ul id="shortcuts" class="shortcuts tab-content">
            <li>
                <div class="shortcut-icon">
                    <a class="shortcut-description" href="{$itemBoutonsForm.annuler.url}"><img src="http://dgentreprises.biz/logicielprospection_v3/img/icons/25x25/blue/bended-arrow-left.png" width="25" height="25"><a>
                            <div class="divider"></div>
                </div>
                <a class="shortcut-description" href={$itemBoutonsForm.annuler.url}"> <strong>Retour</strong>  </a>
            </li>
        </ul>
        {/if}
        <div class="grid_6">
            <div class="box">
                <div class="header">
                    <img src="vue/version1/img/icons/packs/fugue/16x16/application-form.png" alt="" width="16"
                         height="16">
                    <h3>{$sCategorieForm}</h3>
                    <span></span>
                </div>

                <form
                        method="POST"
                        action="{$url_form}{if $bDuplicate}&duplicate=ok{/if}"
                        {if isset($bTelechargementFichier) and $bTelechargementFichier}enctype="multipart/form-data"{/if}
                >
                    <input type="hidden" name="fi_keymutiple" value="{$fi_keymutiple}">
                    <div class="content no-padding">
                    {foreach from=$aForm item=objForm}
                        {if isset($objForm.type_champ) and ((isset($objForm.aff_form) and $objForm.aff_form eq 'ok') or ($objForm.type_champ eq 'category' or $objForm.type_champ eq 'bouton'))}

                            {if $objForm.type_champ eq 'text'}

                                <div class="{if isset($objForm.class_div)}{$objForm.class_div}{else}section _100"{/if}> <label>{$objForm.text_label}{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}<span  style="color:red;">&nbsp;**</span>{/if}  {if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}<span  style="color:red;">&nbsp;**</span>{/if}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</label>
                                            <div >
                                                <input
                                                        type="text"
                                                        class="inp-form"
                                                        id="id_{$objForm.nom_variable}"
                                                        name="{$objForm.nom_variable}"
                                                        {if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
                                                    {if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
                                                        {if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
                                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                {if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
                                                    {foreach from=$objForm.fonction_javascript key=cle item=valeur}
                                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                    {/foreach}
                                                {/if}
                                                >


                                            </div>
                                </div>
                            {elseif $objForm.type_champ eq 'select'}
                        <div class="{if isset($objForm.class_div)}{$objForm.class_div}{else}section _100"{/if}>
                                {if isset($objForm.select_autocomplete) and $objForm.select_autocomplete eq 'ok'}
                                    <tr>
                                        <th valign="top" style="border:0;">{$objForm.text_label_filtre}</th>
                                        <td style="border:0;text-align: left;">
                                            <input type='text'
                                                   name='rech_{$objForm.nom_variable}'
                                                   id='id_rech{$objForm.nom_variable}'
                                                   class="inp-form"
                                            {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                                {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                                    {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                                {/foreach}
                                            {/if}
                                            onKeyUp="affiche_liste_generique('{$objForm.table_item}','{$objForm.id_table_item}','{$objForm.affichage_table_item}','{$objForm.supplogique_table_item}', '{$objForm.tabfiltre_autocomplete}',this.value,'id_form_{$objForm.nom_variable}');">
                                        </td>
                                    </tr>
                                {/if}
                                 <label>{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</label>

                                         <div >
                                        {if $objForm.is_keymultiple=="ok" and $fi_keymutiple=="modif"}

                                            {if is_array($objForm.lesitem)}
                                                {foreach from=$objForm.lesitem key=id_valeur_possible item=valeur_possible_bdd}
                                                    {if $objForm.valeur_variable eq $id_valeur_possible}{$valeur_possible_bdd}
                                                        <input type="hidden" name="{$objForm.nom_variable}" value="{$id_valeur_possible}">
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                        {else}

                                            <select
                                                    id="id_form_{$objForm.nom_variable}"
                                                    name="{$objForm.nom_variable}{if isset($objForm.multiple)}[]{/if}"
                                                    {if isset($objForm.multiple)}multiple{/if}

                                                    {$objForm.fonction_javascript}
                                            >
                                                <option value="" {if $objForm.valeur_variable eq ''}selected{/if}></option>
                                                {if is_array($objForm.lesitem)}
                                                    {foreach from=$objForm.lesitem key=id_valeur_possible item=valeur_possible_bdd}
                                                        <option value="{$id_valeur_possible}" {if $objForm.valeur_variable eq $id_valeur_possible}selected{/if}>
                                                            {$valeur_possible_bdd}
                                                        </option>
                                                    {/foreach}
                                                {/if}
                                            </select>

                                        {/if}
                                         </div>
                               </div>
                            {else}

                            {/if}


                         {/if}


                    {/foreach}
                    </div>

                    <div class="actions">
                        <div class="actions-left">
                            {if isset($itemBoutonsForm.annuler) and $bAffAnnuler}
                                <a href="{$itemBoutonsForm.annuler.url}">
                                    <input
                                            type="button"
                                            class="btn btn-primary btnResForm"
                                            value="{$itemBoutonsForm.annuler.text_label}"
                                    {if isset($itemBoutonsForm.annuler.style) and $itemBoutonsForm.annuler.style neq ''}{/if}
                                    {if isset($itemBoutonsForm.annuler.tableau_attribut) and $itemBoutonsForm.annuler.tableau_attribut|is_array}
                                        {foreach from=$itemBoutonsForm.annuler.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($itemBoutonsForm.annuler.fonction_javascript) and $itemBoutonsForm.annuler.fonction_javascript|is_array}
                                        {foreach from=$itemBoutonsForm.annuler.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                        {/foreach}
                                    {/if}
                                    >
                                </a>
                            {/if}
                        </div>
                        <div class="actions-right">
                            {if isset($itemBoutonsForm.valider)}
                            <input
                                    type="submit"
                                    class=""
                                    id="id_{$itemBoutonsForm.valider.nom_variable}"
                                    name="{$itemBoutonsForm.valider.nom_variable}"
                                    value="{$itemBoutonsForm.valider.text_label}"
                            {if isset($itemBoutonsForm.valider.style) and $itemBoutonsForm.valider.style neq ''}{/if}
                            {if isset($itemBoutonsForm.valider.tableau_attribut) and $itemBoutonsForm.valider.tableau_attribut|is_array}
                                {foreach from=$itemBoutonsForm.valider.tableau_attribut key=cle item=valeur}
                                    {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                {/foreach}
                            {/if}
                            {if isset($itemBoutonsForm.valider.fonction_javascript) and $itemBoutonsForm.valider.fonction_javascript|is_array}
                                {foreach from=$itemBoutonsForm.valider.fonction_javascript key=cle item=valeur}
                                    {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                {/foreach}
                            {/if}
                            >
                            {/if}
                        </div>
                    </div>

                </form>

            </div>
        </div>


    </div>
</div>