{*<div class="container_12">
    <!-- Start of the main navigation -->
    <ul id="nav_main">
        {if isset($aTableauMenu)}
            {section name=sec1 loop=$aTableauMenu}
                <li {$aTableauMenu[sec1][3]}>
                    <a href="main.php?dir={$aTableauMenu[sec1][2]}">
                        <img src="{$aTableauMenu[sec1][0]}" width=25 height=25 alt="">
                        {$aTableauMenu[sec1][1]}</a>
                    <ul>
                        {section name=sec2 loop=$aTableauMenu[sec1][4]}
                            <li {$aTableauMenu[sec1][4][sec2][2]}>
                                <a href="main.php?dir={$aTableauMenu[sec1][4][sec2][1]}">{$aTableauMenu[sec1][4][sec2][0]}</a>
                            </li>
                        {/section}
                    </ul>

                </li>
            {/section}
        {/if}


    </ul>
     End of the main navigation
</div> -->
*}
<div class="container_12">
    <!-- Start of the main navigation -->
    <ul id="nav_main">
        {foreach from=$aTabMenu item=entry}
            {if isset($entry.est_parent)  and $entry.est_parent}
                <li {if $entry.check=="ok"}class="current"{/if}>
                    <a href="{if !empty($entry.fils)}{$entry.fils[0].route_route}{else}#{/if}">
                        <img src="{$entry.icone_routes}" width=25 height=25 alt="">
                        {$entry.intitule_menu_route}</a>
                        {if is_array($entry.fils)}
                          <ul>
                            {foreach from=$entry.fils item=entryfils}
                                <li >
                                    <a href="{$entryfils.route_route}">{$entryfils.intitule_menu_route}</a>
                                </li>
                            {/foreach}
                         </ul>
                        {/if}
                </li>
            {/if}
        {/foreach}
    </ul>
</div>