<div role="main" class="container_12" id="content-wrapper">

    <div id="main_content" style="overflow:visible">

        <h2 class="grid_12">{$sTitreListe}</h2>
        <div class="clean"></div>
        <ul id="shortcuts" class="shortcuts tab-content">
            <li>
                <div class="shortcut-icon">
                    <a class="shortcut-description" href="{$sLabelCreationElemUrl}"><img src="http://dgentreprises.biz/logicielprospection_v3/img/icons/25x25/blue/list-w--images.png" width="25" height="25"></a>
                    <div class="divider"></div>
                </div>
                <a class="shortcut-description" href="{$sLabelCreationElemUrl}"> <strong> {$sLabelCreationElem}</strong>  </a>
            </li>
            {if isset($sRetourListe) and $sRetourListe neq ''}
            <li>
                <div class="shortcut-icon">
                    <a class="shortcut-description" href="{$sRetourListe}"><img src="http://dgentreprises.biz/logicielprospection_v3/img/icons/25x25/blue/list-w--images.png" width="25" height="25"></a>
                    <div class="divider"></div>
                </div>
                <a class="shortcut-description" href="{$sRetourListe}"> <strong> {$sLabelRetourListe}</strong>  </a>
            </li>
            {/if}

        </ul>

        {if isset($bMessageSuccesForm) and $bMessageSuccesForm}
            <div class="alert success grid_6">
                <span class="icon"></span><strong>Success</strong><br>
                {if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}
            </div>
        {/if}


        {if !empty($aRech)}
            <div class="grid_4">
                <div class="box">
                    <div class="header">
                        <img src="http://dgentreprises.biz/logicielprospection_v3/img/icons/packs/fugue/16x16/application-form.png" alt="" width="16"
                             height="16">
                        <h3>{$sLabelRecherche}{if $bActivtraduction}&nbsp;<a href="#"
                                                                             onclick="renvoi_info_traduction('fli_titrerech','{$sFli_LinkUrl}','{$sLangueUser}','{$sLabelRecherche|replace:'\'':'\\\''}','{$sNomTable}','fli_titrerech');$('#fli_modallg').modal('show');">
                                    <span class="glyphicon glyphicon-pencil"></span></a>{/if}</h3>
                        <span></span>
                    </div>

                    <form
                            class="form-inline"
                            action=""
                            id="formList"
                            method="GET"
                            {if isset($bTelechargementFichier) and $bTelechargementFichier}enctype="multipart/form-data"{/if}
                    >
                        <input type="hidden" name="fli_rechercher" value="ok">
                        <input type="hidden" name="sTypeSortie" value="" id="idtypesortie">
                        <div class="content no-padding">
                            {if isset($aParametreWizardListe) and $aParametreWizardListe|is_array}
                                {foreach from=$aParametreWizardListe key=CleParametreWizardListe item=ValeurParametreWizardListe}
                                    <input type="hidden" name="{$CleParametreWizardListe}" value="{$ValeurParametreWizardListe}">
                                {/foreach}
                            {/if}
                            {if isset($aRetourListe) and $aRetourListe|is_array}
                                {foreach from=$aRetourListe key=sCleRetourListe item=sValeurRetourListe}
                                    <input type="hidden" name="{$sCleRetourListe}" value="{$sValeurRetourListe}">
                                {/foreach}
                            {/if}
                            {foreach from=$aRech item=objRech}

                                {if isset($objRech.type_champ) and ((isset($objRech.aff_recherche) and $objRech.aff_recherche eq 'ok')
                                or ($objRech.type_champ eq 'category' or $objRech.type_champ eq 'bouton' or $objRech.transfert_inter_module eq 'ok' ) )}

                                {if $objRech.type_champ eq 'text'}
                                    <div class="section _100">
                                        <label>{$objRech.text_label}</label>
                                        <div>
                                            <input
                                                    type="text"
                                                    class="inp-form form-control"
                                                    id="id_{$objRech.nom_variable}"
                                                    name="{$objRech.nom_variable}"
                                                    {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                                    {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                            {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                                {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                                    {if $cle!='size'}
                                                        {$cle}="{$valeur}"
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                            {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                                {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                                    {if $cle!='size'}
                                                        {$cle}="{$valeur}"
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                            >
                                        </div>
                                    </div>
                                {elseif $objRech.type_champ eq 'category'}
                                    <div class="form-group">
                                        <label>{$objRech.text_label}</label>
                                    </div>
                                {elseif $objRech.type_champ eq 'hidden'}

                                        <input
                                                type="hidden"
                                                {if isset($objRech.nom_variable)}name="{$objRech.nom_variable}"{/if}
                                                {if isset($objRech.valeur_variable)}value="{$objRech.valeur_variable}"{/if}
                                        >

                                {elseif $objRech.type_champ eq 'select' or $objRech.type_champ eq 'selectdist'}
                                    <div class="section _100">
                                        <label>{$objRech.text_label}</label>
                                        <div>
                                            <select

                                                    id="id_{$objRech.nom_variable}"
                                                    name="{$objRech.nom_variable}{if isset($objRech.multiple)}[]{/if}"
                                                    {if isset($objRech.multiple)}multiple{/if}
                                                    {$objRech.fonction_javascript}

                                            >
                                                <option value="" {if $objRech.valeur_variable eq ''}selected{/if}></option>
                                                {if is_array($objRech.lesitem)}
                                                    {foreach from=$objRech.lesitem key=id_valeur_possible item=valeur_possible_bdd}
                                                        <option value="{$id_valeur_possible}" {if $objRech.valeur_variable eq $id_valeur_possible}selected{/if}>
                                                            {$valeur_possible_bdd}
                                                        </option>
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        </div>
                                    </div>

                                 {else}
                                    {*<div class="section _100">
                                        <label>{$objRech.text_label}</label>
                                        <div>
                                            <input
                                                    type="text"
                                                    class="inp-form form-control"
                                                    id="id_{$objRech.nom_variable}"
                                                    name="{$objRech.nom_variable}"
                                                    {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                                    {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                            {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                                {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                                    {if $cle!='size'}
                                                        {$cle}="{$valeur}"
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                            {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                                {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                                    {if $cle!='size'}
                                                        {$cle}="{$valeur}"
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                            >
                                        </div>
                                    </div>*}
                                {/if}


                                {/if}


                            {/foreach}


                            <div class="actions">
                                <div class="actions-left">
                                    {if isset($itemBoutons.reset)}
                                             <input
                                                     class="btn btn-primary btnResForm"
                                                     type="button"
                                                     value="{$itemBoutons.reset.text_label}"
                                                     {if isset($itemBoutons.reset.style) and $itemBoutons.reset.style neq ''}style="margin-bottom: 5px;"{/if}
                                             {if isset($itemBoutons.reset.tableau_attribut) and $itemBoutons.reset.tableau_attribut|is_array}
                                                 {foreach from=$itemBoutons.reset.tableau_attribut key=cle item=valeur}
                                                     {$cle}="{$valeur}"
                                                 {/foreach}
                                             {/if}
                                             {if isset($itemBoutons.reset.fonction_javascript) and $itemBoutons.reset.fonction_javascript|is_array}
                                                 {foreach from=$itemBoutons.reset.fonction_javascript key=cle item=valeur}
                                                     {$cle}="{$valeur}"
                                                 {/foreach}
                                             {/if}
                                             onclick="location.href='{$itemBoutons.reset.url}';"
                                             >
                                     {/if}
                                </div>


                                <div class="actions-right">
                                    {if isset($itemBoutons.valider)}
                                          <input
                                          class="btn btn-primary btnbtnValForm"
                                          type="submit"
                                          id="id_{$itemBoutons.valider.nom_variable}"
                                          name="{$itemBoutons.valider.nom_variable}"
                                          value="{$itemBoutons.valider.text_label}"
                                          {if isset($itemBoutons.valider.style) and $itemBoutons.valider.style neq ''} style="margin-bottom: 5px;"{/if}
                                          {if isset($itemBoutons.valider.tableau_attribut) and $itemBoutons.valider.tableau_attribut|is_array}
                                              {foreach from=$itemBoutons.valider.tableau_attribut key=cle item=valeur}
                                                  {$cle}="{$valeur}"
                                              {/foreach}
                                          {/if}
                                          {if isset($itemBoutons.valider.fonction_javascript) and $itemBoutons.valider.fonction_javascript|is_array}
                                              {foreach from=$itemBoutons.valider.fonction_javascript key=cle item=valeur}
                                                  {$cle}="{$valeur}"
                                              {/foreach}
                                          {/if}
                                          onclick="valiformsimple()"
                                          >
                                     {/if}
                                </div>
                            </div>

                        </div>
                </div>
            </div>
        {/if}



     <div class={if !empty($aRech)}"grid_6"{else}"grid_10"{/if}>
        <div class="box">
            <div class="header">
                <img src="http://dgentreprises.biz/logicielprospection_v3/img/icons/packs/fugue/16x16/shadeless/table.png" width="16" height="16">
                <h3>{$iTabListeCount} {$sLabelNbrLigne}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_textnbrligne','{$sFli_LinkUrl}','{$sLangueUser}','{$sLabelNbrLigne|replace:'\'':'\\\''}','{$sNomTable}','fli_textnbrligne');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if} </h3>
            </div>

            <div class="content  no-padding">
                <table class="table"  border="0" width="100%">
                    <thead>
                    <tr>
                        {if $bAffMod or $bAffSupp or !empty($objListe.aMenuDeroulant ) or $bAffRecapLigne }

                            <th  class="wwe-lang-rank" scope="col">Action</th>
                        {/if}
                        {foreach from=$aEnteteListe item=objEnteteListe}
                            <th  class="wwe-lang-rank" scope="col" {$objEnteteListe.objElement.html_perso_th} {$objEnteteListe.sLabel} >
                               {if $objEnteteListe.sUrl!=""}<a href="{$objEnteteListe.sUrl}">{/if}{$objEnteteListe.sLabel}{if $objEnteteListe.sUrl!=""}</a>{/if}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objEnteteListe.objElement.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objEnteteListe.sLabel|replace:'\'':'\\\''}','{$sNomTable}','{$objEnteteListe.objElement.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
                        {/foreach}

                    </tr>
                    </thead>
                    <tbody>
                    {foreach from=$aListe item=objListe}
                    <tr class="no" {if $bLigneCliquable}onclick="document.location = '{$sLienLigne}{$objListe.id}';" style="cursor:pointer;"{/if}
                            {if isset($objListe.html_perso_tr)}{$objListe.html_perso_tr}{/if}>
                        {if $bAffMod or $bAffSupp or !empty($objListe.aMenuDeroulant) or $bAffRecapLigne}
                        <td style="text-align:center;vertical-align: middle;" class="td-action">

                                    {if $bAffMod}
                                        <a href="{$objListe.sUrlForm}" title="Modifier" ><img src="vues/version2/img/icons/packs/diagona/16x16/018.png" width="15px"></a>
                                    {/if}
                                    {if $bAffSupp}
                                             <a href="#"  onclick="bconf=confirm('Voulez-vous supprimer cette ligne ?');if(bconf)location.replace('{$objListe.sUrlSupp}');" title="Supprimer" ><img src="vues/version2/img/icons/packs/diagona/16x16/101.png" width="15px" ></a>

                                    {/if}
                                    {if $bAffDup}<li><a href="#" onclick="bconf=confirm('Voulez-vous dupliquer cette ligne ?');if(bconf)location.replace('{$objListe.sUrlDuplicate}');"><span class="glyphicon glyphicon-copy"></span> Duppliquer</a></li>{/if}
                                    {if $bAffRecapLigne}
                                           <a href="{$objListe.sUrlRecapLigne}" title="Visualiser" ><img src="vues/version2/img/icons/packs/diagona/16x16/176.png" width="15px"></a>
                                    {/if}
                                    {if !empty($objListe.aMenuDeroulant)}
                                        {foreach from=$objListe.aMenuDeroulant item=objListeMenu}
                                            <li><a href="{$objListeMenu.href}{$surlVarGET}" onclick="{$objListeMenu.javascript}" {if isset($objListeMenu.target)}{$objListeMenu.target}{/if}>{if !empty($objListeMenu.attribut)}<span {$objListeMenu.attribut}></span> {/if}{$objListeMenu.intitule}</a></li>
                                        {/foreach}
                                    {/if}




                        </td>
                        {/if}

                        {foreach name="enteteliste" from=$aEnteteListe item=objEnteteListe}
                            <td style="vertical-align: middle;text-align: center;" {$objEnteteListe.objElement.html_perso_td} >
                                {if $smarty.foreach.enteteliste.first}
                                    {if isset($objListe.liste_fils) and $objListe.liste_fils|is_array and $objListe.liste_fils|@count > 0 and !empty($objListe.liste_fils[0][0][0])}
                                        <button class="btn btn-warning nprintable glyphicon glyphicon-resize-full" onclick="$(this).toggleClass('glyphicon glyphicon-resize-small').toggleClass('glyphicon glyphicon-resize-full');$(this).closest('tr').next().stop().fadeToggle('fast')"></button>
                                    {/if}
                                {/if}

                                {if $bActiveFormSelect}
                                    {if $objEnteteListe.objElement.activ_form_select=='ok'}
                                        <!-- ---------------- TEXT ---------------- -->
                                        {if $objEnteteListe.objElement.type_champ=='text'}
                                            <input   type="text"  name=" {$objEnteteListe.objElement.nom_variable}" value="{$objListe[$objEnteteListe.objElement.nom_variable].value}" >
                                        {/if}
                                        <!-- ---------------- SELECT ---------------- -->
                                        {if $objEnteteListe.objElement.type_champ=='select'}
                                            <select
                                                    style="padding:5px;"
                                                    id="id_form_{$objEnteteListe.objElement.nom_variable}"
                                                    name="{$objEnteteListe.objElement.nom_variable}{if isset($objEnteteListe.objElement.multiple)}[]{/if}"
                                                    {if isset($objEnteteListe.objElement.multiple)}multiple{/if}

                                                    {$objEnteteListe.objElement.fonction_javascript}
                                            >
                                                <option value=""></option>
                                                {if is_array($listeElemSelect)}
                                                    {foreach from=$listeElemSelect key=id_valeur_possible item=valeur_possible_bdd}
                                                        <option value="{$id_valeur_possible}" {if $objListe[$objEnteteListe.objElement.nom_variable].value eq $valeur_possible_bdd}selected{/if}>
                                                            {$valeur_possible_bdd}
                                                        </option>
                                                    {/foreach}
                                                {/if}
                                            </select>
                                        {/if}
                                        <!-- ---------------- CHECKBOX ---------------- -->
                                        {if $objEnteteListe.objElement.type_champ=='checkbox'}
                                            <table class="table table-hover table-condensed table-bordered">
                                                <tbody>
                                                {foreach from=$allElements.lesitem key=valeur_checkbox item=nom_checkbox}
                                                    <tr>
                                                        <td style="padding-right:10px;">
                                                            <input
                                                                    type="checkbox"
                                                                    name="{$objEnteteListe.objElement.nom_variable}[]"
                                                                    value="{$valeur_checkbox}"
                                                                    {if is_array($objListe.tableauChecked) and in_array($valeur_checkbox, $objListe.tableauChecked)}checked{/if}
                                                            >
                                                        <td style="padding-right:20px;">{$nom_checkbox}</td>
                                                    </tr>
                                                {/foreach}
                                                </tbody>
                                            </table>
                                        {/if}
                                        <!-- ---------------- RADIO ---------------- -->
                                        {if $objEnteteListe.objElement.type_champ=='radio'}
                                            <table class="table table-hover table-condensed table-bordered">
                                                <tbody>
                                                {foreach from=$allElements.lesitem key=valeur_radio item=nom_radio}
                                                    <tr class="test">
                                                        <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                            <input
                                                                    type="radio"
                                                                    name="{$objEnteteListe.objElement.nom_variable}"
                                                                    value="{$valeur_radio}"
                                                                    {if $valeur_radio eq $objListe.checked}checked{/if}
                                                            >
                                                        </td>
                                                        <td style="padding-bottom:0;vertical-align:middle;height:32px;">{$nom_radio}</td>
                                                    </tr>
                                                {/foreach}
                                                </tbody>
                                            </table>
                                        {/if}
                                    {else}
                                        <span {if $objEnteteListe.objElement.html_editable_td=="ok"} contenteditable="true" onblur="save_edtable('{$sNomTable}','{$sChampId}','{$objEnteteListe.objElement.mapping_champ}','{$objListe.id}',$(this).html())" {/if}>{if $objEnteteListe.objElement.format_affiche_liste != ''}{$objListe[$objEnteteListe.objElement.nom_variable].value|number_format:$objEnteteListe.objElement.format_affiche_liste:".":"."}{elseif $objEnteteListe.objElement.alias_champ!=''}{$objListe[$objEnteteListe.objElement.alias_champ].value}{else}{$objListe[$objEnteteListe.objElement.nom_variable].value}&nbsp;{/if}</span>
                                    {/if}
                                {else}
                                    <span {if $objEnteteListe.objElement.html_editable_td=="ok" and $objEnteteListe.objElement.type_champ !='date' }contenteditable="true" onblur="save_edtable('{$sNomTable}','{$sChampId}','{$objEnteteListe.objElement.mapping_champ}','{$objListe.id}',$(this).html())"{/if}>
                                  {if $objEnteteListe.objElement.html_editable_radio=='ok'}
                                    <input type="checkbox" data-on-text="Oui" data-off-text="Non" name="checkbox_visible_{$objListe.id}" id="checkbox_visible_{$objListe.id}" >
                                        <script type="text/javascript">

                                            $("[name=checkbox_visible_{$objListe.id}]").bootstrapSwitch();

                                            {if $objListe[$objEnteteListe.objElement.nom_variable].value == "OUI"}
                                            $("[name=checkbox_visible_{$objListe.id}]").bootstrapSwitch("state", true);
                                            {/if}

                                            $("input[name=checkbox_visible_{$objListe.id}]").on("switchChange.bootstrapSwitch", function(e, state) {
                                                if( e.target.checked ){
                                                    save_edtable("{$sNomTable}","{$sChampId}","{$objEnteteListe.objElement.mapping_champ}","{$objListe.id}","{$objEnteteListe.objElement.html_value_bouton_on_radio}");
                                                }else{
                                                    save_edtable("{$sNomTable}","{$sChampId}","{$objEnteteListe.objElement.mapping_champ}","{$objListe.id}","{$objEnteteListe.objElement.html_value_bouton_off_radio}");
                                                }
                                            });
                                        </script>

                                  {else}
                                  {if $objEnteteListe.objElement.type_champ=='date' and $objEnteteListe.objElement.html_editable_td=="ok" }
                             <div onclick="apparition('{$objListe.id}{$objEnteteListe.objElement.mapping_champ}')" style="cursor: pointer;">

                             <input onblur="save_edtable('{$sNomTable}','{$sChampId}','{$objEnteteListe.objElement.mapping_champ}','{$objListe.id}',$(this).val())" style="display:none" type="date" id="inputdate{$objListe.id}{$objEnteteListe.objElement.mapping_champ}" >


                                            {/if}

                                            {if $objEnteteListe.objElement.format_affiche_liste != ''}
                                                {$objListe[$objEnteteListe.objElement.nom_variable].value|number_format:$objEnteteListe.objElement.format_affiche_liste:".":"."}
                                            {elseif $objEnteteListe.objElement.alias_champ!=''}
                                                {$objListe[$objEnteteListe.objElement.alias_champ].value}
                                            {else}
                                                {if isset($objListe[$objEnteteListe.objElement.nom_variable].value)}{$objListe[$objEnteteListe.objElement.nom_variable].value}{/if}&nbsp;
                                            {/if}
                                            {/if}
                                            {if $objEnteteListe.objElement.type_champ=='date' and $objEnteteListe.objElement.html_editable_td=="ok" }
                             </div>

                                        {/if}
                              </span>
                                {/if}

                            </td>
                        {/foreach}
                    </tr>
                        {if isset($objListe.liste_fils) and $objListe.liste_fils|is_array and $objListe.liste_fils|@count > 0 and !empty($objListe.liste_fils[0][0][0])}
                            <tr style="display: none;">
                                <td style="padding:0;vertical-align: middle;" colspan="{$iNombreColonneListe}">
                                    <div class="class_form_list_toggle_div">
                                        <div class="pt0Print" style="padding:20px 20px 0 20px;">
                                            {foreach from=$objListe.liste_fils item=sTpl}
                                                {$sTpl[0][0]}
                                            {/foreach}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        {/if}


                    </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
            {if isset($bPagination) and $bPagination and isset($aPaginationListe) and isset($aPaginationListe.select) and isset($aPaginationListe.liste)}
                <div class="actions-right">
                    <div id="table-example_paginate" class="dataTables_paginate paging_full_numbers">
                        {*<span id="table-example_previous" class="previous paginate_button"  onclick="document.location.href='{$pageprecedent}'">Previous</span>*}
                        <span>
						 {foreach from=$aPaginationListe.liste.page_liste item=page}
                             <span {if isset($page.selected) and $page.selected} class="paginate_active" {else} class="paginate_button" {/if} onclick="document.location.href='{$page.url}'">{$page.numero}</span>
                         {/foreach}
						</span>
                        {*<span id="table-example_next" class="next paginate_button" onclick="document.location.href='{$pagesuivante}'">Next</span>*}
                    </div>
                </div>
            {/if}
        </div>

        </div>
    </div>

</div>