<div class="content">
    <div style="margin: auto;width:  600px;">
    <h1>Login</h1>
    <div class="background"></div>
    <div class="wrapper">
        <div class="box">
            <div class="header grey">
                <img src="http://dgentreprises.biz/logicielprospection_v3/img/icons/packs/fugue/16x16/lock.png" width="16" height="16">
                <h3>Login</h3> {if !empty($erreur)}&nbsp;  <font style="color: red">{$erreur}</font>{/if}
            </div>
            <form method="POST" >
                <input type="hidden" name="validation" value="ok">
                <div class="content no-padding">
                    <div class="section _100">
                        <label>
                            Username
                        </label>
                        <div>
                            <input name="login" class="required">
                        </div>
                    </div>
                    <div class="section _100">
                        <label>
                            Password
                        </label>
                        <div>
                            <input name="password" type="password" class="required">
                        </div>
                    </div>
                </div>
                <div class="actions">
                    <div class="actions-left" style="margin-top: 8px;">

                    </div>
                    <div class="actions-right">
                        <input type="submit" value="Login"/>
                    </div>
                </div>
            </form>
        </div>
        <div class="shadow"></div>
    </div>
    </div>
</div>