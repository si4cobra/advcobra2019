<?php
/* Smarty version 3.1.29, created on 2019-10-09 15:58:31
  from "/var/www/html/vues/version1/templates/liste.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5d9de7873a0334_27024518',
  'file_dependency' => 
  array (
    '631163fb7a74e2704426bc32d5e0651275c18469' => 
    array (
      0 => '/var/www/html/vues/version1/templates/liste.tpl',
      1 => 1570546329,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:formulaire.tpl' => 1,
  ),
),false)) {
function content_5d9de7873a0334_27024518 ($_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/var/www/html/libs/plugins/modifier.replace.php';
?>
<style>
    .floatingHeader {
        position: fixed;
        top: 50px;
        visibility: hidden;
    }
</style>
<?php echo '<script'; ?>
>
    function UpdateTableHeaders() {
        $(".persist-area").each(function () {

            var el = $(this),
                offset = el.offset(),
                scrollTop = $(window).scrollTop(),
                floatingHeader = $(".floatingHeader", this)

            if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
                floatingHeader.css({
                    "visibility": "visible"
                });
            } else {
                floatingHeader.css({
                    "visibility": "hidden"
                });
            }
            ;
        });
    }

    // DOM Ready
    $(function () {

        var clonedHeaderRow;

        $(".persist-area").each(function () {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width())
                .addClass("floatingHeader");

        });

        $(window)
            .scroll(UpdateTableHeaders)
            .trigger("scroll");

    });
<?php echo '</script'; ?>
>
<style>




    @media print {

        @page {
            size: landscape
        }

    ;

        body {
            line-height: 1 !important;
        }

        .printable {
            display: block !important;
        }

        .nprintable {
            display: none !important;
        }

        a[href]:after {
            content: "";
        }

        h1 {
            font-size: 2.375em !important;
        }

        h3 {
            font-size: 28px !important;
        }

        p {
            font-size: 24px !important;
        }

        tr {
            width: 100% !important;
        }

        .pt0Print {
            padding-top: 5px !important;
            padding-bottom: 5px !important;
        }

        .text-head-search {
            font-size: 20px !important;
        }

        .col-sm-1,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12 {
            float: none;
            width: 100%;
        }

        @-moz-document url-prefix() {
            @page {
                margin: 1cm;
            }

            body {
                line-height: 1 !important;
            }

            h1 {
                font-size: 1px !important;
            }

            .h1nomenseigne b {
                font-size: 20px !important;
            }
            .h3printable {
                font-size: 18px !important;
            }

            /*h3 span span span span span {
                font-size: 20px !important;
            }*/

            p {
                font-size: 10px !important;
            }

            #dateVentePrivee {
                font-size: 12px !important;
            }

            .box1 {
                margin: 1px !important;
                padding: 1px !important;
                font-size: 12px !important;
            }

            /* .coupon-meta p span
             {
                 font-size: 12px!important;
             }*/

            /*.coupon-meta p span {
                font-size: 19px !important;
            }*/

            .print-image-coupon {
                width: 200px !important;
            }

            .print-image-mini-coupon {
                width: 60px !important;
                height: 60px !important;
            }

            .diventetevp {
                margin: 0 !important;
                padding: 0 !important;
            }

            .divenseigne {
                padding: 0 !important;
                margin: 0 !important;
            }

            .th-action {
                display: none !important;
            }
            .td-action {
                display: none !important;
            }

        }
    }

</style>

<div style="color:#313131;margin: 0 auto;padding: 20px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;padding-bottom:0;">
    <?php if ($_smarty_tpl->tpl_vars['bDebugRequete']->value) {?>
        <p>
            Requete Select : <br><br>
            <?php echo $_smarty_tpl->tpl_vars['sDebugRequeteSelect']->value;?>
<br><br>
            Requete Insert/Update : <br><br>
            <?php echo $_smarty_tpl->tpl_vars['sDebugRequeteInsertUpdate']->value;?>
<br><br>
        </p>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['bMessageSupprElem']->value) && $_smarty_tpl->tpl_vars['bMessageSupprElem']->value) {?>
        <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            <?php if (isset($_smarty_tpl->tpl_vars['sMessageSupprElem']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageSupprElem']->value;
}?>
        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['bMessageSuccesForm']->value) && $_smarty_tpl->tpl_vars['bMessageSuccesForm']->value) {?>
        <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            <?php if (isset($_smarty_tpl->tpl_vars['sMessageSuccesForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageSuccesForm']->value;
}?>
        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['bMessageErreurForm']->value) && $_smarty_tpl->tpl_vars['bMessageErreurForm']->value) {?>
        <div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            <?php if (isset($_smarty_tpl->tpl_vars['sMessageErreurForm']->value)) {
echo $_smarty_tpl->tpl_vars['sMessageErreurForm']->value;
}?>
        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['sScriptJavascriptInsert']->value)) {?>
        <?php echo $_smarty_tpl->tpl_vars['sScriptJavascriptInsert']->value;?>

    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['sScriptJavascriptUpdate']->value)) {?>
        <?php echo $_smarty_tpl->tpl_vars['sScriptJavascriptUpdate']->value;?>

    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['aListe']->value)) {?>
    <?php if ($_smarty_tpl->tpl_vars['bAffTitre']->value) {?>
        <div id="page-heading" style="margin-bottom:20px;">
            <div style="display:inline-block;border:1px solid #D0D1D5;">
                <div class="hidden-xs title-head-search nprintable"
                     style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;"><span
                            class="glyphicon glyphicon-link"
                            style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;"></span></div>
                <h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;"
                    class="text-head-search"><?php echo $_smarty_tpl->tpl_vars['sTitreListe']->value;
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;
                    <span
                       onclick="renvoi_info_traduction('fli_titreliste','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['sTitreListe']->value,'\'','\\\'');?>
}','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','fli_titreliste');$('#fli_modallg').modal('show');">
                            <span class="glyphicon glyphicon-pencil"></span></span><?php }?></h1>
                <?php if ($_smarty_tpl->tpl_vars['bdoc']->value == 'Y') {?>
                    <div class="hidden-xs title-head-search nprintable"
                         style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;cursor:pointer"
                         onclick="location.replace('fli_admin-ctrl_documentation-fli_documentation?route=<?php echo $_smarty_tpl->tpl_vars['sLaRoute']->value;?>
')">
                        Doc<span class="glyphicon glyphicon-book"
                                 style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px"></span></div>
                <?php }?>
            </div>
        </div>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['sCategorieListe']->value) && $_smarty_tpl->tpl_vars['sCategorieListe']->value != '') {?>
        <?php if (is_array($_smarty_tpl->tpl_vars['sCategorieListe']->value)) {?>
            <?php
$_from = $_smarty_tpl->tpl_vars['sCategorieListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_scategorieliste_0_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_scategorieliste']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_scategorieliste'] : false;
$__foreach_scategorieliste_0_saved_item = isset($_smarty_tpl->tpl_vars['objCategorieListe']) ? $_smarty_tpl->tpl_vars['objCategorieListe'] : false;
$__foreach_scategorieliste_0_total = $_smarty_tpl->smarty->ext->_foreach->count($_from);
$_smarty_tpl->tpl_vars['objCategorieListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_scategorieliste'] = new Smarty_Variable(array());
$__foreach_scategorieliste_0_iteration=0;
$_smarty_tpl->tpl_vars['objCategorieListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objCategorieListe']->value) {
$_smarty_tpl->tpl_vars['objCategorieListe']->_loop = true;
$__foreach_scategorieliste_0_iteration++;
$_smarty_tpl->tpl_vars['__smarty_foreach_scategorieliste']->value['last'] = $__foreach_scategorieliste_0_iteration == $__foreach_scategorieliste_0_total;
$__foreach_scategorieliste_0_saved_local_item = $_smarty_tpl->tpl_vars['objCategorieListe'];
?>
                <div style="background-color:#FFF;display:inline-block;vertical-align:top;height:20px;font-size:18px;padding:10px 0;font-family:Tahoma;"><?php echo $_smarty_tpl->tpl_vars['objCategorieListe']->value['index'];?>
</div>
            <div style="background-color:<?php if ($_smarty_tpl->tpl_vars['objCategorieListe']->value['suivant']) {?>#e3e3e3<?php } else { ?>#777<?php }?>;height:40px;display:inline-block;vertical-align:middle;">
                <div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div>
                <div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['objCategorieListe']->value['texte'];?>
</div><?php if (!(isset($_smarty_tpl->tpl_vars['__smarty_foreach_scategorieliste']->value['last']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_scategorieliste']->value['last'] : null)) {?>
                <div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-right-radius: 0;border-bottom-right-radius: 0;background-color:#FFF;"></div>
            <?php }?></div><?php
$_smarty_tpl->tpl_vars['objCategorieListe'] = $__foreach_scategorieliste_0_saved_local_item;
}
if ($__foreach_scategorieliste_0_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_scategorieliste'] = $__foreach_scategorieliste_0_saved;
}
if ($__foreach_scategorieliste_0_saved_item) {
$_smarty_tpl->tpl_vars['objCategorieListe'] = $__foreach_scategorieliste_0_saved_item;
}
?>
            <div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
        <?php } else { ?>
            <div style="background-color:#777;height:40px;display:inline-block;vertical-align:middle;">
                <div style="display:inline-block;width:17px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div>
                <div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['sCategorieListe']->value;?>
</div>
            </div>
            <div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
        <?php }?>
        <br>
        <br>
        <br>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['sRetourListe']->value) && $_smarty_tpl->tpl_vars['sRetourListe']->value != '') {?>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td><a href="<?php echo $_smarty_tpl->tpl_vars['sRetourListe']->value;?>
"><img src="images/retour.png" height="40px" border="0"
                                                   alt="<?php echo $_smarty_tpl->tpl_vars['sLabelRetourListe']->value;?>
"></a></td>
                <td width="5px"></td>
                <td><a href="<?php echo $_smarty_tpl->tpl_vars['sRetourListe']->value;?>
" style="text-decoration:none;color:#000;"><b><?php echo $_smarty_tpl->tpl_vars['sLabelRetourListe']->value;?>
</b></a>
                </td>
            </tr>
        </table>
        <br>
    <?php }?>
    <?php if (isset($_smarty_tpl->tpl_vars['aRech']->value) && count($_smarty_tpl->tpl_vars['aRech']->value) > 0) {?>
    <!--<div style="margin-bottom:20px;"><div style="background-color:#777;height:40px;display:inline-block;vertical-align:middle;"><img src="images/loupe.png" height="20px" border="0" alt="<?php echo $_smarty_tpl->tpl_vars['sLabelRecherche']->value;?>
" style="padding:10px 0;background-color:#FFF;"><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;"><?php echo $_smarty_tpl->tpl_vars['sLabelRecherche']->value;?>
</div></div><div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div></div>-->
    <div class="nprintable"
         style="background-color: #fff;border: 1px solid #D0D1D5;padding: 20px 20px;margin-bottom: 20px;margin-top:0px;display: inline-block;">
        <div style="margin-bottom: 20px;">
            <div style="display: inline-block;" class="title-search">
                <span class="glyphicon glyphicon-search title-search"
                      style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;"
                    class="text-search"><?php echo $_smarty_tpl->tpl_vars['sLabelRecherche']->value;
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#"
                                                                                         onclick="renvoi_info_traduction('fli_titrerech','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['sLabelRecherche']->value,'\'','\\\'');?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','fli_titrerech');$('#fli_modallg').modal('show');">
                            <span class="glyphicon glyphicon-pencil"></span></a><?php }?></h4>
            </div>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['bAffPrintBtn']->value) {?>

        <?php }?>
        <form
                class="form-inline"
                action=""
                id="formList"
                method="GET"
                <?php if (isset($_smarty_tpl->tpl_vars['bTelechargementFichier']->value) && $_smarty_tpl->tpl_vars['bTelechargementFichier']->value) {?>enctype="multipart/form-data"<?php }?>
        >
            <input type="hidden" name="fli_rechercher" value="ok">
            <input type="hidden" name="sTypeSortie" value="" id="idtypesortie">

            <div>

                <?php if (isset($_smarty_tpl->tpl_vars['aParametreWizardListe']->value) && is_array($_smarty_tpl->tpl_vars['aParametreWizardListe']->value)) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['aParametreWizardListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_ValeurParametreWizardListe_1_saved_item = isset($_smarty_tpl->tpl_vars['ValeurParametreWizardListe']) ? $_smarty_tpl->tpl_vars['ValeurParametreWizardListe'] : false;
$__foreach_ValeurParametreWizardListe_1_saved_key = isset($_smarty_tpl->tpl_vars['CleParametreWizardListe']) ? $_smarty_tpl->tpl_vars['CleParametreWizardListe'] : false;
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['CleParametreWizardListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['CleParametreWizardListe']->value => $_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->value) {
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->_loop = true;
$__foreach_ValeurParametreWizardListe_1_saved_local_item = $_smarty_tpl->tpl_vars['ValeurParametreWizardListe'];
?>
                        <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['CleParametreWizardListe']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['ValeurParametreWizardListe']->value;?>
">
                    <?php
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe'] = $__foreach_ValeurParametreWizardListe_1_saved_local_item;
}
if ($__foreach_ValeurParametreWizardListe_1_saved_item) {
$_smarty_tpl->tpl_vars['ValeurParametreWizardListe'] = $__foreach_ValeurParametreWizardListe_1_saved_item;
}
if ($__foreach_ValeurParametreWizardListe_1_saved_key) {
$_smarty_tpl->tpl_vars['CleParametreWizardListe'] = $__foreach_ValeurParametreWizardListe_1_saved_key;
}
?>
                <?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['aRetourListe']->value) && is_array($_smarty_tpl->tpl_vars['aRetourListe']->value)) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['aRetourListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_sValeurRetourListe_2_saved_item = isset($_smarty_tpl->tpl_vars['sValeurRetourListe']) ? $_smarty_tpl->tpl_vars['sValeurRetourListe'] : false;
$__foreach_sValeurRetourListe_2_saved_key = isset($_smarty_tpl->tpl_vars['sCleRetourListe']) ? $_smarty_tpl->tpl_vars['sCleRetourListe'] : false;
$_smarty_tpl->tpl_vars['sValeurRetourListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['sCleRetourListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['sValeurRetourListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['sCleRetourListe']->value => $_smarty_tpl->tpl_vars['sValeurRetourListe']->value) {
$_smarty_tpl->tpl_vars['sValeurRetourListe']->_loop = true;
$__foreach_sValeurRetourListe_2_saved_local_item = $_smarty_tpl->tpl_vars['sValeurRetourListe'];
?>
                        <input type="hidden" name="<?php echo $_smarty_tpl->tpl_vars['sCleRetourListe']->value;?>
" value="<?php echo $_smarty_tpl->tpl_vars['sValeurRetourListe']->value;?>
">
                    <?php
$_smarty_tpl->tpl_vars['sValeurRetourListe'] = $__foreach_sValeurRetourListe_2_saved_local_item;
}
if ($__foreach_sValeurRetourListe_2_saved_item) {
$_smarty_tpl->tpl_vars['sValeurRetourListe'] = $__foreach_sValeurRetourListe_2_saved_item;
}
if ($__foreach_sValeurRetourListe_2_saved_key) {
$_smarty_tpl->tpl_vars['sCleRetourListe'] = $__foreach_sValeurRetourListe_2_saved_key;
}
?>
                <?php }?>

                <!-- NOUVELLE RECHERCHE BOOTSTRAP -->

                <?php
$_from = $_smarty_tpl->tpl_vars['aRech']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objRech_3_saved_item = isset($_smarty_tpl->tpl_vars['objRech']) ? $_smarty_tpl->tpl_vars['objRech'] : false;
$_smarty_tpl->tpl_vars['objRech'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objRech']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objRech']->value) {
$_smarty_tpl->tpl_vars['objRech']->_loop = true;
$__foreach_objRech_3_saved_local_item = $_smarty_tpl->tpl_vars['objRech'];
?>
                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['type_champ']) && ((isset($_smarty_tpl->tpl_vars['objRech']->value['aff_recherche']) && $_smarty_tpl->tpl_vars['objRech']->value['aff_recherche'] == 'ok') || ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'category' || $_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'bouton' || $_smarty_tpl->tpl_vars['objRech']->value['transfert_inter_module'] == 'ok'))) {?>
                        <?php if ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'text') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                            type="text"
                                            class="inp-form form-control"
                                            id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_4_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_4_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_4_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_4_saved_local_item;
}
if ($__foreach_valeur_4_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_4_saved_item;
}
if ($__foreach_valeur_4_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_4_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_5_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_5_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_5_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_5_saved_local_item;
}
if ($__foreach_valeur_5_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_5_saved_item;
}
if ($__foreach_valeur_5_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_5_saved_key;
}
?>
                                    <?php }?>
                                    >
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'category') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'date' || $_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'datetime') {?>
                            <div class="form-group">
                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date']) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date'] == 'ok') {?>

                                <label class="label-form-upper" style="width: 100%;"><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0] != '') {?>
                                    <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;"><span class="input-group-addon" id="basic-addon2" style="width:auto;min-width:52px;"><?php echo $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0];?>
</span>
                                <?php }?>
                                <input
                                    class="form-control"
                                    type="date"
                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
[]"
                                    aria-describedby="basic-addon2"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][0]) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][0] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][0];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="height: 28px;<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_6_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_6_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_6_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_6_saved_local_item;
}
if ($__foreach_valeur_6_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_6_saved_item;
}
if ($__foreach_valeur_6_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_6_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_7_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_7_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_7_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_7_saved_local_item;
}
if ($__foreach_valeur_7_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_7_saved_item;
}
if ($__foreach_valeur_7_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_7_saved_key;
}
?>
                                    <?php }?>
                                    >
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][0] != '') {?>
                                    </div>
                                <?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1] != '') {?>
                                    <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;"><span class="input-group-addon" id="basic-addon2" style="width:auto;"><?php echo $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1];?>
</span>
                                <?php }?>
                                    <input
                                        class="form-control"
                                        type="date"
                                        name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
[]"
                                        aria-describedby="basic-addon2"
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][1]) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][1] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'][1];?>
"<?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="height: 28px;<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_8_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_8_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_8_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                                <?php }?>
                                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_8_saved_local_item;
}
if ($__foreach_valeur_8_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_8_saved_item;
}
if ($__foreach_valeur_8_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_8_saved_key;
}
?>
                                        <?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                            <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_9_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_9_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_9_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                                <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                                <?php }?>
                                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_9_saved_local_item;
}
if ($__foreach_valeur_9_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_9_saved_item;
}
if ($__foreach_valeur_9_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_9_saved_key;
}
?>
                                        <?php }?>
                                    >
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label']) && isset($_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1]) && $_smarty_tpl->tpl_vars['objRech']->value['recherche_intervalle_date_label'][1] != '') {?>
                                        </div>
                                    <?php }?>
                            <?php } else { ?>
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                    class="form-control"
                                    type="date"
                                    id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_10_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_10_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_10_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_10_saved_local_item;
}
if ($__foreach_valeur_10_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_10_saved_item;
}
if ($__foreach_valeur_10_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_10_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_11_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_11_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_11_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_11_saved_local_item;
}
if ($__foreach_valeur_11_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_11_saved_item;
}
if ($__foreach_valeur_11_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_11_saved_key;
}
?>
                                    <?php }?>
                                >
                            <?php }?>
                            </div>

                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'textarea') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <textarea
                                            class="form-control"
                                            id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_12_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_12_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_12_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_12_saved_local_item;
}
if ($__foreach_valeur_12_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_12_saved_item;
}
if ($__foreach_valeur_12_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_12_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_13_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_13_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_13_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_13_saved_local_item;
}
if ($__foreach_valeur_13_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_13_saved_item;
}
if ($__foreach_valeur_13_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_13_saved_key;
}
?>
                                    <?php }?>
                                    ><?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
</textarea>
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'checkbox') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <div style="display:inline-block;">
                                    <table style="display:inline-block;">
                                        <tbody>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_checkbox_14_saved_item = isset($_smarty_tpl->tpl_vars['nom_checkbox']) ? $_smarty_tpl->tpl_vars['nom_checkbox'] : false;
$__foreach_nom_checkbox_14_saved_key = isset($_smarty_tpl->tpl_vars['valeur_checkbox']) ? $_smarty_tpl->tpl_vars['valeur_checkbox'] : false;
$_smarty_tpl->tpl_vars['nom_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_checkbox']->value => $_smarty_tpl->tpl_vars['nom_checkbox']->value) {
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = true;
$__foreach_nom_checkbox_14_saved_local_item = $_smarty_tpl->tpl_vars['nom_checkbox'];
?>
                                            <tr>
                                                <td style="padding-right:10px;">
                                                    <input
                                                            class="form-control"
                                                            type="checkbox"
                                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
[]"
                                                            value="<?php echo $_smarty_tpl->tpl_vars['valeur_checkbox']->value;?>
"
                                                            <?php if (is_array($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && in_array($_smarty_tpl->tpl_vars['valeur_checkbox']->value,$_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'])) {?>checked<?php }?>
                                                            >
                                                </td>
                                                <td style="padding-right:20px;"><?php echo $_smarty_tpl->tpl_vars['nom_checkbox']->value;?>
</td>
                                            </tr>
                                        <?php
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_14_saved_local_item;
}
if ($__foreach_nom_checkbox_14_saved_item) {
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_14_saved_item;
}
if ($__foreach_nom_checkbox_14_saved_key) {
$_smarty_tpl->tpl_vars['valeur_checkbox'] = $__foreach_nom_checkbox_14_saved_key;
}
?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                       <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'select' || $_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'selectdist') {?>

                        <?php if ($_smarty_tpl->tpl_vars['objRech']->value['tags'] == 'ok') {?>
                                <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <style>
                                .select2-container
                                {
                                min-width: 125px;
                                }
                                </style>
							<select name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
" class="js-example-responsive tags"  style="width: 100%;"  id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
">
							 <option value="" <?php if ($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] == '') {?>selected<?php }?>></option>
                                <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_checkbox_15_saved_item = isset($_smarty_tpl->tpl_vars['nom_checkbox']) ? $_smarty_tpl->tpl_vars['nom_checkbox'] : false;
$__foreach_nom_checkbox_15_saved_key = isset($_smarty_tpl->tpl_vars['valeur_checkbox']) ? $_smarty_tpl->tpl_vars['valeur_checkbox'] : false;
$_smarty_tpl->tpl_vars['nom_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_checkbox']->value => $_smarty_tpl->tpl_vars['nom_checkbox']->value) {
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = true;
$__foreach_nom_checkbox_15_saved_local_item = $_smarty_tpl->tpl_vars['nom_checkbox'];
?>
									<option value="<?php echo $_smarty_tpl->tpl_vars['valeur_checkbox']->value;?>
" <?php if (is_array($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && in_array($_smarty_tpl->tpl_vars['valeur_checkbox']->value,$_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'])) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['nom_checkbox']->value;?>
</option>
                                <?php
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_15_saved_local_item;
}
if ($__foreach_nom_checkbox_15_saved_item) {
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_15_saved_item;
}
if ($__foreach_nom_checkbox_15_saved_key) {
$_smarty_tpl->tpl_vars['valeur_checkbox'] = $__foreach_nom_checkbox_15_saved_key;
}
?>
							</select>

                        </div>
                        <?php } elseif (isset($_smarty_tpl->tpl_vars['objRech']->value['select_autocomplete']) && $_smarty_tpl->tpl_vars['objRech']->value['select_autocomplete'] == 'ok') {?>

                                <div class="form-group">
                                    <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label_filtre'];?>
</label>
                                    <input type='text'
                                               name='rech_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
'
                                               id='id_rech<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
'
                                               class="inp-form form-control"
                                               onKeyUp="affiche_liste_generique('<?php echo $_smarty_tpl->tpl_vars['objRech']->value['table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objRech']->value['id_table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objRech']->value['affichage_table_item'];?>
','<?php echo $_smarty_tpl->tpl_vars['objRech']->value['supplogique_table_item'];?>
', '<?php echo $_smarty_tpl->tpl_vars['objRech']->value['tabfiltre_autocomplete'];?>
',this.value,'id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
');">
                                </div>
                                <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <select
                                        class="form-control"
                                        style="padding:5px;width: 200px;"
                                        id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];
if (isset($_smarty_tpl->tpl_vars['objRech']->value['multiple'])) {?>[]<?php }?>"
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['multiple'])) {?>multiple<?php }?>
                                        <?php echo $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];?>


                                        >
                                    <option value="" <?php if ($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] == '') {?>selected<?php }?>></option>

                                </select>
                            </div>
                            <?php } else { ?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <select
                                        class="form-control"
                                        style="padding:5px;width: 100px;"
                                        id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];
if (isset($_smarty_tpl->tpl_vars['objRech']->value['multiple'])) {?>[]<?php }?>"
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['multiple'])) {?>multiple<?php }?>
                                        <?php echo $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];?>


                                        >
                                    <option value="" <?php if ($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] == '') {?>selected<?php }?>></option>
                                    <?php if (is_array($_smarty_tpl->tpl_vars['objRech']->value['lesitem'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_possible_bdd_16_saved_item = isset($_smarty_tpl->tpl_vars['valeur_possible_bdd']) ? $_smarty_tpl->tpl_vars['valeur_possible_bdd'] : false;
$__foreach_valeur_possible_bdd_16_saved_key = isset($_smarty_tpl->tpl_vars['id_valeur_possible']) ? $_smarty_tpl->tpl_vars['id_valeur_possible'] : false;
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['id_valeur_possible'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['id_valeur_possible']->value => $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = true;
$__foreach_valeur_possible_bdd_16_saved_local_item = $_smarty_tpl->tpl_vars['valeur_possible_bdd'];
?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['id_valeur_possible']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] == $_smarty_tpl->tpl_vars['id_valeur_possible']->value) {?>selected<?php }?>>
                                                <?php echo $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value;?>

                                            </option>
                                        <?php
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_16_saved_local_item;
}
if ($__foreach_valeur_possible_bdd_16_saved_item) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_16_saved_item;
}
if ($__foreach_valeur_possible_bdd_16_saved_key) {
$_smarty_tpl->tpl_vars['id_valeur_possible'] = $__foreach_valeur_possible_bdd_16_saved_key;
}
?>
                                    <?php }?>
                                </select>
                            </div>


                            <?php }?>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'radio') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <div style="display:inline-block;">
                                    <table>
                                        <tbody>
                                         <tr>
                                                <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                    <input
                                                            class="form-control"
                                                            type="radio"
                                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                                            value=""
                                                                >
                                                </td>
                                                <td style="padding-bottom:0;vertical-align:middle;height:32px;">Tout</td>
                                            </tr>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_radio_17_saved_item = isset($_smarty_tpl->tpl_vars['nom_radio']) ? $_smarty_tpl->tpl_vars['nom_radio'] : false;
$__foreach_nom_radio_17_saved_key = isset($_smarty_tpl->tpl_vars['valeur_radio']) ? $_smarty_tpl->tpl_vars['valeur_radio'] : false;
$_smarty_tpl->tpl_vars['nom_radio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_radio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_radio']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_radio']->value => $_smarty_tpl->tpl_vars['nom_radio']->value) {
$_smarty_tpl->tpl_vars['nom_radio']->_loop = true;
$__foreach_nom_radio_17_saved_local_item = $_smarty_tpl->tpl_vars['nom_radio'];
?>
                                            <tr>
                                                <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                    <input
                                                            class="form-control"
                                                            type="radio"
                                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                                            value="<?php echo $_smarty_tpl->tpl_vars['valeur_radio']->value;?>
"
                                                            <?php if ($_smarty_tpl->tpl_vars['valeur_radio']->value == $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) {?>checked<?php }?>
                                                            >
                                                </td>
                                                <td style="padding-bottom:0;vertical-align:middle;height:32px;"><?php echo $_smarty_tpl->tpl_vars['nom_radio']->value;?>
</td>
                                            </tr>
                                        <?php
$_smarty_tpl->tpl_vars['nom_radio'] = $__foreach_nom_radio_17_saved_local_item;
}
if ($__foreach_nom_radio_17_saved_item) {
$_smarty_tpl->tpl_vars['nom_radio'] = $__foreach_nom_radio_17_saved_item;
}
if ($__foreach_nom_radio_17_saved_key) {
$_smarty_tpl->tpl_vars['valeur_radio'] = $__foreach_nom_radio_17_saved_key;
}
?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'file') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                        class="form-control"
                                        type="file"
                                        id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_18_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_18_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_18_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                        <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_18_saved_local_item;
}
if ($__foreach_valeur_18_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_18_saved_item;
}
if ($__foreach_valeur_18_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_18_saved_key;
}
?>
                                <?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_19_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_19_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_19_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                        <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_19_saved_local_item;
}
if ($__foreach_valeur_19_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_19_saved_item;
}
if ($__foreach_valeur_19_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_19_saved_key;
}
?>
                                <?php }?>
                                >
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'time') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                        type="text"
                                        class="timepicker inp-form form-control"
                                        id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_20_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_20_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_20_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_20_saved_local_item;
}
if ($__foreach_valeur_20_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_20_saved_item;
}
if ($__foreach_valeur_20_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_20_saved_key;
}
?>
                                <?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_21_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_21_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_21_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_21_saved_local_item;
}
if ($__foreach_valeur_21_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_21_saved_item;
}
if ($__foreach_valeur_21_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_21_saved_key;
}
?>
                                <?php }?>
                                >
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'hour') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                        type="text"
                                        class="hourpicker inp-form form-control"
                                        id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                        <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_22_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_22_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_22_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_22_saved_local_item;
}
if ($__foreach_valeur_22_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_22_saved_item;
}
if ($__foreach_valeur_22_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_22_saved_key;
}
?>
                                <?php }?>
                                <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                    <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_23_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_23_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_23_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                        <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_23_saved_local_item;
}
if ($__foreach_valeur_23_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_23_saved_item;
}
if ($__foreach_valeur_23_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_23_saved_key;
}
?>
                                <?php }?>
                                >
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'hidden') {?>
                            <div class="form-group">
                                <input
                                    type="hidden"
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['nom_variable'])) {?>name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'])) {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                    >
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'password') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                            type="password"
                                            class="inp-form form-control"
                                            id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_24_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_24_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_24_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_24_saved_local_item;
}
if ($__foreach_valeur_24_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_24_saved_item;
}
if ($__foreach_valeur_24_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_24_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_25_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_25_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_25_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_25_saved_local_item;
}
if ($__foreach_valeur_25_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_25_saved_item;
}
if ($__foreach_valeur_25_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_25_saved_key;
}
?>
                                    <?php }?>
                                    >
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'email') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                            type="email"
                                            class="inp-form form-control"
                                            id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_26_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_26_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_26_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_26_saved_local_item;
}
if ($__foreach_valeur_26_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_26_saved_item;
}
if ($__foreach_valeur_26_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_26_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_27_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_27_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_27_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_27_saved_local_item;
}
if ($__foreach_valeur_27_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_27_saved_item;
}
if ($__foreach_valeur_27_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_27_saved_key;
}
?>
                                    <?php }?>
                                    >
                            </div>
                        <?php } elseif ($_smarty_tpl->tpl_vars['objRech']->value['type_champ'] == 'telephone') {?>
                            <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                            type="tel"
                                            class="inp-form form-control"
                                            id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_28_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_28_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_28_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_28_saved_local_item;
}
if ($__foreach_valeur_28_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_28_saved_item;
}
if ($__foreach_valeur_28_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_28_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_29_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_29_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_29_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_29_saved_local_item;
}
if ($__foreach_valeur_29_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_29_saved_item;
}
if ($__foreach_valeur_29_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_29_saved_key;
}
?>
                                    <?php }?>
                                    >
                            </div>
                        <?php } else { ?>
                                 <div class="form-group">
                                <label><?php echo $_smarty_tpl->tpl_vars['objRech']->value['text_label'];?>
</label>
                                <input
                                            type="text"
                                            class="inp-form form-control"
                                            id="id_<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            name="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['nom_variable'];?>
"
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['valeur_variable']) && $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'] != '') {?>value="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['valeur_variable'];?>
"<?php }?>
                                            <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['style']) && $_smarty_tpl->tpl_vars['objRech']->value['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['objRech']->value['style'];?>
"<?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_30_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_30_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_30_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_30_saved_local_item;
}
if ($__foreach_valeur_30_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_30_saved_item;
}
if ($__foreach_valeur_30_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_30_saved_key;
}
?>
                                    <?php }?>
                                    <?php if (isset($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'])) {?>
                                        <?php
$_from = $_smarty_tpl->tpl_vars['objRech']->value['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_31_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_31_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_31_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                            <?php if ($_smarty_tpl->tpl_vars['cle']->value != 'size') {?>
                                            <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                            <?php }?>
                                        <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_31_saved_local_item;
}
if ($__foreach_valeur_31_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_31_saved_item;
}
if ($__foreach_valeur_31_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_31_saved_key;
}
?>
                                    <?php }?>
                                    >
                            </div>
                        <?php }?>
                    <?php }?>
                <?php
$_smarty_tpl->tpl_vars['objRech'] = $__foreach_objRech_3_saved_local_item;
}
if ($__foreach_objRech_3_saved_item) {
$_smarty_tpl->tpl_vars['objRech'] = $__foreach_objRech_3_saved_item;
}
?>

                <br/><br/>

                <div class="form-group">
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider'])) {?>
                          <input
                          class="btn btn-primary btnbtnValForm"
                          type="submit"
                          id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['nom_variable'];?>
"
                          name="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['nom_variable'];?>
"
                          value="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['text_label'];?>
"
                          <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['style']) && $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['style'] != '') {?> style="margin-bottom: 5px;"<?php }?>
                          <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['tableau_attribut'])) {?>
                              <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_32_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_32_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_32_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                  <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                              <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_32_saved_local_item;
}
if ($__foreach_valeur_32_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_32_saved_item;
}
if ($__foreach_valeur_32_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_32_saved_key;
}
?>
                          <?php }?>
                          <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['fonction_javascript'])) {?>
                              <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['valider']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_33_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_33_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_33_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                  <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                              <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_33_saved_local_item;
}
if ($__foreach_valeur_33_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_33_saved_item;
}
if ($__foreach_valeur_33_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_33_saved_key;
}
?>
                          <?php }?>
                          onclick="valiformsimple()"
                          >
                      <?php }?>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset'])) {?>
                            <input
                                    class="btn btn-primary btnResForm"
                                    type="button"
                                    value="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['text_label'];?>
"
                                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['style']) && $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['style'] != '') {?>style="margin-bottom: 5px;"<?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['tableau_attribut'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_34_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_34_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_34_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_34_saved_local_item;
}
if ($__foreach_valeur_34_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_34_saved_item;
}
if ($__foreach_valeur_34_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_34_saved_key;
}
?>
                            <?php }?>
                            <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['fonction_javascript'])) {?>
                                <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_35_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_35_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_35_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                    <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                                <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_35_saved_local_item;
}
if ($__foreach_valeur_35_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_35_saved_item;
}
if ($__foreach_valeur_35_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_35_saved_key;
}
?>
                            <?php }?>
                            onclick="location.href='<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['reset']['url'];?>
';"
                            >
                    <?php }?>
                    <?php echo '<script'; ?>
>
                        function valiform() {
                            $("#formList").prop("target", '_blank');
                            $("#idtypesortie").val('csv');
                            $("#formList").submit();
                        }

                        function valiformpdf() {
                            $("#formList").prop("target", '_blank');
                            $("#idtypesortie").val('pdf');
                            $("#formList").submit();
                        }

                        function valiformsimple() {
                            $("#formList").prop("target", '');
                            $("#idtypesortie").val('');
                            $("#formList").submit();
                        }
                    <?php echo '</script'; ?>
>
                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']) && $_smarty_tpl->tpl_vars['bCsv']->value) {?>
                        <input
                        class="btn btn-primary"
                        type="submit"
                        value="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['text_label'];?>
"
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['style']) && $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['style'];?>
font-size:13px;margin-bottom: 5px;"<?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['tableau_attribut'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_36_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_36_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_36_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_36_saved_local_item;
}
if ($__foreach_valeur_36_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_36_saved_item;
}
if ($__foreach_valeur_36_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_36_saved_key;
}
?>
                        <?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['fonction_javascript'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_37_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_37_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_37_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_37_saved_local_item;
}
if ($__foreach_valeur_37_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_37_saved_item;
}
if ($__foreach_valeur_37_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_37_saved_key;
}
?>
                        <?php }?>
                        onclick="valiform();">
                    <?php }?>

                    <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']) && $_smarty_tpl->tpl_vars['bPdf']->value) {?>
                        <input
                        class="btn btn-primary"
                        type="submit"
                        value="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['text_label'];?>
"
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['style']) && $_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutons']->value['csv']['style'];?>
font-size:13px;margin-bottom: 5px;"<?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['tableau_attribut'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_38_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_38_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_38_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_38_saved_local_item;
}
if ($__foreach_valeur_38_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_38_saved_item;
}
if ($__foreach_valeur_38_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_38_saved_key;
}
?>
                        <?php }?>
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['fonction_javascript'])) {?>
                            <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutons']->value['pdf']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_39_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_39_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_39_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                                <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                            <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_39_saved_local_item;
}
if ($__foreach_valeur_39_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_39_saved_item;
}
if ($__foreach_valeur_39_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_39_saved_key;
}
?>
                        <?php }?>
                        onclick="valiformpdf();">
                    <?php }?>
                </div>

                <!-- FIN NOUVELLE RECHERCHE BOOTSTRAP -->

            </div>
        </form>
        <?php }?>
    </div>

    <?php if ($_smarty_tpl->tpl_vars['bLabelCreationElem']->value || $_smarty_tpl->tpl_vars['bBtnRetour']->value || $_smarty_tpl->tpl_vars['bAffPrintBtn']->value) {?>
        <div style="margin-bottom:20px;text-align: center;">
            <?php if ($_smarty_tpl->tpl_vars['bLabelCreationElem']->value) {?>
                <?php if (!$_smarty_tpl->tpl_vars['bFormPopup']->value) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['sLabelCreationElemUrl']->value;?>
" class="btn btn-default" role="button"><span
                                class="glyphicon glyphicon-plus"></span> <?php echo $_smarty_tpl->tpl_vars['sLabelCreationElem']->value;
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;
                        <a href="#"
                           onclick="renvoi_info_traduction('fli_btaj','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['sLabelCreationElem']->value,'\'','\\\'');?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','fli_btaj');$('#fli_modallg').modal('show');">
                                <span class="glyphicon glyphicon-pencil"></span></a><?php }?></a>
                <?php if ($_smarty_tpl->tpl_vars['bRetourSpecifique']->value) {?>
                    &nbsp;&nbsp;
                    <a href="<?php echo $_smarty_tpl->tpl_vars['sRetourElemUrl']->value;?>
" class="btn btn-default" role="button"><span
                                class="glyphicon glyphicon-plus"></span> <?php echo $_smarty_tpl->tpl_vars['sLabelFileRetourElem']->value;?>
</a>
                <?php }?>
                <?php } else { ?>
                    <a href="#creerModal" class="openModal" id="test"
                       style="text-decoration:none;color:#00e;"><?php echo $_smarty_tpl->tpl_vars['sLabelCreationElem']->value;
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a
                                href="#"
                                onclick="renvoi_info_traduction('fli_btaj','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['sLabelCreationElem']->value,'\'','\\\'');?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','fli_btaj');$('#fli_modallg').modal('show');">
                                <span class="glyphicon glyphicon-pencil"></span></a><?php }?></a>
                    <aside id="creerModal" class="modal">
                        <div style="overflow-y:auto;max-height:85%;background:#FFF;text-shadow:none;">
                            <?php $_smarty_tpl->smarty->ext->_subtemplate->render($_smarty_tpl, "file:formulaire.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

                            <a href="#close" title="Fermer" style="top:15px;right:15px;">Fermer</a>
                        </div>
                    </aside>
                <?php if (!$_smarty_tpl->tpl_vars['bMessageSuccesForm']->value && $_smarty_tpl->tpl_vars['bMessageErreurForm']->value) {?>
                    <?php echo '<script'; ?>
>
                        creermodal = document.getElementById('test').click();
                    <?php echo '</script'; ?>
>
                <?php }?>
                <?php }?>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['bBtnRetour']->value) {?>
                <a href="<?php echo $_smarty_tpl->tpl_vars['sDirRetour']->value;?>
" class="btn btn-default" role="button"><span
                            class="glyphicon glyphicon-share-alt" style="transform: scaleX(-1);"></span> Retour</a>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['bAffPrintBtn']->value) {?>
                <a href="Javascript:Void(0);" class="btn btn-default hidden-xs" role="button" onclick="window.print()"
                   style="position: fixed;top: 8px;z-index: 10000;right: 20px;"><span class="glyphicon glyphicon-print"
                                                                                      style="transform: scaleX(-1);"></span></a>
            <?php }?>


        </div>
    <?php }?>


</div>
<?php if ($_smarty_tpl->tpl_vars['bAffListe']->value) {
if ($_smarty_tpl->tpl_vars['bAffNombreResult']->value) {?>
<div class="nprintable" style="margin:0 auto;text-align: center;" id="blockAffInfo">
  <div style="display:inline-block;border: 1px solid #D0D1D5;margin-top:0px;margin-bottom:20px;background-color: #333;"><div style="display:inline-block;padding:20px;vertical-align: middle;" id="rechNbr" class="sub-title-head-search"><?php echo $_smarty_tpl->tpl_vars['iTabListeCount']->value;?>
</div><div id="rechLabel" style="display:inline-block;padding:20px;vertical-align: middle;" class="sub-text-head-search"><?php echo $_smarty_tpl->tpl_vars['sLabelNbrLigne']->value;
if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_textnbrligne','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['sLabelNbrLigne']->value,'\'','\\\'');?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','fli_textnbrligne');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?></div></div>
</div>
<?php }?>
  <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?><form method="post" action="<?php echo $_smarty_tpl->tpl_vars['urlFormList']->value;?>
" enctype="multipart/form-data" ><?php }?>
  <div style="margin: 0 15px;" class="row">
  <button class="btn btn-success btnScroll btnScrollLeft" style="position: fixed;margin-top: -34px;left: 16px;z-index: 1;"> <span class="glyphicon glyphicon-arrow-left"></span></button>
  <button class="btn btn-success btnScroll btnScrollRight" style="position: fixed;right: 15px;margin-top: -34px;z-index: 1;"> <span class="glyphicon glyphicon-arrow-right"></span></button>
  <div class="table-responsive" style="min-height: 500px;">
    <table style="background-color: #fafafa;" class="col-sm-12 table table-hover table-condensed table-bordered table-striped persist-area">
      <thead>
        <tr class="persist-header">
          <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
              <?php if ($_smarty_tpl->tpl_vars['bRadioSelect']->value) {?><th class="table-header-repeat line-left minwidth-1" style="color:#fff;font-size:16px;text-align: center;min-width: 20px;vertical-align: middle;">°</th><?php }?>
              <?php if ($_smarty_tpl->tpl_vars['bCheckboxSelect']->value) {?><th class="table-header-repeat line-left minwidth-1" style="color:#fff;font-size:16px;text-align: center;min-width: 20px;vertical-align: middle;">#</th><?php }?>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['bAffMod']->value || $_smarty_tpl->tpl_vars['bAffSupp']->value || $_smarty_tpl->tpl_vars['baffAction']->value || $_smarty_tpl->tpl_vars['bAffRecapLigne']->value) {?>
          <?php if (empty($_smarty_tpl->tpl_vars['bActiveFormSelect']->value)) {?>
              <th class="table-header-repeat line-left minwidth-1 title-table-head th-action" id="actions" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                  <a href="javascript:Void(0);" style="color:#fff;">Actions</a>
              </th>
              <?php echo '<script'; ?>
>
              $(document).ready(function() {
  var elmnt = document.getElementsByClassName("th-action");
  console.log(elmnt);


    elmnt[1].style.width = elmnt[0].offsetWidth+"px";
});
<?php echo '</script'; ?>
>
<?php }?>
          <?php }?>
          
          <?php
$_from = $_smarty_tpl->tpl_vars['aEnteteListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objEnteteListe_40_saved_item = isset($_smarty_tpl->tpl_vars['objEnteteListe']) ? $_smarty_tpl->tpl_vars['objEnteteListe'] : false;
$_smarty_tpl->tpl_vars['objEnteteListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objEnteteListe']->value) {
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = true;
$__foreach_objEnteteListe_40_saved_local_item = $_smarty_tpl->tpl_vars['objEnteteListe'];
?>
              <th class="table-header-repeat line-left minwidth-1 title-table-head  <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
 " id="" style="padding: 5px;background:none;text-align: center;vertical-align: middle;" >
                  <a href="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['sUrl'];?>
" style="color:#fff;" ><?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['sLabel'];?>
</a><?php if ($_smarty_tpl->tpl_vars['bActivtraduction']->value) {?>&nbsp;<a href="#" onclick="renvoi_info_traduction('<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
','<?php echo $_smarty_tpl->tpl_vars['sFli_LinkUrl']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sLangueUser']->value;?>
','<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['objEnteteListe']->value['sLabel'],'\'','\\\'');?>
','<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a><?php }?>
              </th>
<?php echo '<script'; ?>
>
$(document).ready(function() {
  var elmnt = document.getElementsByClassName("<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
");


    elmnt[1].style.width = elmnt[0].offsetWidth+"px";
});
<?php echo '</script'; ?>
>
          <?php
$_smarty_tpl->tpl_vars['objEnteteListe'] = $__foreach_objEnteteListe_40_saved_local_item;
}
if ($__foreach_objEnteteListe_40_saved_item) {
$_smarty_tpl->tpl_vars['objEnteteListe'] = $__foreach_objEnteteListe_40_saved_item;
}
?>

        </tr>
      </thead>
      <tbody>

      <?php
$_from = $_smarty_tpl->tpl_vars['aListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objListe_41_saved_item = isset($_smarty_tpl->tpl_vars['objListe']) ? $_smarty_tpl->tpl_vars['objListe'] : false;
$_smarty_tpl->tpl_vars['objListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objListe']->value) {
$_smarty_tpl->tpl_vars['objListe']->_loop = true;
$__foreach_objListe_41_saved_local_item = $_smarty_tpl->tpl_vars['objListe'];
?>
        <tr class="no" <?php if ($_smarty_tpl->tpl_vars['bLigneCliquable']->value) {?>onclick="document.location = '<?php echo $_smarty_tpl->tpl_vars['sLienLigne']->value;
echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
';" style="cursor:pointer;<?php if (isset($_smarty_tpl->tpl_vars['objListe']->value['html_perso_tr'])) {
echo $_smarty_tpl->tpl_vars['objListe']->value['html_perso_tr'];
}?>"<?php }?>
        >
          <?php if ($_smarty_tpl->tpl_vars['bAffMod']->value || $_smarty_tpl->tpl_vars['bAffSupp']->value || !empty($_smarty_tpl->tpl_vars['objListe']->value['aMenuDeroulant']) || $_smarty_tpl->tpl_vars['bAffRecapLigne']->value) {?>
          <?php if (empty($_smarty_tpl->tpl_vars['bActiveFormSelect']->value)) {?>
          <td style="text-align:center;vertical-align: middle;" class="td-action">
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="glyphicon glyphicon-option-vertical"></span>
              </button>
              <ul class="dropdown-menu" role="menu" style="left: 0 !important;">
                <?php if ($_smarty_tpl->tpl_vars['bAffMod']->value) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['sUrlForm'];?>
"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>
                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['bAffSupp']->value) {?><li><a href="#" onclick="bconf=confirm('Voulez-vous supprimer cette ligne ?');if(bconf)location.replace('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['sUrlSupp'];?>
');"><span class="glyphicon glyphicon-trash"></span> Supprimer</a></li><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['bAffDup']->value) {?><li><a href="#" onclick="bconf=confirm('Voulez-vous dupliquer cette ligne ?');if(bconf)location.replace('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['sUrlDuplicate'];?>
');"><span class="glyphicon glyphicon-copy"></span> Duppliquer</a></li><?php }?>
                <?php if ($_smarty_tpl->tpl_vars['bAffRecapLigne']->value) {?><li><a href="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['sUrlRecapLigne'];?>
" target="_blank"><span class="glyphicon glyphicon-eye-open"></span> Voir</a></li><?php }?>
                <?php if (!empty($_smarty_tpl->tpl_vars['objListe']->value['aMenuDeroulant'])) {?>
                  <?php
$_from = $_smarty_tpl->tpl_vars['objListe']->value['aMenuDeroulant'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_objListeMenu_42_saved_item = isset($_smarty_tpl->tpl_vars['objListeMenu']) ? $_smarty_tpl->tpl_vars['objListeMenu'] : false;
$_smarty_tpl->tpl_vars['objListeMenu'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['objListeMenu']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objListeMenu']->value) {
$_smarty_tpl->tpl_vars['objListeMenu']->_loop = true;
$__foreach_objListeMenu_42_saved_local_item = $_smarty_tpl->tpl_vars['objListeMenu'];
?>



                       <?php if (!isset($_smarty_tpl->tpl_vars['objListeMenu']->value['isbloque'])) {?>

                      <li><a href="<?php if ($_smarty_tpl->tpl_vars['objListeMenu']->value['href'] != '') {
echo $_smarty_tpl->tpl_vars['objListeMenu']->value['href'];
echo $_smarty_tpl->tpl_vars['surlVarGET']->value;
}?>"  onclick="<?php echo $_smarty_tpl->tpl_vars['objListeMenu']->value['javascript'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['objListeMenu']->value['target'])) {
echo $_smarty_tpl->tpl_vars['objListeMenu']->value['target'];
}?>><?php if (!empty($_smarty_tpl->tpl_vars['objListeMenu']->value['attribut'])) {?><span <?php echo $_smarty_tpl->tpl_vars['objListeMenu']->value['attribut'];?>
></span> <?php }
echo $_smarty_tpl->tpl_vars['objListeMenu']->value['intitule'];?>
</a></li>
                      <?php }?>
                  <?php
$_smarty_tpl->tpl_vars['objListeMenu'] = $__foreach_objListeMenu_42_saved_local_item;
}
if ($__foreach_objListeMenu_42_saved_item) {
$_smarty_tpl->tpl_vars['objListeMenu'] = $__foreach_objListeMenu_42_saved_item;
}
?>
                 <?php }?>
              </ul>
            </div>
          </td>


<?php }?>
          <?php }?>
          <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
              <?php if ($_smarty_tpl->tpl_vars['bRadioSelect']->value) {?><td style="width: 28px;"><input type="radio" value="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" name="iIdSelected"></td><?php }?>
              <?php if ($_smarty_tpl->tpl_vars['bCheckboxSelect']->value) {?><td style="width: 28px;"><input type="checkbox" value="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" name="iIds[]"></td><?php }?>
          <?php }?>
          <?php
$_from = $_smarty_tpl->tpl_vars['aEnteteListe']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_enteteliste_43_saved = isset($_smarty_tpl->tpl_vars['__smarty_foreach_enteteliste']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_enteteliste'] : false;
$__foreach_enteteliste_43_saved_item = isset($_smarty_tpl->tpl_vars['objEnteteListe']) ? $_smarty_tpl->tpl_vars['objEnteteListe'] : false;
$_smarty_tpl->tpl_vars['objEnteteListe'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['__smarty_foreach_enteteliste'] = new Smarty_Variable(array());
$__foreach_enteteliste_43_first = true;
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['objEnteteListe']->value) {
$_smarty_tpl->tpl_vars['objEnteteListe']->_loop = true;
$_smarty_tpl->tpl_vars['__smarty_foreach_enteteliste']->value['first'] = $__foreach_enteteliste_43_first;
$__foreach_enteteliste_43_first = false;
$__foreach_enteteliste_43_saved_local_item = $_smarty_tpl->tpl_vars['objEnteteListe'];
?>
              <td style="vertical-align: middle;text-align: center;<?php if (isset($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['html_td'])) {
echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['html_td'];
}?>" <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_perso_td'];?>
  >
                  <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_foreach_enteteliste']->value['first']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_enteteliste']->value['first'] : null)) {?>
                      <?php if (isset($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && is_array($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && count($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) > 0 && !empty($_smarty_tpl->tpl_vars['objListe']->value['liste_fils'][0][0][0])) {?>
                          <button class="btn btn-warning nprintable glyphicon glyphicon-resize-full" onclick="$(this).toggleClass('glyphicon glyphicon-resize-small').toggleClass('glyphicon glyphicon-resize-full');$(this).closest('tr').next().stop().fadeToggle('fast')"></button>
                      <?php }?>
                  <?php }?>

                  <?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
                      <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['activ_form_select'] == 'ok') {?>
                          <!-- ---------------- TEXT ---------------- -->
                          <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'text') {?>
                              <input   type="text"  name=" <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'];?>
" >
                          <?php }?>
                          <!-- ---------------- SELECT ---------------- -->
                          <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'select') {?>
                              <select
                                  style="padding:5px;"
                                  id="id_form_<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
"
                                  name="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];
if (isset($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['multiple'])) {?>[]<?php }?>"
                                  <?php if (isset($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['multiple'])) {?>multiple<?php }?>

                                  <?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['fonction_javascript'];?>

                              >
                                  <option value=""></option>
                                  <?php if (is_array($_smarty_tpl->tpl_vars['listeElemSelect']->value)) {?>
                                      <?php
$_from = $_smarty_tpl->tpl_vars['listeElemSelect']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_possible_bdd_44_saved_item = isset($_smarty_tpl->tpl_vars['valeur_possible_bdd']) ? $_smarty_tpl->tpl_vars['valeur_possible_bdd'] : false;
$__foreach_valeur_possible_bdd_44_saved_key = isset($_smarty_tpl->tpl_vars['id_valeur_possible']) ? $_smarty_tpl->tpl_vars['id_valeur_possible'] : false;
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['id_valeur_possible'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['id_valeur_possible']->value => $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd']->_loop = true;
$__foreach_valeur_possible_bdd_44_saved_local_item = $_smarty_tpl->tpl_vars['valeur_possible_bdd'];
?>
                                          <option value="<?php echo $_smarty_tpl->tpl_vars['id_valeur_possible']->value;?>
" <?php if ($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'] == $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value) {?>selected<?php }?>>
                                              <?php echo $_smarty_tpl->tpl_vars['valeur_possible_bdd']->value;?>

                                          </option>
                                      <?php
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_44_saved_local_item;
}
if ($__foreach_valeur_possible_bdd_44_saved_item) {
$_smarty_tpl->tpl_vars['valeur_possible_bdd'] = $__foreach_valeur_possible_bdd_44_saved_item;
}
if ($__foreach_valeur_possible_bdd_44_saved_key) {
$_smarty_tpl->tpl_vars['id_valeur_possible'] = $__foreach_valeur_possible_bdd_44_saved_key;
}
?>
                                  <?php }?>
                              </select>
                          <?php }?>
                          <!-- ---------------- CHECKBOX ---------------- -->
                          <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'checkbox') {?>
                              <table class="table table-hover table-condensed table-bordered">
                                  <tbody>
                                 <?php
$_from = $_smarty_tpl->tpl_vars['allElements']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_checkbox_45_saved_item = isset($_smarty_tpl->tpl_vars['nom_checkbox']) ? $_smarty_tpl->tpl_vars['nom_checkbox'] : false;
$__foreach_nom_checkbox_45_saved_key = isset($_smarty_tpl->tpl_vars['valeur_checkbox']) ? $_smarty_tpl->tpl_vars['valeur_checkbox'] : false;
$_smarty_tpl->tpl_vars['nom_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_checkbox'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_checkbox']->value => $_smarty_tpl->tpl_vars['nom_checkbox']->value) {
$_smarty_tpl->tpl_vars['nom_checkbox']->_loop = true;
$__foreach_nom_checkbox_45_saved_local_item = $_smarty_tpl->tpl_vars['nom_checkbox'];
?>
                                  <tr>
                                      <td style="padding-right:10px;">
                                          <input
                                              type="checkbox"
                                              name="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
[]"
                                              value="<?php echo $_smarty_tpl->tpl_vars['valeur_checkbox']->value;?>
"
                                              <?php if (is_array($_smarty_tpl->tpl_vars['objListe']->value['tableauChecked']) && in_array($_smarty_tpl->tpl_vars['valeur_checkbox']->value,$_smarty_tpl->tpl_vars['objListe']->value['tableauChecked'])) {?>checked<?php }?>
                                          >
                                      <td style="padding-right:20px;"><?php echo $_smarty_tpl->tpl_vars['nom_checkbox']->value;?>
</td>
                                  </tr>
                                  <?php
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_45_saved_local_item;
}
if ($__foreach_nom_checkbox_45_saved_item) {
$_smarty_tpl->tpl_vars['nom_checkbox'] = $__foreach_nom_checkbox_45_saved_item;
}
if ($__foreach_nom_checkbox_45_saved_key) {
$_smarty_tpl->tpl_vars['valeur_checkbox'] = $__foreach_nom_checkbox_45_saved_key;
}
?>
                                  </tbody>
                              </table>
                          <?php }?>
                          <!-- ---------------- RADIO ---------------- -->
                          <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'radio') {?>
                              <table class="table table-hover table-condensed table-bordered">
                                  <tbody>
                                 <?php
$_from = $_smarty_tpl->tpl_vars['allElements']->value['lesitem'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_nom_radio_46_saved_item = isset($_smarty_tpl->tpl_vars['nom_radio']) ? $_smarty_tpl->tpl_vars['nom_radio'] : false;
$__foreach_nom_radio_46_saved_key = isset($_smarty_tpl->tpl_vars['valeur_radio']) ? $_smarty_tpl->tpl_vars['valeur_radio'] : false;
$_smarty_tpl->tpl_vars['nom_radio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur_radio'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['nom_radio']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['valeur_radio']->value => $_smarty_tpl->tpl_vars['nom_radio']->value) {
$_smarty_tpl->tpl_vars['nom_radio']->_loop = true;
$__foreach_nom_radio_46_saved_local_item = $_smarty_tpl->tpl_vars['nom_radio'];
?>
                                  <tr class="test">
                                      <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                          <input
                                              type="radio"
                                              name="<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable'];?>
"
                                              value="<?php echo $_smarty_tpl->tpl_vars['valeur_radio']->value;?>
"
                                              <?php if ($_smarty_tpl->tpl_vars['valeur_radio']->value == $_smarty_tpl->tpl_vars['objListe']->value['checked']) {?>checked<?php }?>
                                          >
                                      </td>
                                      <td style="padding-bottom:0;vertical-align:middle;height:32px;"><?php echo $_smarty_tpl->tpl_vars['nom_radio']->value;?>
</td>
                                  </tr>
                                  <?php
$_smarty_tpl->tpl_vars['nom_radio'] = $__foreach_nom_radio_46_saved_local_item;
}
if ($__foreach_nom_radio_46_saved_item) {
$_smarty_tpl->tpl_vars['nom_radio'] = $__foreach_nom_radio_46_saved_item;
}
if ($__foreach_nom_radio_46_saved_key) {
$_smarty_tpl->tpl_vars['valeur_radio'] = $__foreach_nom_radio_46_saved_key;
}
?>
                                  </tbody>
                              </table>
                          <?php }?>
                      <?php } else { ?>
                         <span <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok" && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] != 'date') {?>  contenteditable="true" onblur="save_edtable('<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
','<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
',$(this).html())"<?php }?>>
                                  <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_radio'] == 'ok') {?>
                                     <input type="checkbox" data-on-text="Oui" data-off-text="Non" name="checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" id="checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" >
                                        <?php echo '<script'; ?>
 type="text/javascript">

                                            $("[name=checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
]").bootstrapSwitch();

                                           <?php if ($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'] == "OUI") {?>
                                                   $("[name=checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
]").bootstrapSwitch("state", true);
                                           <?php }?>

                                            $("input[name=checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
]").on("switchChange.bootstrapSwitch", function(e, state) {
                                                if( e.target.checked ){
                                                    save_edtable("<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
","<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_value_bouton_on_radio'];?>
");
                                                }else{
                                                    save_edtable("<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
","<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_value_bouton_off_radio'];?>
");
                                                }
                                            });
                                        <?php echo '</script'; ?>
>

                                  <?php } else { ?>
                                  <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'date' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok") {?>
                             <div onclick="apparition('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
')" style="cursor: pointer;">
                            <div style="display:none" id="inputdate<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
">
                             <input id="date<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
"  type="date"  ><br>
                             <span  onclick="save_edtable('<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
','<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
',$('#date<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
').val())" class="btn btn-success">Valider</span>
</div>

<?php }?>

                                      <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['format_affiche_liste'] != '') {?>
                                          <?php echo number_format($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'],$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['format_affiche_liste'],".",".");?>

                                      <?php } elseif ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['alias_champ'] != '' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] != 'checkbox' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] != 'date') {?>
                                         <?php if (isset($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'])) {?> <?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['alias_champ']]['value'];
}?>
                                      <?php } elseif ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'checkbox' || $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'date') {?>
                                              <?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['alias_champ']]['value'];?>
&nbsp;
                                      <?php } else { ?>
                                          <?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'];?>
&nbsp;
                                      <?php }?>
                                  <?php }?>
                                  <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'date' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok") {?>
                             </div>

<?php }?>
                              </span>
                      <?php }?>
                  <?php } else {
if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok" && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] != 'date') {?>
                   <div >
                   <div style="display:none" id="inputdate<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
">
                             <input id="text<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
"  type="text" value="<?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'];?>
"  ><br>
                             <span  onclick="save_edtable('<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
','<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
',$('#text<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
').val())" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></span>
                             <span   onclick="dispation('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
')" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></span>
</div>
                      <span style="cursor: pointer;color: black;<?php if ($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'] == '') {?> font-size: 37px;letter-spacing: 150px; <?php }?>"  onclick="apparition('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
')" id="<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
" >
                      <?php }?>
                                  <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_radio'] == 'ok') {?>
                                     <input type="checkbox" data-on-text="Oui" data-off-text="Non" name="checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" id="checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
" >
                                        <?php echo '<script'; ?>
 type="text/javascript">

                                            $("[name=checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
]").bootstrapSwitch();

                                           <?php if ($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'] == "OUI") {?>
                                                   $("[name=checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
]").bootstrapSwitch("state", true);
                                           <?php }?>

                                            $("input[name=checkbox_visible_<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
]").on("switchChange.bootstrapSwitch", function(e, state) {
                                                if( e.target.checked ){
                                                    save_edtable("<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
","<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_value_bouton_on_radio'];?>
");
                                                }else{
                                                    save_edtable("<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
","<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
","<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_value_bouton_off_radio'];?>
");
                                                }
                                            });
                                        <?php echo '</script'; ?>
>

                                  <?php } else { ?>
                                  <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'date' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok") {?>

                            <div  style="display:none" id="inputdate<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
">
                             <input id="date<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
"  type="date"  ><br>
                             <span  onclick="save_edtable('<?php echo $_smarty_tpl->tpl_vars['sNomTable']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['sChampId']->value;?>
','<?php echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
','<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];?>
',$('#date<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
').val())" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></span>
                             <span  onclick="dispation('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
')" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></span>
</div>

<?php }?>

                                      <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['format_affiche_liste'] != '') {?>
                                          <?php echo number_format($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'],$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['format_affiche_liste'],".",".");?>

                                      <?php } elseif ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['alias_champ'] != '' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] != 'checkbox' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] != 'date') {?>
                                         <?php if (isset($_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'])) {?> <?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['alias_champ']]['value'];
}?>
                                      <?php } elseif ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'checkbox' || $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'date') {?>
                                      <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'date' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok") {?>
                                      <span style="cursor: pointer" <?php ob_start();
echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'];
$_tmp1=ob_get_clean();
if ($_tmp1 == '') {?> style="font-size: 37px;letter-spacing: 150px;" <?php }?> onclick="apparition('<?php echo $_smarty_tpl->tpl_vars['objListe']->value['id'];
echo $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['mapping_champ'];?>
')">
                                      <?php }?>
                                              <?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['alias_champ']]['value'];?>
&nbsp;
                                              <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] == 'date' && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok") {?>
                                      </span>
                                      <?php }?>
                                      <?php } else { ?>
                                          <?php echo $_smarty_tpl->tpl_vars['objListe']->value[$_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['nom_variable']]['value'];?>
&nbsp;
                                      <?php }?>
                                  <?php }?>

                              </span>
                              <?php if ($_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['html_editable_td'] == "ok" && $_smarty_tpl->tpl_vars['objEnteteListe']->value['objElement']['type_champ'] != 'date') {?>
                              </div>
                              <?php }?>
                  <?php }?>

                  </td>
          <?php
$_smarty_tpl->tpl_vars['objEnteteListe'] = $__foreach_enteteliste_43_saved_local_item;
}
if ($__foreach_enteteliste_43_saved) {
$_smarty_tpl->tpl_vars['__smarty_foreach_enteteliste'] = $__foreach_enteteliste_43_saved;
}
if ($__foreach_enteteliste_43_saved_item) {
$_smarty_tpl->tpl_vars['objEnteteListe'] = $__foreach_enteteliste_43_saved_item;
}
?>
          </tr>
          <?php if (isset($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && is_array($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) && count($_smarty_tpl->tpl_vars['objListe']->value['liste_fils']) > 0 && !empty($_smarty_tpl->tpl_vars['objListe']->value['liste_fils'][0][0][0])) {?>
              <tr style="display: none;">
                  <td style="padding:0;vertical-align: middle;" colspan="<?php echo $_smarty_tpl->tpl_vars['iNombreColonneListe']->value;?>
">
                      <div class="class_form_list_toggle_div">
                          <div class="pt0Print" style="padding:20px 20px 0 20px;">
                              <?php
$_from = $_smarty_tpl->tpl_vars['objListe']->value['liste_fils'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_sTpl_47_saved_item = isset($_smarty_tpl->tpl_vars['sTpl']) ? $_smarty_tpl->tpl_vars['sTpl'] : false;
$_smarty_tpl->tpl_vars['sTpl'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['sTpl']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['sTpl']->value) {
$_smarty_tpl->tpl_vars['sTpl']->_loop = true;
$__foreach_sTpl_47_saved_local_item = $_smarty_tpl->tpl_vars['sTpl'];
?>
                                  <?php echo $_smarty_tpl->tpl_vars['sTpl']->value[0][0];?>

                              <?php
$_smarty_tpl->tpl_vars['sTpl'] = $__foreach_sTpl_47_saved_local_item;
}
if ($__foreach_sTpl_47_saved_item) {
$_smarty_tpl->tpl_vars['sTpl'] = $__foreach_sTpl_47_saved_item;
}
?>
                          </div>
                      </div>
                  </td>
              </tr>
          <?php }?>
      <?php
$_smarty_tpl->tpl_vars['objListe'] = $__foreach_objListe_41_saved_local_item;
}
if ($__foreach_objListe_41_saved_item) {
$_smarty_tpl->tpl_vars['objListe'] = $__foreach_objListe_41_saved_item;
}
?>
      </tbody>
    </table>
  </div>
<?php if ($_smarty_tpl->tpl_vars['bActiveFormSelect']->value) {?>
    <div style="clear:both;float:right;margin-top:20px;">
        <input type="submit" value="Valider" style="background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;">
    </div>
    </form>
<?php }
if (isset($_smarty_tpl->tpl_vars['bPagination']->value) && $_smarty_tpl->tpl_vars['bPagination']->value && isset($_smarty_tpl->tpl_vars['aPaginationListe']->value) && isset($_smarty_tpl->tpl_vars['aPaginationListe']->value['select']) && isset($_smarty_tpl->tpl_vars['aPaginationListe']->value['liste'])) {?>
  <div class="nprintable" style="text-align: center;">
      <ul class="pagination">
        <li>
          <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_premiere'];?>
" aria-label="Previous">
            <span aria-hidden="true" class="glyphicon glyphicon-fast-backward"></span>
          </a>
        </li>
        <li>
          <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_precedente'];?>
" aria-label="Previous">
            <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
          </a>
        </li>
        <?php
$_from = $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_liste'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_page_48_saved_item = isset($_smarty_tpl->tpl_vars['page']) ? $_smarty_tpl->tpl_vars['page'] : false;
$_smarty_tpl->tpl_vars['page'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['page']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['page']->value) {
$_smarty_tpl->tpl_vars['page']->_loop = true;
$__foreach_page_48_saved_local_item = $_smarty_tpl->tpl_vars['page'];
?>
            <?php if (isset($_smarty_tpl->tpl_vars['page']->value['selected']) && $_smarty_tpl->tpl_vars['page']->value['selected']) {?>
                <li class="active"><a href="#"><strong><?php echo $_smarty_tpl->tpl_vars['page']->value['numero'];?>
</strong><span class="sr-only">(current)</span></a></li>
            <?php } else { ?>
                <li><a href="<?php echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
"><?php echo $_smarty_tpl->tpl_vars['page']->value['numero'];?>
</a></li>
            <?php }?>
        <?php
$_smarty_tpl->tpl_vars['page'] = $__foreach_page_48_saved_local_item;
}
if ($__foreach_page_48_saved_item) {
$_smarty_tpl->tpl_vars['page'] = $__foreach_page_48_saved_item;
}
?>
        <li>
          <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_suivante'];?>
" aria-label="Next">
            <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </li>
        <li>
          <a href="<?php echo $_smarty_tpl->tpl_vars['aPaginationListe']->value['liste']['page_derniere'];?>
" aria-label="Previous">
            <span aria-hidden="true" class="glyphicon glyphicon-fast-forward"></span>
          </a>
        </li>
      </ul>
      <select onchange="document.location.href=this.value" style="display: inline-block;margin: 20px 0;vertical-align: top;height: 34px;margin-left: 5px;">
          <option value="">Numéro de la page</option>
          <?php
$_from = $_smarty_tpl->tpl_vars['aPaginationListe']->value['select'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_page_49_saved_item = isset($_smarty_tpl->tpl_vars['page']) ? $_smarty_tpl->tpl_vars['page'] : false;
$_smarty_tpl->tpl_vars['page'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['page']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['page']->value) {
$_smarty_tpl->tpl_vars['page']->_loop = true;
$__foreach_page_49_saved_local_item = $_smarty_tpl->tpl_vars['page'];
?>
              <?php if ($_smarty_tpl->tpl_vars['page']->value['active'] != '') {?>
                  <option value="<?php echo $_smarty_tpl->tpl_vars['page']->value['url'];?>
" <?php if (isset($_smarty_tpl->tpl_vars['page']->value['selected']) && $_smarty_tpl->tpl_vars['page']->value['selected']) {?>selected<?php }?>><?php echo $_smarty_tpl->tpl_vars['page']->value['numero'];?>
</option>
              <?php }?>
          <?php
$_smarty_tpl->tpl_vars['page'] = $__foreach_page_49_saved_local_item;
}
if ($__foreach_page_49_saved_item) {
$_smarty_tpl->tpl_vars['page'] = $__foreach_page_49_saved_item;
}
?>
      </select>
  </div>
<?php }
}
if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value) && count($_smarty_tpl->tpl_vars['itemBoutonsListe']->value) > 0) {?>
    <div style="clear:both;float:right;margin-top:20px;">
        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent'])) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['url'];?>
">
                <input
                        type="button"
                        value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['text_label'];?>
"
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['style'];?>
"<?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['tableau_attribut'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_50_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_50_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_50_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_50_saved_local_item;
}
if ($__foreach_valeur_50_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_50_saved_item;
}
if ($__foreach_valeur_50_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_50_saved_key;
}
?>
                <?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['fonction_javascript'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['precedent']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_51_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_51_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_51_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_51_saved_local_item;
}
if ($__foreach_valeur_51_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_51_saved_item;
}
if ($__foreach_valeur_51_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_51_saved_key;
}
?>
                <?php }?>
                >
            </a>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant'])) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['url'];?>
">
                <input
                        type="button"
                        id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['nom_variable'];?>
"
                        name="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['nom_variable'];?>
"
                        value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['text_label'];?>
"
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['style'];?>
"<?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['tableau_attribut'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_52_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_52_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_52_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_52_saved_local_item;
}
if ($__foreach_valeur_52_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_52_saved_item;
}
if ($__foreach_valeur_52_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_52_saved_key;
}
?>
                <?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['fonction_javascript'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['suivant']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_53_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_53_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_53_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_53_saved_local_item;
}
if ($__foreach_valeur_53_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_53_saved_item;
}
if ($__foreach_valeur_53_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_53_saved_key;
}
?>
                <?php }?>
                >
            </a>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider'])) {?>
            <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['url'];?>
">
                <input
                        type="button"
                        id="id_<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['nom_variable'];?>
"
                        name="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['nom_variable'];?>
"
                        value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['text_label'];?>
"
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['style'];?>
"<?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['tableau_attribut'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_54_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_54_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_54_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_54_saved_local_item;
}
if ($__foreach_valeur_54_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_54_saved_item;
}
if ($__foreach_valeur_54_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_54_saved_key;
}
?>
                <?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['fonction_javascript'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['valider']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_55_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_55_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_55_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_55_saved_local_item;
}
if ($__foreach_valeur_55_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_55_saved_item;
}
if ($__foreach_valeur_55_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_55_saved_key;
}
?>
                <?php }?>
                >
            </a>
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler'])) {?>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['url'];?>
">
                <input
                        type="button"
                        value="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['text_label'];?>
"
                        <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['style']) && $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['style'] != '') {?>style="<?php echo $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['style'];?>
"<?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['tableau_attribut']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['tableau_attribut'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['tableau_attribut'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_56_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_56_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_56_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_56_saved_local_item;
}
if ($__foreach_valeur_56_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_56_saved_item;
}
if ($__foreach_valeur_56_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_56_saved_key;
}
?>
                <?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['fonction_javascript']) && is_array($_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['fonction_javascript'])) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['itemBoutonsListe']->value['annuler']['fonction_javascript'];
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_valeur_57_saved_item = isset($_smarty_tpl->tpl_vars['valeur']) ? $_smarty_tpl->tpl_vars['valeur'] : false;
$__foreach_valeur_57_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['valeur'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['valeur']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['valeur']->value) {
$_smarty_tpl->tpl_vars['valeur']->_loop = true;
$__foreach_valeur_57_saved_local_item = $_smarty_tpl->tpl_vars['valeur'];
?>
                        <?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
="<?php echo $_smarty_tpl->tpl_vars['valeur']->value;?>
"
                    <?php
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_57_saved_local_item;
}
if ($__foreach_valeur_57_saved_item) {
$_smarty_tpl->tpl_vars['valeur'] = $__foreach_valeur_57_saved_item;
}
if ($__foreach_valeur_57_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_valeur_57_saved_key;
}
?>
                <?php }?>
                >
            </a>
        <?php }?>
    </div>
<?php }
}?>

<?php echo '<script'; ?>
>


    /* $('#checkAll').click(function() {
     // on cherche les checkbox à l'intérieur de l'id  'magazine'
     //var allcheked = $(".allchecked").find(':checkbox');
     //alert('guy');
     if(this.checked){ // si 'checkAll' est coché
     $(':checkbox.allchecked').prop('checked', true);
     //alert('ok');
     }else{ // si on décoche 'checkAll'
     $(':checkbox.allchecked').prop('checked', false);
     //alert('pasok');
     }
     });
     */

    function allcheck(statut) {
        //alert('guy');
        if (statut) { // si 'checkAll' est coché
            $(':checkbox.allchecked').prop('checked', true);
            //alert('ok');
        } else { // si on décoche 'checkAll'
            $(':checkbox.allchecked').prop('checked', false);
            //alert('pasok');
        }
    }

    function apparition(id) {

        //$("#inputdate"+id).css('display','block');
        $("#inputdate" + id).fadeIn();
    }

    function dispation(id) {


        $("#inputdate" + id).fadeOut();

        console.log($('#text1516info_crea_contratproduit').val());

    }


<?php echo '</script'; ?>
><?php }
}
