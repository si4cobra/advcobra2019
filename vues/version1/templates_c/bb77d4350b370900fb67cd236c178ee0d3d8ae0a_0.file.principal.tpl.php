<?php
/* Smarty version 3.1.29, created on 2019-10-09 15:57:55
  from "/var/www/html/vues/version1/templates/principal.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5d9de763e3d269_07964836',
  'file_dependency' => 
  array (
    'bb77d4350b370900fb67cd236c178ee0d3d8ae0a' => 
    array (
      0 => '/var/www/html/vues/version1/templates/principal.tpl',
      1 => 1567407798,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d9de763e3d269_07964836 ($_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (!empty($_smarty_tpl->tpl_vars['metaModules']->value)) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['metaModules']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_metaModule_0_saved_item = isset($_smarty_tpl->tpl_vars['metaModule']) ? $_smarty_tpl->tpl_vars['metaModule'] : false;
$__foreach_metaModule_0_saved_key = isset($_smarty_tpl->tpl_vars['cle']) ? $_smarty_tpl->tpl_vars['cle'] : false;
$_smarty_tpl->tpl_vars['metaModule'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cle'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['metaModule']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cle']->value => $_smarty_tpl->tpl_vars['metaModule']->value) {
$_smarty_tpl->tpl_vars['metaModule']->_loop = true;
$__foreach_metaModule_0_saved_local_item = $_smarty_tpl->tpl_vars['metaModule'];
?>
            <meta name="<?php echo $_smarty_tpl->tpl_vars['cle']->value;?>
" content="<?php echo $_smarty_tpl->tpl_vars['metaModule']->value;?>
">
        <?php
$_smarty_tpl->tpl_vars['metaModule'] = $__foreach_metaModule_0_saved_local_item;
}
if ($__foreach_metaModule_0_saved_item) {
$_smarty_tpl->tpl_vars['metaModule'] = $__foreach_metaModule_0_saved_item;
}
if ($__foreach_metaModule_0_saved_key) {
$_smarty_tpl->tpl_vars['cle'] = $__foreach_metaModule_0_saved_key;
}
?>
    <?php }?>
    <title><?php echo $_smarty_tpl->tpl_vars['sTitreDeLaPage']->value;?>
</title>

    <!-- Bootstrap -->
    <link href="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/css/simple-sidebar.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"><?php echo '</script'; ?>
>
    <![endif]-->

    <meta name="google-site-verification" content="sOuXqP6oDwIfUf6biVsfI_wNMug2oNR1I6J8G8pdgBg"/>
    <!--[if IE]>
    <link rel="stylesheet" media="all" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/css/pro_dropline_ie.css"/>
    <![endif]-->

    <!--<link type='text/css' href='css/experiment.css' rel='stylesheet' media='screen' />-->

    <!--  date picker script -->
    <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/css/datePicker.css" type="text/css"/>
    <link href="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/css/select2.min.css" rel="stylesheet" type="text/css"/>

    <?php if (!empty($_smarty_tpl->tpl_vars['favicon']->value)) {?><link rel="icon" type="image/png" href="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['favicon']->value;?>
" /><?php }?>

    <?php if (!empty($_smarty_tpl->tpl_vars['cssModules']->value)) {?>
        <?php
$_from = $_smarty_tpl->tpl_vars['cssModules']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_cssModule_1_saved_item = isset($_smarty_tpl->tpl_vars['cssModule']) ? $_smarty_tpl->tpl_vars['cssModule'] : false;
$_smarty_tpl->tpl_vars['cssModule'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['cssModule']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['cssModule']->value) {
$_smarty_tpl->tpl_vars['cssModule']->_loop = true;
$__foreach_cssModule_1_saved_local_item = $_smarty_tpl->tpl_vars['cssModule'];
?>
            <link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->tpl_vars['cssModule']->value;?>
">
        <?php
$_smarty_tpl->tpl_vars['cssModule'] = $__foreach_cssModule_1_saved_local_item;
}
if ($__foreach_cssModule_1_saved_item) {
$_smarty_tpl->tpl_vars['cssModule'] = $__foreach_cssModule_1_saved_item;
}
?>
    <?php }?>

    <!--<link href="css/color-custom.php" rel="stylesheet" type="text/css"  />-->
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/fonction_adv.js" type="text/javascript"><?php echo '</script'; ?>
>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) 1.11.2 -->
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/jquery-1.11.2.min.js"><?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/bootstrap-switch.min.js"><?php echo '</script'; ?>
>

    
    <style>
        @media print { 
            @page  {
                size: landscape ;
        }
            a[href]:after {
                content: "";
            }
            h1
            {
                font-size: 20px!important;
            }
            #big-contain #page-heading
            {
                background-color: #9B410E!important;
            }
        }

        .ui-menu {
            list-style:none;
            padding: 10px;
            margin: 0;
            display:block;
            width:227px;
        }
        .ui-menu .ui-menu {
            margin-top: -3px;
        }
        .ui-menu .ui-menu-item {
            margin:0;
            padding: 0;
            width: 200px;
        }
        .ui-menu .ui-menu-item a {
            text-decoration:none;
            display:block;
            padding:.2em .4em;
            line-height:1.5;
            zoom:1;
        }
        .ui-menu .ui-menu-item a.ui-state-hover,
        .ui-menu .ui-menu-item a.ui-state-active {
            margin: -1px;
        }
        .ui-menu-item-wrapper
        {
            background-color: white;
        }
        .ui-helper-hidden-accessible
        {
            display: none;
        }

    </style>


    

</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top hidden-print" style="padding: 0 10px;">
    <div>

        <div class="navbar-header">
            <?php if (!empty($_smarty_tpl->tpl_vars['tTplMenu']->value)) {?>
                <a href="#menu-toggle" class="btn btn-default navbar-brand" id="menu-toggle"><span
                            class="glyphicon glyphicon-menu-hamburger"></span></a>
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['bEstConnecte']->value) {?>
                <a href="deco.php" class="btn btn-default navbar-brand" id="btnLogOut"
                   style="height: 34px !important;margin-left:10px;"><span class="glyphicon glyphicon-off"></span></a>
                <a href="utilisateur-ctrl_fli_notification-fli_renvoi_notification" style="margin-left: 10px;padding: 6px;
    margin-top: 8px;
    margin-right: 10px;" class="btn btn-default navbar-brand" id="menu-toggle"><span
                            class="glyphicon glyphicon-comment"></span><span style="color: red" id="nbNotiification"></span></a>
            <?php } else { ?>
                <a href="fli_connexion-ctrl_fli_connexion-connexion" class="btn btn-default navbar-brand" id="btnLogOut"
                   style="height: 34px !important;margin-left:10px;"><span class="glyphicon glyphicon-user"></span></a>
            <?php }?>
            
            <?php if (!empty($_smarty_tpl->tpl_vars['logo']->value)) {?><a href="<?php echo $_smarty_tpl->tpl_vars['fli_module_defaut']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['fli_controleur_defaut']->value;?>
-<?php echo $_smarty_tpl->tpl_vars['fli_fonction_defaut']->value;?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['logo']->value;?>
" alt="logo" id="logoHome" style="float: right;"></a><?php }?>
            <a href="crm-ctrl_tableaubord-fli_tableaubord" class="btn btn-default navbar-brand hiddenMenu" id="btnLogOut">Dashboard</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse hidden-print">

        </div><!--/.nav-collapse -->
    </div>
</nav>

<?php if (isset($_smarty_tpl->tpl_vars['tTplMenu']->value)) {?>
    <?php echo $_smarty_tpl->tpl_vars['tTplMenu']->value;?>

<?php }?>

<!-- Page Content -->
<div id="page-content-wrapper" <?php if (empty($_smarty_tpl->tpl_vars['tTplMenu']->value)) {?>style="margin-top: 40px;"<?php }?>>
    <div class="container-fluid">
        <div class="row">
            <?php if (isset($_smarty_tpl->tpl_vars['pagedirection']->value)) {?>
                <?php if (is_array($_smarty_tpl->tpl_vars['pagedirection']->value)) {?>
                    <?php
$_from = $_smarty_tpl->tpl_vars['pagedirection']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tabTpl_2_saved_item = isset($_smarty_tpl->tpl_vars['tabTpl']) ? $_smarty_tpl->tpl_vars['tabTpl'] : false;
$_smarty_tpl->tpl_vars['tabTpl'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['tabTpl']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['tabTpl']->value) {
$_smarty_tpl->tpl_vars['tabTpl']->_loop = true;
$__foreach_tabTpl_2_saved_local_item = $_smarty_tpl->tpl_vars['tabTpl'];
?>
                        <?php
$_from = $_smarty_tpl->tpl_vars['tabTpl']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_tpl_3_saved_item = isset($_smarty_tpl->tpl_vars['tpl']) ? $_smarty_tpl->tpl_vars['tpl'] : false;
$_smarty_tpl->tpl_vars['tpl'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['tpl']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['tpl']->value) {
$_smarty_tpl->tpl_vars['tpl']->_loop = true;
$__foreach_tpl_3_saved_local_item = $_smarty_tpl->tpl_vars['tpl'];
?>
                            <?php echo $_smarty_tpl->tpl_vars['tpl']->value;?>

                        <?php
$_smarty_tpl->tpl_vars['tpl'] = $__foreach_tpl_3_saved_local_item;
}
if ($__foreach_tpl_3_saved_item) {
$_smarty_tpl->tpl_vars['tpl'] = $__foreach_tpl_3_saved_item;
}
?>
                    <?php
$_smarty_tpl->tpl_vars['tabTpl'] = $__foreach_tabTpl_2_saved_local_item;
}
if ($__foreach_tabTpl_2_saved_item) {
$_smarty_tpl->tpl_vars['tabTpl'] = $__foreach_tabTpl_2_saved_item;
}
?>
                <?php } else { ?>
                    <?php echo $_smarty_tpl->tpl_vars['pagedirection']->value;?>

                <?php }?>
            <?php }?>

        </div>
    </div>
</div>
<!-- /#wrapper -->


<!-- Include all compiled plugins (below), or include individual files as needed -->


<!--  fonction personnaliser -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/fonction_adv.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/fonction_menu_principal.js" type="text/javascript"><?php echo '</script'; ?>
>

    <!-- Custom jquery scripts -->
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/jquery/custom_jquery.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/jquery/date.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/jquery/jquery.datePicker.js" type="text/javascript"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/jquery/jquery.maskedinput.js" type="text/javascript"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" charset="utf-8">
        $(function () {

            // initialise the "Select date" link
            $('#date-pick')
                    .datePicker(
                            // associate the link with a date picker
                            {
                                createButton: false,
                                startDate: '01/01/2005',
                                endDate: '31/12/2020'
                            }
                    ).bind(
                    // when the link is clicked display the date picker
                    'click',
                    function () {
                        updateSelects($(this).dpGetSelected()[0]);
                        $(this).dpDisplay();
                        return false;
                    }
            ).bind(
                    // when a date is selected update the SELECTs
                    'dateSelected',
                    function (e, selectedDate, $td, state) {
                        updateSelects(selectedDate);
                    }
            ).bind(
                    'dpClosed',
                    function (e, selected) {
                        updateSelects(selected[0]);
                    }
            );

            var updateSelects = function (selectedDate) {
                var selectedDate = new Date(selectedDate);
                $('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
                $('#m option[value=' + (selectedDate.getMonth() + 1) + ']').attr('selected', 'selected');
                $('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
            }
            // listen for when the selects are changed and update the picker
            $('#d, #m, #y')
                    .bind(
                            'change',
                            function () {
                                var d = new Date(
                                        $('#y').val(),
                                        $('#m').val() - 1,
                                        $('#d').val()
                                );
                                $('#date-pick').dpSetSelected(d.asString());
                            }
                    );

            // default the position of the selects to today
            //var today = new Date();
            //updateSelects(today.getTime());

            // and update the datePicker to reflect it...
            $('#d').trigger('change');
        });

        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            $('.nav-tabs a').click(function (e) {
                $(this).tab('show');
                var scrollmem = $('body').scrollTop();
                window.location.hash = this.hash;
                $('html,body').scrollTop(scrollmem);
            });
        });
    <?php echo '</script'; ?>
>

    <?php echo '<script'; ?>
 type="text/javascript">

        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        $("#menu-toggle2").click(function (e) {
            e.preventDefault();
            $("#wrapper2").toggleClass("toggled");
        });

        $(document).mouseup(function (e) {
            var container = $("#sidebar-wrapper");
            var container2 = $("#menu-toggle");

            if (!container.is(e.target) && container.has(e.target).length === 0 && !container2.is(e.target) && container2.has(e.target).length === 0 && $("#wrapper").hasClass("toggled")) {
                $("#wrapper").toggleClass("toggled");
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#sidebar-wrapper2");
            var container2 = $("#menu-toggle2");

            if (!container.is(e.target) && container.has(e.target).length === 0 && !container2.is(e.target) && container2.has(e.target).length === 0 && $("#wrapper2").hasClass("toggled")) {
                $("#wrapper2").toggleClass("toggled");
            }
        });

        $('.carousel').carousel({
            interval: false
        });

    <?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/ckeditor/ckeditor.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/ckfinder/ckfinder.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/nbnotif.js" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['cheminAccesPublic']->value;?>
/js/select2.full.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>$(".js-example-responsive").select2();<?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript">
        if ($('.table-responsive .table').width() < $('#page-content-wrapper').width() || (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))) {
            $('.btnScroll').hide();
        }

        $(".btnScrollLeft").mousedown(startScrollingLeft).mouseup(stopScrolling);
        $(".btnScrollRight").mousedown(startScrollingRight).mouseup(stopScrolling);

        function startScrollingLeft() {
            // contintually increase scroll position
            $('.table-responsive').animate({scrollLeft: '-=250'}, 'easeInOutQuint', startScrollingLeft);
        }

        function startScrollingRight() {
            // contintually increase scroll position
            $('.table-responsive').animate({scrollLeft: '+=250'}, 'easeInOutQuint', startScrollingRight);
        }

        function stopScrolling() {
            // stop increasing scroll position
            $('.table-responsive').stop();
        }

    <?php echo '</script'; ?>
>

<?php if (!empty($_smarty_tpl->tpl_vars['jsModules']->value)) {?>
    <?php
$_from = $_smarty_tpl->tpl_vars['jsModules']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$__foreach_jsModule_4_saved_item = isset($_smarty_tpl->tpl_vars['jsModule']) ? $_smarty_tpl->tpl_vars['jsModule'] : false;
$_smarty_tpl->tpl_vars['jsModule'] = new Smarty_Variable();
$_smarty_tpl->tpl_vars['jsModule']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['jsModule']->value) {
$_smarty_tpl->tpl_vars['jsModule']->_loop = true;
$__foreach_jsModule_4_saved_local_item = $_smarty_tpl->tpl_vars['jsModule'];
?>
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['jsModule']->value;?>
"><?php echo '</script'; ?>
>
    <?php
$_smarty_tpl->tpl_vars['jsModule'] = $__foreach_jsModule_4_saved_local_item;
}
if ($__foreach_jsModule_4_saved_item) {
$_smarty_tpl->tpl_vars['jsModule'] = $__foreach_jsModule_4_saved_item;
}
}
if ($_smarty_tpl->tpl_vars['bDebug']->value && !empty($_smarty_tpl->tpl_vars['aDebug']->value)) {?>
    <?php echo '<script'; ?>
 type="text/javascript">
        var win=window.open("debug.php?fli_module_debug=<?php echo $_smarty_tpl->tpl_vars['fli_module']->value;?>
&fli_fonction_debug=<?php echo $_smarty_tpl->tpl_vars['fli_fonction']->value;?>
", "Fli Debug", "width=800, height=600");
    <?php echo '</script'; ?>
>
<?php }?>
</body>
</html><?php }
}
