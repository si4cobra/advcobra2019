<?php
/* Smarty version 3.1.29, created on 2019-10-09 15:58:02
  from "/var/www/html/vues/version1/templates/login.tpl" */

if ($_smarty_tpl->smarty->ext->_validateCompiled->decodeProperties($_smarty_tpl, array (
  'has_nocache_code' => false,
  'version' => '3.1.29',
  'unifunc' => 'content_5d9de76a093cd3_78379329',
  'file_dependency' => 
  array (
    'a96c90d55b5561641903d3af2cbb8bc2794dde66' => 
    array (
      0 => '/var/www/html/vues/version1/templates/login.tpl',
      1 => 1536224891,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d9de76a093cd3_78379329 ($_smarty_tpl) {
?>
<div class="container">

    <form class="form-signin" method="post">
        <?php if (!empty($_smarty_tpl->tpl_vars['erreur']->value)) {?>
            <div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                Erreur de connexion !<br/>
                <?php echo $_smarty_tpl->tpl_vars['erreur']->value;?>

            </div>
        <?php }?>
        <h2 class="form-signin-heading">FRAMEWORK</h2>
        <input type="hidden" name="validation" value="ok">
        <label for="inputUsername" class="sr-only">Utilisateur</label>
        <input type="text" id="inputUsername" class="form-control" placeholder="Utilisateur" name="login" required
               autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" name="password"
               required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Se connecter</button>
    </form>

</div><?php }
}
