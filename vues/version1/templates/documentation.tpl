<div style="color:#313131;margin: 0 auto;padding: 15px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;">

    <div id="page-heading"><div style="display:inline-block;border:1px solid #D0D1D5;margin-bottom: 20px;"><div class="hidden-xs title-head-search" style="display: inline-block;height: 103px;width: 73px;vertical-align: middle;padding: 20px;"><span class="glyphicon glyphicon-link" style="color:#fff;vertical-align: bottom;font-size: 20px;top: 18px;"></span></div><h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;" class="text-head-search">{$sTitreForm}</h1></div></div>

    <form  method="POST" action="">
        <input type="hidden" name="validation" value="ok">
        <input type="hidden" name="route" value="{$route}">

<table border="1" cellpadding="0" cellspacing="0" id="id-form" style="margin: 0 auto;padding:20px;background-color:#fff;" class="table table-condensed table-hover">
    <tbody>
        <tr>
            <td>
                <div class="formaff" id="test">
                    {$description}
                </div>
                <div class="formmodif" style="display:none" id="modif">
                <textarea
                            id="id_contenu"
                            name="contenu"
                            class="ckeditor"
                            cols="30"
                            rows="40">{$description}
                    </textarea>
                </div>

            </td>
            <td>
                <b>Historique des modifications</b>
                <table width="100%">
                    <tr>
                        <td><b>Date Action</b></td><td><b>Nom personne</b></td>
                    </tr>
                    {foreach from=$aTabdoclist item=objList}
                        <tr>
                            <td>
                                {$objList.datemodif}
                            </td>
                            <td>
                                {$objList.nom_user} {$objList.prenom_user}
                            </td>
                            <td>
                                <button type ="button" onclick="location.replace('fli_admin-ctrl_documentation-fli_documentation?route={$route}&guidhisto={$objList.guid_historiquedoc}')">Voir</button>
                            </td>
                        </tr>
                    {/foreach}
                </table>
            </td>
        </tr>
    <tr>
        <td align="center">
            <table>
                <tr>
                    <td><button id="formmodif"  type="submit" style="display:none" >Valider</button>&nbsp;&nbsp;&nbsp;</td>
                <td><button type="button"  id="formaff" onclick="document.getElementById('modif').style.display='block';document.getElementById('formmodif').style.display='block';document.getElementById('formaff').style.display='none';document.getElementById('formaff').style.display='none';">Modifier</button>&nbsp;&nbsp;<td>
                    <td><button type="button"  id="formaff" onclick="document.getElementById('modif').style.display='none';document.getElementById('formmodif').style.display='none';document.getElementById('formaff').style.display='block';document.getElementById('formaff').style.display='block';">Annuler</button>&nbsp;&nbsp;<td>
                    <td><button type="button" onclick="location.replace('{$route}')">Retour</button></td>
                </tr>
            </table>
        </td>
    </tr>
    {if !empty($tabhistoriquetext)}
        <tr>
            <td height="30px">

            </td>
        </tr>
        <tr>
            <td>
                <b>Modifié le {$tabhistoriquetext.datemodif} par {$tabhistoriquetext.nom_user} {$tabhistoriquetext.prenom_user}  </b><br>
                {$tabhistoriquetext.description_historiquedoc}
            </td>
        </tr>
    {/if}
    </tbody>
</table>
</form>
</div>
</div>