{if $aListe|is_array and $aListe|@count > 0}
    <div class="table-responsive">
        <table style="background-color: #fafafa;margin-bottom: 30px;" class="table table-hover table-condensed table-bordered table-striped">
            <thead>
            <tr>
                {if $bAffFiche or $bAffMod or $bAffSupp}
                    <th class="table-header-repeat line-left minwidth-1 title-table-head th-action" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                        <a href="javascript:Void(0);" style="color:#fff;">Actions</a>
                    </th>
                {/if}
                {foreach from=$aEnteteListe item=objEnteteListe}
                    <th class="table-header-repeat line-left minwidth-1 title-table-head " style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                        <a href="javascript:Void(0);" style="color:#fff;">{$objEnteteListe.sLabel}</a>
                    </th>
                {/foreach}
            </tr>
            </thead>
            <tbody>
            {foreach from=$aListe item=objListe}
                <tr>
                    {if $bAffFiche or $bAffMod or $bAffSupp}
                        <td style="text-align:center;vertical-align: middle;" class="td-action">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <span class="glyphicon glyphicon-option-vertical"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" style="left: 123px !important;top: inherit;">
                                {if $bAffFiche}<li><a href="{$objListe.sUrlFiche}" class="icon-5 info-tooltip"></a></li>{/if}
                                {if $bAffMod}<li><a href="{$objListe.sUrlForm}" class="icon-1 info-tooltip"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>{/if}
                                {if $bAffSupp}<li><a href="#" onclick="bconf=confirm('Voulez-vous supprimer cette ligne ?');if(bconf)location.replace('{$objListe.sUrlSupp}');" class="icon-2 info-tooltip"><span class="glyphicon glyphicon-trash"></span> Supprimer</a></li>{/if}
                            </ul>
                        </td>
                    {/if}
                   {* {$objListe|@print_r}*}
                    {foreach name=enteteliste from=$aEnteteListe item=objEnteteListe}
                        <td style="vertical-align: middle;text-align: center;">
                            {if $smarty.foreach.enteteliste.first}
                                {if isset($objListe.liste_fils) and $objListe.liste_fils|is_array and $objListe.liste_fils|@count > 0}
                                    <input type="submit" value=" + " class="class_form_list_toggle_input">
                                {/if}
                            {/if}
                           {* {$objListe[$objEnteteListe.objElement.nom_variable]}
                            {$objEnteteListe.objElement.nom_variable|print_r}*}
                            {if $objEnteteListe.objElement.format_affiche_liste != ''}
                                 {$objListe[$objEnteteListe.objElement.nom_variable].value|number_format:$objEnteteListe.objElement.format_affiche_liste:".":"."}
                            {elseif $objEnteteListe.objElement.alias_champ!=''}
                                {$objListe[$objEnteteListe.objElement.alias_champ].value}
                            {else}
                                {$objListe[$objEnteteListe.objElement.nom_variable].value}
                            {/if}
                        </td>
                    {/foreach}
                </tr>

            {/foreach}
            </tbody>
        </table>
    </div>
{/if}