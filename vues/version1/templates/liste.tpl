<style>
    .floatingHeader {
        position: fixed;
        top: 50px;
        visibility: hidden;
    }
</style>
<script>
    function UpdateTableHeaders() {
        $(".persist-area").each(function () {

            var el = $(this),
                offset = el.offset(),
                scrollTop = $(window).scrollTop(),
                floatingHeader = $(".floatingHeader", this)

            if ((scrollTop > offset.top) && (scrollTop < offset.top + el.height())) {
                floatingHeader.css({
                    "visibility": "visible"
                });
            } else {
                floatingHeader.css({
                    "visibility": "hidden"
                });
            }
            ;
        });
    }

    // DOM Ready
    $(function () {

        var clonedHeaderRow;

        $(".persist-area").each(function () {
            clonedHeaderRow = $(".persist-header", this);
            clonedHeaderRow
                .before(clonedHeaderRow.clone())
                .css("width", clonedHeaderRow.width())
                .addClass("floatingHeader");

        });

        $(window)
            .scroll(UpdateTableHeaders)
            .trigger("scroll");

    });
</script>
<style>




    @media print {

        @page {
            size: landscape
        }

    ;

        body {
            line-height: 1 !important;
        }

        .printable {
            display: block !important;
        }

        .nprintable {
            display: none !important;
        }

        a[href]:after {
            content: "";
        }

        h1 {
            font-size: 2.375em !important;
        }

        h3 {
            font-size: 28px !important;
        }

        p {
            font-size: 24px !important;
        }

        tr {
            width: 100% !important;
        }

        .pt0Print {
            padding-top: 5px !important;
            padding-bottom: 5px !important;
        }

        .text-head-search {
            font-size: 20px !important;
        }

        .col-sm-1,
        .col-sm-2,
        .col-sm-3,
        .col-sm-4,
        .col-sm-5,
        .col-sm-6,
        .col-sm-7,
        .col-sm-8,
        .col-sm-9,
        .col-sm-10,
        .col-sm-11,
        .col-sm-12 {
            float: none;
            width: 100%;
        }

        @-moz-document url-prefix() {
            @page {
                margin: 1cm;
            }

            body {
                line-height: 1 !important;
            }

            h1 {
                font-size: 1px !important;
            }

            .h1nomenseigne b {
                font-size: 20px !important;
            }
            .h3printable {
                font-size: 18px !important;
            }

            /*h3 span span span span span {
                font-size: 20px !important;
            }*/

            p {
                font-size: 10px !important;
            }

            #dateVentePrivee {
                font-size: 12px !important;
            }

            .box1 {
                margin: 1px !important;
                padding: 1px !important;
                font-size: 12px !important;
            }

            /* .coupon-meta p span
             {
                 font-size: 12px!important;
             }*/

            /*.coupon-meta p span {
                font-size: 19px !important;
            }*/

            .print-image-coupon {
                width: 200px !important;
            }

            .print-image-mini-coupon {
                width: 60px !important;
                height: 60px !important;
            }

            .diventetevp {
                margin: 0 !important;
                padding: 0 !important;
            }

            .divenseigne {
                padding: 0 !important;
                margin: 0 !important;
            }

            .th-action {
                display: none !important;
            }
            .td-action {
                display: none !important;
            }

        }
    }

</style>

<div style="color:#313131;margin: 0 auto;padding: 20px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;padding-bottom:0;">
    {if $bDebugRequete}
        <p>
            Requete Select : <br><br>
            {$sDebugRequeteSelect}<br><br>
            Requete Insert/Update : <br><br>
            {$sDebugRequeteInsertUpdate}<br><br>
        </p>
    {/if}
    {if isset($bMessageSupprElem) and $bMessageSupprElem}
        <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            {if isset($sMessageSupprElem)}{$sMessageSupprElem}{/if}
        </div>
    {/if}
    {if isset($bMessageSuccesForm) and $bMessageSuccesForm}
        <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            {if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}
        </div>
    {/if}
    {if isset($bMessageErreurForm) and $bMessageErreurForm}
        <div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">×</span></button>
            {if isset($sMessageErreurForm)}{$sMessageErreurForm}{/if}
        </div>
    {/if}
    {if isset($sScriptJavascriptInsert)}
        {$sScriptJavascriptInsert}
    {/if}
    {if isset($sScriptJavascriptUpdate)}
        {$sScriptJavascriptUpdate}
    {/if}
    {if isset($aListe)}
    {if $bAffTitre}
        <div id="page-heading" style="margin-bottom:20px;">
            <div style="display:inline-block;border:1px solid #D0D1D5;">
                <div class="hidden-xs title-head-search nprintable"
                     style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;"><span
                            class="glyphicon glyphicon-link"
                            style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;"></span></div>
                <h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;"
                    class="text-head-search">{$sTitreListe}{if $bActivtraduction}&nbsp;
                    <span
                       onclick="renvoi_info_traduction('fli_titreliste','{$sFli_LinkUrl}','{$sLangueUser}','{$sTitreListe|replace:'\'':'\\\''}}','{$sNomTable}','fli_titreliste');$('#fli_modallg').modal('show');">
                            <span class="glyphicon glyphicon-pencil"></span></span>{/if}</h1>
                {if $bdoc=='Y'}
                    <div class="hidden-xs title-head-search nprintable"
                         style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;cursor:pointer"
                         onclick="location.replace('fli_admin-ctrl_documentation-fli_documentation?route={$sLaRoute}')">
                        Doc<span class="glyphicon glyphicon-book"
                                 style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px"></span></div>
                {/if}
            </div>
        </div>
    {/if}
    {if isset($sCategorieListe) and $sCategorieListe neq ''}
        {if $sCategorieListe|is_array}
            {foreach name=scategorieliste from=$sCategorieListe item=objCategorieListe}
                <div style="background-color:#FFF;display:inline-block;vertical-align:top;height:20px;font-size:18px;padding:10px 0;font-family:Tahoma;">{$objCategorieListe.index}</div>
            <div style="background-color:{if $objCategorieListe.suivant}#e3e3e3{else}#777{/if};height:40px;display:inline-block;vertical-align:middle;">
                <div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div>
                <div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;">{$objCategorieListe.texte}</div>{if ! $smarty.foreach.scategorieliste.last}
                <div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-right-radius: 0;border-bottom-right-radius: 0;background-color:#FFF;"></div>
            {/if}</div>{/foreach}
            <div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
        {else}
            <div style="background-color:#777;height:40px;display:inline-block;vertical-align:middle;">
                <div style="display:inline-block;width:17px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div>
                <div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;">{$sCategorieListe}</div>
            </div>
            <div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
        {/if}
        <br>
        <br>
        <br>
    {/if}
    {if isset($sRetourListe) and $sRetourListe neq ''}
        <table border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td><a href="{$sRetourListe}"><img src="images/retour.png" height="40px" border="0"
                                                   alt="{$sLabelRetourListe}"></a></td>
                <td width="5px"></td>
                <td><a href="{$sRetourListe}" style="text-decoration:none;color:#000;"><b>{$sLabelRetourListe}</b></a>
                </td>
            </tr>
        </table>
        <br>
    {/if}
    {if isset($aRech) and $aRech|@count > 0}
    <!--<div style="margin-bottom:20px;"><div style="background-color:#777;height:40px;display:inline-block;vertical-align:middle;"><img src="images/loupe.png" height="20px" border="0" alt="{$sLabelRecherche}" style="padding:10px 0;background-color:#FFF;"><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;">{$sLabelRecherche}</div></div><div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div></div>-->
    <div class="nprintable"
         style="background-color: #fff;border: 1px solid #D0D1D5;padding: 20px 20px;margin-bottom: 20px;margin-top:0px;display: inline-block;">
        <div style="margin-bottom: 20px;">
            <div style="display: inline-block;" class="title-search">
                <span class="glyphicon glyphicon-search title-search"
                      style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
                <h4 style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;"
                    class="text-search">{$sLabelRecherche}{if $bActivtraduction}&nbsp;<a href="#"
                                                                                         onclick="renvoi_info_traduction('fli_titrerech','{$sFli_LinkUrl}','{$sLangueUser}','{$sLabelRecherche|replace:'\'':'\\\''}','{$sNomTable}','fli_titrerech');$('#fli_modallg').modal('show');">
                            <span class="glyphicon glyphicon-pencil"></span></a>{/if}</h4>
            </div>
        </div>
        {if $bAffPrintBtn}

        {/if}
        <form
                class="form-inline"
                action=""
                id="formList"
                method="GET"
                {if isset($bTelechargementFichier) and $bTelechargementFichier}enctype="multipart/form-data"{/if}
        >
            <input type="hidden" name="fli_rechercher" value="ok">
            <input type="hidden" name="sTypeSortie" value="" id="idtypesortie">

            <div>

                {if isset($aParametreWizardListe) and $aParametreWizardListe|is_array}
                    {foreach from=$aParametreWizardListe key=CleParametreWizardListe item=ValeurParametreWizardListe}
                        <input type="hidden" name="{$CleParametreWizardListe}" value="{$ValeurParametreWizardListe}">
                    {/foreach}
                {/if}
                {if isset($aRetourListe) and $aRetourListe|is_array}
                    {foreach from=$aRetourListe key=sCleRetourListe item=sValeurRetourListe}
                        <input type="hidden" name="{$sCleRetourListe}" value="{$sValeurRetourListe}">
                    {/foreach}
                {/if}

                <!-- NOUVELLE RECHERCHE BOOTSTRAP -->

                {foreach from=$aRech item=objRech}
                    {if isset($objRech.type_champ) and ((isset($objRech.aff_recherche) and $objRech.aff_recherche eq 'ok')
                    or ($objRech.type_champ eq 'category' or $objRech.type_champ eq 'bouton' or $objRech.transfert_inter_module eq 'ok' ) )}
                        {if $objRech.type_champ eq 'text'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                            type="text"
                                            class="inp-form form-control"
                                            id="id_{$objRech.nom_variable}"
                                            name="{$objRech.nom_variable}"
                                            {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                            {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    >
                            </div>
                        {elseif $objRech.type_champ eq 'category'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                            </div>
                        {elseif $objRech.type_champ eq 'date' or  $objRech.type_champ eq 'datetime'}
                            <div class="form-group">
                            {if isset($objRech.recherche_intervalle_date) and $objRech.recherche_intervalle_date eq 'ok'}

                                <label class="label-form-upper" style="width: 100%;">{$objRech.text_label}</label>
                                {if isset($objRech.recherche_intervalle_date_label) and isset($objRech.recherche_intervalle_date_label[0]) and $objRech.recherche_intervalle_date_label[0] neq ''}
                                    <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;"><span class="input-group-addon" id="basic-addon2" style="width:auto;min-width:52px;">{$objRech.recherche_intervalle_date_label[0]}</span>
                                {/if}
                                <input
                                    class="form-control"
                                    type="date"
                                    name="{$objRech.nom_variable}[]"
                                    aria-describedby="basic-addon2"
                                    {if isset($objRech.valeur_variable) and isset($objRech.valeur_variable[0]) and $objRech.valeur_variable[0] neq ''}value="{$objRech.valeur_variable[0]}"{/if}
                                    {if isset($objRech.style) and $objRech.style neq ''}style="height: 28px;{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    >
                                {if isset($objRech.recherche_intervalle_date_label) and isset($objRech.recherche_intervalle_date_label[0]) and $objRech.recherche_intervalle_date_label[0] neq ''}
                                    </div>
                                {/if}
                                {if isset($objRech.recherche_intervalle_date_label) and isset($objRech.recherche_intervalle_date_label[1]) and $objRech.recherche_intervalle_date_label[1] neq ''}
                                    <div class="input-group" style="display: inline-table;vertical-align: middle;border: 0;box-shadow: none;width: auto;"><span class="input-group-addon" id="basic-addon2" style="width:auto;">{$objRech.recherche_intervalle_date_label[1]}</span>
                                {/if}
                                    <input
                                        class="form-control"
                                        type="date"
                                        name="{$objRech.nom_variable}[]"
                                        aria-describedby="basic-addon2"
                                        {if isset($objRech.valeur_variable) and isset($objRech.valeur_variable[1]) and $objRech.valeur_variable[1] neq ''}value="{$objRech.valeur_variable[1]}"{/if}
                                        {if isset($objRech.style) and $objRech.style neq ''}style="height: 28px;{$objRech.style}"{/if}
                                        {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                            {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                                {if $cle!='size'}
                                                {$cle}="{$valeur}"
                                                {/if}
                                            {/foreach}
                                        {/if}
                                        {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                            {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                                {if $cle!='size'}
                                                {$cle}="{$valeur}"
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    >
                                    {if isset($objRech.recherche_intervalle_date_label) and isset($objRech.recherche_intervalle_date_label[1]) and $objRech.recherche_intervalle_date_label[1] neq ''}
                                        </div>
                                    {/if}
                            {else}
                                <label>{$objRech.text_label}</label>
                                <input
                                    class="form-control"
                                    type="date"
                                    id="id_{$objRech.nom_variable}"
                                    name="{$objRech.nom_variable}"
                                    {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                    {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                >
                            {/if}
                            </div>

                        {elseif $objRech.type_champ eq 'textarea'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <textarea
                                            class="form-control"
                                            id="id_{$objRech.nom_variable}"
                                            name="{$objRech.nom_variable}"
                                            {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    >{$objRech.valeur_variable}</textarea>
                            </div>
                        {elseif $objRech.type_champ eq 'checkbox'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <div style="display:inline-block;">
                                    <table style="display:inline-block;">
                                        <tbody>
                                        {foreach from=$objRech.lesitem key=valeur_checkbox item=nom_checkbox}
                                            <tr>
                                                <td style="padding-right:10px;">
                                                    <input
                                                            class="form-control"
                                                            type="checkbox"
                                                            name="{$objRech.nom_variable}[]"
                                                            value="{$valeur_checkbox}"
                                                            {if is_array($objRech.valeur_variable) and in_array($valeur_checkbox, $objRech.valeur_variable)}checked{/if}
                                                            >
                                                </td>
                                                <td style="padding-right:20px;">{$nom_checkbox}</td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                       {elseif $objRech.type_champ eq 'select' or $objRech.type_champ eq 'selectdist'}

                        {if $objRech.tags eq 'ok'}
                                <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <style>
                                .select2-container
                                {
                                min-width: 125px;
                                }
                                </style>
							<select name="{$objRech.nom_variable}" class="js-example-responsive tags"  style="width: 100%;"  id="id_{$objRech.nom_variable}">
							 <option value="" {if $objRech.valeur_variable eq ''}selected{/if}></option>
                                {foreach from=$objRech.lesitem key=valeur_checkbox item=nom_checkbox}
									<option value="{$valeur_checkbox}" {if is_array($objRech.valeur_variable) and in_array($valeur_checkbox, $objRech.valeur_variable)}selected{/if}>{$nom_checkbox}</option>
                                {/foreach}
							</select>

                        </div>
                        {elseif isset($objRech.select_autocomplete) and $objRech.select_autocomplete eq 'ok'  }

                                <div class="form-group">
                                    <label>{$objRech.text_label_filtre}</label>
                                    <input type='text'
                                               name='rech_{$objRech.nom_variable}'
                                               id='id_rech{$objRech.nom_variable}'
                                               class="inp-form form-control"
                                               onKeyUp="affiche_liste_generique('{$objRech.table_item}','{$objRech.id_table_item}','{$objRech.affichage_table_item}','{$objRech.supplogique_table_item}', '{$objRech.tabfiltre_autocomplete}',this.value,'id_{$objRech.nom_variable}');">
                                </div>
                                <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <select
                                        class="form-control"
                                        style="padding:5px;width: 200px;"
                                        id="id_{$objRech.nom_variable}"
                                        name="{$objRech.nom_variable}{if isset($objRech.multiple)}[]{/if}"
                                        {if isset($objRech.multiple)}multiple{/if}
                                        {$objRech.fonction_javascript}

                                        >
                                    <option value="" {if $objRech.valeur_variable eq ''}selected{/if}></option>

                                </select>
                            </div>
                            {else}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <select
                                        class="form-control"
                                        style="padding:5px;width: 100px;"
                                        id="id_{$objRech.nom_variable}"
                                        name="{$objRech.nom_variable}{if isset($objRech.multiple)}[]{/if}"
                                        {if isset($objRech.multiple)}multiple{/if}
                                        {$objRech.fonction_javascript}

                                        >
                                    <option value="" {if $objRech.valeur_variable eq ''}selected{/if}></option>
                                    {if is_array($objRech.lesitem)}
                                        {foreach from=$objRech.lesitem key=id_valeur_possible item=valeur_possible_bdd}
                                            <option value="{$id_valeur_possible}" {if $objRech.valeur_variable eq $id_valeur_possible}selected{/if}>
                                                {$valeur_possible_bdd}
                                            </option>
                                        {/foreach}
                                    {/if}
                                </select>
                            </div>


                            {/if}
                        {elseif $objRech.type_champ eq 'radio'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <div style="display:inline-block;">
                                    <table>
                                        <tbody>
                                         <tr>
                                                <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                    <input
                                                            class="form-control"
                                                            type="radio"
                                                            name="{$objRech.nom_variable}"
                                                            value=""
                                                                >
                                                </td>
                                                <td style="padding-bottom:0;vertical-align:middle;height:32px;">Tout</td>
                                            </tr>
                                        {foreach from=$objRech.lesitem key=valeur_radio item=nom_radio}
                                            <tr>
                                                <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                                    <input
                                                            class="form-control"
                                                            type="radio"
                                                            name="{$objRech.nom_variable}"
                                                            value="{$valeur_radio}"
                                                            {if $valeur_radio eq $objRech.valeur_variable}checked{/if}
                                                            >
                                                </td>
                                                <td style="padding-bottom:0;vertical-align:middle;height:32px;">{$nom_radio}</td>
                                            </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        {elseif $objRech.type_champ eq 'file'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                        class="form-control"
                                        type="file"
                                        id="id_{$objRech.nom_variable}"
                                        name="{$objRech.nom_variable}"
                                        {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                    {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                        {if $cle!='size'}
                                        {$cle}="{$valeur}"
                                        {/if}
                                    {/foreach}
                                {/if}
                                {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                    {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                        {if $cle!='size'}
                                        {$cle}="{$valeur}"
                                        {/if}
                                    {/foreach}
                                {/if}
                                >
                            </div>
                        {elseif $objRech.type_champ eq 'time'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                        type="text"
                                        class="timepicker inp-form form-control"
                                        id="id_{$objRech.nom_variable}"
                                        name="{$objRech.nom_variable}"
                                        {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                        {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                    {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                        {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                    {/foreach}
                                {/if}
                                {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                    {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                        {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                    {/foreach}
                                {/if}
                                >
                            </div>
                        {elseif $objRech.type_champ eq 'hour'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                        type="text"
                                        class="hourpicker inp-form form-control"
                                        id="id_{$objRech.nom_variable}"
                                        name="{$objRech.nom_variable}"
                                        {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                        {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                    {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                        {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                    {/foreach}
                                {/if}
                                {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                    {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                        {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                    {/foreach}
                                {/if}
                                >
                            </div>
                        {elseif $objRech.type_champ eq 'hidden'}
                            <div class="form-group">
                                <input
                                    type="hidden"
                                    {if isset($objRech.nom_variable)}name="{$objRech.nom_variable}"{/if}
                                    {if isset($objRech.valeur_variable)}value="{$objRech.valeur_variable}"{/if}
                                    >
                            </div>
                        {elseif $objRech.type_champ eq 'password'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                            type="password"
                                            class="inp-form form-control"
                                            id="id_{$objRech.nom_variable}"
                                            name="{$objRech.nom_variable}"
                                            {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                            {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    >
                            </div>
                        {elseif $objRech.type_champ eq 'email'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                            type="email"
                                            class="inp-form form-control"
                                            id="id_{$objRech.nom_variable}"
                                            name="{$objRech.nom_variable}"
                                            {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                            {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    >
                            </div>
                        {elseif $objRech.type_champ eq 'telephone'}
                            <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                            type="tel"
                                            class="inp-form form-control"
                                            id="id_{$objRech.nom_variable}"
                                            name="{$objRech.nom_variable}"
                                            {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                            {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    >
                            </div>
                        {else}
                                 <div class="form-group">
                                <label>{$objRech.text_label}</label>
                                <input
                                            type="text"
                                            class="inp-form form-control"
                                            id="id_{$objRech.nom_variable}"
                                            name="{$objRech.nom_variable}"
                                            {if isset($objRech.valeur_variable) and $objRech.valeur_variable neq ''}value="{$objRech.valeur_variable}"{/if}
                                            {if isset($objRech.style) and $objRech.style neq ''}style="{$objRech.style}"{/if}
                                    {if isset($objRech.tableau_attribut) and $objRech.tableau_attribut|is_array}
                                        {foreach from=$objRech.tableau_attribut key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    {if isset($objRech.fonction_javascript) and $objRech.fonction_javascript|is_array}
                                        {foreach from=$objRech.fonction_javascript key=cle item=valeur}
                                            {if $cle!='size'}
                                            {$cle}="{$valeur}"
                                            {/if}
                                        {/foreach}
                                    {/if}
                                    >
                            </div>
                        {/if}
                    {/if}
                {/foreach}

                <br/><br/>

                <div class="form-group">
                    {if isset($itemBoutons.valider)}
                          <input
                          class="btn btn-primary btnbtnValForm"
                          type="submit"
                          id="id_{$itemBoutons.valider.nom_variable}"
                          name="{$itemBoutons.valider.nom_variable}"
                          value="{$itemBoutons.valider.text_label}"
                          {if isset($itemBoutons.valider.style) and $itemBoutons.valider.style neq ''} style="margin-bottom: 5px;"{/if}
                          {if isset($itemBoutons.valider.tableau_attribut) and $itemBoutons.valider.tableau_attribut|is_array}
                              {foreach from=$itemBoutons.valider.tableau_attribut key=cle item=valeur}
                                  {$cle}="{$valeur}"
                              {/foreach}
                          {/if}
                          {if isset($itemBoutons.valider.fonction_javascript) and $itemBoutons.valider.fonction_javascript|is_array}
                              {foreach from=$itemBoutons.valider.fonction_javascript key=cle item=valeur}
                                  {$cle}="{$valeur}"
                              {/foreach}
                          {/if}
                          onclick="valiformsimple()"
                          >
                      {/if}
                    {if isset($itemBoutons.reset)}
                            <input
                                    class="btn btn-primary btnResForm"
                                    type="button"
                                    value="{$itemBoutons.reset.text_label}"
                                    {if isset($itemBoutons.reset.style) and $itemBoutons.reset.style neq ''}style="margin-bottom: 5px;"{/if}
                            {if isset($itemBoutons.reset.tableau_attribut) and $itemBoutons.reset.tableau_attribut|is_array}
                                {foreach from=$itemBoutons.reset.tableau_attribut key=cle item=valeur}
                                    {$cle}="{$valeur}"
                                {/foreach}
                            {/if}
                            {if isset($itemBoutons.reset.fonction_javascript) and $itemBoutons.reset.fonction_javascript|is_array}
                                {foreach from=$itemBoutons.reset.fonction_javascript key=cle item=valeur}
                                    {$cle}="{$valeur}"
                                {/foreach}
                            {/if}
                            onclick="location.href='{$itemBoutons.reset.url}';"
                            >
                    {/if}
                    <script>
                        function valiform() {
                            $("#formList").prop("target", '_blank');
                            $("#idtypesortie").val('csv');
                            $("#formList").submit();
                        }

                        function valiformpdf() {
                            $("#formList").prop("target", '_blank');
                            $("#idtypesortie").val('pdf');
                            $("#formList").submit();
                        }

                        function valiformsimple() {
                            $("#formList").prop("target", '');
                            $("#idtypesortie").val('');
                            $("#formList").submit();
                        }
                    </script>
                    {if isset($itemBoutons.csv) && $bCsv}
                        <input
                        class="btn btn-primary"
                        type="submit"
                        value="{$itemBoutons.csv.text_label}"
                        {if isset($itemBoutons.csv.style) and $itemBoutons.csv.style neq ''}style="{$itemBoutons.csv.style}font-size:13px;margin-bottom: 5px;"{/if}
                        {if isset($itemBoutons.csv.tableau_attribut) and $itemBoutons.csv.tableau_attribut|is_array}
                            {foreach from=$itemBoutons.csv.tableau_attribut key=cle item=valeur}
                                {$cle}="{$valeur}"
                            {/foreach}
                        {/if}
                        {if isset($itemBoutons.csv.fonction_javascript) and $itemBoutons.csv.fonction_javascript|is_array}
                            {foreach from=$itemBoutons.csv.fonction_javascript key=cle item=valeur}
                                {$cle}="{$valeur}"
                            {/foreach}
                        {/if}
                        onclick="valiform();">
                    {/if}

                    {if isset($itemBoutons.pdf) && $bPdf}
                        <input
                        class="btn btn-primary"
                        type="submit"
                        value="{$itemBoutons.pdf.text_label}"
                        {if isset($itemBoutons.pdf.style) and $itemBoutons.pdf.style neq ''}style="{$itemBoutons.csv.style}font-size:13px;margin-bottom: 5px;"{/if}
                        {if isset($itemBoutons.pdf.tableau_attribut) and $itemBoutons.pdf.tableau_attribut|is_array}
                            {foreach from=$itemBoutons.pdf.tableau_attribut key=cle item=valeur}
                                {$cle}="{$valeur}"
                            {/foreach}
                        {/if}
                        {if isset($itemBoutons.pdf.fonction_javascript) and $itemBoutons.pdf.fonction_javascript|is_array}
                            {foreach from=$itemBoutons.pdf.fonction_javascript key=cle item=valeur}
                                {$cle}="{$valeur}"
                            {/foreach}
                        {/if}
                        onclick="valiformpdf();">
                    {/if}
                </div>

                <!-- FIN NOUVELLE RECHERCHE BOOTSTRAP -->

            </div>
        </form>
        {/if}
    </div>

    {if $bLabelCreationElem || $bBtnRetour || $bAffPrintBtn}
        <div style="margin-bottom:20px;text-align: center;">
            {if $bLabelCreationElem}
                {if ! $bFormPopup}
                    <a href="{$sLabelCreationElemUrl}" class="btn btn-default" role="button"><span
                                class="glyphicon glyphicon-plus"></span> {$sLabelCreationElem}{if $bActivtraduction}&nbsp;
                        <a href="#"
                           onclick="renvoi_info_traduction('fli_btaj','{$sFli_LinkUrl}','{$sLangueUser}','{$sLabelCreationElem|replace:'\'':'\\\''}','{$sNomTable}','fli_btaj');$('#fli_modallg').modal('show');">
                                <span class="glyphicon glyphicon-pencil"></span></a>{/if}</a>
                {if  $bRetourSpecifique}
                    &nbsp;&nbsp;
                    <a href="{$sRetourElemUrl}" class="btn btn-default" role="button"><span
                                class="glyphicon glyphicon-plus"></span> {$sLabelFileRetourElem}</a>
                {/if}
                {else}
                    <a href="#creerModal" class="openModal" id="test"
                       style="text-decoration:none;color:#00e;">{$sLabelCreationElem}{if $bActivtraduction}&nbsp;<a
                                href="#"
                                onclick="renvoi_info_traduction('fli_btaj','{$sFli_LinkUrl}','{$sLangueUser}','{$sLabelCreationElem|replace:'\'':'\\\''}','{$sNomTable}','fli_btaj');$('#fli_modallg').modal('show');">
                                <span class="glyphicon glyphicon-pencil"></span></a>{/if}</a>
                    <aside id="creerModal" class="modal">
                        <div style="overflow-y:auto;max-height:85%;background:#FFF;text-shadow:none;">
                            {include file='formulaire.tpl'}
                            <a href="#close" title="Fermer" style="top:15px;right:15px;">Fermer</a>
                        </div>
                    </aside>
                {if ! $bMessageSuccesForm and $bMessageErreurForm}
                    <script>
                        creermodal = document.getElementById('test').click();
                    </script>
                {/if}
                {/if}
            {/if}
            {if $bBtnRetour}
                <a href="{$sDirRetour}" class="btn btn-default" role="button"><span
                            class="glyphicon glyphicon-share-alt" style="transform: scaleX(-1);"></span> Retour</a>
            {/if}
            {if $bAffPrintBtn}
                <a href="Javascript:Void(0);" class="btn btn-default hidden-xs" role="button" onclick="window.print()"
                   style="position: fixed;top: 8px;z-index: 10000;right: 20px;"><span class="glyphicon glyphicon-print"
                                                                                      style="transform: scaleX(-1);"></span></a>
            {/if}


        </div>
    {/if}


</div>
{if $bAffListe}
{if $bAffNombreResult}
<div class="nprintable" style="margin:0 auto;text-align: center;" id="blockAffInfo">
  <div style="display:inline-block;border: 1px solid #D0D1D5;margin-top:0px;margin-bottom:20px;background-color: #333;"><div style="display:inline-block;padding:20px;vertical-align: middle;" id="rechNbr" class="sub-title-head-search">{$iTabListeCount}</div><div id="rechLabel" style="display:inline-block;padding:20px;vertical-align: middle;" class="sub-text-head-search">{$sLabelNbrLigne}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_textnbrligne','{$sFli_LinkUrl}','{$sLangueUser}','{$sLabelNbrLigne|replace:'\'':'\\\''}','{$sNomTable}','fli_textnbrligne');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</div></div>
</div>
{/if}
  {if $bActiveFormSelect}<form method="post" action="{$urlFormList}" enctype="multipart/form-data" >{/if}
  <div style="margin: 0 15px;" class="row">
  <button class="btn btn-success btnScroll btnScrollLeft" style="position: fixed;margin-top: -34px;left: 16px;z-index: 1;"> <span class="glyphicon glyphicon-arrow-left"></span></button>
  <button class="btn btn-success btnScroll btnScrollRight" style="position: fixed;right: 15px;margin-top: -34px;z-index: 1;"> <span class="glyphicon glyphicon-arrow-right"></span></button>
  <div class="table-responsive" style="min-height: 500px;">
    <table style="background-color: #fafafa;" class="col-sm-12 table table-hover table-condensed table-bordered table-striped persist-area">
      <thead>
        <tr class="persist-header">
          {if $bActiveFormSelect}
              {if $bRadioSelect}<th class="table-header-repeat line-left minwidth-1" style="color:#fff;font-size:16px;text-align: center;min-width: 20px;vertical-align: middle;">°</th>{/if}
              {if $bCheckboxSelect}<th class="table-header-repeat line-left minwidth-1" style="color:#fff;font-size:16px;text-align: center;min-width: 20px;vertical-align: middle;">#</th>{/if}
          {/if}
          {if $bAffMod or $bAffSupp or $baffAction or $bAffRecapLigne }
          {if empty($bActiveFormSelect)}
              <th class="table-header-repeat line-left minwidth-1 title-table-head th-action" id="actions" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                  <a href="javascript:Void(0);" style="color:#fff;">Actions</a>
              </th>
              <script>
              $(document).ready(function() {
  var elmnt = document.getElementsByClassName("th-action");
  console.log(elmnt);


    elmnt[1].style.width = elmnt[0].offsetWidth+"px";
});
</script>
{/if}
          {/if}
          {*{$aEnteteListe|@print_r}*}
          {foreach from=$aEnteteListe item=objEnteteListe}
              <th class="table-header-repeat line-left minwidth-1 title-table-head  {$objEnteteListe.objElement.mapping_champ} " id="" style="padding: 5px;background:none;text-align: center;vertical-align: middle;" >
                  <a href="{$objEnteteListe.sUrl}" style="color:#fff;" >{$objEnteteListe.sLabel}</a>{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objEnteteListe.objElement.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objEnteteListe.sLabel|replace:'\'':'\\\''}','{$sNomTable}','{$objEnteteListe.objElement.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}
              </th>
<script>
$(document).ready(function() {
  var elmnt = document.getElementsByClassName("{$objEnteteListe.objElement.mapping_champ}");


    elmnt[1].style.width = elmnt[0].offsetWidth+"px";
});
</script>
          {/foreach}

        </tr>
      </thead>
      <tbody>

      {foreach from=$aListe item=objListe}
        <tr class="no" {if $bLigneCliquable}onclick="document.location = '{$sLienLigne}{$objListe.id}';" style="cursor:pointer;{if isset($objListe.html_perso_tr)}{$objListe.html_perso_tr}{/if}"{/if}
        >
          {if $bAffMod or $bAffSupp or !empty($objListe.aMenuDeroulant) or $bAffRecapLigne }
          {if empty($bActiveFormSelect)}
          <td style="text-align:center;vertical-align: middle;" class="td-action">
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <span class="glyphicon glyphicon-option-vertical"></span>
              </button>
              <ul class="dropdown-menu" role="menu" style="left: 0 !important;">
                {if $bAffMod}<li><a href="{$objListe.sUrlForm}"><span class="glyphicon glyphicon-pencil"></span> Modifier</a></li>
                {/if}
                {if $bAffSupp}<li><a href="#" onclick="bconf=confirm('Voulez-vous supprimer cette ligne ?');if(bconf)location.replace('{$objListe.sUrlSupp}');"><span class="glyphicon glyphicon-trash"></span> Supprimer</a></li>{/if}
                {if $bAffDup}<li><a href="#" onclick="bconf=confirm('Voulez-vous dupliquer cette ligne ?');if(bconf)location.replace('{$objListe.sUrlDuplicate}');"><span class="glyphicon glyphicon-copy"></span> Duppliquer</a></li>{/if}
                {if $bAffRecapLigne}<li><a href="{$objListe.sUrlRecapLigne}" target="_blank"><span class="glyphicon glyphicon-eye-open"></span> Voir</a></li>{/if}
                {if !empty($objListe.aMenuDeroulant)}
                  {foreach from=$objListe.aMenuDeroulant item=objListeMenu}



                       {if !isset($objListeMenu.isbloque)}

                      <li><a href="{if $objListeMenu.href!="" }{$objListeMenu.href}{$surlVarGET}{/if}"  onclick="{$objListeMenu.javascript}" {if isset($objListeMenu.target)}{$objListeMenu.target}{/if}>{if !empty($objListeMenu.attribut)}<span {$objListeMenu.attribut}></span> {/if}{$objListeMenu.intitule}</a></li>
                      {/if}
                  {/foreach}
                 {/if}
              </ul>
            </div>
          </td>

{*{if !empty($objListe.aMenuDeroulant)}
                  {foreach from=$objListe.aMenuDeroulant item=objListeMenu}

                      {print_r($objListeMenu)}
                  {/foreach}
                 {/if}*}
{/if}
          {/if}
          {if $bActiveFormSelect}
              {if $bRadioSelect}<td style="width: 28px;"><input type="radio" value="{$objListe.id}" name="iIdSelected"></td>{/if}
              {if $bCheckboxSelect}<td style="width: 28px;"><input type="checkbox" value="{$objListe.id}" name="iIds[]"></td>{/if}
          {/if}
          {foreach name="enteteliste" from=$aEnteteListe item=objEnteteListe}
              <td style="vertical-align: middle;text-align: center;{if isset($objListe[$objEnteteListe.objElement.nom_variable].html_td)}{$objListe[$objEnteteListe.objElement.nom_variable].html_td}{/if}" {$objEnteteListe.objElement.html_perso_td}  >
                  {if $smarty.foreach.enteteliste.first}
                      {if isset($objListe.liste_fils) and $objListe.liste_fils|is_array and $objListe.liste_fils|@count > 0 and !empty($objListe.liste_fils[0][0][0])}
                          <button class="btn btn-warning nprintable glyphicon glyphicon-resize-full" onclick="$(this).toggleClass('glyphicon glyphicon-resize-small').toggleClass('glyphicon glyphicon-resize-full');$(this).closest('tr').next().stop().fadeToggle('fast')"></button>
                      {/if}
                  {/if}

                  {if $bActiveFormSelect}
                      {if $objEnteteListe.objElement.activ_form_select=='ok'}
                          <!-- ---------------- TEXT ---------------- -->
                          {if $objEnteteListe.objElement.type_champ=='text'}
                              <input   type="text"  name=" {$objEnteteListe.objElement.nom_variable}" value="{$objListe[$objEnteteListe.objElement.nom_variable].value}" >
                          {/if}
                          <!-- ---------------- SELECT ---------------- -->
                          {if $objEnteteListe.objElement.type_champ=='select'}
                              <select
                                  style="padding:5px;"
                                  id="id_form_{$objEnteteListe.objElement.nom_variable}"
                                  name="{$objEnteteListe.objElement.nom_variable}{if isset($objEnteteListe.objElement.multiple)}[]{/if}"
                                  {if isset($objEnteteListe.objElement.multiple)}multiple{/if}

                                  {$objEnteteListe.objElement.fonction_javascript}
                              >
                                  <option value=""></option>
                                  {if is_array($listeElemSelect)}
                                      {foreach from=$listeElemSelect key=id_valeur_possible item=valeur_possible_bdd}
                                          <option value="{$id_valeur_possible}" {if $objListe[$objEnteteListe.objElement.nom_variable].value eq $valeur_possible_bdd}selected{/if}>
                                              {$valeur_possible_bdd}
                                          </option>
                                      {/foreach}
                                  {/if}
                              </select>
                          {/if}
                          <!-- ---------------- CHECKBOX ---------------- -->
                          {if $objEnteteListe.objElement.type_champ=='checkbox'}
                              <table class="table table-hover table-condensed table-bordered">
                                  <tbody>
                                 {foreach from=$allElements.lesitem key=valeur_checkbox item=nom_checkbox}
                                  <tr>
                                      <td style="padding-right:10px;">
                                          <input
                                              type="checkbox"
                                              name="{$objEnteteListe.objElement.nom_variable}[]"
                                              value="{$valeur_checkbox}"
                                              {if is_array($objListe.tableauChecked) and in_array($valeur_checkbox, $objListe.tableauChecked)}checked{/if}
                                          >
                                      <td style="padding-right:20px;">{$nom_checkbox}</td>
                                  </tr>
                                  {/foreach}
                                  </tbody>
                              </table>
                          {/if}
                          <!-- ---------------- RADIO ---------------- -->
                          {if $objEnteteListe.objElement.type_champ=='radio'}
                              <table class="table table-hover table-condensed table-bordered">
                                  <tbody>
                                 {foreach from=$allElements.lesitem key=valeur_radio item=nom_radio}
                                  <tr class="test">
                                      <td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
                                          <input
                                              type="radio"
                                              name="{$objEnteteListe.objElement.nom_variable}"
                                              value="{$valeur_radio}"
                                              {if $valeur_radio eq $objListe.checked}checked{/if}
                                          >
                                      </td>
                                      <td style="padding-bottom:0;vertical-align:middle;height:32px;">{$nom_radio}</td>
                                  </tr>
                                  {/foreach}
                                  </tbody>
                              </table>
                          {/if}
                      {else}
                         <span {if $objEnteteListe.objElement.html_editable_td=="ok" and $objEnteteListe.objElement.type_champ !='date' }  contenteditable="true" onblur="save_edtable('{$sNomTable}','{$sChampId}','{$objEnteteListe.objElement.mapping_champ}','{$objListe.id}',$(this).html())"{/if}>
                                  {if $objEnteteListe.objElement.html_editable_radio=='ok'}
                                     <input type="checkbox" data-on-text="Oui" data-off-text="Non" name="checkbox_visible_{$objListe.id}" id="checkbox_visible_{$objListe.id}" >
                                        <script type="text/javascript">

                                            $("[name=checkbox_visible_{$objListe.id}]").bootstrapSwitch();

                                           {if $objListe[$objEnteteListe.objElement.nom_variable].value == "OUI"}
                                                   $("[name=checkbox_visible_{$objListe.id}]").bootstrapSwitch("state", true);
                                           {/if}

                                            $("input[name=checkbox_visible_{$objListe.id}]").on("switchChange.bootstrapSwitch", function(e, state) {
                                                if( e.target.checked ){
                                                    save_edtable("{$sNomTable}","{$sChampId}","{$objEnteteListe.objElement.mapping_champ}","{$objListe.id}","{$objEnteteListe.objElement.html_value_bouton_on_radio}");
                                                }else{
                                                    save_edtable("{$sNomTable}","{$sChampId}","{$objEnteteListe.objElement.mapping_champ}","{$objListe.id}","{$objEnteteListe.objElement.html_value_bouton_off_radio}");
                                                }
                                            });
                                        </script>

                                  {else}
                                  {if $objEnteteListe.objElement.type_champ=='date' and $objEnteteListe.objElement.html_editable_td=="ok" }
                             <div onclick="apparition('{$objListe.id}{$objEnteteListe.objElement.mapping_champ}')" style="cursor: pointer;">
                            <div style="display:none" id="inputdate{$objListe.id}{$objEnteteListe.objElement.mapping_champ}">
                             <input id="date{$objListe.id}{$objEnteteListe.objElement.mapping_champ}"  type="date"  ><br>
                             <span  onclick="save_edtable('{$sNomTable}','{$sChampId}','{$objEnteteListe.objElement.mapping_champ}','{$objListe.id}',$('#date{$objListe.id}{$objEnteteListe.objElement.mapping_champ}').val())" class="btn btn-success">Valider</span>
</div>

{/if}

                                      {if $objEnteteListe.objElement.format_affiche_liste != ''}
                                          {$objListe[$objEnteteListe.objElement.nom_variable].value|number_format:$objEnteteListe.objElement.format_affiche_liste:".":"."}
                                      {elseif $objEnteteListe.objElement.alias_champ!='' and $objEnteteListe.objElement.type_champ!='checkbox' and $objEnteteListe.objElement.type_champ!='date' }
                                         {if isset($objListe[$objEnteteListe.objElement.nom_variable].value)} {$objListe[$objEnteteListe.objElement.alias_champ].value}{/if}
                                      {elseif $objEnteteListe.objElement.type_champ=='checkbox' or $objEnteteListe.objElement.type_champ=='date'}
                                              {$objListe[$objEnteteListe.objElement.alias_champ].value}&nbsp;
                                      {else}
                                          {$objListe[$objEnteteListe.objElement.nom_variable].value}&nbsp;
                                      {/if}
                                  {/if}
                                  {if $objEnteteListe.objElement.type_champ=='date' and $objEnteteListe.objElement.html_editable_td=="ok" }
                             </div>

{/if}
                              </span>
                      {/if}
                  {else}{if $objEnteteListe.objElement.html_editable_td=="ok" and $objEnteteListe.objElement.type_champ !='date' }
                   <div >
                   <div style="display:none" id="inputdate{$objListe.id}{$objEnteteListe.objElement.mapping_champ}">
                             <input id="text{$objListe.id}{$objEnteteListe.objElement.mapping_champ}"  type="text" value="{$objListe[$objEnteteListe.objElement.nom_variable].value}"  ><br>
                             <span  onclick="save_edtable('{$sNomTable}','{$sChampId}','{$objEnteteListe.objElement.mapping_champ}','{$objListe.id}',$('#text{$objListe.id}{$objEnteteListe.objElement.mapping_champ}').val())" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></span>
                             <span   onclick="dispation('{$objListe.id}{$objEnteteListe.objElement.mapping_champ}')" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></span>
</div>
                      <span style="cursor: pointer;color: black;{if $objListe[$objEnteteListe.objElement.nom_variable].value == '' } font-size: 37px;letter-spacing: 150px; {/if}"  onclick="apparition('{$objListe.id}{$objEnteteListe.objElement.mapping_champ}')" id="{$objListe.id}{$objEnteteListe.objElement.mapping_champ}" >
                      {/if}
                                  {if $objEnteteListe.objElement.html_editable_radio=='ok'}
                                     <input type="checkbox" data-on-text="Oui" data-off-text="Non" name="checkbox_visible_{$objListe.id}" id="checkbox_visible_{$objListe.id}" >
                                        <script type="text/javascript">

                                            $("[name=checkbox_visible_{$objListe.id}]").bootstrapSwitch();

                                           {if $objListe[$objEnteteListe.objElement.nom_variable].value == "OUI"}
                                                   $("[name=checkbox_visible_{$objListe.id}]").bootstrapSwitch("state", true);
                                           {/if}

                                            $("input[name=checkbox_visible_{$objListe.id}]").on("switchChange.bootstrapSwitch", function(e, state) {
                                                if( e.target.checked ){
                                                    save_edtable("{$sNomTable}","{$sChampId}","{$objEnteteListe.objElement.mapping_champ}","{$objListe.id}","{$objEnteteListe.objElement.html_value_bouton_on_radio}");
                                                }else{
                                                    save_edtable("{$sNomTable}","{$sChampId}","{$objEnteteListe.objElement.mapping_champ}","{$objListe.id}","{$objEnteteListe.objElement.html_value_bouton_off_radio}");
                                                }
                                            });
                                        </script>

                                  {else}
                                  {if $objEnteteListe.objElement.type_champ=='date' and $objEnteteListe.objElement.html_editable_td=="ok" }

                            <div  style="display:none" id="inputdate{$objListe.id}{$objEnteteListe.objElement.mapping_champ}">
                             <input id="date{$objListe.id}{$objEnteteListe.objElement.mapping_champ}"  type="date"  ><br>
                             <span  onclick="save_edtable('{$sNomTable}','{$sChampId}','{$objEnteteListe.objElement.mapping_champ}','{$objListe.id}',$('#date{$objListe.id}{$objEnteteListe.objElement.mapping_champ}').val())" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span></span>
                             <span  onclick="dispation('{$objListe.id}{$objEnteteListe.objElement.mapping_champ}')" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></span>
</div>

{/if}

                                      {if $objEnteteListe.objElement.format_affiche_liste != ''}
                                          {$objListe[$objEnteteListe.objElement.nom_variable].value|number_format:$objEnteteListe.objElement.format_affiche_liste:".":"."}
                                      {elseif $objEnteteListe.objElement.alias_champ!='' and $objEnteteListe.objElement.type_champ!='checkbox' and $objEnteteListe.objElement.type_champ!='date' }
                                         {if isset($objListe[$objEnteteListe.objElement.nom_variable].value)} {$objListe[$objEnteteListe.objElement.alias_champ].value}{/if}
                                      {elseif $objEnteteListe.objElement.type_champ=='checkbox' or $objEnteteListe.objElement.type_champ=='date'}
                                      {if $objEnteteListe.objElement.type_champ=='date' and $objEnteteListe.objElement.html_editable_td=="ok" }
                                      <span style="cursor: pointer" {if {$objListe[$objEnteteListe.objElement.nom_variable].value} == '' } style="font-size: 37px;letter-spacing: 150px;" {/if} onclick="apparition('{$objListe.id}{$objEnteteListe.objElement.mapping_champ}')">
                                      {/if}
                                              {$objListe[$objEnteteListe.objElement.alias_champ].value}&nbsp;
                                              {if $objEnteteListe.objElement.type_champ=='date' and $objEnteteListe.objElement.html_editable_td=="ok" }
                                      </span>
                                      {/if}
                                      {else}
                                          {$objListe[$objEnteteListe.objElement.nom_variable].value}&nbsp;
                                      {/if}
                                  {/if}

                              </span>
                              {if $objEnteteListe.objElement.html_editable_td=="ok" and $objEnteteListe.objElement.type_champ !='date' }
                              </div>
                              {/if}
                  {/if}

                  </td>
          {/foreach}
          </tr>
          {if isset($objListe.liste_fils) and $objListe.liste_fils|is_array and $objListe.liste_fils|@count > 0 and !empty($objListe.liste_fils[0][0][0])}
              <tr style="display: none;">
                  <td style="padding:0;vertical-align: middle;" colspan="{$iNombreColonneListe}">
                      <div class="class_form_list_toggle_div">
                          <div class="pt0Print" style="padding:20px 20px 0 20px;">
                              {foreach from=$objListe.liste_fils item=sTpl}
                                  {$sTpl[0][0]}
                              {/foreach}
                          </div>
                      </div>
                  </td>
              </tr>
          {/if}
      {/foreach}
      </tbody>
    </table>
  </div>
{if $bActiveFormSelect}
    <div style="clear:both;float:right;margin-top:20px;">
        <input type="submit" value="Valider" style="background-color:#94B52C;border:1px solid #719C27;border-radius:8px;padding:7px 16px;font-size:11px;color:#FFF;cursor:pointer;">
    </div>
    </form>
{/if}
{if isset($bPagination) and $bPagination and isset($aPaginationListe) and isset($aPaginationListe.select) and isset($aPaginationListe.liste)}
  <div class="nprintable" style="text-align: center;">
      <ul class="pagination">
        <li>
          <a href="{$aPaginationListe.liste.page_premiere}" aria-label="Previous">
            <span aria-hidden="true" class="glyphicon glyphicon-fast-backward"></span>
          </a>
        </li>
        <li>
          <a href="{$aPaginationListe.liste.page_precedente}" aria-label="Previous">
            <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
          </a>
        </li>
        {foreach from=$aPaginationListe.liste.page_liste item=page}
            {if isset($page.selected) and $page.selected}
                <li class="active"><a href="#"><strong>{$page.numero}</strong><span class="sr-only">(current)</span></a></li>
            {else}
                <li><a href="{$page.url}">{$page.numero}</a></li>
            {/if}
        {/foreach}
        <li>
          <a href="{$aPaginationListe.liste.page_suivante}" aria-label="Next">
            <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </li>
        <li>
          <a href="{$aPaginationListe.liste.page_derniere}" aria-label="Previous">
            <span aria-hidden="true" class="glyphicon glyphicon-fast-forward"></span>
          </a>
        </li>
      </ul>
      <select onchange="document.location.href=this.value" style="display: inline-block;margin: 20px 0;vertical-align: top;height: 34px;margin-left: 5px;">
          <option value="">Numéro de la page</option>
          {foreach from=$aPaginationListe.select item=page}
              {if $page.active neq ''}
                  <option value="{$page.url}" {if isset($page.selected) and $page.selected}selected{/if}>{$page.numero}</option>
              {/if}
          {/foreach}
      </select>
  </div>
{/if}
{/if}
{if isset($itemBoutonsListe) and $itemBoutonsListe|is_array and $itemBoutonsListe|@count > 0}
    <div style="clear:both;float:right;margin-top:20px;">
        {if isset($itemBoutonsListe.precedent)}
            <a href="{$itemBoutonsListe.precedent.url}">
                <input
                        type="button"
                        value="{$itemBoutonsListe.precedent.text_label}"
                        {if isset($itemBoutonsListe.precedent.style) and $itemBoutonsListe.precedent.style neq ''}style="{$itemBoutonsListe.precedent.style}"{/if}
                {if isset($itemBoutonsListe.precedent.tableau_attribut) and $itemBoutonsListe.precedent.tableau_attribut|is_array}
                    {foreach from=$itemBoutonsListe.precedent.tableau_attribut key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                {if isset($itemBoutonsListe.precedent.fonction_javascript) and $itemBoutonsListe.precedent.fonction_javascript|is_array}
                    {foreach from=$itemBoutonsListe.precedent.fonction_javascript key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                >
            </a>
        {/if}
        {if isset($itemBoutonsListe.suivant)}
            <a href="{$itemBoutonsListe.suivant.url}">
                <input
                        type="button"
                        id="id_{$itemBoutonsListe.suivant.nom_variable}"
                        name="{$itemBoutonsListe.suivant.nom_variable}"
                        value="{$itemBoutonsListe.suivant.text_label}"
                        {if isset($itemBoutonsListe.suivant.style) and $itemBoutonsListe.suivant.style neq ''}style="{$itemBoutonsListe.suivant.style}"{/if}
                {if isset($itemBoutonsListe.suivant.tableau_attribut) and $itemBoutonsListe.suivant.tableau_attribut|is_array}
                    {foreach from=$itemBoutonsListe.suivant.tableau_attribut key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                {if isset($itemBoutonsListe.suivant.fonction_javascript) and $itemBoutonsListe.suivant.fonction_javascript|is_array}
                    {foreach from=$itemBoutonsListe.suivant.fonction_javascript key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                >
            </a>
        {/if}
        {if isset($itemBoutonsListe.valider)}
            <a href="{$itemBoutonsListe.valider.url}">
                <input
                        type="button"
                        id="id_{$itemBoutonsListe.valider.nom_variable}"
                        name="{$itemBoutonsListe.valider.nom_variable}"
                        value="{$itemBoutonsListe.valider.text_label}"
                        {if isset($itemBoutonsListe.valider.style) and $itemBoutonsListe.valider.style neq ''}style="{$itemBoutonsListe.valider.style}"{/if}
                {if isset($itemBoutonsListe.valider.tableau_attribut) and $itemBoutonsListe.valider.tableau_attribut|is_array}
                    {foreach from=$itemBoutonsListe.valider.tableau_attribut key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                {if isset($itemBoutonsListe.valider.fonction_javascript) and $itemBoutonsListe.valider.fonction_javascript|is_array}
                    {foreach from=$itemBoutonsListe.valider.fonction_javascript key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                >
            </a>
        {/if}
        {if isset($itemBoutonsListe.annuler)}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{$itemBoutonsListe.annuler.url}">
                <input
                        type="button"
                        value="{$itemBoutonsListe.annuler.text_label}"
                        {if isset($itemBoutonsListe.annuler.style) and $itemBoutonsListe.annuler.style neq ''}style="{$itemBoutonsListe.annuler.style}"{/if}
                {if isset($itemBoutonsListe.annuler.tableau_attribut) and $itemBoutonsListe.annuler.tableau_attribut|is_array}
                    {foreach from=$itemBoutonsListe.annuler.tableau_attribut key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                {if isset($itemBoutonsListe.annuler.fonction_javascript) and $itemBoutonsListe.annuler.fonction_javascript|is_array}
                    {foreach from=$itemBoutonsListe.annuler.fonction_javascript key=cle item=valeur}
                        {$cle}="{$valeur}"
                    {/foreach}
                {/if}
                >
            </a>
        {/if}
    </div>
{/if}
{/if}

<script>


    /* $('#checkAll').click(function() {
     // on cherche les checkbox à l'intérieur de l'id  'magazine'
     //var allcheked = $(".allchecked").find(':checkbox');
     //alert('guy');
     if(this.checked){ // si 'checkAll' est coché
     $(':checkbox.allchecked').prop('checked', true);
     //alert('ok');
     }else{ // si on décoche 'checkAll'
     $(':checkbox.allchecked').prop('checked', false);
     //alert('pasok');
     }
     });
     */

    function allcheck(statut) {
        //alert('guy');
        if (statut) { // si 'checkAll' est coché
            $(':checkbox.allchecked').prop('checked', true);
            //alert('ok');
        } else { // si on décoche 'checkAll'
            $(':checkbox.allchecked').prop('checked', false);
            //alert('pasok');
        }
    }

    function apparition(id) {

        //$("#inputdate"+id).css('display','block');
        $("#inputdate" + id).fadeIn();
    }

    function dispation(id) {


        $("#inputdate" + id).fadeOut();

        console.log($('#text1516info_crea_contratproduit').val());

    }


</script>