<div style="color:#313131;margin: 0 auto;padding: 20px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;padding-bottom:0;">
    {if $bAffTitre}
        <div id="page-heading" style="margin-bottom:20px;"><div style="display:inline-block;border:1px solid #D0D1D5;"><div class="hidden-xs title-head-search" style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;"><span class="glyphicon glyphicon-link" style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;"></span></div><h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;" class="text-head-search">{$sTitreListe}</h1></div></div>
    {/if}
    <div style="margin-bottom:20px;text-align: center;">
        {if $bBtnRetour}
            <a href="{$sDirRetour}" class="btn btn-default" role="button"><span class="glyphicon glyphicon-share-alt" style="transform: scaleX(-1);"></span> Retour</a>
        {/if}
    </div>
    {if isset($sMessageResultForm)}
        <div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            {$sMessageResultForm}
        </div>
    {/if}
    <table style="background-color: #fafafa;" class="table table-hover table-condensed table-bordered table-striped">
        <thead>
        <tr>
            <th class="table-header-repeat line-left minwidth-1 title-table-head th-action" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                <a style="color:#fff;">Menu Général</a>
            </th>
            <th  class="table-header-repeat line-left minwidth-1 title-table-head th-action" style="padding: 5px;background:none;text-align: center;vertical-align: middle;">
                <a style="color:#fff;">Menu groupe</a>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td style="text-align:left;">
                <form action="" method="POST">
                    <input type="hidden" name="validation" value="ok">
                    <input type="hidden" name="guid_groupe" value="{$guid_groupe}">
                    {if !empty($aTabMenuFiltre)}
                        <ul >
                            {foreach from=$aTabMenuFiltre item=objListe}
                                <li><input type="checkbox" name="tabmenu[]" value="{$objListe.id_route}">&nbsp;
                                    {$objListe.intitule_menu_route}
                                    {if $objListe.est_parent}
                                        <ul>
                                            {foreach from=$objListe.fils item=objListefils}
                                                <li>---&nbsp;<input type="checkbox" name="tabmenu[]" value="{$objListefils.id_route}">&nbsp; <abbr title="{$objListefils.route_route}">{if $objListefils.intitule_menu_route==""}Sans intitulé{else}{$objListefils.intitule_menu_route}{/if}</abbr></li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/foreach}
                        </ul>
                        <button type="submit">Affecter au groupe</button>
                    {/if}
                </form>
            </td>
            <td style="text-align:left;">
                <form action="" method="POST">
                    <input type="hidden" name="validation" value="desaffecter">
                    <input type="hidden" name="guid_groupe" value="{$guid_groupe}">
                    {if !empty($aTabMenuGroupe)}

                        <ul >
                            {foreach from=$aTabMenuGroupe item=objListe}
                                <li><input type="checkbox" name="tabmenu[]" value="{$objListe.id_route}">&nbsp;
                                    {$objListe.intitule_menu_route}
                                    {if $objListe.est_parent}
                                        <ul>
                                            {foreach from=$objListe.fils item=objListefils}
                                                <li><input type="checkbox" name="tabmenu[]" value="{$objListefils.id_route}">&nbsp; <abbr title="{$objListefils.route_route}">{$objListefils.intitule_menu_route} </abbr> A: <input type="checkbox" {if $objListefils.ajout_groupe_route==1} checked onclick="bconf=confirm('Voulez vous désactiver l\'ajout de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','ajout_groupe_route','{$objListefils.guid_groupe_route}','0')" {else} onclick="bconf=confirm('Voulez vous activer l\'ajout de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','ajout_groupe_route','{$objListefils.guid_groupe_route}','1')"{/if}>
                                                                                                                             M: <input type="checkbox" {if $objListefils.modif_groupe_route==1} checked onclick="bconf=confirm('Voulez vous désactiver la modification de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','modif_groupe_route','{$objListefils.guid_groupe_route}','0')"{else}  onclick="bconf=confirm('Voulez vous activer la modification de cette fonctionnalité a ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','modif_groupe_route','{$objListefils.guid_groupe_route}','1')"{/if}>
                                                                                                                             S: <input type="checkbox" {if $objListefils.suppr_groupe_route==1} checked  onclick="bconf=confirm('Voulez vous désactiver la suppression de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','suppr_groupe_route','{$objListefils.guid_groupe_route}','0')"{else} onclick="bconf=confirm('Voulez vous activer la suppression de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','suppr_groupe_route','{$objListefils.guid_groupe_route}','1')"{/if}>
                                                                                                                             v:  <input type="checkbox" {if $objListefils.visu_groupe_route==1} checked onclick="bconf=confirm('Voulez vous désactiver la visualisation de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','visu_groupe_route','{$objListefils.guid_groupe_route}','0')" {else} onclick="bconf=confirm('Voulez vous activer la visualisation de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','visu_groupe_route','{$objListefils.guid_groupe_route}','1')"{/if}>
                                                                                                                             w: <input type="checkbox" {if $objListefils.visu_groupe_route==1} checked onclick="bconf=confirm('Voulez vous désactiver la visualisation de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','switch_wizard_groupes_routes','{$objListefils.guid_groupe_route}','0')"{else} onclick="bconf=confirm('Voulez vous désactiver la visualisation de cette fonctionnalité à ce groupe?');if(bconf)save_edtable('{$sNomTable}','guid_groupe_route','switch_wizard_groupes_routes','{$objListefils.guid_groupe_route}','1')"{/if}> </li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/foreach}
                        </ul>
                    {/if}
                    <button type="submit">Désacffecter au groupe</button>
                </form>
            </td>
        </tr>
        </tbody>
    </table>


</div>