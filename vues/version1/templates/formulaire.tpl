<div style="color:#313131;margin: 0 auto;padding: 15px;margin-top: 10px;margin: 0 auto;text-align: center;padding-top: 10px;">
{if $bDebugRequete}
<p>
	Requete Select : <br><br>
	{$sDebugRequeteSelect}<br><br>
	Requete Insert/Update : <br><br>
	{$sDebugRequeteInsertUpdate}<br><br>
</p>
{/if}
{if isset($sScriptJavascriptInsert)}
	{$sScriptJavascriptInsert}
{/if}
{if isset($sScriptJavascriptUpdate)}
	{$sScriptJavascriptUpdate}
{/if}
{if isset($aForm)}
<div id="page-heading"><div style="display:inline-block;border:1px solid #D0D1D5;margin-bottom: 20px;"><div class="hidden-xs title-head-search" style="display: inline-block;height: 73px;width: 73px;vertical-align: middle;padding: 20px;"><span class="glyphicon glyphicon-link" style="color:#fff;vertical-align: bottom;font-size: 20px;top: 6px;"></span></div><h1 style="font-size:30px;margin: 0px;display:inline-block;padding: 20px;vertical-align: bottom;" class="text-head-search">{$sTitreForm}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_titrefrom','{$sFli_LinkUrl}','{$sLangueUser}','{$sTitreForm|replace:'\'':'\\\''}','{$sNomTable}','fli_titrefrom');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</h1></div></div>
	<div style="margin-bottom:20px;">
		{if $sCategorieForm|is_array}
			{foreach name=scategorieform from=$sCategorieForm item=objCategorieForm}<div style="background-color:#FFF;display:inline-block;vertical-align:top;height:20px;font-size:18px;padding:10px 0;font-family:Tahoma;">{$objCategorieForm.index}</div><div style="background-color:{if $objCategorieForm.suivant}#e3e3e3{else}#777{/if};height:40px;display:inline-block;vertical-align:middle;"><div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-left-radius: 0;border-bottom-left-radius: 0;background-color:#FFF;"></div><div style="display:inline-block;height:14px;font-size:13px;color:#FFF;padding:13px;font-weight:bold;vertical-align:top;">{$objCategorieForm.texte}</div>{if ! $smarty.foreach.scategorieform.last}<div style="display:inline-block;width:20px;height:40px;border-radius:100% / 50%;border-top-right-radius: 0;border-bottom-right-radius: 0;background-color:#FFF;"></div>{/if}</div>{/foreach}<div style="display:inline-block;width:8px;height:40px;border-radius:0 4px 4px 0;background-color:#e3e3e3;vertical-align:middle;"></div>
		{else}
		<div style="margin-bottom: 20px;">
          <div style="display: inline-block;" class="sub-title-head-search">
            <span class="glyphicon glyphicon-tags sub-title-head-search" style="display: inline-block;height: 35px;width: 35px;padding: 10px;vertical-align: top;top:0px;"></span>
            <h4 class="sub-text-head-search" style="display: inline-block;margin: 0;padding: 10px;font-size: 14px;">{$sCategorieForm}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('fli_catfrom','{$sFli_LinkUrl}','{$sLangueUser}','{$sCategorieForm|replace:'\'':'\\\''}','{$sNomTable}','fli_catfrom');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</h4>
          </div>
        </div>										
		{/if}
	</div>
	{if isset($bMessageSuccesForm) and $bMessageSuccesForm}
	<div class="popupAlert alert alert-success alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      {if isset($sMessageSuccesForm)}{$sMessageSuccesForm}{/if}
    </div>
	{/if}
	{if isset($bMessageErreurForm) and $bMessageErreurForm}
	<div class="popupAlert alert alert-danger alert-dismissible fade in" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
      {if isset($sMessageErreurForm)}{$sMessageErreurForm}{/if}
    </div>
	{/if}
	<form
		method="POST"
		action="{$url_form}{if $bDuplicate}&duplicate=ok{/if}"
		{if isset($bTelechargementFichier) and $bTelechargementFichier}enctype="multipart/form-data"{/if}
	>
		<input type="hidden" name="fi_keymutiple" value="{$fi_keymutiple}">

		{if isset($aForm)}
		<div style="display: inline-block;background-color: #fff;padding: 15px;border: 1px solid #D0D1D5;">
		<table border="0" cellpadding="0" cellspacing="0" id="id-form" style="margin: 0 auto;padding:20px;background-color:#fff;" class="table table-condensed table-hover">
			<tbody>
			{*{$aForm|@print_r}*}
			{foreach from=$aForm item=objForm}

				{if isset($objForm.type_champ) and ((isset($objForm.aff_form) and $objForm.aff_form eq 'ok') or ($objForm.type_champ eq 'category' or $objForm.type_champ eq 'bouton'))}
					{if $objForm.type_champ eq 'text'}
						<tr>
							<th valign="top" style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
							<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
								<input 
									type="text"
									class="inp-form"
									id="id_{$objForm.nom_variable}"
									name="{$objForm.nom_variable}"
									{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
									{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
									{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{if $cle!='size'}{$cle}="{$valeur}"{/if}
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{if $cle!='size'}{$cle}="{$valeur}"{/if}
										{/foreach}
									{/if}
								>
								{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
								<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
								{/if}
							</td>
						</tr>
					{elseif $objForm.type_champ eq 'color'}
						<tr>
							<th valign="top" style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
							<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
								<input
										type="color"
										class="inp-form"
										id="id_{$objForm.nom_variable}"
										name="{$objForm.nom_variable}"
										{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
									{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
										{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								>
								{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
									<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
								{/if}
							</td>
						</tr>

					{elseif $objForm.type_champ eq 'category'}
					<tr>
						<th colspan="2" style="border:0;">
							<h1>{$objForm.text_label}</h1>
						</th>
					</tr>
					{elseif $objForm.type_champ eq 'date'}
					<tr>
						<th valign="top" style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
							<input
								type="date"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value='{$objForm.valeur_variable|date_format:"%Y-%m-%d"}'{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'textarea'}
					<tr>
						<th valign="top" style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
							<textarea
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.wysiwyg) and $objForm.wysiwyg eq 'ok'}class="ckeditor"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>{$objForm.valeur_variable}</textarea>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'checkbox'}
					<tr>
						<th style="vertical-align:top;border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="padding-top:5px;border:0;text-align: left;{$objForm.html_perso_form_td}">
							{if $objForm.tags eq 'ok'}
								<select name="{$objForm.nom_variable}[]" class="js-example-responsive" multiple="multiple" style="width: 100%;" id="id_form_{$objForm.nom_variable}">
									{foreach from=$objForm.lesitem key=valeur_checkbox item=nom_checkbox}
								  		<option value="{$valeur_checkbox}" {if is_array($objForm.valeur_variable) and in_array($valeur_checkbox, $objForm.valeur_variable)}selected{/if}>{$nom_checkbox}</option>
								  	{/foreach}						  
								</select>								
							{else}
							<table style="display:inline-block;">
								<tbody>
									{foreach from=$objForm.lesitem key=valeur_checkbox item=nom_checkbox}
									<tr>
										<td style="padding-right:10px;">
											<input
												type="checkbox"
												name="{$objForm.nom_variable}[]"
												value="{$valeur_checkbox}"
												{if is_array($objForm.valeur_variable) and in_array($valeur_checkbox, $objForm.valeur_variable)}checked{/if}
											>
										</td>
										<td style="padding-right:20px;">{$nom_checkbox}</td>
									</tr>
									{/foreach}
								</tbody>
							</table>
							{/if}
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'select'}
						{if isset($objForm.select_autocomplete) and $objForm.select_autocomplete eq 'ok'}
						<tr>
							<th valign="top" style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label_filtre}</th>
							<td style="border:0;text-align: left;">
								<input type='text' 
								name='rech_{$objForm.nom_variable}' 	
								id='id_rech{$objForm.nom_variable}'
								class="inp-form"
                                {if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
                                    {foreach from=$objForm.tableau_attribut key=cle item=valeur}
                                        {if $cle!='size'}{$cle}="{$valeur}"{/if}
                                    {/foreach}
                                {/if}
								onKeyUp="affiche_liste_generique('{$objForm.table_item}','{$objForm.id_table_item}','{$objForm.affichage_table_item}','{$objForm.supplogique_table_item}', '{$objForm.tabfiltre_autocomplete}',this.value,'id_form_{$objForm.nom_variable}');">
							</td>
						</tr>		
						{/if}
					<tr>
						<th style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="padding-top:8px;border:0;text-align: left;{$objForm.html_perso_form_td}">

 						{if $objForm.is_keymultiple=="ok" and $fi_keymutiple=="modif"}

                            {if is_array($objForm.lesitem)}
                                {foreach from=$objForm.lesitem key=id_valeur_possible item=valeur_possible_bdd}
									{if $objForm.valeur_variable eq $id_valeur_possible}{$valeur_possible_bdd}
										<input type="hidden" name="{$objForm.nom_variable}" value="{$id_valeur_possible}">
                                    {/if}
							    {/foreach}
                            {/if}
						{else}

							<select
								style="padding:5px;"
								id="id_form_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}{if isset($objForm.multiple)}[]{/if}"
								{if isset($objForm.multiple)}multiple{/if}

    							{$objForm.fonction_javascript}
									>
								<option value="" {if $objForm.valeur_variable eq ''}selected{/if}></option>
								{if is_array($objForm.lesitem)}
								{foreach from=$objForm.lesitem key=id_valeur_possible item=valeur_possible_bdd}
									<option value="{$id_valeur_possible}" {if $objForm.valeur_variable eq $id_valeur_possible}selected{/if}>
										{$valeur_possible_bdd}
									</option>
								{/foreach}
								{/if}
							</select>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
                        {/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'radio'}
					<tr>
						<th valign="top" style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="padding-top:8px;border:0;text-align: left;{$objForm.html_perso_form_td}">
							<div style="display:inline-block;">
								<table>
									<tbody>
										{foreach from=$objForm.lesitem key=valeur_radio item=nom_radio}
										<tr>
											<td style="padding-right:20px;padding-bottom:0;vertical-align:middle;height:32px;">
												<input
													type="radio"
													name="{$objForm.nom_variable}"
													value="{$valeur_radio}"
													{if $valeur_radio eq $objForm.valeur_variable}checked{/if}
												>
											</td>
											<td style="padding-bottom:0;vertical-align:middle;height:32px;">{$nom_radio}</td>
										</tr>
										{/foreach} 
									</tbody>
								</table>
							</div>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'file'}
					<tr>
						<th style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="border:0;text-align: left;">
							<div style="border-radius:6px;border:1px solid #ACACAC;padding:3px;display:inline-block;vertical-align:middle;">
								<input
									type="file"
									id="id_{$objForm.nom_variable}"
									name="{$objForm.nom_variable}"
									{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
									{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{if $cle!='size'}{$cle}="{$valeur}"{/if}
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{if $cle!='size'}{$cle}="{$valeur}"{/if}
										{/foreach}
									{/if}
								>
							</div>
							{if isset($objForm.file_aff_modif_form) and $objForm.file_aff_modif_form eq 'ok'}
								{if $objForm.valeur_variable neq ''}
									{if preg_match("/\.png$/", $objForm.valeur_variable) or
										preg_match("/\.jpeg$/", $objForm.valeur_variable) or 
										preg_match("/\.gif$/", $objForm.valeur_variable)
									}
									<div style="
										border-radius:6px;
										border:1px solid #ACACAC;
										padding:3px;
										display:inline-block;
										vertical-align:middle;
										{if isset($objForm.file_aff_modif_form_couleur_fond)}background-color:{$objForm.file_aff_modif_form_couleur_fond}{/if};"
									>
										<img
											src="{$objForm.file_visu}{$objForm.valeur_variable}"
											style="display:inline-block;vertical-align:middle;"
											{if isset($objForm.file_aff_modif_form_taille)}width="{$objForm.file_aff_modif_form_taille}"{/if}
										>
									</div>
									{elseif $objForm.valeur_variable neq '' and preg_match("/\.swf$/", $objForm.valeur_variable)}
									<div style="
										border-radius:6px;
										border:1px solid #ACACAC;
										padding:3px;
										display:inline-block;
										vertical-align:middle;
										{if isset($objForm.file_aff_modif_form_couleur_fond)}background-color:{$objForm.file_aff_modif_form_couleur_fond}{/if};"
									>
										<object
											type="application/x-shockwave-flash"
											data="{$objForm.file_visu}{$objForm.valeur_variable}"
											{if isset($objForm.file_aff_modif_form_taille)}width="{$objForm.file_aff_modif_form_taille}"{/if}
										>
											<param name="movie" value="{$objForm.file_visu}{$objForm.valeur_variable}" />
											<param name="wmode" value="transparent" />
											<p>swf non affichable</p>
										</object>
									</div>
									{else}
									<a target="_blank" href="{$objForm.file_visu}{$objForm.valeur_variable}">Voir le fichier</a>
									{/if}
								{/if}
							{/if}
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'time'}
					<tr>
						<th style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
							<input
								type="time"
								class="timepicker inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'hour'}
					<tr>
						<th style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
							<input
								type="text"
								class="hourpicker inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'hidden'}
					<tr>
						<td style="padding:0;border:0;{$objForm.html_perso_form_th}"></td>
						<td style="padding:0;border:0;">
							<input
								type="hidden"
								{if isset($objForm.nom_variable)}name="{$objForm.nom_variable}"  id="id_{$objForm.nom_variable}"{/if}
								{if isset($objForm.valeur_variable)}value="{$objForm.valeur_variable}"{/if}
							>
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'password'}
						{if $objForm.double_password eq 'ok'}
							<tr>
								<th valign="top">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_{$objForm.nom_variable}"
											name="{$objForm.nom_variable}"
											{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
										{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
											{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									>
									{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
									{/if}
								</td>
							</tr>
							<tr>
								<th valign="top">{$objForm.text_label} 2</th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_{$objForm.nom_variable}_2"
											name="{$objForm.nom_variable}_2"
											{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
										{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
											{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									>
									{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
									{/if}
								</td>
							</tr>
						{else}
							<tr>
								<th valign="top">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
								<td>
									<input
											type="password"
											class="inp-form"
											id="id_{$objForm.nom_variable}"
											name="{$objForm.nom_variable}"
											{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
										{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
											{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
									{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
										{foreach from=$objForm.tableau_attribut key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
										{foreach from=$objForm.fonction_javascript key=cle item=valeur}
											{$cle}="{$valeur}"
										{/foreach}
									{/if}
									>
									{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
										<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
									{/if}
								</td>
							</tr>
						{/if}
					{elseif $objForm.type_champ eq 'email'}
					<tr>
						<th valign="top" style="border:0;{$objForm.html_perso_form_th}">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
							<input
								type="email"
								class="inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{elseif $objForm.type_champ eq 'telephone'}
					<tr>
						<th style="border:0;{$objForm.html_perso_form_th}" valign="top">{$objForm.text_label}{if $bActivtraduction}&nbsp;<a href="#" onclick="renvoi_info_traduction('{$objForm.nom_variable}','{$sFli_LinkUrl}','{$sLangueUser}','{$objForm.text_label}','{$sNomTable}','{$objForm.mapping_champ}');$('#fli_modallg').modal('show');"><span class="glyphicon glyphicon-pencil"></span></a>{/if}</th>
						<td style="border:0;text-align: left;{$objForm.html_perso_form_td}">
							<input
								type="tel"
								class="inp-form"
								id="id_{$objForm.nom_variable}"
								name="{$objForm.nom_variable}"
								{if isset($objForm.valeur_variable) and $objForm.valeur_variable neq ''}value="{$objForm.valeur_variable}"{/if}
								{if isset($objForm.size_champ) and $objForm.size_champ neq ''}size="{$objForm.size_champ}"{/if}
								{if isset($objForm.style) and $objForm.style neq ''}style="{$objForm.style}"{/if}
								{if isset($objForm.tableau_attribut) and $objForm.tableau_attribut|is_array}
									{foreach from=$objForm.tableau_attribut key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
								{if isset($objForm.fonction_javascript) and $objForm.fonction_javascript|is_array}
									{foreach from=$objForm.fonction_javascript key=cle item=valeur}
										{if $cle!='size'}{$cle}="{$valeur}"{/if}
									{/foreach}
								{/if}
							>
							{if isset($objForm.ctrl_champ) and $objForm.ctrl_champ eq 'ok'}
							<div class="error-left" style="float:none;display:inline-block;vertical-align:top;"></div><div class="error-inner" style="float:none;display:inline-block;vertical-align:top;">{$objForm.mess_erreur}</div>
							{/if}
						</td>
					</tr>
					{/if}
				{/if}
			{/foreach}
			</tbody>
		</table>
			{if isset($itemBoutonsForm) and $itemBoutonsForm|is_array and $itemBoutonsForm|@count > 0}
			<div style="clear:both;margin-top:20px;text-align: center;">
				{if isset($itemBoutonsForm.precedent)}
				<a href="{$itemBoutonsForm.precedent.url}">
					<input
						type="button"
						class="btn btn-primary btnResForm"
						value="{$itemBoutonsForm.precedent.text_label}"
						{if isset($itemBoutonsForm.precedent.style) and $itemBoutonsForm.precedent.style neq ''}{/if}
						{if isset($itemBoutonsForm.precedent.tableau_attribut) and $itemBoutonsForm.precedent.tableau_attribut|is_array}
							{foreach from=$itemBoutonsForm.precedent.tableau_attribut key=cle item=valeur}
								{if $cle!='size'}{$cle}="{$valeur}"{/if}
							{/foreach}
						{/if}
						{if isset($itemBoutonsForm.precedent.fonction_javascript) and $itemBoutonsForm.precedent.fonction_javascript|is_array}
							{foreach from=$itemBoutonsForm.precedent.fonction_javascript key=cle item=valeur}
								{if $cle!='size'}{$cle}="{$valeur}"{/if}
							{/foreach}
						{/if}
					>
				</a>
				{/if}
				{if isset($itemBoutonsForm.suivant)}
				<input
					type="submit"
					id="id_{$itemBoutonsForm.suivant.nom_variable}"
					class="btn btn-primary btnValForm"
					name="{$itemBoutonsForm.suivant.nom_variable}"
					value="{$itemBoutonsForm.suivant.text_label}"
					{if isset($itemBoutonsForm.suivant.style) and $itemBoutonsForm.suivant.style neq ''}{/if}
					{if isset($itemBoutonsForm.suivant.tableau_attribut) and $itemBoutonsForm.suivant.tableau_attribut|is_array}
						{foreach from=$itemBoutonsForm.suivant.tableau_attribut key=cle item=valeur}
							{if $cle!='size'}{$cle}="{$valeur}"{/if}
						{/foreach}
					{/if}
					{if isset($itemBoutonsForm.suivant.fonction_javascript) and $itemBoutonsForm.suivant.fonction_javascript|is_array}
						{foreach from=$itemBoutonsForm.suivant.fonction_javascript key=cle item=valeur}
							{if $cle!='size'}{$cle}="{$valeur}"{/if}
						{/foreach}
					{/if}
				>
				{/if}
				{if isset($itemBoutonsForm.valider)}
				<input
					type="submit"
					class="btn btn-primary btnValForm"
					id="id_{$itemBoutonsForm.valider.nom_variable}"
					name="{$itemBoutonsForm.valider.nom_variable}"
					value="{$itemBoutonsForm.valider.text_label}"
					{if isset($itemBoutonsForm.valider.style) and $itemBoutonsForm.valider.style neq ''}{/if}
					{if isset($itemBoutonsForm.valider.tableau_attribut) and $itemBoutonsForm.valider.tableau_attribut|is_array}
						{foreach from=$itemBoutonsForm.valider.tableau_attribut key=cle item=valeur}
							{if $cle!='size'}{$cle}="{$valeur}"{/if}
						{/foreach}
					{/if}
					{if isset($itemBoutonsForm.valider.fonction_javascript) and $itemBoutonsForm.valider.fonction_javascript|is_array}
						{foreach from=$itemBoutonsForm.valider.fonction_javascript key=cle item=valeur}
							{if $cle!='size'}{$cle}="{$valeur}"{/if}
						{/foreach}
					{/if}
				>
				{/if}												
				{if isset($itemBoutonsForm.annuler) and $bAffAnnuler}
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="{$itemBoutonsForm.annuler.url}">
					<input
						type="button"
						class="btn btn-primary btnResForm"
						value="{$itemBoutonsForm.annuler.text_label}"
						{if isset($itemBoutonsForm.annuler.style) and $itemBoutonsForm.annuler.style neq ''}{/if}
						{if isset($itemBoutonsForm.annuler.tableau_attribut) and $itemBoutonsForm.annuler.tableau_attribut|is_array}
							{foreach from=$itemBoutonsForm.annuler.tableau_attribut key=cle item=valeur}
								{if $cle!='size'}{$cle}="{$valeur}"{/if}
							{/foreach}
						{/if}
						{if isset($itemBoutonsForm.annuler.fonction_javascript) and $itemBoutonsForm.annuler.fonction_javascript|is_array}
							{foreach from=$itemBoutonsForm.annuler.fonction_javascript key=cle item=valeur}
								{if $cle!='size'}{$cle}="{$valeur}"{/if}
							{/foreach}
						{/if}
					>
				</a>
				{/if}
			</div>
			{/if}
			</div>
		{/if}
	</form>
{/if}
</div>