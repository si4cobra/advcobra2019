{if !empty($aTabMenu)}
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="javascript:void(0);">
                    <span class="glyphicon glyphicon-th-list"></span> Menu
                </a>
            </li>

            {function menu level=0}
                {if $level==0}
                    <li style="margin-bottom: 50px;">
                {/if}
                {foreach from=$data item=entry}
                    {if isset($entry.est_parent) and $entry.est_parent}
                        <a data-toggle="collapse" href="#collapseChamp{$entry.id_route}" data-target="#collapseChamp{$entry.id_route}" aria-expanded="false" aria-controls="collapseChamp{$entry.id_route}" style="margin-left: {$level*20}px;">{$entry.intitule_menu_route}</a>
                    {else}
                       {if isset($entry.route_route)} <a href="{$entry.route_route}" onclick="$('#wrapper').toggleClass('toggled');" style="margin-left: {$level*20}px;">{$entry.intitule_menu_route}</a>{/if}
                    {/if}
                    {if isset($entry.fils) and is_array($entry.fils)}
                        {if !empty($entry.fils)}
                            <div class="collapse" id="collapseChamp{$entry.id_route}">
                            {menu data=$entry.fils level=$level+1}
                            </div>
                        {/if}
                    {/if}
                {/foreach}
                {if $level==0}
                    </li>
                {/if}
            {/function}

            {menu data=$aTabMenu}

        </ul>
    </div>
    <!-- /#sidebar-wrapper -->
</div>
{/if}