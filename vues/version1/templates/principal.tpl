<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {if !empty($metaModules)}
        {foreach from=$metaModules item=metaModule key=cle}
            <meta name="{$cle}" content="{$metaModule}">
        {/foreach}
    {/if}
    <title>{$sTitreDeLaPage}</title>

    <!-- Bootstrap -->
    <link href="{$cheminAccesPublic}/css/simple-sidebar.css" rel="stylesheet">
    <link href="{$cheminAccesPublic}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{$cheminAccesPublic}/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta name="google-site-verification" content="sOuXqP6oDwIfUf6biVsfI_wNMug2oNR1I6J8G8pdgBg"/>
    <!--[if IE]>
    <link rel="stylesheet" media="all" type="text/css" href="{$cheminAccesPublic}/css/pro_dropline_ie.css"/>
    <![endif]-->

    <!--<link type='text/css' href='css/experiment.css' rel='stylesheet' media='screen' />-->

    <!--  date picker script -->
    <link rel="stylesheet" href="{$cheminAccesPublic}/css/datePicker.css" type="text/css"/>
    <link href="{$cheminAccesPublic}/css/select2.min.css" rel="stylesheet" type="text/css"/>

    {if !empty($favicon)}<link rel="icon" type="image/png" href="{$cheminAccesPublic}/{$favicon}" />{/if}

    {if !empty($cssModules)}
        {foreach from=$cssModules item=cssModule}
            <link rel="stylesheet" type="text/css" href="{$cssModule}">
        {/foreach}
    {/if}

    <!--<link href="css/color-custom.php" rel="stylesheet" type="text/css"  />-->
    <script src="{$cheminAccesPublic}/js/fonction_adv.js" type="text/javascript"></script>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) 1.11.2 -->
    <script src="{$cheminAccesPublic}/js/jquery-1.11.2.min.js"></script>

    <script src="{$cheminAccesPublic}/js/bootstrap.min.js"></script>
    <script src="{$cheminAccesPublic}/js/bootstrap-switch.min.js"></script>

    {literal}
    <style>
        @media print { 
            @page  {
                size: landscape ;
        }
            a[href]:after {
                content: "";
            }
            h1
            {
                font-size: 20px!important;
            }
            #big-contain #page-heading
            {
                background-color: #9B410E!important;
            }
        }

        .ui-menu {
            list-style:none;
            padding: 10px;
            margin: 0;
            display:block;
            width:227px;
        }
        .ui-menu .ui-menu {
            margin-top: -3px;
        }
        .ui-menu .ui-menu-item {
            margin:0;
            padding: 0;
            width: 200px;
        }
        .ui-menu .ui-menu-item a {
            text-decoration:none;
            display:block;
            padding:.2em .4em;
            line-height:1.5;
            zoom:1;
        }
        .ui-menu .ui-menu-item a.ui-state-hover,
        .ui-menu .ui-menu-item a.ui-state-active {
            margin: -1px;
        }
        .ui-menu-item-wrapper
        {
            background-color: white;
        }
        .ui-helper-hidden-accessible
        {
            display: none;
        }

    </style>


    {/literal}

</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top hidden-print" style="padding: 0 10px;">
    <div>

        <div class="navbar-header">
            {if !empty($tTplMenu)}
                <a href="#menu-toggle" class="btn btn-default navbar-brand" id="menu-toggle"><span
                            class="glyphicon glyphicon-menu-hamburger"></span></a>
            {/if}
            {if $bEstConnecte}
                <a href="deco.php" class="btn btn-default navbar-brand" id="btnLogOut"
                   style="height: 34px !important;margin-left:10px;"><span class="glyphicon glyphicon-off"></span></a>
                <a href="utilisateur-ctrl_fli_notification-fli_renvoi_notification" style="margin-left: 10px;padding: 6px;
    margin-top: 8px;
    margin-right: 10px;" class="btn btn-default navbar-brand" id="menu-toggle"><span
                            class="glyphicon glyphicon-comment"></span><span style="color: red" id="nbNotiification"></span></a>
            {else}
                <a href="fli_connexion-ctrl_fli_connexion-connexion" class="btn btn-default navbar-brand" id="btnLogOut"
                   style="height: 34px !important;margin-left:10px;"><span class="glyphicon glyphicon-user"></span></a>
            {/if}
            {*<a href="#menu-toggle2" class="btn btn-default navbar-brand" id="menu-toggle2" style="height: 34px !important;padding: 6px;margin-top: 8px;margin-right: 10px;"><span class="glyphicon glyphicon-list"></span> Menu</a>*}
            {if !empty($logo)}<a href="{$fli_module_defaut}-{$fli_controleur_defaut}-{$fli_fonction_defaut}"><img src="{$cheminAccesPublic}/{$logo}" alt="logo" id="logoHome" style="float: right;"></a>{/if}
            <a href="crm-ctrl_tableaubord-fli_tableaubord" class="btn btn-default navbar-brand hiddenMenu" id="btnLogOut">Dashboard</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse hidden-print">

        </div><!--/.nav-collapse -->
    </div>
</nav>

{if isset($tTplMenu)}
    {$tTplMenu}
{/if}

<!-- Page Content -->
<div id="page-content-wrapper" {if empty($tTplMenu)}style="margin-top: 40px;"{/if}>
    <div class="container-fluid">
        <div class="row">
            {if isset($pagedirection)}
                {if $pagedirection|is_array}
                    {foreach from=$pagedirection item=tabTpl}
                        {foreach from=$tabTpl item=tpl}
                            {$tpl}
                        {/foreach}
                    {/foreach}
                {else}
                    {$pagedirection}
                {/if}
            {/if}

        </div>
    </div>
</div>
<!-- /#wrapper -->


<!-- Include all compiled plugins (below), or include individual files as needed -->


<!--  fonction personnaliser -->
<script src="{$cheminAccesPublic}/js/fonction_adv.js" type="text/javascript"></script>
<script src="{$cheminAccesPublic}/js/fonction_menu_principal.js" type="text/javascript"></script>
{literal}
    <!-- Custom jquery scripts -->
<script src="{/literal}{$cheminAccesPublic}{literal}/js/jquery/custom_jquery.js" type="text/javascript"></script>
<script src="{/literal}{$cheminAccesPublic}{literal}/js/jquery/date.js" type="text/javascript"></script>
<script src="{/literal}{$cheminAccesPublic}{literal}/js/jquery/jquery.datePicker.js" type="text/javascript"></script>
<script src="{/literal}{$cheminAccesPublic}{literal}/js/jquery/jquery.maskedinput.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8">
        $(function () {

            // initialise the "Select date" link
            $('#date-pick')
                    .datePicker(
                            // associate the link with a date picker
                            {
                                createButton: false,
                                startDate: '01/01/2005',
                                endDate: '31/12/2020'
                            }
                    ).bind(
                    // when the link is clicked display the date picker
                    'click',
                    function () {
                        updateSelects($(this).dpGetSelected()[0]);
                        $(this).dpDisplay();
                        return false;
                    }
            ).bind(
                    // when a date is selected update the SELECTs
                    'dateSelected',
                    function (e, selectedDate, $td, state) {
                        updateSelects(selectedDate);
                    }
            ).bind(
                    'dpClosed',
                    function (e, selected) {
                        updateSelects(selected[0]);
                    }
            );

            var updateSelects = function (selectedDate) {
                var selectedDate = new Date(selectedDate);
                $('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
                $('#m option[value=' + (selectedDate.getMonth() + 1) + ']').attr('selected', 'selected');
                $('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
            }
            // listen for when the selects are changed and update the picker
            $('#d, #m, #y')
                    .bind(
                            'change',
                            function () {
                                var d = new Date(
                                        $('#y').val(),
                                        $('#m').val() - 1,
                                        $('#d').val()
                                );
                                $('#date-pick').dpSetSelected(d.asString());
                            }
                    );

            // default the position of the selects to today
            //var today = new Date();
            //updateSelects(today.getTime());

            // and update the datePicker to reflect it...
            $('#d').trigger('change');
        });

        $(function () {
            var hash = window.location.hash;
            hash && $('ul.nav a[href="' + hash + '"]').tab('show');

            $('.nav-tabs a').click(function (e) {
                $(this).tab('show');
                var scrollmem = $('body').scrollTop();
                window.location.hash = this.hash;
                $('html,body').scrollTop(scrollmem);
            });
        });
    </script>

    <script type="text/javascript">

        $("#menu-toggle").click(function (e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        $("#menu-toggle2").click(function (e) {
            e.preventDefault();
            $("#wrapper2").toggleClass("toggled");
        });

        $(document).mouseup(function (e) {
            var container = $("#sidebar-wrapper");
            var container2 = $("#menu-toggle");

            if (!container.is(e.target) && container.has(e.target).length === 0 && !container2.is(e.target) && container2.has(e.target).length === 0 && $("#wrapper").hasClass("toggled")) {
                $("#wrapper").toggleClass("toggled");
            }
        });

        $(document).mouseup(function (e) {
            var container = $("#sidebar-wrapper2");
            var container2 = $("#menu-toggle2");

            if (!container.is(e.target) && container.has(e.target).length === 0 && !container2.is(e.target) && container2.has(e.target).length === 0 && $("#wrapper2").hasClass("toggled")) {
                $("#wrapper2").toggleClass("toggled");
            }
        });

        $('.carousel').carousel({
            interval: false
        });

    </script>

<script type="text/javascript" src="{/literal}{$cheminAccesPublic}{literal}/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="{/literal}{$cheminAccesPublic}{literal}/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="{/literal}{$cheminAccesPublic}{literal}/js/nbnotif.js" ></script>
<script src="{/literal}{$cheminAccesPublic}{literal}/js/select2.full.min.js"></script>
    <script>$(".js-example-responsive").select2();</script>
    <script type="text/javascript">
        if ($('.table-responsive .table').width() < $('#page-content-wrapper').width() || (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
                || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4)))) {
            $('.btnScroll').hide();
        }

        $(".btnScrollLeft").mousedown(startScrollingLeft).mouseup(stopScrolling);
        $(".btnScrollRight").mousedown(startScrollingRight).mouseup(stopScrolling);

        function startScrollingLeft() {
            // contintually increase scroll position
            $('.table-responsive').animate({scrollLeft: '-=250'}, 'easeInOutQuint', startScrollingLeft);
        }

        function startScrollingRight() {
            // contintually increase scroll position
            $('.table-responsive').animate({scrollLeft: '+=250'}, 'easeInOutQuint', startScrollingRight);
        }

        function stopScrolling() {
            // stop increasing scroll position
            $('.table-responsive').stop();
        }

    </script>
{/literal}
{if !empty($jsModules)}
    {foreach from=$jsModules item=jsModule}
        <script type="text/javascript" src="{$jsModule}"></script>
    {/foreach}
{/if}
{if $bDebug && !empty($aDebug)}
    <script type="text/javascript">
        var win=window.open("debug.php?fli_module_debug={$fli_module}&fli_fonction_debug={$fli_fonction}", "Fli Debug", "width=800, height=600");
    </script>
{/if}
</body>
</html>